﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXKafkaClients
{

    public enum KafkaVolatilityFeedSource
    {
        dbabfx,
        citi,
        jpm
    }

    public enum KafkaRFQSource
    {
        dbabfx,
        citi,
        jpm
    }

    public enum KafkaSpotFeedSource
    {
        fenics
    }

    public class KafkaVolTopics
    {
        // e.g. providers.vol.jpm.*
        // e.g. providers.vol.jpm.EURUSD
        // e.g. providers.vol.citi.*
        // e.g. providers.vol.citi.USDJPY
        public static readonly string Kafka_Providers_Vol_Topic_Template = "providers.vol.{0}";
        public static readonly string Kafka_Providers_Vol_Per_Security_Topic_Template = "providers.vol.{0}.{1}";
    }

    public class KafkaRFQTopics
    {
        // e.g. providers.rfq.dbabfx
        // e.g. providers.rfq.dbabfx.EURUSD
        // e.g. providers.rfq.vols.dbabfx.EURUSD
        // e.g. providers.rfq.tenordates.dbabfx.EURUSD
        public static readonly string Kafka_Providers_Rfq_Topic_Template = "providers.rfq.{0}";
        public static readonly string Kafka_Providers_Rfq_Per_Security_Topic_Template = "providers.rfq.{0}.{1}";

        public static readonly string Kafka_Providers_Rfq_Vols_Topic_Template = "providers.rfq.vols.{0}";
        public static readonly string Kafka_Providers_Rfq_Vols_Per_Security_Topic_Template = "providers.rfq.vols.{0}.{1}";

        public static readonly string Kafka_Providers_Rfq_Tenor_Dates_Topic_Template = "providers.rfq.tenordates.{0}";
        public static readonly string Kafka_Providers_Rfq_Tenor_Dates_Per_Security_Topic_Template = "providers.rfq.tenordates.{0}.{1}";

        public static readonly string Kafka_Providers_Rfq_Tenor_Dates_Topic = "providers.rfq.tenordates";
        public static readonly string Kafka_Providers_Rfq_Tenor_Dates_Per_Security_Topic = "providers.rfq.tenordates.{0}";
    }

    public class KafkaSpotsTopics
    {
        public static readonly string Kafka_Providers_Spots_Template = "providers.spots.{0}";
    }

}
