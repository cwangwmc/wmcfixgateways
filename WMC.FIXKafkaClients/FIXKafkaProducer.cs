﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

using Confluent.Kafka;
using Confluent.Kafka.Serialization;

using log4net;

namespace WMC.FIXKafkaClients
{
    public class FIXKafkaProducer
    {

        protected static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly Dictionary<string, object> _config = 
            new Dictionary<string, object>() { { "bootstrap.servers", ConfigurationManager.AppSettings["kafka.bootstrap.servers"] } };

        private static readonly Producer<string, string> _producer = new Producer<string, string>(_config,
            new StringSerializer(Encoding.UTF8),
            new StringSerializer(Encoding.UTF8));

        public async static Task SendVolsAsync(KafkaVolatilityFeedSource source, string message)
        {

            var topic = string.Format(KafkaVolTopics.Kafka_Providers_Vol_Topic_Template, source);

            _log.DebugFormat("Sending Vol message to Topic {0} ...", topic);

            await SendMessage(topic, message);

        }

        public async static Task SendVolsAsync(KafkaVolatilityFeedSource source, string ccypair, string message)
        {

            var topic = string.Format(KafkaVolTopics.Kafka_Providers_Vol_Per_Security_Topic_Template, source, ccypair);

            _log.DebugFormat("Sending Vol message for {0} to Topic {1} ...", ccypair, topic);

            await SendMessage(topic, message);

        }

        public async static Task SendRfqAsync(KafkaRFQSource source, string ccypair, string message)
        {

            var topic = string.Format(KafkaRFQTopics.Kafka_Providers_Rfq_Per_Security_Topic_Template, source, ccypair);

            _log.DebugFormat("Sending Rfq message for {0} to Topic {1} ...", ccypair, topic);

            await SendMessage(topic, message);

        }

        public async static Task SendRfqAsync(KafkaRFQSource source, string message)
        {

            var topic = string.Format(KafkaRFQTopics.Kafka_Providers_Rfq_Topic_Template, source);

            _log.DebugFormat("Sending Rfq message to Topic {0} ...", topic);

            await SendMessage(topic, message);

        }

        public async static Task SendRfqTenorDatesAsync(KafkaRFQSource source, string ccypair, string message)
        {

            var topic = string.Format(KafkaRFQTopics.Kafka_Providers_Rfq_Tenor_Dates_Per_Security_Topic_Template, source, ccypair);

            _log.DebugFormat("Sending Rfq Tenor Dates message for {0} to Topic {1} ...", ccypair, topic);

            await SendMessage(topic, message);

        }

        public async static Task SendRfqTenorDatesAsync(KafkaRFQSource source, string message)
        {

            var topic = string.Format(KafkaRFQTopics.Kafka_Providers_Rfq_Tenor_Dates_Topic_Template, source);

            _log.DebugFormat("Sending Rfq Tenor Dates message to Topic {0} ...", topic);

            await SendMessage(topic, message);

        }

        public async static Task SendRfqTenorDatesAsync(string message)
        {

            var topic = KafkaRFQTopics.Kafka_Providers_Rfq_Tenor_Dates_Topic;

            _log.DebugFormat("Sending Rfq Tenor Dates message to Topic {0} ...", topic);

            await SendMessage(topic, message);

        }

        public async static Task SendRfqTenorDatesAsync(string ccypair, string message)
        {

            var topic = string.Format(KafkaRFQTopics.Kafka_Providers_Rfq_Tenor_Dates_Per_Security_Topic, ccypair);

            _log.InfoFormat("Sending Rfq Tenor Dates message to Topic {0} ...", topic);

            await SendMessage(topic, message);

        }

        public async static Task SendRfqVolsAsync(KafkaRFQSource source, string message)
        {

            var topic = string.Format(KafkaRFQTopics.Kafka_Providers_Rfq_Vols_Per_Security_Topic_Template, source);

            _log.DebugFormat("Sending Rfq Vols message to Topic {0} ...", topic);

            await SendMessage(topic, message);

        }

        public async static Task SendRfqVolsAsync(KafkaRFQSource source, string ccypair, string message)
        {

            var topic = string.Format(KafkaRFQTopics.Kafka_Providers_Rfq_Vols_Per_Security_Topic_Template, source, ccypair);

            _log.DebugFormat("Sending Rfq Vols message for {0} to Topic {1} ...", ccypair, topic);

            await SendMessage(topic, message);

        }

        private async static Task SendMessage(string topic, string message)
        {
            _log.DebugFormat("Sending Kafka Message to Topic {0} ... ", topic);
            _log.DebugFormat("Message:\n{0}", message);
            _log.DebugFormat("Topic:\n{0}", topic);

            var result = await _producer.ProduceAsync(topic, string.Empty, message);

            if (result.Error.Code == ErrorCode.NoError)
                _log.InfoFormat("Kafka message has been sent by Kafka Producer succesfully to Topic {0}.", topic);
            else
                _log.ErrorFormat("Error occurred while trying to send Kafka message: {0} - {1}", result.Error.Code, result.Error.Reason);
        }

    }
}
