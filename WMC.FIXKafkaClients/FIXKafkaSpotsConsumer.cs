﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;

using WMC.FIXDataObjects;

using Confluent.Kafka;
using Confluent.Kafka.Serialization;

using Newtonsoft;
using Newtonsoft.Json;

namespace WMC.FIXKafkaClients
{
    public class FIXKafkaSpotsConsumer : FIXKafkaConsumer
    {

        public EventHandler<SpotQuotesEventArgs> SpotQuotesConsumed;
        private bool _consuming;

        public FIXKafkaSpotsConsumer()
            :base()
        {
        }

        public FIXKafkaSpotsConsumer(Dictionary<string, object> kafkaConfig)
            :base(kafkaConfig)
        {
        }

        public void StartConsumingSpots(KafkaSpotFeedSource source)
        {
            _consumer.OnMessage += OnSpotQuotes;
            var topic = string.Format(KafkaSpotsTopics.Kafka_Providers_Spots_Template, source);
            _consumer.Subscribe(topic);
            _consuming = true;
            new Thread(() =>
            {
                while (_consuming)
                {
                    _consumer.Poll(_defaultPollTimeoutInMilliSeconds);
                }
            }).Start();
        }

        private void OnSpotQuotes(object sender, Message<Null, string> e)
        {
            var spotQuotesJson = e.Value;
            
            var spotQuotes = JsonConvert.DeserializeObject<SpotQuotes>(spotQuotesJson);

            var spotQuotesEvtArgs = new SpotQuotesEventArgs();
            spotQuotesEvtArgs.SpotQuotes = spotQuotes;
            spotQuotesEvtArgs.UTCTimestamp = DateTime.UtcNow;
            
            foreach (var sq in spotQuotes.quotes)
            {
                _log.DebugFormat("Spot Quote - {0} - Bid: {1}, Ask: {2}, Mid: {3}, Ts: {4}", sq.ccy_pair, sq.bid, sq.ask, sq.mid, sq.ts);
            }
            
            SpotQuotesConsumed?.Invoke(this, spotQuotesEvtArgs);
        }

        public void StopConsumingSpots(KafkaSpotFeedSource source)
        {
            _consumer.OnMessage -= OnSpotQuotes;
            _consuming = false;
            _consumer.Unsubscribe();
        }

        public static void AssignConsumeSpots(KafkaSpotFeedSource source)
        {

            var kafkaConf = new Dictionary<string, object>
            {
                { "group.id", Guid.NewGuid() },
                { "bootstrap.servers", ConfigurationManager.AppSettings["kafka.bootstrap.servers"] },
                { "enable.auto.commit", false }
            };

            using (var consumer = new Consumer<Null, string>(kafkaConf, null, new StringDeserializer(Encoding.UTF8)))
            {

                var topic = string.Format(KafkaSpotsTopics.Kafka_Providers_Spots_Template, source);

                consumer.OnMessage += (_, msg)
                    => _log.DebugFormat("{0}\r\n{1}", msg.TopicPartitionOffset, msg.Value);

                consumer.Assign(new TopicPartitionOffset[] { new TopicPartitionOffset(topic, 0, Offset.End) });

                var tpParOffsetErr = consumer.Position(new TopicPartition[] { new TopicPartition(topic, 0) });
                _log.DebugFormat(string.Format("Offset: {0}", tpParOffsetErr.First().Offset.IsSpecial));

                var wmOffsets = consumer.QueryWatermarkOffsets(new TopicPartition(topic, 0));
                _log.DebugFormat(string.Format("High Watermark: {0}", wmOffsets.High.Value));
                _log.DebugFormat(string.Format("Low Watermark: {0}", wmOffsets.Low.Value));

                consumer.Poll(_defaultPollTimeoutInMilliSeconds);

            }

        }

        public static void SubscribeConsumeSpots(KafkaSpotFeedSource source)
        {

            var kafkaConf = new Dictionary<string, object>
            {
                { "group.id", Guid.NewGuid() },
                { "bootstrap.servers", ConfigurationManager.AppSettings["kafka.bootstrap.servers"] },
                { "auto.offset.reset", "latest" }
            };

            using (var consumer = new Consumer<Null, string>(kafkaConf, null, new StringDeserializer(Encoding.UTF8)))
            {

                var topic = string.Format(KafkaSpotsTopics.Kafka_Providers_Spots_Template, source);

                consumer.OnMessage += (_, msg)
                    => _log.DebugFormat("{0}\r\n{1}", msg.TopicPartitionOffset, msg.Value);

                consumer.Subscribe(topic);

                consumer.Poll(_defaultPollTimeoutInMilliSeconds);
                
            }

        }

    }

    public class SpotQuotesEventArgs : EventArgs
    {
        public DateTime UTCTimestamp { get; set; }
        public SpotQuotes SpotQuotes { get; set; }
    }

}
