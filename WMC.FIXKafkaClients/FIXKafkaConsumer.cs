﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

using log4net;

using Confluent;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;

namespace WMC.FIXKafkaClients
{
    public class FIXKafkaConsumer
    {

        protected static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected Consumer<Null, string> _consumer;
        protected static Dictionary<string, object> _defaultKafkaConfig;
        protected static int _defaultPollTimeoutInMilliSeconds = 15_000;

        public FIXKafkaConsumer()
        {
            _defaultKafkaConfig = new Dictionary<string, object>()
            {
                { "group.id", Guid.NewGuid().ToString() },
                { "bootstrap.servers", ConfigurationManager.AppSettings["kafka.bootstrap.servers"] },
            };

            _consumer = new Consumer<Null, string>(_defaultKafkaConfig, null, new StringDeserializer(Encoding.UTF8));
        }

        public FIXKafkaConsumer(Dictionary<string, object> kafkaConfig)
        {
            _consumer = new Consumer<Null, string>(kafkaConfig, null, new StringDeserializer(Encoding.UTF8));
        }

    }
}
