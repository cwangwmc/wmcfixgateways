﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

using WMC.FIXStateMachines;
using WMC.FIXDataObjects;
using WMC.FIXKafkaClients;

using log4net;

using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WMC.FIXGateways.Citi
{
    
    public class CitiFIXAdaptor : MessageCracker, IApplication
    {

        protected static readonly log4net.ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected SessionID _sessionID;
        protected bool _isLoggedOn;
        protected DateTime _lastLogOnUtc;
        protected DateTime _lastLogOffUtc;

        public CitiFIXAdaptor()
        {
        }

        public virtual void FromApp(QuickFix.Message msg, SessionID sessionID)
        {
            _log.DebugFormat("FromApp: {0}, {1}", sessionID.ToString(), msg.ToString());
            Crack(msg, sessionID);
        }

        public virtual void OnCreate(SessionID sessionID)
        {
            _log.DebugFormat("OnCreate: {0}", sessionID.ToString());
        }

        public virtual void OnLogout(SessionID sessionID)
        {
            _log.DebugFormat("OnLogout: {0}", sessionID.ToString());
            _sessionID = null;
            _isLoggedOn = false;
            _lastLogOffUtc = DateTime.UtcNow;
        }

        public virtual void OnLogon(SessionID sessionID)
        {
            _log.DebugFormat("OnLogon: {0}", sessionID.ToString());
            _sessionID = sessionID;
            _isLoggedOn = true;
            _lastLogOnUtc = DateTime.UtcNow;
        }

        public virtual void FromAdmin(QuickFix.Message msg, SessionID sessionID)
        {
            _log.DebugFormat("FromAdmin: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        public virtual void ToAdmin(QuickFix.Message msg, SessionID sessionID)
        {
            _log.DebugFormat("ToAdmin: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        public virtual void ToApp(QuickFix.Message msg, SessionID sessionID)
        {
            _log.DebugFormat("ToApp: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

    }

}
