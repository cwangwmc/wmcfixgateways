﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXStateMachines;

namespace WMC.FIXGateways.Citi
{
    
    public class CitiMarketDataRequestFIXMessageArgs : MarketDataRequestFIXMessageArgs
    {

        public IEnumerable<string> Tenors { get; private set; }

        public CitiMarketDataRequestFIXMessageArgs(string ccy1, string ccy2, IEnumerable<string> tenors) 
            : base(ccy1, ccy2)
        {
            Tenors = tenors;
        }

    }

    public class CitiOptionsQuoteRequestFIXMessageArgs : QuoteRequestFIXMessageArgs
    {

        public CitiOptionsQuoteRequestFIXMessageArgs(string ccy1, string ccy2)
            : base(ccy1, ccy2)
        {
        }

    }

}
