﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXGateways.Citi
{
    public class CitiFIXConstants
    {
        public static readonly int Tag_963_MDReportID = 963;
        public static readonly int Tag_5280_MDIndicator = 5280;
        public static readonly int Tag_5830_PremiumCurrency = 5830;
        public static readonly int Tag_6215_TenorValue = 6215;
        public static readonly int Tag_6216_NoTenorValues = 6216;
        public static readonly int Tag_6305_DeltaCode = 6305;
        public static readonly int Tag_7538_DeltaBasis = 7538;
        public static readonly int Tag_9016_DeltaType = 9016;
        public static readonly int Tag_9072_CallOrPut = 9072;
        public static readonly int Tag_9074_DeltaCurrency = 9074;
        public static readonly int Tag_9125_ExpiryTimeCode = 9125;

        public static readonly char SubReqType_Sub = '1'; // Snapshot + Updates
        public static readonly char SubReqType_UnSub = '2'; // Disable previous Snapshot + Update Request (Unsubscribe)

        public static readonly int MarketDepth_TopOfBook = 1; // 1 - Top of Book (Currently, only 1 supported)

        public static readonly int ExpiryCut_NewYork10AM = 1;

        public static readonly Dictionary<string, int> NonNYCcyExpiryCuts = new Dictionary<string, int>()
        {
            { "SGD", 2 },   //TOK   - Tokyo 3PM
            { "THB", 2 },   //TOK   - Tokyo 3PM
            { "PHP", 21 },  //MAN2  - Manila 11:30AM
            { "MYR", 22 },  //SG11  - Singapore 11AM
            { "INR", 4 },   //MUM   - Mumbai 12:30PM
            { "HKD", 2 },   //TOK   - Tokyo 3PM
            { "TWD", 7 },   //TPE   - Taipei 11AM
            { "KRW", 23 },  //K3PM  - Korea 3PM
            { "CNY", 6 },   //BEJ   - Beijing 9:15AM
            { "CNH", 2 },   //TOK   - Tokyo 3PM
            { "ILS", 14 },  //TEL   - Tel Aviv 3PM
            { "RUB", 12 },  //MOS1   - Moscow 12:30PM
            { "TRY", 11 },  //HUFE  - London 12PM
            { "PLN", 9 },   //WAR   - Warsaw 11AM
            { "HUF", 10 },  //BD12  - Budapest 12PM
            { "RON", 13 },  //BC13  - Bucharest 1PM
            { "CLP", 19 },  //SN10  - Santiago 10:30AM
            { "COP", 20 },  //BG12  - Bogota 12:30PM
            { "MXN", 16 },  //AFT   - New York 12:30PM
            { "BRL", 24 },  //SPON  - San Paulo 1:15PM
        };
    }
}
