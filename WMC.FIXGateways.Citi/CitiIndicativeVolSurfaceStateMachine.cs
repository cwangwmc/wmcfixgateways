﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXStateMachines;

namespace WMC.FIXGateways.Citi
{

    public class CitiIndicativeVolSurfaceStateMachine : IndicativeVolSurfaceStateMachine
    {
        public IEnumerable<string> Tenors;
        public static readonly IEnumerable<string> DefaultVolSurfaceTenors = new string[] { "1W", "2W", "1M", "2M", "3M", "6M", "9M", "1Y" };

        public CitiIndicativeVolSurfaceStateMachine(string stateMachineRefId, string securitySymbol, bool loggedOn)
            : this(stateMachineRefId, securitySymbol, loggedOn,
                  _defaultTimeToSendVolSurfaceRequest, _defaultTimeToWaitForVolSurfaceRefresh, _defaultTimeInBetweenVolSurfaceRefreshes)
        {
        }

        public CitiIndicativeVolSurfaceStateMachine(string stateMachineRefId, string securitySymbol, bool loggedOn,
            long? timeToSendVolSurfaceRequestArg, long? timeToWaitForVolSurfaceRefreshArg, long? timeInBetweenVolSurfaceRefreshesArg)
            : this(stateMachineRefId, securitySymbol, loggedOn, DefaultVolSurfaceTenors,
                  timeToSendVolSurfaceRequestArg, timeToWaitForVolSurfaceRefreshArg, timeInBetweenVolSurfaceRefreshesArg)
        {
        }

        public CitiIndicativeVolSurfaceStateMachine(string stateMachineRefId, string securitySymbol, bool loggedOn, IEnumerable<string> tenors,
            long? timeToSendVolSurfaceRequestArg, long? timeToWaitForVolSurfaceRefreshArg, long? timeInBetweenVolSurfaceRefreshesArg)
            : base(stateMachineRefId, securitySymbol, loggedOn, timeToSendVolSurfaceRequestArg, timeToWaitForVolSurfaceRefreshArg, timeInBetweenVolSurfaceRefreshesArg)
        {
            Tenors = tenors;

            // States
            var waitingLogonState = new WaitingForLogonS(stateMachineRefId);
            var sendingMDRequestState = new SendingMarketDataRequestS(stateMachineRefId, TimeToSendVolSurfaceRequest);
            var waitingResponseState = new WaitingForResponseS(stateMachineRefId, TimeToWaitForSurfaceRefresh);
            var streamingMDState = new StreamingMarketDataS(stateMachineRefId, TimeInBetweenVolSurfaceRefreshes);
            var doneState = new DoneS(stateMachineRefId);

            // Transitions linking the States
            var LogonT = new LogonT(waitingLogonState, sendingMDRequestState);

            var LogoffT_A = new LogoffT(sendingMDRequestState, waitingLogonState);
            var LogoffT_B = new LogoffT(waitingResponseState, waitingLogonState);
            var LogoffT_C = new LogoffT(streamingMDState, waitingLogonState);

            var MDRequestT = new MDRequestT(sendingMDRequestState, waitingResponseState);

            var MDRequestRejectT_A = new MDRequestRejectT(waitingResponseState, doneState);
            var MDRequestRejectT_B = new MDRequestRejectT(streamingMDState, doneState);

            var TimeOutT = new TimedOutT(waitingResponseState, doneState);

            var MDSnapshotT_A = new MDRefreshT(waitingResponseState, streamingMDState);
            var MDSnapshotT_B = new MDRefreshT(streamingMDState, streamingMDState);

            // Decide the starting state based on whether the FIX engine has already logged on
            if (loggedOn)
                OriginState = sendingMDRequestState;
            else
                OriginState = waitingLogonState;

            TerminalState = doneState;

            CurrentState = OriginState;
            CurrentState.StateFIXMessageOut += OnOutboundFIXMessage;
            CurrentState.StateEntered += OnStateEntered;
            CurrentState.StateExited += OnStateExited;
            
            foreach (var t in CurrentState.Transitions)
                t.Triggered += OnStateTransition;
        }

        protected override FIXMessageDetailedEventArgs MakeFIXMessageDetailedEventArgs(FIXMessageEventArgs args)
        {
            FIXMessageDetailedEventArgs fixMsgDtlEvtArgs = null;

            if (args.MessageType == FIXMessageType.MarketDataRequest_V)
            {
                var ccy1 = SecuritySymbol.Substring(0, 3);
                var ccy2 = SecuritySymbol.Substring(3, 3);

                var fixMsgArgs = new CitiMarketDataRequestFIXMessageArgs(ccy1, ccy2, Tenors);
                fixMsgDtlEvtArgs = new FIXMessageDetailedEventArgs(StateMachineRefId, FIXMessageType.MarketDataRequest_V, fixMsgArgs);
            }

            return fixMsgDtlEvtArgs;
        }

    }
}
