﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using WMC.FIXStateMachines;
using WMC.FIXDataObjects;
using WMC.FIXKafkaClients;

using log4net;

using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WMC.FIXGateways.Citi
{
    public class CitiFIXMarketDataAdaptor : CitiFIXAdaptor
    {
        IEnumerable<string> CurrencyPairs;
        private Dictionary<string, CitiIndicativeVolSurfaceStateMachine> _indicativeVolSurfStateMachines;

        private FIXKafkaSpotsConsumer _ksc;
        private SpotQuotes _refSpotQuotes;
        private ReaderWriterLockSlim _refSpotsRWLock;
        private static readonly int _refSpotsRWLockTimeout = 12_000;
        private static readonly int _refSpotsTsToleranceInMinutes = 15; 

        private readonly Account _account_field;
        private readonly Password _password_field;

        event EventHandler<MDSnapshotEventArgs> MarketDataSnapshotFullRefreshInbound;
        event EventHandler<FIXMessageEventArgs> FIXMessageInbound;

        public CitiFIXMarketDataAdaptor(string account, string password, IEnumerable<string> ccypairs)
            : base()
        {
            _refSpotsRWLock = new ReaderWriterLockSlim();
            _ksc = new FIXKafkaSpotsConsumer();
            _ksc.SpotQuotesConsumed += OnSpotQuotesConsumed;
            _ksc.StartConsumingSpots(KafkaSpotFeedSource.fenics);

            _account_field = new Account(account);
            _password_field = new Password(password);

            CurrencyPairs = ccypairs.Select(ccypair => ccypair.ToUpper());
            _indicativeVolSurfStateMachines = new Dictionary<string, CitiIndicativeVolSurfaceStateMachine>();

            MarketDataSnapshotFullRefreshInbound += OnMarketDataSnapshotFullRefresh;

            CreateAndActivateIndicativeVolSurfaceStateMachines(CurrencyPairs);
        }

        private void CreateAndActivateIndicativeVolSurfaceStateMachines(IEnumerable<string> currencypairs)
        {
            foreach (var ccypair in currencypairs)
            {
                var uuid = Guid.NewGuid();
                var stateMachineRefId = string.Format("{0}_{1}", ccypair, uuid);
                var indicativeVolSurfStateMachineForPair = new CitiIndicativeVolSurfaceStateMachine(stateMachineRefId, ccypair, loggedOn: false, 
                    timeToSendVolSurfaceRequestArg: 2 * 60_000, timeToWaitForVolSurfaceRefreshArg: 10 * 60_000, timeInBetweenVolSurfaceRefreshesArg: 30 * 60_000);

                FIXMessageInbound += indicativeVolSurfStateMachineForPair.OnInboundFIXMessage;
                indicativeVolSurfStateMachineForPair.StateMachineFIXMessageOut += OnStateMachineOutboundFIXMessage;

                indicativeVolSurfStateMachineForPair.Activate();
                _indicativeVolSurfStateMachines.Add(stateMachineRefId, indicativeVolSurfStateMachineForPair);
            }
        }

        private void DeactivateIndicativeVolSurfaceStateMachines()
        {
            foreach (var volSurfaceStateMachine in _indicativeVolSurfStateMachines.Values)
            {
                volSurfaceStateMachine.Deactivate();
            }
        }

        public override void OnLogon(SessionID sessionID)
        {
            base.OnLogon(sessionID);

            foreach (var stateMachineRefId in _indicativeVolSurfStateMachines.Keys)
            {
                var logon_A_args = new FIXMessageEventArgs(stateMachineRefId, FIXMessageType.Logon_A, _lastLogOnUtc);
                FIXMessageInbound?.Invoke(this, logon_A_args);
            }
        }

        public override void OnLogout(SessionID sessionID)
        {
            base.OnLogout(sessionID);

            foreach (var stateMachineRefId in _indicativeVolSurfStateMachines.Keys)
            {
                var logoff_5_args = new FIXMessageEventArgs(stateMachineRefId, FIXMessageType.Logoff_5, _lastLogOffUtc);
                FIXMessageInbound?.Invoke(this, logoff_5_args);
            }
        }

        public override void ToAdmin(QuickFix.Message msg, SessionID sessionID)
        {
            base.ToAdmin(msg, sessionID);

            if (msg is Logon)
            {
                var logonMsg = (Logon)msg;
                logonMsg.SetField(_password_field);
                logonMsg.ResetSeqNumFlag = new ResetSeqNumFlag(true);
            }
        }

        private void OnSpotQuotesConsumed(object sender, SpotQuotesEventArgs args)
        {
            if (_refSpotsRWLock.TryEnterWriteLock(_refSpotsRWLockTimeout))
            {
                _log.InfoFormat("Updating RefSpotQuotes, UTC Timestamp: {0}", args.UTCTimestamp.ToString("yyyy-MM-ddTHH:mm:ss"));
                _refSpotQuotes = args.SpotQuotes;
                _log.Info("Updated RefSpotQuotes.");
                _refSpotsRWLock.ExitWriteLock();
            }
        }

        private void OnStateMachineOutboundFIXMessage(object sender, FIXMessageDetailedEventArgs msgDetailsEventArgs)
        {
            _log.DebugFormat("Entering OnStateMachineOutboundFIXMessage ...");

            QuickFix.Message message = null;

            if (msgDetailsEventArgs.MessageType == FIXMessageType.MarketDataRequest_V)
            {
                var MDReqMessageArgs = (CitiMarketDataRequestFIXMessageArgs)msgDetailsEventArgs.MessageArgs;
                message = MakeMarketDataRequestSub(msgDetailsEventArgs.RefId, MDReqMessageArgs.Ccy1, MDReqMessageArgs.Ccy2, MDReqMessageArgs.Tenors);
            }

            if (message != null)
            {
                _log.InfoFormat("Sending out FIX message type {0} over Session Id: {1}", msgDetailsEventArgs.MessageType, _sessionID);
                Session.SendToTarget(message, _sessionID);
            }

            _log.DebugFormat("Exiting OnStateMachineOutboundFIXMessage ...");
        }

        public void OnMessage(MarketDataSnapshotFullRefresh mdSnpshtRefresh, SessionID sessionID)
        {
            _log.DebugFormat("Entering OnMessage: marketDataSnapshotFullRefresh SessionId - {0} ...", sessionID);

            // Tag 262 MDReqID - Required - Unique identifier for Market Data Request
            var mdReqId = mdSnpshtRefresh.MDReqID.getValue();
            _log.InfoFormat("MarketDataSnapshotFullRefresh received with MDReqId: {0}.", mdReqId);

            if (!_indicativeVolSurfStateMachines.ContainsKey(mdReqId))
            {
                _log.WarnFormat("MDReqId: {0} is not recognised in this session ...", mdReqId);
                _log.InfoFormat("Constructing Market Data Request Unsubscribe for MDReqId: {0}", mdReqId);
                var unsubMdr = MakeMarketDataRequestUnsub(mdReqId);
                _log.InfoFormat("Sending Market Data Request Unsubscribe ...");
                Session.SendToTarget(unsubMdr, _sessionID);
                return;
            }

            var utcNow = DateTime.UtcNow;

            // Tag 963 MDReportID - Unique identifier for Market Data Report
            var mdReportId = string.Empty;
            if (mdSnpshtRefresh.IsSetField(CitiFIXConstants.Tag_963_MDReportID))
                mdReportId = mdSnpshtRefresh.GetString(CitiFIXConstants.Tag_963_MDReportID);

            _log.DebugFormat("Start preparing FIXMessageEventArgs ...");

            var FIXMsgEvtArgs = new FIXMessageEventArgs(mdReqId, FIXMessageType.MarketDataSnapshotFullRefresh_W, utcNow);

            _log.DebugFormat("End preparing FIXMessageEventArgs.");

            _log.DebugFormat("Invoking Event FIXMessageInbound ...");

            FIXMessageInbound?.Invoke(this, FIXMsgEvtArgs);

            _log.DebugFormat("Start procesing marketDataSnapshotFullRefresh MDReqID: {0} ...", mdReqId);

            #region MarketDataSnapshotFullRefresh

            // Tag 5280 MDIndicator - Required - Market Data Indicator
            // 0 = Incremental update
            // 1 = Complete snapshot
            // (Only 1 supported currently)
            var mdIndicator = mdSnpshtRefresh.GetInt(CitiFIXConstants.Tag_5280_MDIndicator);

            // Standalone
            // Tag 55 - Symbol e.g. EURUSD
            var symbol = string.Empty;
            if (mdSnpshtRefresh.IsSetField(Tags.Symbol))
            {
                symbol = mdSnpshtRefresh.GetString(Tags.Symbol);
                _log.DebugFormat("MDReqID {0} - Symbol: {1}", mdReqId, symbol);
            }

            // Grouped security symbols
            // NoRelatedSym Repeating group
            // Tag 146 NoRelatedSym
            var noRelatedSym = new NoRelatedSym();

            string[] symbolArray = null;
            if (mdSnpshtRefresh.IsSetField(noRelatedSym))
            {
                var noRelatedSymGrp = new Group(Tags.NoRelatedSym, Tags.Symbol);
                for (int nrsGrpIndex = 1; nrsGrpIndex <= mdSnpshtRefresh.GetInt(Tags.NoRelatedSym); nrsGrpIndex++)
                {
                    mdSnpshtRefresh.GetGroup(nrsGrpIndex, noRelatedSymGrp);
                    symbol = noRelatedSymGrp.GetString(Tags.Symbol);
                    symbolArray[nrsGrpIndex - 1] = symbol;
                }
            }

            var mdQuoteSetsList = new List<MDQuoteSet>();

            // NoQuoteSets Repeating group
            // Tag 296 - Required - NoQuotesSets
            var noQuoteSetsGrp = new Group(Tags.NoQuoteSets, Tags.QuoteSetID);

            var mdQuoteSetsCount = mdSnpshtRefresh.GetInt(Tags.NoQuoteSets);
            _log.DebugFormat("MDReqID {0} - QuoteSets group count: {1}", mdReqId, mdQuoteSetsCount);

            for (int qnsGrpIdx = 1; qnsGrpIdx <= mdQuoteSetsCount; qnsGrpIdx++)
            {

                mdSnpshtRefresh.GetGroup(qnsGrpIdx, noQuoteSetsGrp);

                // Tag 302 QuoteSetID - Required - Sequence id for the Quote Set
                var quoteSetId = noQuoteSetsGrp.GetString(Tags.QuoteSetID);
                _log.DebugFormat("MDReqID {0} - Start processing quoteSet QuoteSetID: {1} ...", mdReqId, quoteSetId);

                #region QuoteSet

                // Tag 48 SecurityID - Security identifier
                var securityId = string.Empty;
                if (noQuoteSetsGrp.IsSetField(Tags.SecurityID))
                {
                    _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - SecurityId: {2}", mdReqId, quoteSetId, securityId);
                    securityId = noQuoteSetsGrp.GetString(Tags.SecurityID);
                }

                // Tag 847 TargetStrategy - Required - Option strategy
                // 1 = Vanilla
                // 6 = Straddle
                // 7 = Strangle
                // 8 = Risk Reversal
                // 9 = Butterfly
                var targetStrategy = noQuoteSetsGrp.GetInt(Tags.TargetStrategy);
                _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - TargetStrategy: {2}", mdReqId, quoteSetId, targetStrategy);

                // Tag 9072 CallOrPut - Only required for Vanilla and Risk Reversal
                // 1 = Call
                // 2 = Put
                // 3 = Call over Put
                // 4 = Put over Call
                int? callOrPut = null;
                if (noQuoteSetsGrp.IsSetField(CitiFIXConstants.Tag_9072_CallOrPut))
                {
                    callOrPut = noQuoteSetsGrp.GetInt(CitiFIXConstants.Tag_9072_CallOrPut);
                    _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - CallOrPut: {2}", mdReqId, quoteSetId, callOrPut);
                }

                // Tag 9125 ExpiryTimeCode - Required - Expiry cutoff
                // See Appendix (3.3.1) for supported valuesStart Processing
                var expiryTimeCode = noQuoteSetsGrp.GetInt(CitiFIXConstants.Tag_9125_ExpiryTimeCode);
                _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - ExpiryTimeCode: {2}", mdReqId, quoteSetId, expiryTimeCode);

                // Tag 5830 PremiumCurrency - Required - ISO code of Premium Currency
                var premCcy = noQuoteSetsGrp.GetString(CitiFIXConstants.Tag_5830_PremiumCurrency);
                _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - PremCcy: {2}", mdReqId, quoteSetId, premCcy);

                var mdEntriesList = new List<MDEntry>();
                // NoMDEntries Repeating group
                // Tag 268 - Required - NoMDEntries
                var noMdEntriesGrp = new Group(Tags.NoMDEntries, Tags.MDEntryType);

                var mdEntriesCount = noQuoteSetsGrp.GetInt(Tags.NoMDEntries);
                _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntries group count: {2}", mdReqId, quoteSetId, mdEntriesCount);

                for (int nmdeGrpIdx = 1; nmdeGrpIdx <= mdEntriesCount; nmdeGrpIdx++)
                {

                    noQuoteSetsGrp.GetGroup(nmdeGrpIdx, noMdEntriesGrp);

                    // Tag 278 MDEntryID - Required - Entry identifier
                    var mdEntryId = noMdEntriesGrp.GetString(Tags.MDEntryID);
                    _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - Start processing mdEntry MDEntryID: {2} ...", mdReqId, quoteSetId, mdEntryId);

                    #region MDEntry

                    // Tag 269 MDEntryType - Required - ...
                    // 0 = Bid 
                    // 1 = Offer
                    // H = Mid
                    char mdEntryType = noMdEntriesGrp.GetChar(Tags.MDEntryType);
                    _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - MDEntryType: {3}", mdReqId, quoteSetId, mdEntryId, mdEntryType);

                    // Tag 270 MDEntryPx = Required - Volatility Price in decimal format
                    var mdEntryPx = noMdEntriesGrp.GetDecimal(Tags.MDEntryPx);
                    _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - MDEntryPx: {3}", mdReqId, quoteSetId, mdEntryId, mdEntryPx);

                    // Tag 611 LegMaturityDate - Expiry date in the YYYYMMDD format for the leg
                    DateTime? legMaturityDate = null;
                    if (noMdEntriesGrp.IsSetField(Tags.LegMaturityDate))
                    {
                        legMaturityDate = noMdEntriesGrp.GetDateOnly(Tags.LegMaturityDate);
                        _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - LegMaturityDate: {3}", mdReqId, quoteSetId, mdEntryId, legMaturityDate);
                    }

                    // Tag 6215 TenorValue - Tenor value for the leg 
                    // e.g. ON (ForOvernight), 1W(For Weeks), 3M(For Months), 1Y(For Years), etc
                    var tenorValue = string.Empty;
                    if (noMdEntriesGrp.IsSetField(Tags.LegMaturityDate))
                    {
                        tenorValue = noMdEntriesGrp.GetString(CitiFIXConstants.Tag_6215_TenorValue);
                        _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - TenorValue: {3}", mdReqId, quoteSetId, mdEntryId, tenorValue);
                    }

                    // Tag 612 StrikePrice - Strike Price
                    decimal? strikePrice = null;
                    if (noMdEntriesGrp.IsSetField(Tags.StrikePrice))
                    {
                        strikePrice = noMdEntriesGrp.GetDecimal(Tags.StrikePrice);
                        _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - StrikePrice: {3}", mdReqId, quoteSetId, mdEntryId, strikePrice);
                    }

                    // Tag 7538 DeltaBasis
                    // 1 = Smile
                    // 2 = Market
                    int? deltaBasis = null;
                    if (noMdEntriesGrp.IsSetField(CitiFIXConstants.Tag_7538_DeltaBasis))
                    {
                        deltaBasis = noMdEntriesGrp.GetInt(CitiFIXConstants.Tag_7538_DeltaBasis);
                        _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - DeltaBasis: {3}", mdReqId, quoteSetId, mdEntryId, deltaBasis);
                    }

                    // Tag 9016 DeltaType
                    // 1 = Spot
                    // 2 = Forward
                    int? deltaType = null;
                    if (noMdEntriesGrp.IsSetField(CitiFIXConstants.Tag_9016_DeltaType))
                    {
                        deltaType = noMdEntriesGrp.GetInt(CitiFIXConstants.Tag_9016_DeltaType);
                        _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - DeltaType: {3}", mdReqId, quoteSetId, mdEntryId, deltaType);
                    }

                    // Tag 9074 DeltaCurrency - Currency code for delta currency
                    var deltaCcy = string.Empty;
                    if (noMdEntriesGrp.IsSetField(CitiFIXConstants.Tag_9074_DeltaCurrency))
                    {
                        deltaCcy = noMdEntriesGrp.GetString(CitiFIXConstants.Tag_9074_DeltaCurrency);
                        _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - DeltaCcy: {3}", mdReqId, quoteSetId, mdEntryId, deltaCcy);
                    }

                    // Tag 6305 DeltaCode - Required - DeltaCode
                    // 0 = DN
                    // 1 = ATM
                    // 2 = 10D
                    // 3 = 25D
                    var deltaCode = noMdEntriesGrp.GetInt(CitiFIXConstants.Tag_6305_DeltaCode);
                    _log.DebugFormat("MDReqID {0} - QuoteSetID {1} - MDEntryID {2} - DeltaCode: {3}", mdReqId, quoteSetId, mdEntryId, deltaCode);

                    var mdEntry = new MDEntry();
                    MDEntryTypeEnum mdEntryTypeEnum = MDEntryTypeEnum.Bid;
                    switch (mdEntryType)
                    {
                        case '0':
                            mdEntryTypeEnum = MDEntryTypeEnum.Bid;
                            break;
                        case '1':
                            mdEntryTypeEnum = MDEntryTypeEnum.Offer;
                            break;
                        case 'H':
                            mdEntryTypeEnum = MDEntryTypeEnum.Mid;
                            break;
                    }

                    mdEntry.MDEntryType = mdEntryTypeEnum;
                    mdEntry.MDEntryID = mdEntryId;
                    mdEntry.MDEntryPx = mdEntryPx;
                    mdEntry.LegMaturityDate = legMaturityDate;
                    mdEntry.TenorValue = tenorValue;

                    if (deltaBasis.HasValue)
                        mdEntry.DeltaBasis = (DeltaBasisEnum)deltaBasis;

                    if (deltaType.HasValue)
                        mdEntry.DeltaType = (DeltaTypeEnum)deltaType;

                    mdEntry.DeltaCcy = deltaCcy;
                    mdEntry.DeltaCode = (DeltaCodeEnum)deltaCode;

                    #endregion MDEntry

                    _log.DebugFormat("MDreqID {0} - QuoteSetID {1} - End processing mdEntry MDEntryID: {2}.", mdReqId, quoteSetId, mdEntryId);

                    mdEntriesList.Add(mdEntry);
                }

                var mdQuoteSet = new MDQuoteSet();

                mdQuoteSet.QuoteSetID = quoteSetId;
                mdQuoteSet.SecurityID = securityId;
                mdQuoteSet.TargetStrategy = (TargetStrategyEnum)targetStrategy;

                if (callOrPut.HasValue)
                    mdQuoteSet.CallOrPut = (CallOrPutEnum)callOrPut;

                mdQuoteSet.PremiumCurrency = premCcy;
                mdQuoteSet.MDEntries = mdEntriesList;

                mdQuoteSetsList.Add(mdQuoteSet);

                #endregion QuoteSet

                _log.DebugFormat("MDReqID {0} - End processing quoteSet QuoteSetID: {1}.", mdReqId, quoteSetId);
            }

            var mdSnpsht = new MDSnapshot();
            mdSnpsht.MDReportID = mdReportId;
            mdSnpsht.MDReqID = mdReqId;
            mdSnpsht.SecuritySymbol = symbol;
            mdSnpsht.QuoteSets = mdQuoteSetsList;

            #endregion MarketDataSnapshotFullRefresh

            _log.DebugFormat("End processing marketDataSnapshotFullRefresh MDReqID: {0}.", mdReqId);

            _log.DebugFormat("Start preparing MDSnapshotEventArgs ...");

            var mdSnapshotEvtArgs = new MDSnapshotEventArgs();
            mdSnapshotEvtArgs.UTCTimestamp = utcNow;
            mdSnapshotEvtArgs.MDReqID = mdSnpsht.MDReqID;
            mdSnapshotEvtArgs.SecuritySymbol = mdSnpsht.SecuritySymbol;
            mdSnapshotEvtArgs.MDSnapshot = mdSnpsht;

            _log.DebugFormat("End preparing MDSnapshotEventArgs.");

            _log.DebugFormat("Invoking Event MarketDataSnapshotFullRefreshInbound ...");

            MarketDataSnapshotFullRefreshInbound?.Invoke(this, mdSnapshotEvtArgs);

            _log.DebugFormat("Exiting OnMessage: marketDataSnapshotFullRefresh SessionId - {0} ...", sessionID);
        }

        public void OnMessage(MarketDataRequestReject mdReqRej, SessionID sessionID)
        {
            _log.DebugFormat("Entering OnMessage: marketDataRequestReject sessionId - {0} ...", sessionID);

            var mdReqId = mdReqRej.MDReqID.getValue();
            _log.WarnFormat("MarketDataRequestReject received with MDReqId: {0}.", mdReqId);

            if (!_indicativeVolSurfStateMachines.ContainsKey(mdReqId))
            {
                _log.WarnFormat("MDReqId: {0} is not recognised in this session ...", mdReqId);
                _log.WarnFormat("Will not relay MarketDataRequestReject with MDReqId: {0} further.", mdReqId);
                return;
            }

            _log.DebugFormat("Start preparing MarketDataRequestReject_Y FIXMessageEventArgs ...");

            var fixMsgEvtArgs = new FIXMessageEventArgs(mdReqId, FIXMessageType.MarketDataRequestReject_Y, DateTime.UtcNow);

            _log.DebugFormat("End preparing FIXMessageEventArgs.");

            _log.DebugFormat("Calling EventHandler OnInboundFIXMessage ...");

            FIXMessageInbound?.Invoke(this, fixMsgEvtArgs);

            _log.DebugFormat("Exiting OnMessage: marketDataRequestReject SessionId - {0} ...", sessionID);
        }

        private MarketDataRequest MakeMarketDataRequestUnsub(string refId)
        {
            MarketDataRequest mdr = new MarketDataRequest();
            mdr.MDReqID = new MDReqID(refId);
            mdr.SetField(_account_field);

            // Tag 263 - SubscriptionRequestType
            // 0 - Snapshot,
            // 1 - Snapshot + Updates,
            // 2 - Disable previous Snapshot + Update Request (Unsubscribe)
            mdr.SubscriptionRequestType = new SubscriptionRequestType(CitiFIXConstants.SubReqType_UnSub);

            // Tag 264 - Depth of market for Book Snapshot
            // 1 - Top of Book (Currently, only 1 supported)
            mdr.MarketDepth = new MarketDepth(CitiFIXConstants.MarketDepth_TopOfBook);

            return mdr;
        }

        private MarketDataRequest MakeMarketDataRequestSub(string refId, string ccy1, string ccy2, IEnumerable<string> tenors)
        {
            MarketDataRequest mdr = new MarketDataRequest();
            mdr.MDReqID = new MDReqID(refId);
            mdr.SetField(_account_field);

            // Tag 263 - SubscriptionRequestType
            // 0 - Snapshot,
            // 1 - Snapshot + Updates,
            // 2 - Disable previous Snapshot + Update Request (Unsubscribe)
            mdr.SubscriptionRequestType = new SubscriptionRequestType(CitiFIXConstants.SubReqType_Sub);

            // Tag 264 - Depth of market for Book Snapshot
            // 1 - Top of Book (Currently, only 1 supported)
            mdr.MarketDepth = new MarketDepth(CitiFIXConstants.MarketDepth_TopOfBook);

            // Tag 146 - NoRelatedSym Group
            var noRelatedSymGrp = new MarketDataRequest.NoRelatedSymGroup();
            noRelatedSymGrp.Symbol = new Symbol(string.Format("{0}{1}", ccy1.ToUpper(), ccy2.ToUpper()));

            var expiryCut = CitiFIXConstants.ExpiryCut_NewYork10AM; //NYK - New York 10AM
            if (CitiFIXConstants.NonNYCcyExpiryCuts.ContainsKey(ccy2))
                expiryCut = CitiFIXConstants.NonNYCcyExpiryCuts[ccy2];

            noRelatedSymGrp.SetField(new IntField(CitiFIXConstants.Tag_9125_ExpiryTimeCode, expiryCut));

            // Tag 6126 - NoTenorValue Group
            foreach (string t in tenors)
            {
                var noTenorValuesGrp = new Group(CitiFIXConstants.Tag_6216_NoTenorValues, CitiFIXConstants.Tag_6215_TenorValue);
                noTenorValuesGrp.SetField(new StringField(CitiFIXConstants.Tag_6215_TenorValue, t));
                noRelatedSymGrp.AddGroup(noTenorValuesGrp);
            }

            mdr.AddGroup(noRelatedSymGrp);

            return mdr;
        }

        private void OnMarketDataSnapshotFullRefresh(object sender, MDSnapshotEventArgs mdSnpshtEvtArgs)
        {
            _log.DebugFormat("Entering OnMarketDataSnapshotFullRefresh ...");

            ProcessMarketDataSnapshotAsync(mdSnpshtEvtArgs.MDSnapshot, mdSnpshtEvtArgs.UTCTimestamp);

            _log.DebugFormat("Exiting OnMarketDataSnapshotFullRefresh ...");
        }

        private async void ProcessMarketDataSnapshotAsync(MDSnapshot mdSnapshot, DateTime utcAsOfTs)
        {
            await Task.Run(() => 
            {

                var mdReqId = mdSnapshot.MDReqID;
                var secSym = mdSnapshot.SecuritySymbol;
                
                _log.DebugFormat("Start Processing MDSnapshot {0}, {1} ...", mdReqId, secSym);

                _log.DebugFormat("Building vol surface from MarketDataSnapshotFullRefresh {0} for security {1} ...", mdReqId, secSym);

                VolatilitySurface volSurface = null;
                string failureReason = string.Empty;

                if (TryMakeMarketDataVolatilitySurface(mdSnapshot, utcAsOfTs, out volSurface, out failureReason))
                {
                    var volSurface_json = JsonConvert.SerializeObject(volSurface, new StringEnumConverter());

                    _log.DebugFormat("{0} volatility surface JSON-encoded: {1}", secSym, volSurface_json);

                    FIXKafkaProducer.SendVolsAsync(KafkaVolatilityFeedSource.citi, volSurface_json);
                    FIXKafkaProducer.SendVolsAsync(KafkaVolatilityFeedSource.citi, secSym, volSurface_json);

                    _log.DebugFormat("End Processing MDSnapshotEventArgs {0}, {1} ...", mdReqId, secSym);
                }
                else
                    _log.ErrorFormat("Failed to make volatility surface with reason: {0}", failureReason);


            });
        }

        private bool TryMakeMarketDataVolatilitySurface(MDSnapshot snapshot, DateTime utcAsOfTs,
            out VolatilitySurface volSurface, out string failureReason)
        {
            var secSym = snapshot.SecuritySymbol;

            var leftCcy = secSym.Substring(0, 3);
            var rightCcy = secSym.Substring(3, 3);

            var volSurfSnpsht = snapshot;
            var premCcy = volSurfSnpsht.QuoteSets.First().PremiumCurrency;

            var mdRqstId = volSurfSnpsht.MDReqID;
            var mdRprtId = volSurfSnpsht.MDReportID;
            var quoteSets = volSurfSnpsht.QuoteSets;

            var atm_MDEntries = quoteSets.Where(qs => qs.TargetStrategy == TargetStrategyEnum.Straddle).Select(qs => qs.MDEntries).First();
            var c_MDEntries = quoteSets.Where(qs => qs.TargetStrategy == TargetStrategyEnum.Vanilla).Where(qs => qs.CallOrPut != null && qs.CallOrPut == CallOrPutEnum.Call).Select(qs => qs.MDEntries).First();
            var p_MDEntries = quoteSets.Where(qs => qs.TargetStrategy == TargetStrategyEnum.Vanilla).Where(qs => qs.CallOrPut != null && qs.CallOrPut == CallOrPutEnum.Put).Select(qs => qs.MDEntries).First();

            var atm_MDEntries_Bids = atm_MDEntries.Where(mde => mde.MDEntryType == MDEntryTypeEnum.Bid);
            var atm_MDEntries_Offers = atm_MDEntries.Where(mde => mde.MDEntryType == MDEntryTypeEnum.Offer);

            var d25Call_MDEntries_Bids = c_MDEntries.Where(mde => mde.DeltaCode == DeltaCodeEnum.D25 && mde.MDEntryType == MDEntryTypeEnum.Bid);
            var d25Call_MDEntries_Offers = c_MDEntries.Where(mde => mde.DeltaCode == DeltaCodeEnum.D25 && mde.MDEntryType == MDEntryTypeEnum.Offer);

            var d10Call_MDEntries_Bids = c_MDEntries.Where(mde => mde.DeltaCode == DeltaCodeEnum.D10 && mde.MDEntryType == MDEntryTypeEnum.Bid);
            var d10Call_MDEntries_Offers = c_MDEntries.Where(mde => mde.DeltaCode == DeltaCodeEnum.D10 && mde.MDEntryType == MDEntryTypeEnum.Offer);

            var d25Put_MDEntries_Bids = p_MDEntries.Where(mde => mde.DeltaCode == DeltaCodeEnum.D25 && mde.MDEntryType == MDEntryTypeEnum.Bid);
            var d25Put_MDEntries_Offers = p_MDEntries.Where(mde => mde.DeltaCode == DeltaCodeEnum.D25 && mde.MDEntryType == MDEntryTypeEnum.Offer);

            var d10Put_MDEntries_Bids = p_MDEntries.Where(mde => mde.DeltaCode == DeltaCodeEnum.D10 && mde.MDEntryType == MDEntryTypeEnum.Bid);
            var d10Put_MDEntries_Offers = p_MDEntries.Where(mde => mde.DeltaCode == DeltaCodeEnum.D10 && mde.MDEntryType == MDEntryTypeEnum.Offer);

            var tenors = atm_MDEntries.Select(mde => mde.TenorValue).Distinct();
            var tenors_volSmiles = new Dictionary<string, VolatilitySmile>();

            foreach (var tnr in tenors)
            {
                var atm_mdEntry_bid = atm_MDEntries_Bids.Where(atm_mde => atm_mde.TenorValue == tnr).First();
                var atm_mdEntry_offer = atm_MDEntries_Offers.Where(atm_mde => atm_mde.TenorValue == tnr).First();

                var d25c_mdEntry_bid = d25Call_MDEntries_Bids.Where(d25c_mde => d25c_mde.TenorValue == tnr).First();
                var d25c_mdEntry_offer = d25Call_MDEntries_Offers.Where(d25c_mde => d25c_mde.TenorValue == tnr).First();

                var d25p_mdEntry_bid = d25Put_MDEntries_Bids.Where(d25p_mde => d25p_mde.TenorValue == tnr).First();
                var d25p_mdEntry_offer = d25Put_MDEntries_Offers.Where(d25p_mde => d25p_mde.TenorValue == tnr).First();

                var d10c_mdEntry_bid = d10Call_MDEntries_Bids.Where(d10c_mde => d10c_mde.TenorValue == tnr).First();
                var d10c_mdEntry_offer = d10Call_MDEntries_Offers.Where(d10c_mde => d10c_mde.TenorValue == tnr).First();

                var d10p_mdEntry_bid = d10Put_MDEntries_Bids.Where(d10p_mde => d10p_mde.TenorValue == tnr).First();
                var d10p_mdEntry_offer = d10Put_MDEntries_Offers.Where(d10p_mde => d10p_mde.TenorValue == tnr).First();

                VolatilitySmilePoint atm_volpt = null;
                var atm_volatility = AverageScaleBidOfferVolatility((double)atm_mdEntry_bid.MDEntryPx, (double)atm_mdEntry_offer.MDEntryPx);
                if (atm_mdEntry_bid.DeltaCode == DeltaCodeEnum.DN && atm_mdEntry_offer.DeltaCode == DeltaCodeEnum.DN)
                    atm_volpt = new VolatilitySmilePoint(atm_volatility, Atm.Dn);
                else if (atm_mdEntry_bid.DeltaCode == DeltaCodeEnum.ATM && atm_mdEntry_offer.DeltaCode == DeltaCodeEnum.ATM)
                    atm_volpt = new VolatilitySmilePoint(atm_volatility, Atm.Atmf);

                VolatilitySmilePoint d25c_volpt = null;
                VolatilitySmilePoint d25p_volpt = null;
                VolatilitySmilePoint d10c_volpt = null;
                VolatilitySmilePoint d10p_volpt = null;

                var d25c_volatility = AverageScaleBidOfferVolatility((double)d25c_mdEntry_bid.MDEntryPx, (double)d25c_mdEntry_offer.MDEntryPx);
                var d25p_volatility = AverageScaleBidOfferVolatility((double)d25p_mdEntry_bid.MDEntryPx, (double)d25p_mdEntry_offer.MDEntryPx);
                var d10c_volatility = AverageScaleBidOfferVolatility((double)d10c_mdEntry_bid.MDEntryPx, (double)d10c_mdEntry_offer.MDEntryPx);
                var d10p_volatility = AverageScaleBidOfferVolatility((double)d10p_mdEntry_bid.MDEntryPx, (double)d10p_mdEntry_offer.MDEntryPx);

                if (premCcy == leftCcy)
                {
                    //Pc

                    if (d25c_mdEntry_bid.DeltaType == DeltaTypeEnum.Spot && d25c_mdEntry_offer.DeltaType == DeltaTypeEnum.Spot)
                        d25c_volpt = new VolatilitySmilePoint(d25c_volatility, 25, DeltaType.SpotPc, DeltaOrientation.Call);
                    else if (d25c_mdEntry_bid.DeltaType == DeltaTypeEnum.Forward && d25c_mdEntry_offer.DeltaType == DeltaTypeEnum.Forward)
                        d25c_volpt = new VolatilitySmilePoint(d25c_volatility, 25, DeltaType.ForwardPc, DeltaOrientation.Call);

                    if (d25p_mdEntry_bid.DeltaType == DeltaTypeEnum.Spot && d25p_mdEntry_offer.DeltaType == DeltaTypeEnum.Spot)
                        d25p_volpt = new VolatilitySmilePoint(d25p_volatility, 25, DeltaType.SpotPc, DeltaOrientation.Put);
                    else if (d25p_mdEntry_bid.DeltaType == DeltaTypeEnum.Forward && d25p_mdEntry_offer.DeltaType == DeltaTypeEnum.Forward)
                        d25p_volpt = new VolatilitySmilePoint(d25p_volatility, 25, DeltaType.ForwardPc, DeltaOrientation.Put);

                    if (d10c_mdEntry_bid.DeltaType == DeltaTypeEnum.Spot && d10c_mdEntry_offer.DeltaType == DeltaTypeEnum.Spot)
                        d10c_volpt = new VolatilitySmilePoint(d10c_volatility, 10, DeltaType.SpotPc, DeltaOrientation.Call);
                    else if (d10c_mdEntry_bid.DeltaType == DeltaTypeEnum.Forward && d10p_mdEntry_offer.DeltaType == DeltaTypeEnum.Forward)
                        d10c_volpt = new VolatilitySmilePoint(d10c_volatility, 10, DeltaType.ForwardPc, DeltaOrientation.Call);

                    if (d10p_mdEntry_bid.DeltaType == DeltaTypeEnum.Spot && d10p_mdEntry_offer.DeltaType == DeltaTypeEnum.Spot)
                        d10p_volpt = new VolatilitySmilePoint(d10p_volatility, 10, DeltaType.SpotPc, DeltaOrientation.Put);
                    else if (d10p_mdEntry_bid.DeltaType == DeltaTypeEnum.Forward && d10p_mdEntry_offer.DeltaType == DeltaTypeEnum.Forward)
                        d10p_volpt = new VolatilitySmilePoint(d10p_volatility, 10, DeltaType.ForwardPc, DeltaOrientation.Put);

                }
                else if (premCcy == rightCcy)
                {
                    //Bp

                    if (d25c_mdEntry_bid.DeltaType == DeltaTypeEnum.Spot && d25c_mdEntry_offer.DeltaType == DeltaTypeEnum.Spot)
                        d25c_volpt = new VolatilitySmilePoint(d25c_volatility, 25, DeltaType.SpotBp, DeltaOrientation.Call);
                    else if (d25c_mdEntry_bid.DeltaType == DeltaTypeEnum.Forward && d25c_mdEntry_offer.DeltaType == DeltaTypeEnum.Forward)
                        d25c_volpt = new VolatilitySmilePoint(d25c_volatility, 25, DeltaType.ForwardBp, DeltaOrientation.Call);

                    if (d25p_mdEntry_bid.DeltaType == DeltaTypeEnum.Spot && d25p_mdEntry_offer.DeltaType == DeltaTypeEnum.Spot)
                        d25p_volpt = new VolatilitySmilePoint(d25p_volatility, 25, DeltaType.SpotBp, DeltaOrientation.Put);
                    else if (d25p_mdEntry_bid.DeltaType == DeltaTypeEnum.Forward && d25p_mdEntry_offer.DeltaType == DeltaTypeEnum.Forward)
                        d25p_volpt = new VolatilitySmilePoint(d25p_volatility, 25, DeltaType.ForwardBp, DeltaOrientation.Put);

                    if (d10c_mdEntry_bid.DeltaType == DeltaTypeEnum.Spot && d10c_mdEntry_offer.DeltaType == DeltaTypeEnum.Spot)
                        d10c_volpt = new VolatilitySmilePoint(d10c_volatility, 10, DeltaType.SpotBp, DeltaOrientation.Call);
                    else if (d10c_mdEntry_bid.DeltaType == DeltaTypeEnum.Forward && d10p_mdEntry_offer.DeltaType == DeltaTypeEnum.Forward)
                        d10c_volpt = new VolatilitySmilePoint(d10c_volatility, 10, DeltaType.ForwardBp, DeltaOrientation.Call);

                    if (d10p_mdEntry_bid.DeltaType == DeltaTypeEnum.Spot && d10p_mdEntry_offer.DeltaType == DeltaTypeEnum.Spot)
                        d10p_volpt = new VolatilitySmilePoint(d10p_volatility, 10, DeltaType.SpotBp, DeltaOrientation.Put);
                    else if (d10p_mdEntry_bid.DeltaType == DeltaTypeEnum.Forward && d10p_mdEntry_offer.DeltaType == DeltaTypeEnum.Forward)
                        d10p_volpt = new VolatilitySmilePoint(d10p_volatility, 10, DeltaType.ForwardBp, DeltaOrientation.Put);
                }

                var tenor = new Tenor(tnr);
                var volSmile = new VolatilitySmile(tenor, new VolatilitySmilePoint[] { atm_volpt, d25c_volpt, d25p_volpt, d10c_volpt, d10p_volpt });
                tenors_volSmiles.Add(tnr, volSmile);
            }

            double? secRefSpot = null;
            volSurface = null;
            failureReason = string.Empty;

            bool success = false;

            #region refSpotsRWLock Read

            if (_refSpotsRWLock.TryEnterReadLock(_refSpotsRWLockTimeout))
            {
                if (_refSpotQuotes != null)
                {
                    var refSpotEntry = _refSpotQuotes.quotes.Where(q => q.ccy_pair.ToUpper() == secSym.ToUpper()).Select(q => q).FirstOrDefault();
                    var refSpotUtcAsTs = refSpotEntry.ts.ToUniversalTime();

                    var refSpotVolSurfTsDiffInMinutes = Math.Abs(refSpotUtcAsTs.Subtract(utcAsOfTs).TotalMinutes);

                    _log.InfoFormat("Vol Surface UTC Timestamp: {0}, Ref Spot UTC Timestamp: {1}",
                        refSpotUtcAsTs, utcAsOfTs, refSpotVolSurfTsDiffInMinutes);

                    if (refSpotVolSurfTsDiffInMinutes <= _refSpotsTsToleranceInMinutes)
                    {
                        secRefSpot = refSpotEntry.mid;
                        volSurface = new VolatilitySurface(secSym, utcAsOfTs, tenors_volSmiles.Select(vs => vs.Value).ToList(),
                            source: KafkaVolatilityFeedSource.citi.ToString(), refDate: utcAsOfTs.Date, refSpot: secRefSpot);
                        success = true;
                    }
                    else
                    {
                        _log.WarnFormat("Ref Spot and Vol Surface differ by {0} mins, exceeding tolerance {1} mins.",
                            refSpotVolSurfTsDiffInMinutes, _refSpotsTsToleranceInMinutes);
                        success = false;
                        failureReason = "Timestamp gap between ref spot and vol surface is larger than tolerance.";
                    }
                }
                else
                {
                    success = false;
                    failureReason = "No ref spot quote available.";
                }
                _refSpotsRWLock.ExitReadLock();
            }

            #endregion refSpotsRWLock Read

            return success;
        }

        private static double AverageScaleBidOfferVolatility(double bidVolatility, double offerVolatility)
        {
            return Math.Round(0.5 * (bidVolatility + offerVolatility) * 100, 4);
        }
    }

    class MDSnapshot
    {
        public string MDReportID { get; set; }
        public string MDReqID { get; set; }
        public string SecuritySymbol { get; set; }
        public IEnumerable<MDQuoteSet> QuoteSets { get; set; }
    }

    class MDQuoteSet
    {
        public string QuoteSetID { get; set; }
        public string SecurityID { get; set; }
        public TargetStrategyEnum TargetStrategy { get; set; }
        public CallOrPutEnum? CallOrPut { get; set; }
        public int ExpiryTimeCode { get; set; }
        public string PremiumCurrency { get; set; }
        public IEnumerable<MDEntry> MDEntries { get; set; }
    }

    class MDEntry
    {
        public MDEntryTypeEnum MDEntryType { get; set; }
        public string MDEntryID { get; set; }
        public decimal MDEntryPx { get; set; }
        public DateTime? LegMaturityDate { get; set; }
        public string TenorValue { get; set; }
        public decimal? StrikePrice { get; set; }
        public DeltaBasisEnum? DeltaBasis { get; set; }
        public DeltaTypeEnum? DeltaType { get; set; }
        public string DeltaCcy { get; set; }
        public DeltaCodeEnum DeltaCode { get; set; }
    }

    class MDSnapshotEventArgs : EventArgs
    {
        public DateTime UTCTimestamp { get; set; }
        public string SecuritySymbol { get; set; }
        public string MDReqID { get; set; }
        public MDSnapshot MDSnapshot { get; set; }
    }

    enum TargetStrategyEnum
    {
        Vanilla = 1,
        Straddle = 6,
        Strangle = 7,
        Risk_Reversal = 8,
        Butterfly = 9
    }

    enum CallOrPutEnum
    {
        Call = 1,
        Put = 2,
        Call_over_Put = 3,
        Put_over_Call = 4
    }

    enum MDEntryTypeEnum
    {
        Bid = '0',
        Offer = '1',
        Mid = 'H'
    }

    enum DeltaBasisEnum
    {
        Smile = 1,
        Market = 2
    }

    enum DeltaTypeEnum
    {
        Spot = 1,
        Forward = 2
    }

    enum DeltaCodeEnum
    {
        DN = 0,
        ATM = 1,
        D10 = 2,
        D25 = 3
    }
}
