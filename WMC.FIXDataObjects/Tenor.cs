﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
    public class Tenor
    {
        private static readonly int _days_in_a_week = 7;
        private static readonly int _days_in_a_month = 30;
        private static readonly int _days_in_a_year = 360;

        private static readonly string _tenorCodeExplanation = "Tenor codes are integer numbers followed by D - Day, W - Week, M - Month, Y - Year";
        
        protected int Numeral { get; private set; }
        protected string Period { get; private set; }

        public string TenorCode { get; private set; }

        public Tenor(string tenorCode)
        {
            TenorCode = tenorCode.ToUpper();
            if (TenorCode == "ON")
            {
                Numeral = 1;
                Period = "D";
            }
            else
            {
                var num = TenorCode.Substring(0, TenorCode.Length - 1);
                var period = TenorCode.Substring(TenorCode.Length - 1);

                if (period == "D" || period == "W" || period == "M" || period == "Y")
                {
                    Period = period;
                    var parsedNum = 0;
                    if (int.TryParse(num, out parsedNum))
                        Numeral = parsedNum;
                    else
                        throw new InvalidOperationException(_tenorCodeExplanation);
                }
                else
                    throw new InvalidOperationException(_tenorCodeExplanation);

            }
        }

        private int ResolveToIndicativeDays()
        {
            int daysInPeriod = 0;

            if (Period == "D")
                daysInPeriod = 1;
            else if (Period == "W")
                daysInPeriod = _days_in_a_week;
            else if (Period == "M")
                daysInPeriod = _days_in_a_month;
            else if (Period == "Y")
                daysInPeriod = _days_in_a_year;

            return Numeral * daysInPeriod;
        }

        public static Tenor Parse(string t)
        {
            return new Tenor(t);
        }

        public static bool operator == (Tenor t1, Tenor t2)
        {
            bool result = false;

            if (t1.TenorCode == t2.TenorCode)
                result = true;

            return result;
        }

        public static bool operator != (Tenor t1, Tenor t2)
        {
            bool result = false;

            if (t1.TenorCode != t2.TenorCode)
                result = true;

            return result;
        }

        public static bool operator < (Tenor t1, Tenor t2)
        {
            bool result = false;

            if (t1.ResolveToIndicativeDays() < t2.ResolveToIndicativeDays())
                result = true;

            return result;
        }

        public static bool operator > (Tenor t1, Tenor t2)
        {
            bool result = false;

            if (t1.ResolveToIndicativeDays() > t2.ResolveToIndicativeDays())
                result = true;

            return result;
        }

        public static bool operator <= (Tenor t1, Tenor t2)
        {
            bool result = false;

            if (t1.ResolveToIndicativeDays() <= t2.ResolveToIndicativeDays())
                result = true;

            return result;
        }

        public static bool operator >= (Tenor t1, Tenor t2)
        {
            bool result = false;

            if (t1.ResolveToIndicativeDays() >= t2.ResolveToIndicativeDays())
                result = true;

            return result;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Tenor))
                return false;

            return ToString() == obj.ToString();
        }

        public override int GetHashCode()
        {
            return TenorCode.GetHashCode();
        }

        public override string ToString()
        {
            return TenorCode;
        }
    }
}
