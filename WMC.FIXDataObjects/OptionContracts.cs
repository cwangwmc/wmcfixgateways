﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
 
    public enum ExoticOptionType
    {
    }

    public enum VanillaOptionStrategy
    {
        SingleVanilla,
        Straddle,
        Strangle,
        Butterfly,
        RiskReversal,
        MultiLegSpread
    }

    public enum OptionCallPut
    {
        Call,
        Put
    }

    public enum OptionStrikeType
    {
        Outright,
        DeltaSpot,
        DeltaForward,
        AtmSpot,
        AtmForward,
        DeltaNeutral
    }

    public enum OptionPremiumDeliveryType
    {
        Spot,
        Forward
    }

    public enum HedgeTradeType
    {
        LIVE,
        Spot,
        Forward
    }

    public class VanillaOptionContract : Contract
    {
        public OptionPremiumDeliveryType OptionPremiumDeliveryType { get; protected set; }
        public HedgeTradeType HedgeTradeType { get; protected set; }
        public string OptionPremiumCurrency { get; protected set; }
        public string OptionNotionalCurrency { get; protected set; }

        public VanillaOptionContract(OptionPremiumDeliveryType optionPremiumDeliveryType, HedgeTradeType hedgeTradeType,
            string optionPremiumCurrency, string optionNotionalCurrency)
        {
            OptionPremiumDeliveryType = optionPremiumDeliveryType;
            HedgeTradeType = hedgeTradeType;
            OptionPremiumCurrency = optionPremiumCurrency;
            OptionNotionalCurrency = optionNotionalCurrency;
        }
    }
    
    public class SingleVanillaOptionContract : VanillaOptionContract
    {
        public string Tenor { get; private set; }
        public DateTime? MaturityDate { get; private set; }
        public double? Strike { get; private set; }
        public OptionStrikeType StrikeType { get; private set; }
        public OptionCallPut CallPut { get; private set; }
        public double Notional { get; private set; }

        public SingleVanillaOptionContract(string tenor, DateTime? maturityDate, double? strike, OptionStrikeType strikeType, OptionCallPut callPut, BuySell side,
            string optionPremiumCurrency, string optionNotionalCurrency,
            double notional = 1_000_000, OptionPremiumDeliveryType optionPremiumDeliveryType = OptionPremiumDeliveryType.Spot,
            HedgeTradeType hedgeTradeType = HedgeTradeType.Spot)
            : base(optionPremiumDeliveryType, hedgeTradeType, optionPremiumCurrency, optionNotionalCurrency)
        {
            Tenor = tenor;
            MaturityDate = maturityDate;
            Strike = strike;
            StrikeType = strikeType;
            CallPut = callPut;
            Side = side;
            Notional = notional;
        }
    }

    public class MultiLegVanillaOptionsContract : VanillaOptionContract
    {
        public VanillaOptionStrategy VanillaOptionStrategy { get; private set; }
        public IEnumerable<SingleVanillaOptionContract> VanillaOptionLegs { get; private set; }

        public MultiLegVanillaOptionsContract(VanillaOptionStrategy vanillaOptionStrategy, IEnumerable<SingleVanillaOptionContract> vanillaOptionsLegs,
            string optionPremiumCurrency, string optionNotionalCurrency,
            OptionPremiumDeliveryType optionPremiumDeliveryType = OptionPremiumDeliveryType.Spot, HedgeTradeType hedgeTradeType = HedgeTradeType.LIVE)
            : base(optionPremiumDeliveryType, hedgeTradeType, optionPremiumCurrency, optionNotionalCurrency)
        {
            VanillaOptionStrategy = vanillaOptionStrategy;
            VanillaOptionLegs = vanillaOptionsLegs;
        }
    }


}
