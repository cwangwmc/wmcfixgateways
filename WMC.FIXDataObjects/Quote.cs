﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
    public abstract class Quote
    {
        public string SecuritySymbol { get; protected set; }
        public string QuoteRequestRefId { get; protected set; }
        public string QuoteId { get; protected set; }

        public Quote(string securitySymbol, string quoteRequestRefId, string quoteId)
        {
            SecuritySymbol = securitySymbol;
            QuoteRequestRefId = quoteRequestRefId;
            QuoteId = quoteId;
        }
    }
}
