﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace WMC.FIXDataObjects
{

    public enum Atm
    {
        Dn = 1,
        Atmf = 2
    }

    public enum DeltaOrientation
    {
        Call = 1,
        Put = 2
    }

    public enum DeltaType
    {
        SpotBp = 1,
        SpotPc = 2,
        ForwardBp = 3,
        ForwardPc = 4
    }

    public class DeltaDetails
    {
        public int Delta { get; private set; }
        public DeltaType DeltaType { get; private set; }
        public DeltaOrientation DeltaOrientation { get; private set; }

        public DeltaDetails(int delta, DeltaType deltaType, DeltaOrientation deltaOrientation)
        {
            Delta = delta;
            DeltaType = deltaType;
            DeltaOrientation = deltaOrientation;
        }
    }

    public class VolatilitySmilePoint
    {
        public double Vol { get; set; }

        public bool IsAtm { get { return AtmType != null; }}

        public Atm? AtmType { get; private set; }

        public DeltaDetails DeltaDetails { get; private set; }

        public VolatilitySmilePoint(double volInPercentage, Atm atmType)
        {
            Vol = volInPercentage;
            AtmType = atmType;
        }

        public VolatilitySmilePoint(double volInPercentage, DeltaDetails deltaDetails)
        {
            Vol = volInPercentage;
            DeltaDetails = deltaDetails;
        }

        public VolatilitySmilePoint(double volInPercentage, int delta, DeltaType deltaType, DeltaOrientation deltaOrientation)
        {
            Vol = volInPercentage;
            DeltaDetails = new DeltaDetails(delta, deltaType, deltaOrientation);
        }
    }

    public class VolatilitySmile
    {
        public string Tenor { get; private set; }
        public IEnumerable<VolatilitySmilePoint> VolSmilePoints { get; private set; }

        public VolatilitySmile(Tenor tenor)
        {
            Tenor = tenor.TenorCode;
            VolSmilePoints = new List<VolatilitySmilePoint>();
        }

        public VolatilitySmile(string tenor)
        {
            Tenor = tenor.ToUpper();
            VolSmilePoints = new List<VolatilitySmilePoint>();
        }

        public VolatilitySmile(Tenor tenor, IEnumerable<VolatilitySmilePoint> volSmilePoints)
        {
            Tenor = tenor.TenorCode;
            VolSmilePoints = volSmilePoints;
        }

        public VolatilitySmile(string tenor, IEnumerable<VolatilitySmilePoint> volSmilePoints)
        {
            Tenor = tenor.ToUpper();
            VolSmilePoints = volSmilePoints;
        }

        public bool HasAtmVolPoint()
        {
            return VolSmilePoints.Any(vpt => vpt.IsAtm);
        }

        public bool HasCallPutWingDeltas(IEnumerable<int> deltas)
        {
            bool hasAllWingDelta = true;

            foreach (var d in deltas)
            {
                hasAllWingDelta = hasAllWingDelta 
                    && HasWingVolPoint(d, DeltaOrientation.Call) 
                    && HasWingVolPoint(d, DeltaOrientation.Put);
            }

            return hasAllWingDelta;
        }

        public bool HasWingVolPoint(int delta, DeltaOrientation callPut)
        {
            return VolSmilePoints.Where(vpt => !vpt.IsAtm).Any(vpt => vpt.DeltaDetails.Delta == delta && vpt.DeltaDetails.DeltaOrientation == callPut);
        }

        public bool AddAtmVolPoint(double volInPercentage, Atm atmType)
        {
            if (HasAtmVolPoint())
                return false;

            var volSmilePtsList = VolSmilePoints.ToList();
            volSmilePtsList.Add(new VolatilitySmilePoint(volInPercentage, atmType));
            VolSmilePoints = volSmilePtsList;

            return true;
        }

        public bool AddWingVolPoint(double volInPercentage, DeltaDetails wingDeltaDetails)
        {
            if (HasWingVolPoint(wingDeltaDetails.Delta, wingDeltaDetails.DeltaOrientation))
                return false;

            var volSmilePtsList = VolSmilePoints.ToList();
            volSmilePtsList.Add(new VolatilitySmilePoint(volInPercentage, wingDeltaDetails.Delta,
                wingDeltaDetails.DeltaType, wingDeltaDetails.DeltaOrientation));

            VolSmilePoints = volSmilePtsList;

            return true;
        }

        public IEnumerable<VolatilitySmilePoint> GetVolSmilePointsForDelta(int delta)
        {
            var volSmilePtsForDelta = VolSmilePoints.Where(vpt => !vpt.IsAtm).Where(vpt => vpt.DeltaDetails.Delta == delta).Select(vpt => vpt);

            return volSmilePtsForDelta;
        }

        public IEnumerable<VolatilitySmilePoint> GetVolSmilePointsForCallOrPut(DeltaOrientation callPut)
        {
            var volSmilePtsForCallPut = VolSmilePoints.Where(vpt => !vpt.IsAtm)
                .Where(vpt => vpt.DeltaDetails.DeltaOrientation == callPut).Select(vpt => vpt);

            return volSmilePtsForCallPut;
        }
        
        public VolatilitySmilePoint GetVolSmileAtmPoint()
        {
            var volSmileAtm = VolSmilePoints.Where(vpt => vpt.IsAtm).Select(vpt => vpt).FirstOrDefault();

            return volSmileAtm;
        }

        public VolatilitySmilePoint GetVolSmilePoint(int delta, DeltaOrientation callOrPut)
        {
            var volSmilePt = VolSmilePoints.Where(vpt => !vpt.IsAtm)
                .Where(vpt => vpt.DeltaDetails.Delta == delta && vpt.DeltaDetails.DeltaOrientation == callOrPut)
                .Select(vpt => vpt).FirstOrDefault();

            return volSmilePt;
        }
    }

    public class VolatilitySurface
    {
        public string Security { get; private set; }
        public DateTime AsOf { get; private set; }
        public Dictionary<string, VolatilitySmile> VolSmiles { get; private set; }

        public string Source { get; private set; }
        public double? ReferenceSpot { get; private set; }
        public DateTime? ReferenceDate { get; private set; }

        public VolatilitySurface(string security, DateTime asOf, string source = "", DateTime? refDate = null, double? refSpot = null)
        {
            Security = security;
            AsOf = asOf;
            Source = source;
            ReferenceSpot = refSpot;
            ReferenceDate = refDate;

            VolSmiles = new Dictionary<string, VolatilitySmile>();
        }

        public VolatilitySurface(string security, DateTime asOf, Dictionary<string, VolatilitySmile> volSmilesDict,
            string source = "", DateTime? refDate = null, double? refSpot = null)
            :this(security, asOf, source, refDate, refSpot)
        {
            VolSmiles = volSmilesDict;
        }

        public VolatilitySurface(string security, DateTime asOf, IList<VolatilitySmile> volSmiles,
            string source = "", DateTime? refDate = null, double? refSpot = null)
            :this(security, asOf, source, refDate, refSpot)
        {
            foreach (var vs in volSmiles)
                VolSmiles.Add(vs.Tenor, vs);
        }

        public bool HasAtmVolPoints(IEnumerable<Tenor> tenors)
        {
            if (tenors.Count() == 0)
                return false;

            var hasAllTenors = tenors.All(tnr => VolSmiles.Keys.Contains(tnr.TenorCode));

            if (!hasAllTenors)
                return false;

            return tenors.All(tnr => VolSmiles[tnr.TenorCode].HasAtmVolPoint());
        }

        public bool HasWingDeltas(IEnumerable<Tenor> tenors, IEnumerable<int> deltas)
        {
            if (tenors.Count() == 0 || deltas.Count() == 0)
                return false;

            var hasAllTenors = tenors.All(tnr => VolSmiles.Keys.Contains(tnr.TenorCode));

            if (!hasAllTenors)
                return false;

            return tenors.Select(tnr => VolSmiles[tnr.TenorCode]).All(vs => vs.HasCallPutWingDeltas(deltas));
        }

        public bool AddAtmVolPointToTenor(Tenor tenor, double volInPercentage, Atm atmType)
        {
            if (HasAtmVolPoints(new List<Tenor>() { tenor }))
                return false;

            VolatilitySmile volSmileForTenor = null;

            if (VolSmiles.TryGetValue(tenor.TenorCode, out volSmileForTenor))
            {
                volSmileForTenor.AddAtmVolPoint(volInPercentage, atmType);
                VolSmiles[tenor.TenorCode] = volSmileForTenor;
            }
            else
            {
                volSmileForTenor = new VolatilitySmile(tenor, new List<VolatilitySmilePoint> { new VolatilitySmilePoint(volInPercentage, atmType) });
                VolSmiles[tenor.TenorCode] = volSmileForTenor;
            }

            AsOf = DateTime.UtcNow;

            return true;
        }

        public bool AddWingVolPointToTenor(Tenor tenor, double volInPercentage, DeltaDetails wingDeltaDetails)
        {

            VolatilitySmile volSmileForTenor = null;

            if (VolSmiles.TryGetValue(tenor.TenorCode, out volSmileForTenor))
            {
                if (volSmileForTenor.HasWingVolPoint(wingDeltaDetails.Delta, wingDeltaDetails.DeltaOrientation))
                    return false;

                volSmileForTenor.AddWingVolPoint(volInPercentage, wingDeltaDetails);
                VolSmiles[tenor.TenorCode] = volSmileForTenor;
            }
            else
            {
                volSmileForTenor = new VolatilitySmile(tenor, new List<VolatilitySmilePoint> { new VolatilitySmilePoint(volInPercentage, 
                    wingDeltaDetails.Delta, wingDeltaDetails.DeltaType, wingDeltaDetails.DeltaOrientation) });
                VolSmiles[tenor.TenorCode] = volSmileForTenor;
            }

            AsOf = DateTime.UtcNow;

            return true;
        }

        public bool AddVolSmilePoint(Tenor tenor, VolatilitySmilePoint volSmilePoint)
        {
            if (volSmilePoint.IsAtm)
                return AddAtmVolPointToTenor(tenor, volSmilePoint.Vol, volSmilePoint.AtmType.Value);
            else
                return AddWingVolPointToTenor(tenor, volSmilePoint.Vol, volSmilePoint.DeltaDetails);
        }

        public IEnumerable<Tenor> GetVolSmileTenors()
        {
            var sortedAscendingTenors = VolSmiles.Keys.Select(t => new Tenor(t)).OrderBy(tnr => tnr);

            return sortedAscendingTenors;
        }

        public VolatilitySmile GetVolatilitySmile(Tenor tenor)
        {
            VolatilitySmile volSmile = null;

            VolSmiles.TryGetValue(tenor.TenorCode, out volSmile);

            return volSmile;
        }

        public IEnumerable<VolatilitySmilePoint> GetAllAtmVolPoints()
        {
            var volSmileAtms = VolSmiles.Values.OrderBy(vs => new Tenor(vs.Tenor)).Where(vs => vs.HasAtmVolPoint())
                .Select(vs => vs.GetVolSmileAtmPoint());

            return volSmileAtms;
        }

        public IEnumerable<VolatilitySmilePoint> GetAtmVolSmilePointsForTenors(IEnumerable<Tenor> tenors)
        {
            var volSmilesAtmsForTenors = new List<VolatilitySmilePoint>();

            foreach (var t in tenors)
            {
                VolatilitySmile vs = null;
                if (VolSmiles.TryGetValue(t.TenorCode, out vs))
                    if (vs.HasAtmVolPoint())
                        volSmilesAtmsForTenors.Add(vs.GetVolSmileAtmPoint());
            }

            return volSmilesAtmsForTenors;
        }

        public IEnumerable<VolatilitySmilePoint> GetWingVolSmilePoints(IEnumerable<Tenor> tenors, int delta, DeltaOrientation callPut)
        {
            var volSmilesWingsForTenors = new List<VolatilitySmilePoint>();

            foreach (var t in tenors)
            {
                VolatilitySmile vs = null;
                if (VolSmiles.TryGetValue(t.TenorCode, out vs))
                    if (vs.HasWingVolPoint(delta, callPut))
                        volSmilesWingsForTenors.Add(vs.GetVolSmilePoint(delta, callPut));
            }

            return volSmilesWingsForTenors;
        }
    }
}
