﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{

    public class SpotQuotes
    {
        public DateTime time { get; set; }
        public string src { get; set; }
        public IEnumerable<SpotEntry> quotes { get; set; }
    }

    public class SpotEntry
    {
        public string ccy_pair { get; set; }
        public double bid { get; set; }
        public double ask { get; set; }
        public double mid { get; set; }
        public DateTime ts { get; set; }
    }

}
