﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
    public class SingleVanillaOptionQuote : Quote
    {

        public double TotalBidPremium { get; private set; }
        public double TotalOfferPremium { get; private set; }
        
        public double BidVolatility { get; private set; }
        public double OfferVolatility { get; private set; }

        public string ExpiryCutoff { get; protected set; }

        public SingleVanilaOptionContractPriceDetails OptionPriceDetails { get; protected set; }

        public SingleVanillaOptionQuote(string securitySymbol, string quoteRequestRefId, string quoteId,
            SingleVanilaOptionContractPriceDetails optionPriceDetails)
            : base(securitySymbol, quoteRequestRefId, quoteId)
        {

            SecuritySymbol = securitySymbol;
            QuoteRequestRefId = quoteRequestRefId;
            QuoteId = quoteId;

            TotalBidPremium = optionPriceDetails.BidPx;
            TotalOfferPremium = optionPriceDetails.OfferPx;

            BidVolatility = optionPriceDetails.BidVolatility;
            OfferVolatility = optionPriceDetails.OfferVolatility;

            ExpiryCutoff = optionPriceDetails.ExpiryCutoff;

            OptionPriceDetails = optionPriceDetails;

        }

    }

    public class MultiLegVanillaOptionsQuote : Quote
    {

        public double TotalBidPremium { get; protected set; }
        public double TotalOfferPremium { get; protected set; }

        public string ExpiryCutoff { get; protected set; }

        public MultiLegVanillaOptionsContractPriceDetails OptionPriceDetails { get; private set; }

        public MultiLegVanillaOptionsQuote(string securitySymbol, string quoteRequestRefId, string quoteId,
            double bidPx, double offerPx,
            MultiLegVanillaOptionsContractPriceDetails optionPriceDetails)
            : base (securitySymbol, quoteRequestRefId, quoteId)
        {
        }

    }

    public class SingleVanilaOptionContractPriceDetails : ContractPriceDetails
    {

        public string ExpiryCutoff { get; set; }
        public string SecuritySymbol { get; set; }
        public string OptionPremiumCurrency { get; set; }
        public string OptionNotionalCurrency { get; set; }

        public OptionCallPut CallPut { get; set; }
        public DateTime MaturityDate { get; set; }
        public DateTime SettlementDate { get; set; }
        public double StrikePrice { get; set; }
        public double Notional { get; set; }

        public double BidVolatility { get; set; }
        public double OfferVolatility { get; set; }

        public SingleVanilaOptionContractPriceDetails() { }

        public SingleVanilaOptionContractPriceDetails(double bidPx, double offerPx,
            string cutoff, string securitySymbol, string premCcy, string notionalCcy,
            OptionCallPut callPut, DateTime maturityDate, DateTime settlementDate, double strikePx, BuySell side, double notional,
            double bidVol, double offerVol)
            : base()
        {

            BidPx = bidPx;
            OfferPx = offerPx;

            ExpiryCutoff = cutoff;
            SecuritySymbol = securitySymbol;
            OptionPremiumCurrency = premCcy;
            OptionNotionalCurrency = notionalCcy;

            Notional = notional;
            StrikePrice = strikePx;
            CallPut = callPut;
            MaturityDate = maturityDate;
            SettlementDate = settlementDate;

            Side = side;

            BidVolatility = bidVol;
            OfferVolatility = offerVol;

        }

    }

    public class MultiLegVanillaOptionsContractPriceDetails: ContractPriceDetails
    {
        public string ExpiryCutoff { get; private set; }
        public string OptionPremiumCurrency { get; private set; }
        public string OptionNotionalCurrency { get; private set; }

        public IEnumerable<SingleVanilaOptionContractPriceDetails> MultiLegVanillaOptions;

        public MultiLegVanillaOptionsContractPriceDetails(double bidPx, double offerPx,
            string cutoff, string premiumCcy, string notionalCcy,
            IEnumerable<SingleVanilaOptionContractPriceDetails> multiLegVanillaOptions)
        {
            BidPx = bidPx;
            OfferPx = offerPx;

            ExpiryCutoff = cutoff;
            OptionPremiumCurrency = premiumCcy;
            OptionNotionalCurrency = notionalCcy;

            MultiLegVanillaOptions = multiLegVanillaOptions;
        }
        
    }
    
    public class DeltaHedgeContractPriceDetails: ContractPriceDetails
    {

        public string HedgeCurrency { get; set; }
        public double HedgeFxRate { get; set; }
        public double HedgeAmount { get; set; }

        public DeltaHedgeContractPriceDetails()
        {
        }

        public DeltaHedgeContractPriceDetails(string hedgeCcy, double hedgeFxRate, double hedgeAmt)
        {

        }

    }

}
