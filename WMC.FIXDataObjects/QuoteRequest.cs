﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
    public abstract class QuoteRequest
    {
        public string QuoteRequestRefId { get; protected set; }
        public string SecuritySymbol { get; protected set; }

        public QuoteRequest(string quoteRequestRefId, string securitySymbol)
        {
            QuoteRequestRefId = quoteRequestRefId;
            SecuritySymbol = securitySymbol;
        }
    }
}
