﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
    public abstract class VanillaOptionsQuoteRequest : QuoteRequest
    {
        public string OptionPremiumCurrency { get; private set; }
        public string OptionNotionalCurrency { get; private set; }
        public VanillaOptionContract VanillaOptionContract { get; protected set; }

        public VanillaOptionsQuoteRequest(string quoteRequestRefId, string securitySymbol, string premCcy, string notionalCcy,
            VanillaOptionContract vanillaOptionContract) : base (securitySymbol, quoteRequestRefId)
        {
            QuoteRequestRefId = quoteRequestRefId;
            SecuritySymbol = securitySymbol;
            OptionPremiumCurrency = premCcy;
            OptionNotionalCurrency = notionalCcy;

            VanillaOptionContract = vanillaOptionContract;
        }
    }

    public class SingleVanillaOptionQuoteRequest : VanillaOptionsQuoteRequest
    {

        public SingleVanillaOptionQuoteRequest(string quoteRequestRefId, string securitySymbol, string premCcy, string notionalCcy,
            SingleVanillaOptionContract singleVanillaOption)
            : base(quoteRequestRefId, securitySymbol, premCcy, notionalCcy, singleVanillaOption)
        {
        }

    }

    public class MultiVanillaOptionsStrategyQuoteRequest : VanillaOptionsQuoteRequest
    {

        public MultiVanillaOptionsStrategyQuoteRequest(string quoteRequestRefId, string securitySymbol, string premCcy, string notionalCcy,
            MultiLegVanillaOptionsContract multiLegVanillaOptionsStrategy)
            : base(quoteRequestRefId, securitySymbol, premCcy, notionalCcy, multiLegVanillaOptionsStrategy)
        {
        }

    }
}
