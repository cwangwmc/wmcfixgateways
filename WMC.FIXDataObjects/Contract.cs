﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
    public abstract class Contract
    {
        public BuySell Side { get; protected set; }
    }

    public abstract class ContractPriceDetails
    {
        public BuySell Side { get; set; }

        public double BidPx { get; set; }
        public double OfferPx { get; set; }
    }

    public enum BuySell
    {
        Buy,
        Sell
    }
}
