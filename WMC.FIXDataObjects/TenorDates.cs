﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
    public class TenorDates
    {

        public DateTime ReferenceDate { get; private set; }

        public DateTime AsOf { get; private set; }

        public string Source { get; private set; }

        public string CurrencyPair { get; private set; }

        public Dictionary<string, DateTime?> TenorMaturityDates { get; private set; }

        public TenorDates(DateTime refDate, string source, string ccypair)
        {
            ReferenceDate = refDate;
            Source = source;
            CurrencyPair = ccypair.ToUpper();

            AsOf = DateTime.UtcNow;
            TenorMaturityDates = new Dictionary<string, DateTime?>();
        }

        public TenorDates(DateTime refDate, DateTime asof, string source, string ccypair, Dictionary<string, DateTime?> tenorMaturityDates)
        {
            ReferenceDate = refDate;
            AsOf = asof;
            Source = source;
            CurrencyPair = ccypair.ToUpper();
            TenorMaturityDates = tenorMaturityDates;
        }

        public bool AddTenorDate(string tenor, DateTime maturityDate)
        {

            if (TenorMaturityDates.ContainsKey(tenor))
                return false;
            else
            {
                TenorMaturityDates.Add(tenor, maturityDate);
                AsOf = DateTime.UtcNow;
                return true;
            }

        }

        public bool TryGetTenorDate(string tenor, out DateTime? maturityDate)
        {
            return TenorMaturityDates.TryGetValue(tenor, out maturityDate);
        }

        public bool HasAllTenorDates(IEnumerable<string> volSurfTenors)
        {
            var hasAll = true;

            foreach (var volSurfTenor in volSurfTenors)
            {
                hasAll = hasAll && TenorMaturityDates.ContainsKey(volSurfTenor);
            }

            return hasAll;
        }
        
    }
}
