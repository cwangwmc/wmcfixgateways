﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXDataObjects
{
    
    public class SpotsForwardsQuoteRequest : QuoteRequest
    {
        public SpotsForwardsQuoteRequest(string quoteRequestRefId, string securitySymbol)
            :base(quoteRequestRefId, securitySymbol)
        {

        }
    }
    
}
