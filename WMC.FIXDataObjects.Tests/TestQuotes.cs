﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WMC.FIXDataObjects.Tests
{
    [TestFixture]
    public class TestQuotes
    {
        private static readonly string eurusd_sec_sym = "EURUSD";
        private static readonly string usdjpy_sec_sym = "USDJPY";
        private static readonly string usdtry_sec_sym = "USDTRY";

        private static readonly string quoteRequestRefIdFormat = "QR_RefId_{0}_ABCxyz123";

        private static readonly string quoteIdFormat = "Q_Id_{0}_987abcXYZ";

        [Test]
        public void TestUuid()
        {
            var uuid = Guid.NewGuid();

            Console.WriteLine(uuid);
        }

        [Test]
        public void TestSingleVanillaOptionContractPriceDetailsJsonEncoding()
        {

            var premCcy = eurusd_sec_sym.Substring(0, 3);
            var notionalCcy = eurusd_sec_sym.Substring(0, 3);

            var sglVanOptPxDtls = new SingleVanilaOptionContractPriceDetails(9020.00, 9220.00, "NY", eurusd_sec_sym, premCcy, notionalCcy, OptionCallPut.Call,
                new DateTime(2018, 10, 4), new DateTime(2018, 10, 9), 1.159, BuySell.Buy, 1_000_000.00, 7.77, 7.93);

            var sglVanOptPxDtlsJson = JsonConvert.SerializeObject(sglVanOptPxDtls, new StringEnumConverter());

            Assert.IsNotEmpty(sglVanOptPxDtlsJson);

            /*
            {"ExpiryCutoff":"NY","SecuritySymbol":"EURUSD","OptionPremiumCurrency":"EUR","OptionNotionalCurrency":"EUR","CallPut":"Call",
            "MaturityDate":"2018-10-04T00:00:00","SettlementDate":"2018-10-09T00:00:00","StrikePrice":1.159,"Notional":1000000.0,
            "BidVolatility":7.77,"OfferVolatility":7.93,"Side":"Buy","BidPx":9020.0,"OfferPx":9220.0}
            */

            Assert.AreEqual(sglVanOptPxDtlsJson, 
                "{\"ExpiryCutoff\":\"NY\",\"SecuritySymbol\":\"EURUSD\",\"OptionPremiumCurrency\":\"EUR\",\"OptionNotionalCurrency\":\"EUR\",\"CallPut\":\"Call\"," +
                "\"MaturityDate\":\"2018-10-04T00:00:00\",\"SettlementDate\":\"2018-10-09T00:00:00\",\"StrikePrice\":1.159,\"Notional\":1000000.0," +
                "\"BidVolatility\":7.77,\"OfferVolatility\":7.93,\"Side\":\"Buy\",\"BidPx\":9020.0,\"OfferPx\":9220.0}");
        }

        [Test]
        public void TestSingleVanillaOptionQuoteJsonEncoding()
        {

            var sglVanOptPxDtls = new SingleVanilaOptionContractPriceDetails(9020.00, 9220.00, "NY", eurusd_sec_sym, "EUR", "EUR", OptionCallPut.Call,
                new DateTime(2018, 10, 4), new DateTime(2018, 10, 9), 1.159, BuySell.Buy, 1_000_000.00, 7.77, 7.93);

            var utcNowPlus10mins = DateTime.UtcNow.AddMinutes(10.0).ToString("yyyy-MM-ddThh:mm:ss");

            Console.WriteLine(utcNowPlus10mins);

            var sglVanOptQuote = new SingleVanillaOptionQuote(eurusd_sec_sym,
                string.Format(quoteRequestRefIdFormat, eurusd_sec_sym),
                string.Format(quoteIdFormat, eurusd_sec_sym),
                sglVanOptPxDtls);

            var sglVanOptQuoteJson = JsonConvert.SerializeObject(sglVanOptQuote, new StringEnumConverter());

            Assert.IsNotEmpty(sglVanOptQuoteJson);

            /*
            {"BidPremium":9020.0,"OfferPremium":9220.0,
            "BidVolatility":7.77,"OfferVolatility":7.93,
            "ExpiryCutoff":"NY",
            "OptionPriceDetails":
                {"ExpiryCutoff":"NY","OptionPremiumCurrency":"EUR","OptionNotionalCurrency":"EUR","CallPut":"Call",
                "MaturityDate":"2018-10-04T00:00:00","SettlementDate":"2018-10-09T00:00:00","StrikePrice":1.159,"Notional":1000000.0,
                "BidVolatility":7.77,"OfferVolatility":7.93,"Sdie":"Buy","BidPx":9020.0,"OfferPx":9220.0},
            "SecuritySymbol":"EURUSD",
            "QuoteRequestRefId":"QR_RefId_EURUSD_ABCxyz123",
            "QuoteId":"Q_Id_EURUSD_987abcXYZ"}
            */

            Assert.AreEqual(sglVanOptQuoteJson,
                "{\"TotalBidPremium\":9020.0,\"TotalOfferPremium\":9220.0," +
                "\"BidVolatility\":7.77,\"OfferVolatility\":7.93," +
                "\"ExpiryCutoff\":\"NY\"," +
                "\"OptionPriceDetails\":" +
                    "{\"ExpiryCutoff\":\"NY\",\"SecuritySymbol\":\"EURUSD\",\"OptionPremiumCurrency\":\"EUR\",\"OptionNotionalCurrency\":\"EUR\",\"CallPut\":\"Call\"," +
                    "\"MaturityDate\":\"2018-10-04T00:00:00\",\"SettlementDate\":\"2018-10-09T00:00:00\",\"StrikePrice\":1.159,\"Notional\":1000000.0," +
                    "\"BidVolatility\":7.77,\"OfferVolatility\":7.93,\"Side\":\"Buy\",\"BidPx\":9020.0,\"OfferPx\":9220.0}," +
                "\"SecuritySymbol\":\"EURUSD\"," +
                "\"QuoteRequestRefId\":\"QR_RefId_EURUSD_ABCxyz123\"," +
                "\"QuoteId\":\"Q_Id_EURUSD_987abcXYZ\"}");

        }

        [Test]
        public void TestSpotQuotesJsonDecoding()
        {
            var spotQuotesJson = "{\"time\":\"2018-10-11T19:09:58.603183\",\"src\":\"FENICS\"," +
                                 "\"quotes\": [{\"bid\": 0.7117, \"ask\": 0.7118, \"ts\": \"2018-10-11T14:09:54\", \"ccy_pair\": \"AUDUSD\", \"mid\": 0.71175}," +
                                 "{\"bid\": 1.1583, \"ask\": 1.1586, \"ts\": \"2018-10-11T14:09:54\", \"ccy_pair\": \"EURUSD\", \"mid\": 1.15845}]}";

            var spotQuotes = JsonConvert.DeserializeObject<SpotQuotes>(spotQuotesJson);

            Assert.IsNotNull(spotQuotes);
        }

        [Test]
        public void TestSpotQuotesJsonDecoding2()
        {
            var spotQuotesJson = "{\"time\":\"2018-10-11T19:09:58.603183\",\"src\":\"FENICS\"," +
                                 "\"quotes\": [{\"bid\": 0.7117, \"ask\": 0.7118, \"ts\": \"2018-10-11T14:09:54\", \"ccy_pair\": \"AUDUSD\", \"mid\": 0.71175}," +
                                 "{\"bid\": 1.1583, \"ask\": 1.1586, \"ts\": \"2018-10-11T14:09:54\", \"ccy_pair\": \"EURUSD\", \"mid\": 1.15845}]}";

            spotQuotesJson = "{\"time\": \"2018-10-11T20:58:48.948376\",\"src\":\"FENICS\"," +
                "\"quotes\": [{\"bid\": 0.7113, \"ask\": 0.7115, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"AUDUSD\", \"mid\": 0.7114}," +
                "{\"bid\": 1.1591, \"ask\": 1.1595, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"EURUSD\", \"mid\": 1.1593}," +
                "{\"bid\": 1.323, \"ask\": 1.3231, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"GBPUSD\", \"mid\": 1.32305}," +
                "{\"bid\": 0.6524, \"ask\": 0.6527, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"NZDUSD\", \"mid\": 0.65255}," +
                "{\"bid\": 3.7756, \"ask\": 3.7781, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDBRL\", \"mid\": 3.77685}," +
                "{\"bid\": 1.3034, \"ask\": 1.3038, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDCAD\", \"mid\": 1.3036}," +
                "{\"bid\": 0.9895, \"ask\": 0.9897, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDCHF\", \"mid\": 0.9896}," +
                "{\"bid\": 6.8731, \"ask\": 6.8747, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDCNH\", \"mid\": 6.8739}," +
                "{\"bid\": 6.888, \"ask\": 6.8918, \"ts\": \"2018-10-11T15:00:22\", \"ccy_pair\": \"USDCNY\", \"mid\": 6.8899}," +
                "{\"bid\": 280.27, \"ask\": 280.51, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDHUF\", \"mid\": 280.39}," +
                "{\"bid\": 74.04, \"ask\": 74.06, \"ts\": \"2018-10-11T15:00:22\", \"ccy_pair\": \"USDINR\", \"mid\": 74.05}," +
                "{\"bid\": 112.01, \"ask\": 112.04, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDJPY\", \"mid\": 112.025}," +
                "{\"bid\": 1135.58, \"ask\": 1137.58, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDKRW\", \"mid\": 1136.58}," +
                "{\"bid\": 19.014, \"ask\": 19.015, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDMXN\", \"mid\": 19.0145}," +
                "{\"bid\": 8.2179, \"ask\": 8.2209, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDNOK\", \"mid\": 8.2194}," +
                "{\"bid\": 3.7143, \"ask\": 3.7163, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDPLN\", \"mid\": 3.7153}," +
                "{\"bid\": 66.245, \"ask\": 66.275, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDRUB\", \"mid\": 66.26}," +
                "{\"bid\": 8.9784, \"ask\": 8.9814, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDSEK\", \"mid\": 8.9799}," +
                "{\"bid\": 1.3766, \"ask\": 1.3769, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDSGD\", \"mid\": 1.37675}," +
                "{\"bid\": 5.9319, \"ask\": 5.9494, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDTRY\", \"mid\": 5.94065}," +
                "{\"bid\": 14.6197, \"ask\": 14.6497, \"ts\": \"2018-10-11T15:58:44\", \"ccy_pair\": \"USDZAR\", \"mid\": 14.6347}," +
                "{\"ccy_pair\": \"GBPCAD\", \"mid\": 1.7247279800000002, \"bid\": 1.7243981999999998, \"ask\": 1.72505778, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURGBP\", \"mid\": 0.8762329466006575, \"bid\": 0.8760486735696471, \"ask\": 0.8764172335600907, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURNZD\", \"mid\": 1.7765688452992108, \"bid\": 1.7758541443235791, \"ask\": 1.77728387492336, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURNOK\", \"mid\": 9.52875042, \"bid\": 9.52536789, \"ask\": 9.53213355, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"NZDJPY\", \"mid\": 73.10191375, \"bid\": 73.075324, \"ask\": 73.128508, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURPLN\", \"mid\": 4.30714729, \"bid\": 4.30524513, \"ask\": 4.30904985, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURAUD\", \"mid\": 1.629603598538094, \"bid\": 1.629093464511595, \"ask\": 1.630113876001687, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"AUDCAD\", \"mid\": 0.9273810400000001, \"bid\": 0.9271084199999999, \"ask\": 0.9276537, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURMXN\", \"mid\": 22.043509850000003, \"bid\": 22.039127399999998, \"ask\": 22.0478925, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURHUF\", \"mid\": 325.056127, \"bid\": 324.860957, \"ask\": 325.25134499999996, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"CHFJPY\", \"mid\": 113.20230396119645, \"bid\": 113.17570981105386, \"ask\": 113.2289034866094, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"AUDNZD\", \"mid\": 1.0901846601792968, \"bid\": 1.0897809100658804, \"ask\": 1.090588595953403, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"NZDCAD\", \"mid\": 0.85066418, \"bid\": 0.85033816, \"ask\": 0.85099026, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"AUDJPY\", \"mid\": 79.694585, \"bid\": 79.672713, \"ask\": 79.71646000000001, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURCAD\", \"mid\": 1.51126348, \"bid\": 1.51077094, \"ask\": 1.5117561000000002, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURBRL\", \"mid\": 4.378502205, \"bid\": 4.37629796, \"ask\": 4.3807069499999995, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURCHF\", \"mid\": 1.14724328, \"bid\": 1.14692945, \"ask\": 1.14755715, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURZAR\", \"mid\": 16.96600771, \"bid\": 16.94569427, \"ask\": 16.986327149999997, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"CADJPY\", \"mid\": 85.93510279226757, \"bid\": 85.91041570793067, \"ask\": 85.95979745281572, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURSEK\", \"mid\": 10.410398070000001, \"bid\": 10.40686344, \"ask\": 10.4139333, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"EURJPY\", \"mid\": 129.8705825, \"bid\": 129.830791, \"ask\": 129.91038, \"ts\": \"2018-10-11T15:58:44\"}," +
                "{\"ccy_pair\": \"GBPJPY\", \"mid\": 148.21467625000002, \"bid\": 148.18923, \"ask\": 148.240124, \"ts\": \"2018-10-11T15:58:44\"}]}";

            var spotQuotes = JsonConvert.DeserializeObject<SpotQuotes>(spotQuotesJson);

            Assert.IsNotNull(spotQuotes);
        }

        [Test]
        public void TestSpotQuotesJsonEncoding()
        {
            var utcnow = DateTime.UtcNow;

            var spotEntry_audusd = new SpotEntry();
            spotEntry_audusd.ccy_pair = "AUDUSD";
            spotEntry_audusd.bid = 0.7117;
            spotEntry_audusd.ask = 0.7118;
            spotEntry_audusd.mid = 0.71175;
            spotEntry_audusd.ts = utcnow;

            var spotEntry_eurusd = new SpotEntry();
            spotEntry_eurusd.ccy_pair = "EURUSD";
            spotEntry_eurusd.bid = 1.1583;
            spotEntry_eurusd.ask = 1.1586;
            spotEntry_eurusd.mid = 1.15845;
            spotEntry_eurusd.ts = utcnow;

            var spotEntries = new SpotEntry[] { spotEntry_audusd, spotEntry_eurusd };

            var sq = new SpotQuotes();

            sq.quotes = spotEntries;
            sq.time = utcnow;
            sq.src = "FENICS";

            var sqJson = JsonConvert.SerializeObject(sq, new StringEnumConverter());

            Console.WriteLine(sqJson);
        }
    }
}
