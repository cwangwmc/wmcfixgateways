﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WMC.FIXDataObjects.Tests
{
    [TestFixture]
    public class TestTenorDates
    {

        [Test]
        public void TestTenorMaturityDatesDictJsonEncoding()
        {
            var tenorMaturityDates = new Dictionary<string, DateTime>();

            var datetimeFormat_yyyyMMdd = "yyyyMMdd";

            tenorMaturityDates.Add("1W", DateTime.ParseExact("20180921", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("2W", DateTime.ParseExact("20180928", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("3W", DateTime.ParseExact("20181005", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("4W", DateTime.ParseExact("20181012", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("1M", DateTime.ParseExact("20181016", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("2M", DateTime.ParseExact("20181115", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("3M", DateTime.ParseExact("20181214", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("4M", DateTime.ParseExact("20191116", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("6M", DateTime.ParseExact("20190314", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("9M", DateTime.ParseExact("20190614", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("1Y", DateTime.ParseExact("20190916", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("18M", DateTime.ParseExact("20200316", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tenorMaturityDates.Add("2Y", DateTime.ParseExact("20200916", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));

            var tenorMaturityDatesJson = JsonConvert.SerializeObject(tenorMaturityDates, new JsonConverter[] { new StringEnumConverter() });

            Assert.IsNotNull(tenorMaturityDatesJson);

            Assert.IsNotEmpty(tenorMaturityDatesJson);

            var expectedJson = "{\"1W\":\"2018-09-21T00:00:00\"," +
                                "\"2W\":\"2018-09-28T00:00:00\"," +
                                "\"3W\":\"2018-10-05T00:00:00\"," +
                                "\"4W\":\"2018-10-12T00:00:00\"," +
                                "\"1M\":\"2018-10-16T00:00:00\"," +
                                "\"2M\":\"2018-11-15T00:00:00\"," +
                                "\"3M\":\"2018-12-14T00:00:00\"," +
                                "\"4M\":\"2019-11-16T00:00:00\"," +
                                "\"6M\":\"2019-03-14T00:00:00\"," +
                                "\"9M\":\"2019-06-14T00:00:00\"," +
                                "\"1Y\":\"2019-09-16T00:00:00\"," +
                                "\"18M\":\"2020-03-16T00:00:00\"," +
                                "\"2Y\":\"2020-09-16T00:00:00\"}";

            Assert.AreEqual(expectedJson, tenorMaturityDatesJson);
        }

        [Test]
        public void TestTenorDatesObjectJsonEncoding()
        {
            var refToday = new DateTime(2018, 9, 14);
            var asof = DateTime.ParseExact("2018-09-14T12:00:00", "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);

            var tnrMatDtsDict = new Dictionary<string, DateTime?>();

            var datetimeFormat_yyyyMMdd = "yyyyMMdd";

            tnrMatDtsDict.Add("1W", DateTime.ParseExact("20180921", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("2W", DateTime.ParseExact("20180928", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("3W", DateTime.ParseExact("20181005", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("4W", DateTime.ParseExact("20181012", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("1M", DateTime.ParseExact("20181016", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("2M", DateTime.ParseExact("20181115", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("3M", DateTime.ParseExact("20181214", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("4M", DateTime.ParseExact("20191116", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("6M", DateTime.ParseExact("20190314", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("9M", DateTime.ParseExact("20190614", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("1Y", DateTime.ParseExact("20190916", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("18M", DateTime.ParseExact("20200316", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));
            tnrMatDtsDict.Add("2Y", DateTime.ParseExact("20200916", datetimeFormat_yyyyMMdd, CultureInfo.InvariantCulture));

            var tnrDtsObj = new TenorDates(refToday, asof, "dbabfx", "EURUSD", tnrMatDtsDict);

            var tnrMatDtsDictJson = JsonConvert.SerializeObject(tnrMatDtsDict);
            var tnrDtsObjJson = JsonConvert.SerializeObject(tnrDtsObj);

            Assert.IsNotNull(tnrMatDtsDictJson);

            Assert.IsNotEmpty(tnrMatDtsDictJson);

            Assert.IsNotNull(tnrDtsObjJson);

            Assert.IsNotEmpty(tnrDtsObjJson);

            Console.WriteLine(tnrMatDtsDictJson);
            Console.WriteLine(tnrDtsObjJson);

            var expectedJson = "{\"ReferenceDate\":\"2018-09-14T00:00:00\"," +
                               "\"AsOf\":\"2018-09-14T12:00:00\"," +
                               "\"Source\":\"dbabfx\"," +
                               "\"CurrencyPair\":\"EURUSD\"," +
                               "\"TenorMaturityDates\":" + tnrMatDtsDictJson + "}";

            Assert.AreEqual(expectedJson, tnrDtsObjJson);
        }

        [Test]
        public void TestTenorDatesObjectMaturityDatesCollections()
        {
            var tnrDtsObj = new TenorDates(new DateTime(2018, 9, 14), "dbabfx", "eurusd");

            var add1WResult = tnrDtsObj.AddTenorDate("1W", new DateTime(2018, 9, 21));
            var add1MResult = tnrDtsObj.AddTenorDate("1M", new DateTime(2018, 10, 16));
            var add1YResult = tnrDtsObj.AddTenorDate("1Y", new DateTime(2019, 9, 16));

            Assert.True(add1WResult);
            Assert.True(add1MResult);
            Assert.True(add1YResult);

            var add1YAgainResult = tnrDtsObj.AddTenorDate("1Y", new DateTime(2018, 9, 15));

            Assert.False(add1YAgainResult);

            var hasAllReusltFalse = tnrDtsObj.HasAllTenorDates(new string[] { "1W", "1M", "3M", "6M", "1Y" });

            Assert.False(hasAllReusltFalse);

            var hasAllReusltTrue = tnrDtsObj.HasAllTenorDates(new string[] { "1W", "1M", "1Y" });

            Assert.True(hasAllReusltTrue);
        }
    }
}
