﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXDataObjects;

using NUnit.Framework;
using Newtonsoft.Json;

namespace WMC.FIXDataObjects.Tests
{
    [TestFixture]
    public class TestTenor
    {
        [Test]
        public void TenorObjectConstructs()
        {
            var t_on = new Tenor("ON");
            var t_1w = new Tenor("1W");
            var t_12w = new Tenor("12w");
            var t_6m = new Tenor("6M");
            var t_1y = new Tenor("1y");

            Assert.IsNotNull(t_on);
            Assert.IsNotNull(t_1w);
            Assert.IsNotNull(t_12w);
            Assert.IsNotNull(t_6m);
            Assert.IsNotNull(t_1y);
        }

        [Test]
        public void TenorObjectConstructionThrowsException()
        {
            try
            {
                var t_err = new Tenor("abc");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOf(typeof(InvalidOperationException), e);
            }

            try
            {
                var t_err = new Tenor("12H");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOf(typeof(InvalidOperationException), e);
            }
        }

        [Test]
        public void TenorCodeIsCapitalised()
        {
            var t_on = new Tenor("on");
            var t_53w = new Tenor("53w");
            var t_6m = new Tenor("6m");
            var t_9M = new Tenor("9M");
            var t_2y = new Tenor("2y");

            Assert.AreEqual(t_on.TenorCode, "ON");
            Assert.AreEqual(t_53w.TenorCode, "53W");
            Assert.AreEqual(t_6m.TenorCode, "6M");
            Assert.AreEqual(t_9M.TenorCode, "9M");
            Assert.AreEqual(t_2y.TenorCode, "2Y");
        }

        [Test]
        public void TenorCodeIsParsedCorrectly()
        {
            var t_on = new Tenor("on");
            var t_53w = new Tenor("53w");
            var t_6m = new Tenor("6m");
            var t_2y = new Tenor("2y");

            Assert.AreEqual(t_on.TenorCode, "ON");
            Assert.AreEqual(t_53w.TenorCode, "53W");
            Assert.AreEqual(t_6m.TenorCode, "6M");
            Assert.AreEqual(t_2y.TenorCode, "2Y");
        }

        [Test]
        public void TenorComparisonWorks()
        {
            var t_on = new Tenor("ON");
            var t_1d = new Tenor("1d");
            var t_2d = new Tenor("2d");

            var t_1w = new Tenor("1w");
            var t_7d = new Tenor("7d");

            Assert.IsFalse(t_on == t_1d);
            Assert.IsFalse(t_on < t_1d);
            Assert.IsFalse(t_on > t_1d);
            Assert.IsTrue(t_on <= t_1d);
            Assert.IsTrue(t_on >= t_1d);

            Assert.True(t_on < t_2d);
            Assert.True(t_2d < t_1w);
        }

        [Test]
        public void TestTenorJsonEncoding()
        {
            Console.WriteLine("Output from Json encoding test...");
            var t_1w = new Tenor("1w");
            var t_1w_json = JsonConvert.SerializeObject(t_1w);
            Console.WriteLine(t_1w_json);
        }

        [Test]
        public void TestTenorJsonDecoding()
        {
            Console.WriteLine("Output from Json decoding test...");
        }
    }
}
