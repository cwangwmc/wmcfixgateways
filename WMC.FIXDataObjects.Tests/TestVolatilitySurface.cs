﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WMC.FIXDataObjects.Tests
{
    [TestFixture]
    public class TestDeltaDetails
    {
        [Test]
        public void Test25SpotBpCallDeltaDetailsJsonEncoding()
        {
            var _25dSpotBpCall = new DeltaDetails(25, DeltaType.SpotBp, DeltaOrientation.Call);
            var _25dSpotBpCall_json = JsonConvert.SerializeObject(_25dSpotBpCall, new StringEnumConverter());

            Assert.IsNotNull(_25dSpotBpCall_json);
            // {"Delta":25,"DeltaType":"SpotBp","DeltaOrientation":"Call"}
            Assert.AreEqual(_25dSpotBpCall_json, "{\"Delta\":25,\"DeltaType\":\"SpotBp\",\"DeltaOrientation\":\"Call\"}");
        }

        [Test]
        public void Test10ForwardPcPutDeltaDetailsJsonEncoding()
        {
            var _10dForwardPcPut = new DeltaDetails(10, DeltaType.ForwardPc, DeltaOrientation.Put);
            var _10dForwardPcPut_json = JsonConvert.SerializeObject(_10dForwardPcPut, new StringEnumConverter());

            Assert.IsNotNull(_10dForwardPcPut_json);
            // {"Delta":10,"DeltaType":"ForwardPc","DeltaOrientation":"Put"}
            Assert.AreEqual(_10dForwardPcPut_json, "{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}");
        }
    }

    [TestFixture]
    public class TestVolatilitySmilePoint
    {

        [Test]
        public void Test_AtmDn_VolSmilePt_JsonEncoding()
        {
            // Atm Dn @ 7.25
            var _atmDn_725 = new VolatilitySmilePoint(7.25, Atm.Dn);
            var _atmDn_725_json = JsonConvert.SerializeObject(_atmDn_725, new StringEnumConverter());

            Assert.IsNotNull(_atmDn_725_json);

            // {"Vol":7.25,"IsAtm":true,"AtmType":"Dn","DeltaDetails":null}
            Assert.AreEqual(_atmDn_725_json, "{\"Vol\":7.25,\"IsAtm\":true,\"AtmType\":\"Dn\",\"DeltaDetails\":null}");
        }

        [Test]
        public void Test_Atmf_VolSmilePt_JsonEncoding()
        {
            // Atm Amtf @ 6.89
            var _atmf_689 = new VolatilitySmilePoint(6.89, Atm.Atmf);
            var _atmf_689_json = JsonConvert.SerializeObject(_atmf_689, new StringEnumConverter());

            Assert.IsNotNull(_atmf_689_json);

            // {"Vol":6.89,"IsAtm":true,"AtmType":"Atmf","DeltaDetails":null}
            Assert.AreEqual(_atmf_689_json, "{\"Vol\":6.89,\"IsAtm\":true,\"AtmType\":\"Atmf\",\"DeltaDetails\":null}");
        }

        [Test]
        public void Test_25SpotBpDelta_Call_VolSmilePt_JsonEncoding()
        {
            // 25 delta spot bp call @ 10.78
            var _25dSpotBpCall = new VolatilitySmilePoint(10.78, 25, DeltaType.SpotBp, DeltaOrientation.Call);
            var _25dSpotBpCall_json = JsonConvert.SerializeObject(_25dSpotBpCall, new StringEnumConverter());

            Assert.IsNotNull(_25dSpotBpCall_json);

            // {"Vol":10.78,"IsAtm":false,"AtmType":null,"DeltaDetails":{"Delta":25,"DeltaType":"SpotBp","DeltaOrientation":"Call"}}
            Assert.AreEqual(_25dSpotBpCall_json, "{\"Vol\":10.78,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotBp\",\"DeltaOrientation\":\"Call\"}}");
        }

        [Test]
        public void Test_10ForwardPcDelta_Put_VolSmilePt_JsonEncoding()
        {
            // 10 delta forward pc put @ 20.42
            var _10dForwardPcPut = new VolatilitySmilePoint(20.42, 10, DeltaType.ForwardPc, DeltaOrientation.Put);
            var _10dForwardPcPut_json = JsonConvert.SerializeObject(_10dForwardPcPut, new StringEnumConverter());

            Assert.IsNotNull(_10dForwardPcPut_json);

            // {"Vol":20.42,"IsAtm":false,"AtmType":null,"DeltaDetails":{"Delta":10,"DeltaType":"ForwardPc","DeltaOrientation":"Put"}}
            Assert.AreEqual(_10dForwardPcPut_json, "{\"Vol\":20.42,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}}");
        }

    }

    [TestFixture]
    public class TestVolatilitySmile
    {
        [Test]
        public void Test_VolSmile_Has_Funcs()
        {
            var _3m_atm = new VolatilitySmilePoint(8.83, Atm.Dn);
            var _3m_25c = new VolatilitySmilePoint(8.48, 25, DeltaType.SpotPc, DeltaOrientation.Call);
            var _3m_25p = new VolatilitySmilePoint(9.64, 25, DeltaType.SpotPc, DeltaOrientation.Put);
            var _3m_10c = new VolatilitySmilePoint(8.50, 10, DeltaType.SpotPc, DeltaOrientation.Call);
            var _3m_10p = new VolatilitySmilePoint(10.53, 10, DeltaType.SpotPc, DeltaOrientation.Put);

            var _3m_VolSmile_full = new VolatilitySmile(new Tenor("3m"), new List<VolatilitySmilePoint> { _3m_atm, _3m_25c, _3m_25p, _3m_10c, _3m_10p });
            var _3m_VolSmile_wingsonly = new VolatilitySmile(new Tenor("3m"), new List<VolatilitySmilePoint> { _3m_25c, _3m_25p, _3m_10c, _3m_10p });
            var _3m_VolSmile_mainbody = new VolatilitySmile(new Tenor("3m"), new List<VolatilitySmilePoint> { _3m_atm, _3m_25c, _3m_25p });
            var _3m_VolSmile_partial = new VolatilitySmile(new Tenor("3m"), new List<VolatilitySmilePoint> { _3m_atm, _3m_25c, _3m_10p });

            Assert.IsTrue(_3m_VolSmile_full.HasCallPutWingDeltas(new int[] { 25 }));
            Assert.IsTrue(_3m_VolSmile_full.HasCallPutWingDeltas(new int[] { 10 }));
            Assert.IsTrue(_3m_VolSmile_full.HasCallPutWingDeltas(new int[] { 25, 10 }));

            Assert.IsFalse(_3m_VolSmile_full.HasCallPutWingDeltas(new int[] { 25, 5 }));
            Assert.IsFalse(_3m_VolSmile_full.HasCallPutWingDeltas(new int[] { 35, 5 }));

            Assert.IsTrue(_3m_VolSmile_wingsonly.HasWingVolPoint(10, DeltaOrientation.Call));
            Assert.IsTrue(_3m_VolSmile_wingsonly.HasWingVolPoint(10, DeltaOrientation.Put));
            Assert.IsTrue(_3m_VolSmile_wingsonly.HasCallPutWingDeltas(new int[] { 25 }));

            Assert.IsFalse(_3m_VolSmile_wingsonly.HasAtmVolPoint());

            Assert.IsTrue(_3m_VolSmile_mainbody.HasAtmVolPoint());
            Assert.IsTrue(_3m_VolSmile_mainbody.HasCallPutWingDeltas(new int[] { 25 }));

            Assert.IsFalse(_3m_VolSmile_mainbody.HasCallPutWingDeltas(new int[] { 10, 35 }));

            Assert.IsTrue(_3m_VolSmile_partial.HasAtmVolPoint());
            Assert.IsTrue(_3m_VolSmile_partial.HasWingVolPoint(25, DeltaOrientation.Call));
            Assert.IsTrue(_3m_VolSmile_partial.HasWingVolPoint(10, DeltaOrientation.Put));

            Assert.IsFalse(_3m_VolSmile_partial.HasCallPutWingDeltas(new int[] { 25, 10 }));
            Assert.IsFalse(_3m_VolSmile_partial.HasCallPutWingDeltas(new int[] { 25 }));
            Assert.IsFalse(_3m_VolSmile_partial.HasCallPutWingDeltas(new int[] { 10 }));
        }

        [Test]
        public void Test_VolSmile_Add_Funcs()
        {
            var volSmile = new VolatilitySmile(new Tenor("6m"));

            volSmile.AddAtmVolPoint(8.83, Atm.Dn);
            volSmile.AddWingVolPoint(9.64, new DeltaDetails(25, DeltaType.SpotBp, DeltaOrientation.Put));

            Assert.IsTrue(volSmile.HasAtmVolPoint());
            Assert.IsFalse(volSmile.HasCallPutWingDeltas(new int[] { 25 }));

            volSmile.AddWingVolPoint(8.48, new DeltaDetails(25, DeltaType.SpotBp, DeltaOrientation.Call));

            Assert.IsTrue(volSmile.HasCallPutWingDeltas(new int[] { 25 }));

            volSmile.AddWingVolPoint(8.50, new DeltaDetails(10, DeltaType.SpotBp, DeltaOrientation.Put));

            Assert.IsFalse(volSmile.HasCallPutWingDeltas(new int[] { 10 }));

            volSmile.AddWingVolPoint(10.53, new DeltaDetails(10, DeltaType.SpotBp, DeltaOrientation.Call));

            Assert.IsTrue(volSmile.HasCallPutWingDeltas(new int[] { 10 }));
        }

        [Test]
        public void Test_1M_USDJPY_VolSmile_JsonEncoding()
        {
            var _1m_atm = new VolatilitySmilePoint(6.95, Atm.Dn);
            var _1m_25c = new VolatilitySmilePoint(6.91, 25, DeltaType.SpotPc, DeltaOrientation.Call);
            var _1m_25p = new VolatilitySmilePoint(7.36, 25, DeltaType.SpotPc, DeltaOrientation.Put);
            var _1m_10c = new VolatilitySmilePoint(7.04, 10, DeltaType.SpotPc, DeltaOrientation.Call);
            var _1m_10p = new VolatilitySmilePoint(7.94, 10, DeltaType.SpotPc, DeltaOrientation.Put);

            var _1m_usdjpy = new VolatilitySmile(new Tenor("1m"), new List<VolatilitySmilePoint> { _1m_atm, _1m_25c, _1m_25p, _1m_10c, _1m_10p });
            var _1m_usdjpy_json = JsonConvert.SerializeObject(_1m_usdjpy, new StringEnumConverter());

            Assert.IsNotNull(_1m_usdjpy_json);

            Assert.AreEqual(_1m_usdjpy_json, "{\"Tenor\":\"1M\",\"VolSmilePoints\":[{\"Vol\":6.95,\"IsAtm\":true,\"AtmType\":\"Dn\",\"DeltaDetails\":null}" +
                ",{\"Vol\":6.91,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Call\"}}" +
                ",{\"Vol\":7.36,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Put\"}}" +
                ",{\"Vol\":7.04,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Call\"}}" +
                ",{\"Vol\":7.94,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Put\"}}]}");
        }

        [Test]
        public void Test_1Y_USDTRY_VolSmile_JsonEncoding()
        {
            var _1y_atm = new VolatilitySmilePoint(17.19, Atm.Atmf);
            var _1y_25c = new VolatilitySmilePoint(20.32, 25, DeltaType.ForwardPc, DeltaOrientation.Call);
            var _1y_25p = new VolatilitySmilePoint(15.79, 25, DeltaType.ForwardPc, DeltaOrientation.Put);
            var _1y_10c = new VolatilitySmilePoint(23.87, 10, DeltaType.ForwardPc, DeltaOrientation.Call);
            var _1y_10p = new VolatilitySmilePoint(15.24, 10, DeltaType.ForwardPc, DeltaOrientation.Put);

            var _1y_usdtry = new VolatilitySmile(new Tenor("1Y"), new List<VolatilitySmilePoint> { _1y_atm, _1y_25c, _1y_25p, _1y_10c, _1y_10p});
            var _1y_usdtry_json = JsonConvert.SerializeObject(_1y_usdtry, new StringEnumConverter());

            Assert.IsNotNull(_1y_usdtry_json);

            Assert.AreEqual(_1y_usdtry_json, "{\"Tenor\":\"1Y\",\"VolSmilePoints\":[{\"Vol\":17.19,\"IsAtm\":true,\"AtmType\":\"Atmf\",\"DeltaDetails\":null}" +
                ",{\"Vol\":20.32,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Call\"}}" +
                ",{\"Vol\":15.79,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}}" +
                ",{\"Vol\":23.87,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Call\"}}" +
                ",{\"Vol\":15.24,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}}]}");
        }
    }

    [TestFixture]
    public class TestVolatilitySurface
    {
        [Test]
        public void Test_VolSurf_Has_Func()
        {
            var _2w_atm = new VolatilitySmilePoint(8.94, Atm.Dn);
            var _2w_25c = new VolatilitySmilePoint(8.73, 25, DeltaType.SpotBp, DeltaOrientation.Call);
            var _2w_25p = new VolatilitySmilePoint(9.48, 25, DeltaType.SpotBp, DeltaOrientation.Put);
            var _2w_10c = new VolatilitySmilePoint(8.45, 10, DeltaType.SpotBp, DeltaOrientation.Call);
            var _2w_10p = new VolatilitySmilePoint(10.77, 10, DeltaType.SpotBp, DeltaOrientation.Put);

            var _2w_volSmile = new VolatilitySmile(new Tenor("2w"), new List<VolatilitySmilePoint> { _2w_atm, _2w_25c, _2w_10c });

            var _6m_atm = new VolatilitySmilePoint(9.63, Atm.Dn);
            var _6m_25c = new VolatilitySmilePoint(8.92, 25, DeltaType.SpotBp, DeltaOrientation.Call);
            var _6m_25p = new VolatilitySmilePoint(11.06, 25, DeltaType.SpotBp, DeltaOrientation.Put);
            var _6m_10c = new VolatilitySmilePoint(9.00, 10, DeltaType.SpotBp, DeltaOrientation.Call);
            var _6m_10p = new VolatilitySmilePoint(12.72, 10, DeltaType.SpotBp, DeltaOrientation.Put);

            var _6m_volSmile = new VolatilitySmile(new Tenor("6m"), new List<VolatilitySmilePoint> { _6m_atm, _6m_10c, _6m_10p });

            var gbpusd_volSurf = new VolatilitySurface("GBPUSD", DateTime.UtcNow, new List<VolatilitySmile> { _2w_volSmile, _6m_volSmile });

            Assert.IsTrue(gbpusd_volSurf.HasAtmVolPoints(new Tenor[] { new Tenor("2w"), new Tenor("6m") }));

            Assert.IsFalse(gbpusd_volSurf.HasAtmVolPoints(new Tenor[] { }));
            Assert.IsFalse(gbpusd_volSurf.HasAtmVolPoints(new Tenor[] { new Tenor("2w"), new Tenor("6m"), new Tenor("9m") }));

            Assert.IsTrue(gbpusd_volSurf.HasWingDeltas(new Tenor[] { new Tenor("6m") }, new int[] { 10 }));

            Assert.IsFalse(gbpusd_volSurf.HasWingDeltas(new Tenor[] { }, new int[] { }));
            Assert.IsFalse(gbpusd_volSurf.HasWingDeltas(new Tenor[] { new Tenor("6m") }, new int[] { 25, 10 }));
            Assert.IsFalse(gbpusd_volSurf.HasWingDeltas(new Tenor[] { new Tenor("6m") }, new int[] { 25 }));

            Assert.IsFalse(gbpusd_volSurf.HasWingDeltas(new Tenor[] { new Tenor("2w") }, new int[] { 25 }));
            Assert.IsFalse(gbpusd_volSurf.HasWingDeltas(new Tenor[] { new Tenor("2w") }, new int[] { 10 }));
        }

        [Test]
        public void Test_VolSurf_Add_Func()
        {
            var _2w_atm = new VolatilitySmilePoint(8.94, Atm.Dn);
            var _2w_25c = new VolatilitySmilePoint(8.73, 25, DeltaType.SpotBp, DeltaOrientation.Call);
            var _2w_25p = new VolatilitySmilePoint(9.48, 25, DeltaType.SpotBp, DeltaOrientation.Put);
            var _2w_10c = new VolatilitySmilePoint(8.45, 10, DeltaType.SpotBp, DeltaOrientation.Call);
            var _2w_10p = new VolatilitySmilePoint(10.77, 10, DeltaType.SpotBp, DeltaOrientation.Put);

            var _2w_volSmile = new VolatilitySmile(new Tenor("2w"), new List<VolatilitySmilePoint> { _2w_atm, _2w_25c, _2w_10c });

            var _6m_atm = new VolatilitySmilePoint(9.63, Atm.Dn);
            var _6m_25c = new VolatilitySmilePoint(8.92, 25, DeltaType.SpotBp, DeltaOrientation.Call);
            var _6m_25p = new VolatilitySmilePoint(11.06, 25, DeltaType.SpotBp, DeltaOrientation.Put);
            var _6m_10c = new VolatilitySmilePoint(9.00, 10, DeltaType.SpotBp, DeltaOrientation.Call);
            var _6m_10p = new VolatilitySmilePoint(12.72, 10, DeltaType.SpotBp, DeltaOrientation.Put);

            var _6m_volSmile = new VolatilitySmile(new Tenor("6m"), new List<VolatilitySmilePoint> { _6m_atm, _6m_10c, _6m_10p });

            var gbpusd_volSurf = new VolatilitySurface("GBPUSD", DateTime.UtcNow, new List<VolatilitySmile> { _2w_volSmile, _6m_volSmile });

            Assert.IsFalse(gbpusd_volSurf.AddAtmVolPointToTenor(new Tenor("6m"), 9.63, Atm.Dn));
            Assert.IsFalse(gbpusd_volSurf.AddWingVolPointToTenor(new Tenor("6m"), 8.92, new DeltaDetails(10, DeltaType.SpotBp, DeltaOrientation.Call)));

            Assert.IsTrue(gbpusd_volSurf.AddWingVolPointToTenor(new Tenor("6m"), 8.92, new DeltaDetails(25, DeltaType.SpotBp, DeltaOrientation.Call)));
            Assert.IsTrue(gbpusd_volSurf.AddWingVolPointToTenor(new Tenor("6m"), 11.06, new DeltaDetails(25, DeltaType.SpotBp, DeltaOrientation.Put)));

            Assert.IsTrue(gbpusd_volSurf.HasWingDeltas(new Tenor[] { new Tenor("6m") }, new int[] { 10, 25 }));

            Assert.IsTrue(gbpusd_volSurf.AddAtmVolPointToTenor(new Tenor("9m"), 9.66, Atm.Dn));

            Assert.IsTrue(gbpusd_volSurf.HasAtmVolPoints(new Tenor[] { new Tenor("9m"), new Tenor("6m"), new Tenor("2w") }));

            Assert.IsTrue(gbpusd_volSurf.AddWingVolPointToTenor(new Tenor("9m"), 8.94, new DeltaDetails(25, DeltaType.SpotBp, DeltaOrientation.Call)));
            Assert.IsTrue(gbpusd_volSurf.AddWingVolPointToTenor(new Tenor("9m"), 11.13, new DeltaDetails(25, DeltaType.SpotBp, DeltaOrientation.Put)));

            Assert.IsTrue(gbpusd_volSurf.HasWingDeltas(new Tenor[] { new Tenor("9m"), new Tenor("6m") }, new int[] { 25 }));
        }

        [Test]
        public void Test_USDJPY_VolSurface_JsonEncoding()
        {
            var _1w_atm = new VolatilitySmilePoint(6.33, Atm.Dn);
            var _1w_25c = new VolatilitySmilePoint(6.37, 25, DeltaType.SpotPc, DeltaOrientation.Call);
            var _1w_25p = new VolatilitySmilePoint(6.60, 25, DeltaType.SpotPc, DeltaOrientation.Put);
            var _1w_10c = new VolatilitySmilePoint(6.56, 10, DeltaType.SpotPc, DeltaOrientation.Call);
            var _1w_10p = new VolatilitySmilePoint(7.01, 10, DeltaType.SpotPc, DeltaOrientation.Put);

            var _1m_atm = new VolatilitySmilePoint(6.94, Atm.Dn);
            var _1m_25c = new VolatilitySmilePoint(6.89, 25, DeltaType.SpotPc, DeltaOrientation.Call);
            var _1m_25p = new VolatilitySmilePoint(7.34, 25, DeltaType.SpotPc, DeltaOrientation.Put);
            var _1m_10c = new VolatilitySmilePoint(7.03, 10, DeltaType.SpotPc, DeltaOrientation.Call);
            var _1m_10p = new VolatilitySmilePoint(7.93, 10, DeltaType.SpotPc, DeltaOrientation.Put);

            var _1y_atm = new VolatilitySmilePoint(8.31, Atm.Dn);
            var _1y_25c = new VolatilitySmilePoint(7.94, 25, DeltaType.SpotPc, DeltaOrientation.Call);
            var _1y_25p = new VolatilitySmilePoint(9.39, 25, DeltaType.SpotPc, DeltaOrientation.Put);
            var _1y_10c = new VolatilitySmilePoint(8.01, 10, DeltaType.SpotPc, DeltaOrientation.Call);
            var _1y_10p = new VolatilitySmilePoint(10.99, 10, DeltaType.SpotPc, DeltaOrientation.Put);

            var _1w_usdjpy = new VolatilitySmile(new Tenor("1w"), new List<VolatilitySmilePoint> { _1w_atm, _1w_25c, _1w_25p, _1w_10c, _1w_10p });
            var _1m_usdjpy = new VolatilitySmile(new Tenor("1m"), new List<VolatilitySmilePoint> { _1m_atm, _1m_25c, _1m_25p, _1m_10c, _1m_10p });
            var _1y_usdjpy = new VolatilitySmile(new Tenor("1y"), new List<VolatilitySmilePoint> { _1y_atm, _1y_25c, _1y_25p, _1y_10c, _1y_10p });

            var usdjpy = new VolatilitySurface("USDJPY", new DateTime(2018, 5, 16), new List<VolatilitySmile> { _1w_usdjpy, _1m_usdjpy, _1y_usdjpy });
            var usdjpy_json = JsonConvert.SerializeObject(usdjpy, new StringEnumConverter());

            Assert.IsNotNull(usdjpy_json);

            Assert.AreEqual(usdjpy_json, "{\"Security\":\"USDJPY\",\"AsOf\":\"2018-05-16T00:00:00\",\"VolSmiles\":{\"1W\":{\"Tenor\":\"1W\",\"VolSmilePoints\":[{\"Vol\":6.33,\"IsAtm\":true,\"AtmType\":\"Dn\",\"DeltaDetails\":null},{\"Vol\":6.37,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":6.6,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Put\"}},{\"Vol\":6.56,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":7.01,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Put\"}}]}" +
                ",\"1M\":{\"Tenor\":\"1M\",\"VolSmilePoints\":[{\"Vol\":6.94,\"IsAtm\":true,\"AtmType\":\"Dn\",\"DeltaDetails\":null},{\"Vol\":6.89,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":7.34,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Put\"}},{\"Vol\":7.03,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":7.93,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Put\"}}]}" +
                ",\"1Y\":{\"Tenor\":\"1Y\",\"VolSmilePoints\":[{\"Vol\":8.31,\"IsAtm\":true,\"AtmType\":\"Dn\",\"DeltaDetails\":null},{\"Vol\":7.94,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":9.39,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Put\"}},{\"Vol\":8.01,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":10.99,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"SpotPc\",\"DeltaOrientation\":\"Put\"}}]}}" +
                ",\"Source\":\"\",\"ReferenceSpot\":null,\"ReferenceDate\":null}");
        }

        [Test]
        public void Test_USDTRY_VolSurface_JsonEncoding()
        {
            var _1w_atm = new VolatilitySmilePoint(22.07, Atm.Atmf);
            var _1w_25c = new VolatilitySmilePoint(23.92, 25, DeltaType.ForwardPc, DeltaOrientation.Call);
            var _1w_25p = new VolatilitySmilePoint(21.32, 25, DeltaType.ForwardPc, DeltaOrientation.Put);
            var _1w_10c = new VolatilitySmilePoint(26.48, 10, DeltaType.ForwardPc, DeltaOrientation.Call);
            var _1w_10p = new VolatilitySmilePoint(21.62, 10, DeltaType.ForwardPc, DeltaOrientation.Put);

            var _1m_atm = new VolatilitySmilePoint(23.21, Atm.Atmf);
            var _1m_25c = new VolatilitySmilePoint(25.81, 25, DeltaType.ForwardPc, DeltaOrientation.Call);
            var _1m_25p = new VolatilitySmilePoint(22.24, 25, DeltaType.ForwardPc, DeltaOrientation.Put);
            var _1m_10c = new VolatilitySmilePoint(28.82, 10, DeltaType.ForwardPc, DeltaOrientation.Call);
            var _1m_10p = new VolatilitySmilePoint(22.30, 10, DeltaType.ForwardPc, DeltaOrientation.Put); 

            var _1y_atm = new VolatilitySmilePoint(17.19, Atm.Atmf);
            var _1y_25c = new VolatilitySmilePoint(20.32, 25, DeltaType.ForwardPc, DeltaOrientation.Call);
            var _1y_25p = new VolatilitySmilePoint(15.79, 25, DeltaType.ForwardPc, DeltaOrientation.Put);
            var _1y_10c = new VolatilitySmilePoint(23.87, 10, DeltaType.ForwardPc, DeltaOrientation.Call);
            var _1y_10p = new VolatilitySmilePoint(15.24, 10, DeltaType.ForwardPc, DeltaOrientation.Put);

            var _1w_usdtry = new VolatilitySmile("1w", new VolatilitySmilePoint[] { _1w_atm, _1w_25c, _1w_25p, _1w_10c, _1w_10p });
            var _1m_usdtry = new VolatilitySmile("1m", new VolatilitySmilePoint[] { _1m_atm, _1m_25c, _1m_25p, _1m_10c, _1m_10p });
            var _1y_usdtry = new VolatilitySmile("1y", new VolatilitySmilePoint[] { _1y_atm, _1y_25c, _1y_25p, _1y_10c, _1y_10p });

            var usdtry = new VolatilitySurface("USDTRY", new DateTime(2018, 6, 5), new VolatilitySmile[] { _1w_usdtry, _1m_usdtry, _1y_usdtry });
            var usdtry_json = JsonConvert.SerializeObject(usdtry, new StringEnumConverter());

            Assert.IsNotNull(usdtry_json);

            Assert.AreEqual(usdtry_json, "{\"Security\":\"USDTRY\",\"AsOf\":\"2018-06-05T00:00:00\",\"VolSmiles\":{\"1W\":{\"Tenor\":\"1W\",\"VolSmilePoints\":[{\"Vol\":22.07,\"IsAtm\":true,\"AtmType\":\"Atmf\",\"DeltaDetails\":null},{\"Vol\":23.92,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":21.32,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}},{\"Vol\":26.48,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":21.62,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}}]}" +
                ",\"1M\":{\"Tenor\":\"1M\",\"VolSmilePoints\":[{\"Vol\":23.21,\"IsAtm\":true,\"AtmType\":\"Atmf\",\"DeltaDetails\":null},{\"Vol\":25.81,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":22.24,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}},{\"Vol\":28.82,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":22.3,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}}]}" +
                ",\"1Y\":{\"Tenor\":\"1Y\",\"VolSmilePoints\":[{\"Vol\":17.19,\"IsAtm\":true,\"AtmType\":\"Atmf\",\"DeltaDetails\":null},{\"Vol\":20.32,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":15.79,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":25,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}},{\"Vol\":23.87,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Call\"}},{\"Vol\":15.24,\"IsAtm\":false,\"AtmType\":null,\"DeltaDetails\":{\"Delta\":10,\"DeltaType\":\"ForwardPc\",\"DeltaOrientation\":\"Put\"}}]}}" +
                ",\"Source\":\"\",\"ReferenceSpot\":null,\"ReferenceDate\":null}");
        }
    }
}
