﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using WMC.FIXDataObjects;
using WMC.FIXGateways.Dbabfx;

using NUnit.Framework;


namespace WMC.FIXStateMachines.Tests
{
    [TestFixture]
    public class TestDbabfxOptionStateMachines : TestRFQStateMachines
    {

        public event EventHandler<FIXMessageEventArgs> FIXMsgInDbabfxOption;

        private DbabfxFIXOptionsAdaptor _dbabfxFIXRfqOptAdptor;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _dbabfxFIXRfqOptAdptor = new DbabfxFIXOptionsAdaptor();
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();

            FIXMsgInDbabfxOption = null;
        }

        [Test]
        public void Test_Quote_Workflow_from_Logoff()
        {
            
            var dbabfxRFQStateMachine = _dbabfxFIXRfqOptAdptor.MakeSingleVanillaOptionRfqStateMachine(Buy_1M_BpGBPUSD_25DS_Put, 5_000, 10_000);
            var refId = dbabfxRFQStateMachine.StateMachineRefId;

            dbabfxRFQStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInDbabfxOption += dbabfxRFQStateMachine.OnInboundFIXMessage;

            Assert.AreEqual("WaitingForLogon", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForLogonS>(dbabfxRFQStateMachine.CurrentState);

            dbabfxRFQStateMachine.Activate();

            Thread.Sleep(7_000);
            Assert.IsNull(OutFIXMsgDtlEvtArgs);

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInDbabfxOption?.Invoke(this, fixMsgEvtArgs);

            // QuoteRequest message out
            Assert.AreEqual("SendingQuoteRequest", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(dbabfxRFQStateMachine.CurrentState);

            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.AreEqual("WaitingForResponse", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(dbabfxRFQStateMachine.CurrentState);

            // Quote message in
            fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Quote_S, DateTime.UtcNow);
            FIXMsgInDbabfxOption?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("Done", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<DoneS>(dbabfxRFQStateMachine.CurrentState);
        }

        [Test]
        public void Test_Timeout_Workflow()
        {

            var dbabfxRFQStateMachine = _dbabfxFIXRfqOptAdptor.MakeSingleVanillaOptionRfqStateMachine(Buy_1M_BpGBPUSD_25DS_Put, 5_000, 10_000);
            var refId = dbabfxRFQStateMachine.StateMachineRefId;

            dbabfxRFQStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInDbabfxOption += dbabfxRFQStateMachine.OnInboundFIXMessage;

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInDbabfxOption?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("SendingQuoteRequest", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(dbabfxRFQStateMachine.CurrentState);

            dbabfxRFQStateMachine.Activate();

            // QuoteRequest message out
            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.AreEqual("WaitingForResponse", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(dbabfxRFQStateMachine.CurrentState);

            // Quote does not come back in time result in timeout
            Thread.Sleep(12_000);
            Assert.AreEqual("Done", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<DoneS>(dbabfxRFQStateMachine.CurrentState);

        }

        [Test]
        public void Test_QuoteCancel_Workflow()
        {

            var dbabfxRFQStateMachine = _dbabfxFIXRfqOptAdptor.MakeSingleVanillaOptionRfqStateMachine(Buy_1M_BpGBPUSD_25DS_Put, 5_000, 10_000);
            var refId = dbabfxRFQStateMachine.StateMachineRefId;

            dbabfxRFQStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInDbabfxOption += dbabfxRFQStateMachine.OnInboundFIXMessage;

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInDbabfxOption?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("SendingQuoteRequest", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(dbabfxRFQStateMachine.CurrentState);

            dbabfxRFQStateMachine.Activate();

            // QuoteRequest message out
            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.AreEqual("WaitingForResponse", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(dbabfxRFQStateMachine.CurrentState);

            // QuoteCancel message in
            fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.QuoteCancel_Z, DateTime.UtcNow);
            FIXMsgInDbabfxOption?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("Done", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<DoneS>(dbabfxRFQStateMachine.CurrentState);
        }

        [Test]
        public void Test_QuoteRequest_Buy_1M_BpGBPUSD_25DS_Put()
        {
            var dbabfxRFQStateMachine = _dbabfxFIXRfqOptAdptor.MakeSingleVanillaOptionRfqStateMachine(Buy_1M_BpGBPUSD_25DS_Put, 5_000, 10_000);
            var refId = dbabfxRFQStateMachine.StateMachineRefId;

            dbabfxRFQStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInDbabfxOption += dbabfxRFQStateMachine.OnInboundFIXMessage;

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInDbabfxOption?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("SendingQuoteRequest", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(dbabfxRFQStateMachine.CurrentState);

            dbabfxRFQStateMachine.Activate();

            // QuoteRequest message out
            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.IsInstanceOf<DbabfxOptionsQuoteRequestFIXMessageArgs>(OutFIXMsgDtlEvtArgs.MessageArgs);

            var qrFIXMsgArgs = (DbabfxOptionsQuoteRequestFIXMessageArgs)OutFIXMsgDtlEvtArgs.MessageArgs;

            Assert.AreEqual("NY", qrFIXMsgArgs.ExpiryCutoff);
            Assert.AreEqual(BpGBPUSD.Substring(0, 3), qrFIXMsgArgs.Ccy1);
            Assert.AreEqual(BpGBPUSD.Substring(3, 3), qrFIXMsgArgs.Ccy2);
            Assert.AreEqual('1', qrFIXMsgArgs.HedgeTradeType);
            Assert.AreEqual("OPT", qrFIXMsgArgs.SecurityType);
            Assert.AreEqual("VAN", qrFIXMsgArgs.SecuritySubType);
            Assert.AreEqual('S', qrFIXMsgArgs.PremDeliveryType);

            var qrOptLegFIXMsgArgs = qrFIXMsgArgs.OptionLegs.First();

            Assert.IsNotNull(qrOptLegFIXMsgArgs);
            Assert.AreEqual("1M", qrOptLegFIXMsgArgs.TenorValue);
            Assert.AreEqual("OPECPN", qrOptLegFIXMsgArgs.CFICode);
            Assert.AreEqual('1', qrOptLegFIXMsgArgs.Side);
            Assert.AreEqual(25.0, qrOptLegFIXMsgArgs.StrikePrice);
            Assert.AreEqual("DS", qrOptLegFIXMsgArgs.StrikeType);
            Assert.AreEqual(1_000_000.0, qrOptLegFIXMsgArgs.Qty);
            Assert.AreEqual(string.Empty, qrOptLegFIXMsgArgs.MaturityDate);
        }

        [Test]
        public void Test_QuoteRequest_Sell_6M_PcUSDJPY_DN_Call()
        {
            var dbabfxRFQStateMachine = _dbabfxFIXRfqOptAdptor.MakeSingleVanillaOptionRfqStateMachine(Sell_6M_PcUSDJPY_DN_Call, 5_000, 10_000);
            var refId = dbabfxRFQStateMachine.StateMachineRefId;

            dbabfxRFQStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInDbabfxOption += dbabfxRFQStateMachine.OnInboundFIXMessage;

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInDbabfxOption?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("SendingQuoteRequest", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(dbabfxRFQStateMachine.CurrentState);

            dbabfxRFQStateMachine.Activate();

            // QuoteRequest message out
            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.IsInstanceOf<DbabfxOptionsQuoteRequestFIXMessageArgs>(OutFIXMsgDtlEvtArgs.MessageArgs);

            var qrFIXMsgArgs = (DbabfxOptionsQuoteRequestFIXMessageArgs)OutFIXMsgDtlEvtArgs.MessageArgs;

            Assert.AreEqual("NY", qrFIXMsgArgs.ExpiryCutoff);
            Assert.AreEqual(PcUSDJPY.Substring(0, 3), qrFIXMsgArgs.Ccy1);
            Assert.AreEqual(PcUSDJPY.Substring(3, 3), qrFIXMsgArgs.Ccy2);
            Assert.AreEqual('1', qrFIXMsgArgs.HedgeTradeType);
            Assert.AreEqual("OPT", qrFIXMsgArgs.SecurityType);
            Assert.AreEqual("VAN", qrFIXMsgArgs.SecuritySubType);
            Assert.AreEqual('S', qrFIXMsgArgs.PremDeliveryType);

            var qrOptLegFIXMsgArgs = qrFIXMsgArgs.OptionLegs.First();

            Assert.IsNotNull(qrOptLegFIXMsgArgs);
            Assert.AreEqual("6M", qrOptLegFIXMsgArgs.TenorValue);
            Assert.AreEqual("OCECPN", qrOptLegFIXMsgArgs.CFICode);
            Assert.AreEqual('2', qrOptLegFIXMsgArgs.Side);
            Assert.AreEqual(null, qrOptLegFIXMsgArgs.StrikePrice);
            Assert.AreEqual("DN", qrOptLegFIXMsgArgs.StrikeType);
            Assert.AreEqual(1_000_000.0, qrOptLegFIXMsgArgs.Qty);
            Assert.AreEqual(string.Empty, qrOptLegFIXMsgArgs.MaturityDate);
        }

        [Test]
        public void Test_QuoteRequest_Sell_1Y_EmUSDTRY_10DF_Call()
        {
            var dbabfxRFQStateMachine = _dbabfxFIXRfqOptAdptor.MakeSingleVanillaOptionRfqStateMachine(Sell_1Y_EmUSDTRY_10DF_Call, 5_000, 10_000);
            var refId = dbabfxRFQStateMachine.StateMachineRefId;

            dbabfxRFQStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInDbabfxOption += dbabfxRFQStateMachine.OnInboundFIXMessage;

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInDbabfxOption?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("SendingQuoteRequest", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(dbabfxRFQStateMachine.CurrentState);

            dbabfxRFQStateMachine.Activate();

            // QuoteRequest message out
            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.IsInstanceOf<DbabfxOptionsQuoteRequestFIXMessageArgs>(OutFIXMsgDtlEvtArgs.MessageArgs);

            var qrFIXMsgArgs = (DbabfxOptionsQuoteRequestFIXMessageArgs)OutFIXMsgDtlEvtArgs.MessageArgs;

            Assert.AreEqual("LO", qrFIXMsgArgs.ExpiryCutoff);
            Assert.AreEqual(EmUSDTRY.Substring(0, 3), qrFIXMsgArgs.Ccy1);
            Assert.AreEqual(EmUSDTRY.Substring(3, 3), qrFIXMsgArgs.Ccy2);
            Assert.AreEqual('1', qrFIXMsgArgs.HedgeTradeType);
            Assert.AreEqual("OPT", qrFIXMsgArgs.SecurityType);
            Assert.AreEqual("VAN", qrFIXMsgArgs.SecuritySubType);
            Assert.AreEqual('S', qrFIXMsgArgs.PremDeliveryType);

            var qrOptLegFIXMsgArgs = qrFIXMsgArgs.OptionLegs.First();

            Assert.IsNotNull(qrOptLegFIXMsgArgs);
            Assert.AreEqual("1Y", qrOptLegFIXMsgArgs.TenorValue);
            Assert.AreEqual("OCECPN", qrOptLegFIXMsgArgs.CFICode);
            Assert.AreEqual('2', qrOptLegFIXMsgArgs.Side);
            Assert.AreEqual(10.0, qrOptLegFIXMsgArgs.StrikePrice);
            Assert.AreEqual("DF", qrOptLegFIXMsgArgs.StrikeType);
            Assert.AreEqual(1_000_000.0, qrOptLegFIXMsgArgs.Qty);
            Assert.AreEqual(string.Empty, qrOptLegFIXMsgArgs.MaturityDate);
        }
    }
}
