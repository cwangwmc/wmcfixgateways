﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace WMC.FIXStateMachines.Tests
{
    public class TestStateMachines
    {
        protected string UUID { get; set; }

        protected FIXMessageDetailedEventArgs OutFIXMsgDtlEvtArgs { get; set; }

        private void Reset()
        {
            OutFIXMsgDtlEvtArgs = null;
        }

        [SetUp]
        public virtual void SetUp()
        {
            UUID = Guid.NewGuid().ToString();
            Reset();
        }

        [TearDown]
        public virtual void TearDown()
        {
            Reset();
        }

        [Test]
        public void TestGUID()
        {
            var uuid = Guid.NewGuid();

            Console.WriteLine(uuid);

            Assert.Greater(uuid.ToString().Length, 0);
        }

        [Test]
        public void TestHashes()
        {
            var utcNow = DateTime.UtcNow;

            Console.WriteLine(utcNow.ToString("o"));

            Assert.Greater(DateTime.UtcNow.GetHashCode().ToString().Length, 0);
        }

        protected virtual void OnStateMahcineOutboundFIXMessage(object sender, FIXMessageDetailedEventArgs args)
        {
            OutFIXMsgDtlEvtArgs = args;
        }
    }
}
