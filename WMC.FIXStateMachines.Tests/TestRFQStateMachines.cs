﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXDataObjects;

using NUnit.Framework;

namespace WMC.FIXStateMachines.Tests
{
    [TestFixture]
    public class TestRFQStateMachines : TestStateMachines
    {
        protected string BpGBPUSD { get; set; }
        protected string PcUSDJPY { get; set; }
        protected string EmUSDTRY { get; set; }

        protected string Tenor_1M { get; set; }
        protected string Tenor_6M { get; set; }
        protected string Tenor_1Y { get; set; }

        protected SingleVanillaOptionQuoteRequest Buy_1M_BpGBPUSD_25DS_Put { get; set; }
        protected SingleVanillaOptionQuoteRequest Sell_6M_PcUSDJPY_DN_Call { get; set; }
        protected SingleVanillaOptionQuoteRequest Sell_1Y_EmUSDTRY_10DF_Call { get; set; }

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            BpGBPUSD = "GBPUSD";
            PcUSDJPY = "USDJPY";
            EmUSDTRY = "USDTRY";

            Tenor_1M = "1M";
            Tenor_6M = "6M";
            Tenor_1Y = "1Y";

            var sglOptLeg = new SingleVanillaOptionContract(Tenor_1M, null, 25.0, OptionStrikeType.DeltaSpot, OptionCallPut.Put, BuySell.Buy,
                BpGBPUSD.Substring(0, 3), BpGBPUSD.Substring(3, 3));

            Buy_1M_BpGBPUSD_25DS_Put = new SingleVanillaOptionQuoteRequest("Buy_1M_BpGBPUSD_25DS_Put", BpGBPUSD, BpGBPUSD.Substring(0, 3), BpGBPUSD.Substring(3, 3), sglOptLeg);

            sglOptLeg = new SingleVanillaOptionContract(Tenor_6M, null, null, OptionStrikeType.DeltaNeutral, OptionCallPut.Call, BuySell.Sell,
                PcUSDJPY.Substring(0, 3), PcUSDJPY.Substring(3, 3));

            Sell_6M_PcUSDJPY_DN_Call = new SingleVanillaOptionQuoteRequest("Sell_6M_PcUSDJPY_DN_Call", PcUSDJPY, PcUSDJPY.Substring(0, 3), PcUSDJPY.Substring(3, 3), sglOptLeg);

            sglOptLeg = new SingleVanillaOptionContract(Tenor_1Y, null, 10.0, OptionStrikeType.DeltaForward, OptionCallPut.Call, BuySell.Sell,
                EmUSDTRY.Substring(0, 3), EmUSDTRY.Substring(3, 3));

            Sell_1Y_EmUSDTRY_10DF_Call = new SingleVanillaOptionQuoteRequest("Sell_1Y_EmUSDTRY_10DF_Call", EmUSDTRY, EmUSDTRY.Substring(0, 3), EmUSDTRY.Substring(3, 3), sglOptLeg);
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
        }

        [Test]
        public void Test_GenericRFQStateMachine()
        {

        }

    }
}
