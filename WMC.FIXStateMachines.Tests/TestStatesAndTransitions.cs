﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using NUnit.Framework;

namespace WMC.FIXStateMachines.Tests
{
    [TestFixture]
    public class TestStatesAndTransitions
    {
        protected bool StateEntered { get; set; }
        protected bool StateExited { get; set; }

        protected FIXMessageEventArgs OutFIXMessageEvtArgs { get; set; }
        protected StateTransitionEventArgs StateTransitionEvtArgs { get; set; }

        protected event EventHandler<FIXMessageEventArgs> FIXMessgeIn;

        protected void Reset()
        {
            StateEntered = false;
            StateExited = false;

            OutFIXMessageEvtArgs = null;
            StateTransitionEvtArgs = null;
        }

        [SetUp]
        public void SetUp()
        {
            Reset();
        }

        [TearDown]
        public void TearDown()
        {
            Reset();
        }

        [Test]
        public void TestRootState()
        {
            var name = "Root";
            var refId = Guid.NewGuid().ToString();
            var rootState = new State(name, refId);

            Assert.AreEqual(name, rootState.Name);
            Assert.AreEqual(refId, rootState.StateMachineRefId);

            rootState.StateEntered += OnStateEntered;
            rootState.StateExited += OnStateExited;

            rootState.OnEnter();
            Assert.AreEqual(StateEntered, true);

            rootState.OnExit();
            Assert.AreEqual(StateExited, true);
        }

        [Test]
        public void TestWaitingForLogonState()
        {

            var uuid = Guid.NewGuid().ToString();
            var refId = String.Format("{0}-{1}", "USDJPY", uuid);
            var waitingForLogonS = new WaitingForLogonS(refId);
            var doneS = new DoneS(refId);
            var logonT = new LogonT(waitingForLogonS, doneS);

            FIXMessgeIn += waitingForLogonS.OnFIXMessageIn;

            logonT.Triggered += OnTransitionTriggered;
            waitingForLogonS.StateExited += OnStateExited;
            doneS.StateEntered += OnStateEntered;

            Assert.AreEqual("WaitingForLogon", waitingForLogonS.Name);
            Assert.AreEqual(refId, waitingForLogonS.StateMachineRefId);

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateEntered);
            Assert.IsFalse(StateExited);

            var logonMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, logonMsgEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(StateTransitionEvtArgs.FromState.Name, waitingForLogonS.Name);
            Assert.AreEqual(StateTransitionEvtArgs.ToState.Name, doneS.Name);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            FIXMessgeIn -= waitingForLogonS.OnFIXMessageIn;
        }

        [Test]
        public void TestSendingMarketDataRequestState()
        {

            var uuid = Guid.NewGuid().ToString();
            var refId = String.Format("{0}-{1}", "GBPUSD", uuid);
            var sendingMDReqS = new SendingMarketDataRequestS(refId, 5_000);
            var idleS_A = new DoneS(refId);
            var idleS_B = new DoneS(refId);

            FIXMessgeIn += sendingMDReqS.OnFIXMessageIn;

            sendingMDReqS.StateEntered += OnStateEntered;
            sendingMDReqS.StateExited += OnStateExited;
            sendingMDReqS.StateFIXMessageOut += OnStateFIXMessageOut;

            idleS_A.StateEntered += OnStateEntered;
            idleS_B.StateEntered += OnStateEntered;

            var MDRequestT = new MDRequestT(sendingMDReqS, idleS_A);
            var logoffT = new LogoffT(sendingMDReqS, idleS_B);

            MDRequestT.Triggered += OnTransitionTriggered;
            logoffT.Triggered += OnTransitionTriggered;

            Assert.AreEqual("SendingMarketDataRequest", sendingMDReqS.Name);
            Assert.AreEqual("Done", idleS_A.Name);
            Assert.AreEqual("Done", idleS_B.Name);
            
            sendingMDReqS.OnEnter();
            Assert.IsTrue(StateEntered);
            Thread.Sleep(8_000);

            Assert.IsNotNull(OutFIXMessageEvtArgs);
            Assert.AreEqual(refId, OutFIXMessageEvtArgs.StateMachineRefId);
            Assert.AreEqual(FIXMessageType.MarketDataRequest_V, OutFIXMessageEvtArgs.MessageType);
            
            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(idleS_A, StateTransitionEvtArgs.ToState);
            Assert.AreEqual(sendingMDReqS, StateTransitionEvtArgs.FromState);
            Assert.IsTrue(StateEntered);
            Assert.IsTrue(StateExited);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateEntered);
            Assert.IsFalse(StateExited);

            var logoffMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logoff_5, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, logoffMsgEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(idleS_B, StateTransitionEvtArgs.ToState);
            Assert.AreEqual(sendingMDReqS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(true, StateExited);
            Assert.AreEqual(true, StateEntered);

            FIXMessgeIn -= sendingMDReqS.OnFIXMessageIn;
        }

        [Test]
        public void TestSendQuoteRequestState()
        {

            var uuid = Guid.NewGuid().ToString();
            var refId = String.Format("{0}-{1}", "CADCHF", uuid);
            var sendingQuoteReqS = new SendingQuoteRequestS(refId, 5_000);
            var idleS_A = new DoneS(refId);
            var idleS_B = new DoneS(refId);

            FIXMessgeIn += sendingQuoteReqS.OnFIXMessageIn;

            sendingQuoteReqS.StateFIXMessageOut += OnStateFIXMessageOut;
            sendingQuoteReqS.StateEntered += OnStateEntered;
            sendingQuoteReqS.StateExited += OnStateExited;

            idleS_A.StateEntered += OnStateEntered;
            idleS_B.StateEntered += OnStateEntered;

            var quoteReqT = new QuoteRequestT(sendingQuoteReqS, idleS_A);
            var logoffT = new LogoffT(sendingQuoteReqS, idleS_B);

            quoteReqT.Triggered += OnTransitionTriggered;
            logoffT.Triggered += OnTransitionTriggered;

            Assert.AreEqual("SendingQuoteRequest", sendingQuoteReqS.Name);

            sendingQuoteReqS.OnEnter();
            Thread.Sleep(8_000);

            Assert.IsNotNull(OutFIXMessageEvtArgs);
            Assert.AreEqual(refId, OutFIXMessageEvtArgs.StateMachineRefId);
            Assert.AreEqual(FIXMessageType.QuoteRequest_R, OutFIXMessageEvtArgs.MessageType);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(sendingQuoteReqS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_A, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateEntered);
            Assert.IsFalse(StateExited);

            var logoffMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logoff_5, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, logoffMsgEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(idleS_B, StateTransitionEvtArgs.ToState);
            Assert.AreEqual(sendingQuoteReqS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(true, StateExited);
            Assert.AreEqual(true, StateEntered);

            FIXMessgeIn -= sendingQuoteReqS.OnFIXMessageIn;
        }

        [Test]
        public void TestWaitForResponseState()
        {

            var uuid = Guid.NewGuid().ToString();
            var refId = String.Format("{0}-{1}", "EURJPY", uuid);
            var waitForRespS = new WaitingForResponseS(refId, 5_000);
            var idleS_A = new DoneS(refId);
            var idleS_B = new DoneS(refId);
            var idleS_C = new DoneS(refId);

            FIXMessgeIn += waitForRespS.OnFIXMessageIn;

            waitForRespS.StateExited += OnStateExited;
            waitForRespS.StateEntered += OnStateEntered;

            var mdRefreshT = new MDRefreshT(waitForRespS, idleS_A);
            var quoteT = new QuoteT(waitForRespS, idleS_A);

            var mdRejT = new MDRequestRejectT(waitForRespS, idleS_B);
            var quoteCancelT = new QuoteCancelT(waitForRespS, idleS_B);

            var timeoutT = new TimedOutT(waitForRespS, idleS_C);

            mdRefreshT.Triggered += OnTransitionTriggered;
            quoteT.Triggered += OnTransitionTriggered;
            mdRejT.Triggered += OnTransitionTriggered;
            quoteCancelT.Triggered += OnTransitionTriggered;
            timeoutT.Triggered += OnTransitionTriggered;

            idleS_A.StateEntered += OnStateEntered;
            idleS_B.StateEntered += OnStateEntered;
            idleS_C.StateEntered += OnStateEntered;

            Assert.AreEqual("WaitingForResponse", waitForRespS.Name);

            var mdSnpshtFullRefreshEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.MarketDataSnapshotFullRefresh_W, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, mdSnpshtFullRefreshEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(waitForRespS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_A, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateExited);
            Assert.IsFalse(StateEntered);

            var mdIncrRefreshEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.MarketDataInrementalRefresh_X, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, mdIncrRefreshEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(waitForRespS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_A, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateExited);
            Assert.IsFalse(StateEntered);

            var quoteEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Quote_S, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, quoteEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(waitForRespS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_A, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateExited);
            Assert.IsFalse(StateEntered);

            var mdRequestRejectedEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.MarketDataRequestReject_Y, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, mdRequestRejectedEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(waitForRespS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_B, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateExited);
            Assert.IsFalse(StateEntered);

            var quoteCancelEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.QuoteCancel_Z, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, quoteCancelEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(waitForRespS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_B, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateExited);
            Assert.IsFalse(StateEntered);

            waitForRespS.OnEnter();
            Thread.Sleep(8_000);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(waitForRespS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_C, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            FIXMessgeIn -= waitForRespS.OnFIXMessageIn;
        }

        [Test]
        public void TestStreamingMarketDataState()
        {

            var uuid = Guid.NewGuid().ToString();
            var refId = String.Format("{0}-{1}", "USDJPY", uuid);
            var streamingMDS = new StreamingMarketDataS(refId, 5_000);
            var idleS_A = new DoneS(refId);
            var idleS_B = new DoneS(refId);

            FIXMessgeIn += streamingMDS.OnFIXMessageIn;

            streamingMDS.StateExited += OnStateExited;
            streamingMDS.StateEntered += OnStateEntered;
            streamingMDS.StateFIXMessageOut += OnStateFIXMessageOut;

            idleS_A.StateEntered += OnStateEntered;
            idleS_B.StateEntered += OnStateEntered;

            var MDRefreshT = new MDRefreshT(streamingMDS, streamingMDS);
            var logoffT = new LogoffT(streamingMDS, idleS_A);

            MDRefreshT.Triggered += OnTransitionTriggered;
            logoffT.Triggered += OnTransitionTriggered;

            Assert.AreEqual("StreamingMarketData", streamingMDS.Name);
            Assert.AreEqual("Done", idleS_A.Name);
            Assert.AreEqual("Done", idleS_B.Name);

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateEntered);
            Assert.IsFalse(StateExited);

            var logoffMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logoff_5, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, logoffMsgEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(idleS_A, StateTransitionEvtArgs.ToState);
            Assert.AreEqual(streamingMDS, StateTransitionEvtArgs.FromState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateEntered);
            Assert.IsFalse(StateExited);

            var MDSnpshtFullRefreshMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.MarketDataSnapshotFullRefresh_W, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, MDSnpshtFullRefreshMsgEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(streamingMDS, StateTransitionEvtArgs.ToState);
            Assert.AreEqual(streamingMDS, StateTransitionEvtArgs.FromState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateEntered);
            Assert.IsFalse(StateExited);

            var MDIncrRefreshMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.MarketDataInrementalRefresh_X, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, MDIncrRefreshMsgEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(streamingMDS, StateTransitionEvtArgs.ToState);
            Assert.AreEqual(streamingMDS, StateTransitionEvtArgs.FromState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            FIXMessgeIn -= streamingMDS.OnFIXMessageIn;
        }

        [Test]
        public void TestQuoteReceivedState()
        {
            var uuid = Guid.NewGuid().ToString();
            var refId = String.Format("{0}-{1}", "CADJPY", uuid);
            var quoteRcvdS = new QuoteReceivedS(refId);
            var idleS_A = new DoneS(refId);
            var idleS_B = new DoneS(refId);

            var quoteT = new QuoteT(quoteRcvdS, idleS_A);
            var quoteCnclT = new QuoteCancelT(quoteRcvdS, idleS_B);

            quoteRcvdS.StateExited += OnStateExited;
            idleS_A.StateEntered += OnStateEntered;
            idleS_B.StateEntered += OnStateEntered;

            quoteT.Triggered += OnTransitionTriggered;
            quoteCnclT.Triggered += OnTransitionTriggered;

            FIXMessgeIn += quoteRcvdS.OnFIXMessageIn;

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateEntered);
            Assert.IsFalse(StateExited);

            var quoteEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Quote_S, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, quoteEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(quoteRcvdS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_A, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            Reset();

            Assert.IsNull(StateTransitionEvtArgs);
            Assert.IsFalse(StateEntered);
            Assert.IsFalse(StateExited);

            var quoteCnclEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.QuoteCancel_Z, DateTime.UtcNow);
            FIXMessgeIn?.Invoke(this, quoteCnclEvtArgs);

            Assert.IsNotNull(StateTransitionEvtArgs);
            Assert.AreEqual(quoteRcvdS, StateTransitionEvtArgs.FromState);
            Assert.AreEqual(idleS_B, StateTransitionEvtArgs.ToState);
            Assert.IsTrue(StateExited);
            Assert.IsTrue(StateEntered);

            FIXMessgeIn -= quoteRcvdS.OnFIXMessageIn;
        }

        protected void OnStateEntered(object sender, EventArgs args)
        {
            StateEntered = true;
        }

        protected void OnStateExited(object sender, EventArgs args)
        {
            StateExited = true;
        }

        protected void OnStateFIXMessageOut(object sender, FIXMessageEventArgs args)
        {
            OutFIXMessageEvtArgs = args;
        }

        protected void OnTransitionTriggered(object sender, StateTransitionEventArgs args)
        {
            StateTransitionEvtArgs = args;
        }
    }
}
