﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using WMC.FIXGateways.Jpm;

using NUnit.Framework;

namespace WMC.FIXStateMachines.Tests
{
    [TestFixture]
    public class TestJpmVolSurfaceStateMachines : TestStateMachines
    {


    }

    [TestFixture]
    public class TestJpmOptionsRfqStateMachines : TestRFQStateMachines
    {

        public event EventHandler<FIXMessageEventArgs> FIXMsgInJpmOptionsRfq;

        private JpmFIXRfqAdaptor _jpmFIXRfqAdaptor;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _jpmFIXRfqAdaptor = new JpmFIXRfqAdaptor("mock_account_id", "mock_password");
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();

            FIXMsgInJpmOptionsRfq = null;
        }

        [Test]
        public void Test_Quote_Workflow_from_Logoff()
        {

            var jpmOptRfqStateMachine = _jpmFIXRfqAdaptor.MakeSingleVanillaOptionRfqStateMachine(Buy_1M_BpGBPUSD_25DS_Put, 5_000, 10_000);
            var refId = jpmOptRfqStateMachine.StateMachineRefId;

            jpmOptRfqStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInJpmOptionsRfq += jpmOptRfqStateMachine.OnInboundFIXMessage;

            Assert.AreEqual("WaitingForLogon", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForLogonS>(jpmOptRfqStateMachine.CurrentState);

            jpmOptRfqStateMachine.Activate();

            Thread.Sleep(7_000);
            Assert.IsNull(OutFIXMsgDtlEvtArgs);

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            // QuoteRequest message out
            Assert.AreEqual("SendingQuoteRequest", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(jpmOptRfqStateMachine.CurrentState);

            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.AreEqual("WaitingForResponse", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(jpmOptRfqStateMachine.CurrentState);

            // Quote Ack Accept message in
            fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.QuoteAck_b_Accepted, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("WaitingForResponse", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(jpmOptRfqStateMachine.CurrentState);

            // Quote message in
            fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Quote_S, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("Done", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<DoneS>(jpmOptRfqStateMachine.CurrentState);

        }


        [Test]
        public void Test_QuoteAckExpired_Workflow_from_Logoff()
        {

            var jpmOptRfqStateMachine = _jpmFIXRfqAdaptor.MakeSingleVanillaOptionRfqStateMachine(Buy_1M_BpGBPUSD_25DS_Put, 5_000, 10_000);
            var refId = jpmOptRfqStateMachine.StateMachineRefId;

            jpmOptRfqStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInJpmOptionsRfq += jpmOptRfqStateMachine.OnInboundFIXMessage;

            Assert.AreEqual("WaitingForLogon", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForLogonS>(jpmOptRfqStateMachine.CurrentState);

            jpmOptRfqStateMachine.Activate();

            Thread.Sleep(7_000);
            Assert.IsNull(OutFIXMsgDtlEvtArgs);

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            // QuoteRequest message out
            Assert.AreEqual("SendingQuoteRequest", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(jpmOptRfqStateMachine.CurrentState);

            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.AreEqual("WaitingForResponse", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(jpmOptRfqStateMachine.CurrentState);

            // Quote Ack Accept message in
            fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.QuoteAck_b_Accepted, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("WaitingForResponse", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(jpmOptRfqStateMachine.CurrentState);

            // Quote message in
            fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Quote_S, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("Done", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<DoneS>(jpmOptRfqStateMachine.CurrentState);

            //Quote Ack Expired message in
            fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.QuoteAck_b_Expired, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("Done", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<DoneS>(jpmOptRfqStateMachine.CurrentState);

        }

        [Test]
        public void Test_QuoteAckReject_Workflow()
        {

            var jpmOptRfqStateMachine = _jpmFIXRfqAdaptor.MakeSingleVanillaOptionRfqStateMachine(Buy_1M_BpGBPUSD_25DS_Put, 5_000, 10_000);
            var refId = jpmOptRfqStateMachine.StateMachineRefId;

            jpmOptRfqStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInJpmOptionsRfq += jpmOptRfqStateMachine.OnInboundFIXMessage;

            Assert.AreEqual("WaitingForLogon", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForLogonS>(jpmOptRfqStateMachine.CurrentState);

            jpmOptRfqStateMachine.Activate();

            Thread.Sleep(7_000);
            Assert.IsNull(OutFIXMsgDtlEvtArgs);

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            // QuoteRequest message out
            Assert.AreEqual("SendingQuoteRequest", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(jpmOptRfqStateMachine.CurrentState);

            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.AreEqual("WaitingForResponse", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(jpmOptRfqStateMachine.CurrentState);

            // Quote Ack Reject message in
            fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.QuoteAck_b_Rejected, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("Done", jpmOptRfqStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<DoneS>(jpmOptRfqStateMachine.CurrentState);

        }

        [Test]
        public void Test_QuoteRequest_Buy_1M_BpGBPUSD_25DS_Put()
        {

            var dbabfxRFQStateMachine = _jpmFIXRfqAdaptor.MakeSingleVanillaOptionRfqStateMachine(Buy_1M_BpGBPUSD_25DS_Put, 5_000, 10_000);
            var refId = dbabfxRFQStateMachine.StateMachineRefId;

            dbabfxRFQStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;
            FIXMsgInJpmOptionsRfq += dbabfxRFQStateMachine.OnInboundFIXMessage;

            // Logon message in
            var fixMsgEvtArgs = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgInJpmOptionsRfq?.Invoke(this, fixMsgEvtArgs);

            Assert.AreEqual("SendingQuoteRequest", dbabfxRFQStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingQuoteRequestS>(dbabfxRFQStateMachine.CurrentState);

            dbabfxRFQStateMachine.Activate();

            // QuoteRequest message out
            Thread.Sleep(7_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.IsInstanceOf<JpmFxOptionsQuoteRequestFIXMessageArgs>(OutFIXMsgDtlEvtArgs.MessageArgs);

            var qrFIXMsgArgs = (JpmFxOptionsQuoteRequestFIXMessageArgs)OutFIXMsgDtlEvtArgs.MessageArgs;

            Assert.AreEqual('1', qrFIXMsgArgs.QuoteRequestSubscriptionType);
            Assert.AreEqual('Y', qrFIXMsgArgs.IsRFQ);
            Assert.AreEqual("GBP", qrFIXMsgArgs.Ccy1);
            Assert.AreEqual("USD", qrFIXMsgArgs.Ccy2);
            Assert.AreEqual("VANILLA", qrFIXMsgArgs.SecurityDesc);
            Assert.AreEqual("PREMIUM", qrFIXMsgArgs.SolvingFor);
            Assert.AreEqual("OPT", qrFIXMsgArgs.SecurityType);
            Assert.AreEqual(2, qrFIXMsgArgs.QuoteRquestType);
            Assert.AreEqual('1', qrFIXMsgArgs.Side);
            Assert.AreEqual("GBP", qrFIXMsgArgs.Currency);
            Assert.AreEqual(2, qrFIXMsgArgs.HedgeType);
            Assert.AreEqual("GBP", qrFIXMsgArgs.PremCcy);
            Assert.AreEqual("%GBP", qrFIXMsgArgs.PxRefMetric);
            Assert.AreEqual(0, qrFIXMsgArgs.PremPayOn);

            var qrOptLegFIXMsgArgs = qrFIXMsgArgs.OptionLegs.First();

            Assert.IsNotNull(qrOptLegFIXMsgArgs);
            Assert.AreEqual('1', qrOptLegFIXMsgArgs.Side);
            Assert.AreEqual(0, qrOptLegFIXMsgArgs.PutOrCall);
            Assert.AreEqual(1_000_000.0, qrOptLegFIXMsgArgs.Qty);
            Assert.AreEqual("25D", qrOptLegFIXMsgArgs.Strike);
            Assert.AreEqual("1M", qrOptLegFIXMsgArgs.Tenor);
            Assert.AreEqual("NYC", qrOptLegFIXMsgArgs.ExpirationCut);
            Assert.AreEqual(0, qrOptLegFIXMsgArgs.SettlementType);
            Assert.True(string.IsNullOrEmpty(qrOptLegFIXMsgArgs.SettlementCcy));

        }

    }
}
