﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using NUnit.Framework;

using WMC.FIXStateMachines;
using WMC.FIXGateways.Citi;
using WMC.FIXGateways.Dbabfx;
using WMC.FIXGateways.Jpm;

namespace WMC.FIXStateMachines.Tests
{
    [TestFixture]
    public class TestCitiStateMachines : TestIndicativeVolSurfaceStateMachines
    {
        protected string RefId { get; set; }
        public event EventHandler<FIXMessageEventArgs> FIXMsgIn;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            FIXMsgIn = null;
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
            FIXMsgIn = null;
        }

        [Test]
        public void Test_CitiIndicativeVolSurfaceStateMachine()
        {

            var uuid = Guid.NewGuid();
            var securitySymbol = "USDJPY";
            var refId = String.Format("{0}-{1}", securitySymbol, uuid);

            var citiIndicativeVolSurfStateMachine = new CitiIndicativeVolSurfaceStateMachine(refId, securitySymbol, false, 5_000, null, null);
            
            // Outbound FIX message from the state machine
            citiIndicativeVolSurfStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;

            // Inbound FIX message for the state machine
            FIXMsgIn += citiIndicativeVolSurfStateMachine.OnInboundFIXMessage;

            Assert.AreEqual("WaitingForLogon", citiIndicativeVolSurfStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForLogonS>(citiIndicativeVolSurfStateMachine.CurrentState);

            citiIndicativeVolSurfStateMachine.Activate();
            
            var logonEvtMsg = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgIn?.Invoke(this, logonEvtMsg);

            Assert.AreEqual("SendingMarketDataRequest", citiIndicativeVolSurfStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingMarketDataRequestS>(citiIndicativeVolSurfStateMachine.CurrentState);
            Assert.IsNull(OutFIXMsgDtlEvtArgs);

            Thread.Sleep(8_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.AreEqual(FIXMessageType.MarketDataRequest_V, OutFIXMsgDtlEvtArgs.MessageType);
            Assert.IsInstanceOf<MarketDataRequestFIXMessageArgs>(OutFIXMsgDtlEvtArgs.MessageArgs);
            Assert.AreEqual("USD", ((MarketDataRequestFIXMessageArgs)OutFIXMsgDtlEvtArgs.MessageArgs).Ccy1);
            Assert.AreEqual("JPY", ((MarketDataRequestFIXMessageArgs)OutFIXMsgDtlEvtArgs.MessageArgs).Ccy2);

            Assert.AreEqual("WaitingForResponse", citiIndicativeVolSurfStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(citiIndicativeVolSurfStateMachine.CurrentState);

            for (int i = 0; i < 5; i++)
            {
                var MDSnpShtEvtArg = new FIXMessageEventArgs(refId, FIXMessageType.MarketDataSnapshotFullRefresh_W, DateTime.UtcNow);
                FIXMsgIn?.Invoke(this, MDSnpShtEvtArg);

                Assert.AreEqual("StreamingMarketData", citiIndicativeVolSurfStateMachine.CurrentState.Name);
                Assert.IsInstanceOf<StreamingMarketDataS>(citiIndicativeVolSurfStateMachine.CurrentState);
            }

            var logoffEvtArg = new FIXMessageEventArgs(refId, FIXMessageType.Logoff_5, DateTime.UtcNow);
            FIXMsgIn?.Invoke(this, logoffEvtArg);

            Assert.AreEqual("WaitingForLogon", citiIndicativeVolSurfStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForLogonS>(citiIndicativeVolSurfStateMachine.CurrentState);

            FIXMsgIn -= citiIndicativeVolSurfStateMachine.OnInboundFIXMessage;
        }

        [Test]
        public void Test_CitiIndicativeVolSurfaceStateMachine_MDRequestReject()
        {
            var uuid = Guid.NewGuid();
            var securitySymbol = "USDJPY";
            var refId = String.Format("{0}-{1}", securitySymbol, uuid);

            var citiIndicativeVolSurfStateMachine = new CitiIndicativeVolSurfaceStateMachine(refId, securitySymbol, false, 5_000, null, null);

            // Outbound FIX message from the state machine
            citiIndicativeVolSurfStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;

            // Inbound FIX message for the state machine
            FIXMsgIn += citiIndicativeVolSurfStateMachine.OnInboundFIXMessage;

            Assert.AreEqual("WaitingForLogon", citiIndicativeVolSurfStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForLogonS>(citiIndicativeVolSurfStateMachine.CurrentState);

            citiIndicativeVolSurfStateMachine.Activate();

            var logonEvtMsg = new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow);
            FIXMsgIn?.Invoke(this, logonEvtMsg);

            Assert.AreEqual("SendingMarketDataRequest", citiIndicativeVolSurfStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingMarketDataRequestS>(citiIndicativeVolSurfStateMachine.CurrentState);
            Assert.IsNull(OutFIXMsgDtlEvtArgs);

            Thread.Sleep(8_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            Assert.AreEqual(FIXMessageType.MarketDataRequest_V, OutFIXMsgDtlEvtArgs.MessageType);
            Assert.IsInstanceOf<MarketDataRequestFIXMessageArgs>(OutFIXMsgDtlEvtArgs.MessageArgs);
            Assert.AreEqual("USD", ((MarketDataRequestFIXMessageArgs)OutFIXMsgDtlEvtArgs.MessageArgs).Ccy1);
            Assert.AreEqual("JPY", ((MarketDataRequestFIXMessageArgs)OutFIXMsgDtlEvtArgs.MessageArgs).Ccy2);

            Assert.AreEqual("WaitingForResponse", citiIndicativeVolSurfStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<WaitingForResponseS>(citiIndicativeVolSurfStateMachine.CurrentState);
        }

        [Test]
        public void Test_CitiIndicativeVolSurfaceStateMachine_LoggedOn()
        {
            var uuid = Guid.NewGuid();
            var securitySymbol = "USDCAD";
            var refId = String.Format("{0}-{1}", securitySymbol, uuid);
            
            var indicativeMDStateMachine = new CitiIndicativeVolSurfaceStateMachine(refId, securitySymbol, true, 5_000, null, null);

            indicativeMDStateMachine.StateMachineFIXMessageOut += OnStateMahcineOutboundFIXMessage;

            FIXMsgIn += indicativeMDStateMachine.OnInboundFIXMessage;

            Assert.AreEqual("SendingMarketDataRequest", indicativeMDStateMachine.CurrentState.Name);
            Assert.IsInstanceOf<SendingMarketDataRequestS>(indicativeMDStateMachine.CurrentState);

            Thread.Sleep(8_000);
            Assert.IsNull(OutFIXMsgDtlEvtArgs);

            indicativeMDStateMachine.Activate();

            Thread.Sleep(8_000);
            Assert.IsNotNull(OutFIXMsgDtlEvtArgs);

            FIXMsgIn -= indicativeMDStateMachine.OnInboundFIXMessage;
        }
    }
}
