﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXDataObjects;

namespace WMC.FXConventionHelper
{
    public interface iFxVolSurfaceConvHelper
    {
        DeltaType VolSurfaceWingDeltaType(string ccypair, string tenor);

        Atm VolSurfaceAtmType(string ccypair, string tenor);
    }

    public interface iFxVolExpiryCutCodeHelper
    {
        string VolExpiryCutCode(string ccypair);
    }
}
