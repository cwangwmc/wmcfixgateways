﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FXConventionHelper
{
    public abstract class FXConvHelper
    {

        public static IEnumerable<string> G10_Currencies =
            new HashSet<string> { "USD", "EUR", "GBP", "JPY", "AUD", "NZD", "CAD", "CHF", "NOK", "SEK" };

        public static IEnumerable<string> EM_Non_Deliverable_Currencies =
            new HashSet<string> { "BRL", "CLP", "CNY", "COP", "IDR", "INR", "KRW", "MYR", "PHP", "THB", "TWD" };

        public static IEnumerable<string> EM_Currencies =
            new HashSet<string> { "ARS", "BRL", "CLP", "COP", "CNH", "CNY", "CZK", "EGP", "HUF", "IDR", "ILS", "INR",
                                  "KRW", "MXN", "MYR", "PEN", "PHP", "PKR", "PLN", "RUB", "THB", "TRY", "TWD", "ZAR" };

        public static IEnumerable<string> EM_Latam_Currencies =
            new HashSet<string> { "ARS", "BRL", "CLP", "COP", "MXN", "PEN" };

        public static bool IsG10(string ccypair)
        {
            var ccy1 = ccypair.Substring(0, 3);
            var ccy2 = ccypair.Substring(3, 3);

            return G10_Currencies.Contains(ccy1) && G10_Currencies.Contains(ccy2);
        }

        public static bool IsEM(string ccypair)
        {
            var ccy1 = ccypair.Substring(0, 3);
            var ccy2 = ccypair.Substring(3, 3);

            return EM_Currencies.Contains(ccy1) || EM_Currencies.Contains(ccy2);
        }

        public static bool IsNDF(string ccypair)
        {
            var ccy1 = ccypair.Substring(0, 3);
            var ccy2 = ccypair.Substring(3, 3);

            return EM_Non_Deliverable_Currencies.Contains(ccy1) || EM_Non_Deliverable_Currencies.Contains(ccy2);
        }

        public static bool IsEMLatam(string ccypair)
        {
            var ccy1 = ccypair.Substring(0, 3);
            var ccy2 = ccypair.Substring(3, 3);

            return EM_Latam_Currencies.Contains(ccy1) || EM_Latam_Currencies.Contains(ccy2);
        }

    }
}
