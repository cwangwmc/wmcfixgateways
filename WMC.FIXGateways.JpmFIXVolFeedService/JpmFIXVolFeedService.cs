﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

using WMC.FIXGateways.Jpm;

using log4net;

using QuickFix;
using QuickFix.Transport;

namespace WMC.FIXGateways.JpmFIXVolFeedService
{
    public partial class JpmFIXVolFeedService : ServiceBase
    {
        private static readonly log4net.ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private SocketInitiator _initiator = null;

        public JpmFIXVolFeedService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

            var password = ConfigurationManager.AppSettings["password"];
            var session_settings_file = ConfigurationManager.AppSettings["session.settings.config.file"];
            var vols_currencypairs_file = ConfigurationManager.AppSettings["vols.currencypairs.file"];

            var vols_ccypairs = ReadCcypairsFile(vols_currencypairs_file);

            IApplication jpmFIXVolFeedApp = new JpmFIXMarketDataAdaptor(password, vols_ccypairs);

            var sessionSettings = new SessionSettings(session_settings_file);
            var storeFactory = new FileStoreFactory(sessionSettings);
            var logFactory = new FileLogFactory(sessionSettings);

            _initiator = new SocketInitiator(jpmFIXVolFeedApp, storeFactory, sessionSettings, logFactory);

            _log.Info("Starting FIX Initiator...");

            _initiator.Start();

            _log.Info("Started.");
        }

        protected override void OnStop()
        {
            _log.Info("Stopping FIX Initiator...");

            _initiator.Stop();

            _log.Info("Stopped.");
        }

        private static IList<string> ReadCcypairsFile(string filename)
        {
            return File.ReadAllLines(filename).ToList();
        }
    }
}
