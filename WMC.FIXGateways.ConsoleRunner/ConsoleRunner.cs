﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Configuration;

using WMC.FIXGateways;
using WMC.FIXKafkaClients;
using WMC.FIXDataObjects;

using QuickFix;
using QuickFix.Transport;

using log4net;
using log4net.Config;

namespace WMC.FIXGateways
{
    public class ConsoleRunner
    {

        private static readonly log4net.ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string[] _FIXTargetMode = { "citi.vol.uat", "citi.vol.prd", "citi.rfq.uat",
                                                            "dbabfx.rfq", "dbabfx.rfq.tenordates", "dbabfx.rfq.vols", "dbabfx.sptfwd",
                                                            "jpm.vol.uat", "jpm.vol.prd", "jpm.rfq.uat", "jpm.rfq.prd", "jpm.rfq.vols.uat", "jpm.rfq.vols.prd" };

        private static readonly string[] _KafkaConsumerModes = { "kafka.spt", "kafka.spt.a", "kafka.spt.c" };

        private static readonly string[] _AdminModes = { "app.settings", "vols.ccypairs", "rfq.vols.ccypairs", "rfq.tenordates.ccypairs", "rfq.vols.tenors", "rfq.tenordates.tenors" };

        static void Main(string[] args)
        {

            if (args.Length == 0)
            {
                Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
                Console.WriteLine("No arguments provided. Exiting...");
                return;
            }

            XmlConfigurator.Configure();

            var arg0 = args[0].ToLower();

            if (_FIXTargetMode.Contains(arg0))
            {
                if (args.Length == 1)
                {
                    Console.WriteLine("FIX Session Settings file is missing for target party: {0}", arg0);
                    return;
                }

                var FIXTargetParty = arg0;
                var FIXTargetSessionSettings = args[1];

                LaunchFIXInitiator(FIXTargetParty, FIXTargetSessionSettings);
            }
            else if (_KafkaConsumerModes.Contains(arg0))
            {
                var topic = string.Empty;

                if (args.Length > 1)
                    topic = args[1];

                LaunchKafkaConsumerClient(arg0);
            }
            else if (_AdminModes.Contains(arg0))
            {
                LaunchAdminConsole(arg0);
            }
            else
                Console.WriteLine("Argument {0} unrecognised. Exiting ...", arg0);

        }

        public static void LaunchKafkaConsumerClient(string kafkaConsumerMode)
        {
            if (kafkaConsumerMode == "kafka.spt")
            {
                var kafkaSpotsConsumer = new FIXKafkaSpotsConsumer();
                kafkaSpotsConsumer.StartConsumingSpots(KafkaSpotFeedSource.fenics);
            }
            else if (kafkaConsumerMode == "kafka.spt.a")
            {
                FIXKafkaSpotsConsumer.AssignConsumeSpots(KafkaSpotFeedSource.fenics);
            }
            else if (kafkaConsumerMode == "kafka.spt.s")
            {
                FIXKafkaSpotsConsumer.SubscribeConsumeSpots(KafkaSpotFeedSource.fenics);
            }
        }

        public static void LaunchFIXInitiator(string targetParty, string targetPartySessionSettings)
        {
            var vols_currencypairs_file = ConfigurationManager.AppSettings["vols.currencypairs.file"];
            var rfq_vols_currencypairs_file = ConfigurationManager.AppSettings["rfq.vols.currencypairs.file"];
            var rfq_tds_currencypairs_file = ConfigurationManager.AppSettings["rfq.tenordates.currencypairs.file"];
            var rfqs_file = ConfigurationManager.AppSettings["rfqs.file"];
            var rfqs_concurrent = int.Parse(ConfigurationManager.AppSettings["rfqs.concurrent"]);

            IApplication iApp = null;

            if (targetParty.ToLower() == "citi.vol.uat")
            {
                var citi_vol_uat_acc = ConfigurationManager.AppSettings["citi.vol.uat.account"];
                var citi_vol_uat_pwd = ConfigurationManager.AppSettings["citi.vol.uat.password"];

                var vols_ccypairs = ReadCcypairsFile(vols_currencypairs_file);

                iApp = new Citi.CitiFIXMarketDataAdaptor(citi_vol_uat_acc, citi_vol_uat_pwd, vols_ccypairs);
            }
            else if (targetParty.ToLower() == "citi.vol.prd")
            {
                var citi_vol_prd_acc = ConfigurationManager.AppSettings["citi.vol.prd.account"];
                var citi_vol_prd_pwd = ConfigurationManager.AppSettings["citi.vol.prd.password"];

                var vols_ccypairs = ReadCcypairsFile(vols_currencypairs_file);

                iApp = new Citi.CitiFIXMarketDataAdaptor(citi_vol_prd_acc, citi_vol_prd_pwd, vols_ccypairs);
            }
            else if (targetParty.ToLower() == "citi.rfq.uat")
            {
                iApp = new Citi.CitiFIXRfqAdaptor();
            }
            else if (targetParty.ToLower() == "dbabfx.rfq")
            {
                iApp = new Dbabfx.DbabfxFIXOptionsAdaptor(rfqs_concurrent, ReadAndParseQuoteRequestsFile(rfqs_file));
            }
            else if (targetParty.ToLower() == "dbabfx.rfq.vols")
            {
                var rfq_vols_ccypairs = ReadCcypairsFile(rfq_vols_currencypairs_file);
                var rfq_vols_tenors = ConfigurationManager.AppSettings["rfq.vols.tenors"].Split(';');

                iApp = new Dbabfx.DbabfxFIXOptsVolsAdaptor(rfqs_concurrent, rfq_vols_ccypairs, rfq_vols_tenors);
            }
            else if (targetParty.ToLower() == "dbabfx.rfq.tenordates")
            {
                var rfq_tds_ccypairs = ReadCcypairsFile(rfq_tds_currencypairs_file);
                var rfq_tds_tenors = ConfigurationManager.AppSettings["rfq.tenordates.tenors"].Split(';');

                iApp = new Dbabfx.DbabfxFIXOptsTenorDatesAdaptor(rfqs_concurrent, rfq_tds_ccypairs, rfq_tds_tenors);
            }
            else if (targetParty.ToLower() == "dbabfx.sptfwd")
                iApp = new Dbabfx.DbabfxFIXSpotsForwardsAdaptor();
            else if (targetParty.ToLower() == "jpm.vol.uat")
            {
                var jpm_vol_uat_pwd = ConfigurationManager.AppSettings["jpm.vol.uat.password"];

                var vols_ccypairs = ReadCcypairsFile(vols_currencypairs_file);

                iApp = new Jpm.JpmFIXMarketDataAdaptor(jpm_vol_uat_pwd, vols_ccypairs);
            }
            else if (targetParty.ToLower() == "jpm.vol.prd")
            {
                var jpm_vol_prd_pwd = ConfigurationManager.AppSettings["jpm.vol.prd.password"];

                var vols_ccypairs = ReadCcypairsFile(vols_currencypairs_file);

                iApp = new Jpm.JpmFIXMarketDataAdaptor(jpm_vol_prd_pwd, vols_ccypairs);
            }
            else if (targetParty.ToLower() == "jpm.rfq.uat")
            {
                var jpm_rfq_uat_acc = ConfigurationManager.AppSettings["jpm.rfq.uat.account"];
                var jpm_rfq_uat_pwd = ConfigurationManager.AppSettings["jpm.rfq.uat.password"];

                iApp = new Jpm.JpmFIXRfqAdaptor(jpm_rfq_uat_acc, jpm_rfq_uat_pwd, rfqs_concurrent, ReadAndParseQuoteRequestsFile(rfqs_file));
            }
            else if (targetParty.ToLower() == "jpm.rfq.prd")
            {
                var jpm_rfq_prd_acc = ConfigurationManager.AppSettings["jpm.rfq.prd.account"];
                var jpm_rfq_prd_pwd = ConfigurationManager.AppSettings["jpm.rfq.prd.password"];

                iApp = new Jpm.JpmFIXRfqAdaptor(jpm_rfq_prd_acc, jpm_rfq_prd_pwd, rfqs_concurrent, ReadAndParseQuoteRequestsFile(rfqs_file));
            }
            else if (targetParty.ToLower() == "jpm.rfq.vols.uat")
            {
                var rfq_vols_ccypairs = ReadCcypairsFile(rfq_vols_currencypairs_file);
                var rfq_vols_tenors = ConfigurationManager.AppSettings["rfq.vols.tenors"].Split(';');

                var jpm_rfq_uat_acc = ConfigurationManager.AppSettings["jpm.rfq.uat.account"];
                var jpm_rfq_uat_pwd = ConfigurationManager.AppSettings["jpm.rfq.uat.password"];

                iApp = new Jpm.JpmFIXRfqVolsAdaptor(jpm_rfq_uat_acc, jpm_rfq_uat_pwd, rfqs_concurrent, rfq_vols_ccypairs, rfq_vols_tenors);
            }
            else if (targetParty.ToLower() == "jpm.rfq.vols.prd")
            {
                var rfq_vols_ccypairs = ReadCcypairsFile(rfq_vols_currencypairs_file);
                var rfq_vols_tenors = ConfigurationManager.AppSettings["rfq.vols.tenors"].Split(';');

                var jpm_rfq_prd_acc = ConfigurationManager.AppSettings["jpm.rfq.prd.account"];
                var jpm_rfq_prd_pwd = ConfigurationManager.AppSettings["jpm.rfq.prd.password"];

                iApp = new Jpm.JpmFIXRfqVolsAdaptor(jpm_rfq_prd_acc, jpm_rfq_prd_pwd, rfqs_concurrent, rfq_vols_ccypairs, rfq_vols_tenors);
            }

            if (iApp == null)
                return;

            var sessionSettings = new SessionSettings(targetPartySessionSettings);
            var storeFactory = new FileStoreFactory(sessionSettings);
            var logFactory = new FileLogFactory(sessionSettings);
            var initiator = new SocketInitiator(iApp, storeFactory, sessionSettings, logFactory);

            _log.Info("Starting FIX Initiator...");

            initiator.Start();

            _log.Info("Started.");

            Console.ReadLine();

            _log.Info("Stopping FIX Initiator...");

            initiator.Stop();

            _log.Info("Stopped.");
        }

        public static void LaunchAdminConsole(string adminTask)
        {
            if (adminTask.ToLower() == "app.settings")
            {
                var appSettings = ConfigurationManager.AppSettings;

                foreach (var k in appSettings.AllKeys)
                {
                    Console.WriteLine("{0}: {1}", k, appSettings[k]);
                }
            }
            else if (adminTask.ToLower() == "vols.ccypairs")
            {
                var vols_ccypairs_config_file = ConfigurationManager.AppSettings["vols.currencypairs.file"];

                foreach (var ccypair_entry in ReadCcypairsFile(vols_ccypairs_config_file))
                {
                    Console.WriteLine(ccypair_entry);
                }
            }
            else if (adminTask.ToLower() == "rfq.vols.ccypairs")
            {
                var rfq_vols_ccypairs_config_file = ConfigurationManager.AppSettings["rfq.vols.currencypairs.file"];

                foreach (var ccypair_entry in ReadCcypairsFile(rfq_vols_ccypairs_config_file))
                {
                    Console.WriteLine(ccypair_entry);
                }
            }
            else if (adminTask.ToLower() == "rfq.vols.tenors")
            {
                var rfq_vols_tenors = ConfigurationManager.AppSettings["rfq.vols.tenors"];

                var tenors = rfq_vols_tenors.Split(';');

                foreach (var tnr in tenors)
                {
                    Console.WriteLine(tnr);
                }
            }
            else if (adminTask.ToLower() == "rfq.tenordates.ccypairs")
            {
                var rfq_td_ccypairs_config_file = ConfigurationManager.AppSettings["rfq.tenordates.currencypairs.file"];

                foreach (var ccypair_entry in ReadCcypairsFile(rfq_td_ccypairs_config_file))
                {
                    Console.WriteLine(ccypair_entry);
                }
            }
            else if (adminTask.ToLower() == "rfq.tenordates.tenors")
            {
                var rfq_tenordates_tenors = ConfigurationManager.AppSettings["rfq.tenordates.tenors"];

                var tenors = rfq_tenordates_tenors.Split(';');

                foreach (var tnr in tenors)
                {
                    Console.WriteLine(tnr);
                }
            }
        }

        private static IList<string> ReadCcypairsFile(string filename)
        {
            return File.ReadAllLines(filename).ToList();
        }

        private static bool TryParseStrikeExpr(string strikeExpr, out double? strike, out OptionStrikeType? optStrikeType, out OptionCallPut? optCallPut)
        {
            strike = null;
            optStrikeType = OptionStrikeType.DeltaNeutral;
            optCallPut = OptionCallPut.Call;

            var success = false;

            var strikeExprlower = strikeExpr.ToLower();

            if (strikeExprlower == "dn")
            {
                optStrikeType = OptionStrikeType.DeltaNeutral;
                success = true;
            }
            else if (strikeExprlower == "atms")
            {
                optStrikeType = OptionStrikeType.AtmSpot;
                success = true;
            }
            else if (strikeExprlower == "atmf")
            {
                optStrikeType = OptionStrikeType.AtmForward;
                success = true;
            }
            else if (strikeExprlower.EndsWith("c"))
            {
                optCallPut = OptionCallPut.Call;
                strikeExprlower = strikeExprlower.Substring(0, strikeExprlower.Length - 1);
                if (strikeExprlower.EndsWith("ds") || strikeExprlower.EndsWith("df"))
                {
                    if (strikeExprlower.EndsWith("ds"))
                        optStrikeType = OptionStrikeType.DeltaSpot;
                    else
                        optStrikeType = OptionStrikeType.DeltaForward;

                    strikeExprlower = strikeExprlower.Substring(0, strikeExprlower.Length - 2);
                    int x_delta;
                    if (int.TryParse(strikeExprlower, out x_delta))
                    {
                        success = true;
                    }
                    strike = x_delta;
                }
                else
                {
                    double x_strike;
                    if (double.TryParse(strikeExprlower, out x_strike))
                    {
                        success = true;
                    }
                    strike = x_strike;
                }
            }
            else if (strikeExprlower.EndsWith("p"))
            {
                optCallPut = OptionCallPut.Put;
                strikeExprlower = strikeExprlower.Substring(0, strikeExprlower.Length - 1);
                if (strikeExprlower.EndsWith("ds") || strikeExprlower.EndsWith("df"))
                {
                    if (strikeExprlower.EndsWith("ds"))
                        optStrikeType = OptionStrikeType.DeltaSpot;
                    else
                        optStrikeType = OptionStrikeType.DeltaForward;

                    strikeExprlower = strikeExprlower.Substring(0, strikeExprlower.Length - 2);
                    int x_delta;
                    if (int.TryParse(strikeExprlower, out x_delta))
                    {
                        success = true;
                    }
                    strike = x_delta;
                }
                else
                {
                    double x_strike;
                    if (double.TryParse(strikeExprlower, out x_strike))
                    {
                        success = true;
                    }
                    strike = x_strike;
                }
            }

            return success;
        }

        private static bool TryParseNotionalExpr(string notionalExpr, out BuySell buySell, out double notional)
        {
            buySell = BuySell.Buy;
            notional = double.NaN;

            var success = false;
            
            if (notionalExpr.StartsWith("+") || notionalExpr.StartsWith("-"))
            {
                if (notionalExpr.StartsWith("-"))
                    buySell = BuySell.Sell;

                notionalExpr = notionalExpr.Substring(1, notionalExpr.Length - 1);
            }

            success = double.TryParse(notionalExpr, out notional);

            return success;
        }

        private static bool TryParseExpiryExpr(string expiryExpr, out DateTime? expiryDate)
        {
            expiryDate = null;
            var expiryDateExact = DateTime.MinValue;
            var success = false;

            var expiryExprLower = expiryExpr.ToLower();
            if (expiryExprLower.EndsWith("d")
                || expiryExprLower.EndsWith("w")
                || expiryExprLower.EndsWith("m")
                || expiryExprLower.EndsWith("y"))
            {
                var num = int.MinValue;
                if (int.TryParse(expiryExprLower.Substring(0, expiryExprLower.Length - 1), out num))
                    success = true;
            }
            else if (DateTime.TryParseExact(expiryExpr, "ddMMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiryDateExact))
            {
                expiryDate = expiryDateExact;
                success = true;
            }

            return success;
        }

        private static IEnumerable<VanillaOptionsQuoteRequest> ReadAndParseQuoteRequestsFile(string filename)
        {
            var quoteReqsLines = File.ReadAllLines(filename).ToList();

            var vanOptRfqs = new List<VanillaOptionsQuoteRequest>();

            var utcNow = DateTime.UtcNow;

            var refId_prefix = string.Format("Rfq{0}{1}", utcNow.ToString("ddMMMyy"), utcNow.ToString("HHmm"));

            foreach (var qrline in quoteReqsLines)
            {
                var qrline_lower = qrline.ToLower();

                if (qrline_lower.StartsWith("single:"))
                {
                    qrline_lower = qrline_lower.Substring(7, qrline_lower.Length - 7);
                    var qrline_args = qrline_lower.Split(';');
                    var ccypair_arg = qrline_args[0].ToUpper();
                    var tenor_arg = qrline_args[1].ToUpper();
                    var strikeExpr_arg = qrline_args[2].ToUpper();
                    var premccy_arg = qrline_args[3].ToUpper();
                    var notional_arg = string.Empty;
                    if (qrline_args.Length > 4)
                        notional_arg = qrline_args[4];

                    var leftCcy = ccypair_arg.Substring(0, 3);
                    var rightCcy = ccypair_arg.Substring(3, 3);

                    double? strike;
                    OptionStrikeType? optStrikeType;
                    OptionCallPut? optCallPut;

                    DateTime? expiryDate;

                    var side = BuySell.Buy;
                    var notional = 1_000_000.00;

                    if (TryParseStrikeExpr(strikeExpr_arg, out strike, out optStrikeType, out optCallPut)
                        && TryParseExpiryExpr(tenor_arg, out expiryDate))
                    {
                        var uuid = Guid.NewGuid().ToString();
                        var uuid_parts = uuid.Split('-');
                        var uuid_final_part = uuid_parts[uuid_parts.Length - 1];

                        var refId = string.Format("{0}-{1}{2}{3}-{4}", refId_prefix, ccypair_arg, tenor_arg, strikeExpr_arg, uuid_final_part);

                        var sglOptQuoteReqLeg = new SingleVanillaOptionContract(tenor_arg, expiryDate, strike, optStrikeType.Value, optCallPut.Value,
                                                         side, premccy_arg, leftCcy, notional);

                        var sglOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair_arg, premccy_arg, leftCcy, sglOptQuoteReqLeg);

                        vanOptRfqs.Add(sglOptQuoteReq);
                    }
                }
                else if (qrline_lower.StartsWith("multi:"))
                {
                    // Will parse the tenor string as a range
                    // Will parse the strike string as a range

                    qrline_lower = qrline_lower.Substring(6, qrline_lower.Length - 6);
                    var qrline_args = qrline_lower.Split(';');
                    var ccypairs_arg = qrline_args[0].ToUpper();
                    var tenors_range_arg = qrline_args[1].ToUpper();
                    var strikeExprs_range_arg = qrline_args[2].ToUpper();
                    var premccys_arg = qrline_args[3].ToUpper();
                    var notional_arg = string.Empty;
                    if (qrline_args.Length > 4)
                        notional_arg = qrline_args[4];

                    var ccypairs = ccypairs_arg.Split('/');
                    var premccys = premccys_arg.Split('/');
                    var tenors = new List<string>();
                    var strikeExprs = new List<string>();

                    if (tenors_range_arg.Contains('/'))
                    {
                        tenors = tenors_range_arg.Split('/').ToList();
                    }
                    else if (tenors_range_arg.Contains(','))
                    {
                        var tenor_unit = tenors_range_arg.Substring(tenors_range_arg.Length - 1, 1);
                        tenors_range_arg = tenors_range_arg.Substring(0, tenors_range_arg.Length - 1);

                        var tenors_range = tenors_range_arg.Split(',').ToList();
                        var start_tnr = int.Parse(tenors_range[0]);
                        var end_tnr = int.Parse(tenors_range[1]);
                        var step_tnr = int.Parse(tenors_range[2]);

                        var i = start_tnr;
                        while (i <= end_tnr)
                        {
                            tenors.Add(string.Format("{0}{1}", i, tenor_unit));
                            i += step_tnr;
                        }
                    }
                    else
                        tenors.Add(tenors_range_arg);

                    if (strikeExprs_range_arg.Contains('/'))
                        strikeExprs = strikeExprs_range_arg.Split('/').ToList();
                    else if (strikeExprs_range_arg.Contains(','))
                    {
                        var putCallExpr = strikeExprs_range_arg.Substring(strikeExprs_range_arg.Length - 1, 1);
                        strikeExprs_range_arg = strikeExprs_range_arg.Substring(0, strikeExprs_range_arg.Length - 1);

                        var strike_unit = strikeExprs_range_arg.Substring(strikeExprs_range_arg.Length - 2, 2);
                        strikeExprs_range_arg = strikeExprs_range_arg.Substring(0, strikeExprs_range_arg.Length - 2);
                        
                        var strikeExprs_range = strikeExprs_range_arg.Split(',').ToList();
                        var start_strikeExpr = strikeExprs_range[0];
                        var end_strikeExpr = strikeExprs_range[1];
                        var step_strikeExpr = strikeExprs_range[2];
                        var start_strike = double.Parse(start_strikeExpr);
                        var end_strike = double.Parse(end_strikeExpr);
                        var step_strike = double.Parse(step_strikeExpr);

                        var i = start_strike;
                        while (i < end_strike)
                        {
                            if (strike_unit.ToLower() == "ds" || strike_unit.ToLower() == "df") // deltas
                            {
                                strikeExprs.Add(string.Format("{0}{1}{2}", i, strike_unit, putCallExpr));
                                i += step_strike;
                            }
                            else if (strike_unit.ToLower() == "ps") // pips
                            {
                                var dp = start_strikeExpr.Split('.')[1].Length;
                                i += step_strike / Math.Pow(10.0, dp);
                                strikeExprs.Add(i.ToString());
                            }
                        }
                    }
                    else
                        strikeExprs.Add(strikeExprs_range_arg);

                    for (int i = 0; i < ccypairs.Length; i++)
                    {
                        var ccypair = ccypairs[i];
                        var premccy = premccys[i];

                        var leftCcy = ccypair.Substring(0, 3);
                        var rightCcy = ccypair.Substring(3, 3);

                        foreach (var tnr in tenors)
                        {
                            DateTime? expiryDate;

                            foreach (var strikeExpr in strikeExprs)
                            {

                                double? strike;
                                OptionStrikeType? optStrikeType;
                                OptionCallPut? optCallPut;

                                if (TryParseStrikeExpr(strikeExpr, out strike, out optStrikeType, out optCallPut)
                                    && TryParseExpiryExpr(tnr, out expiryDate))
                                {
                                    var uuid = Guid.NewGuid().ToString();
                                    var uuid_parts = uuid.Split('-');
                                    var uuid_final_part = uuid_parts[uuid_parts.Length - 1];

                                    var refId = string.Format("{0}-{1}{2}{3}-{4}", refId_prefix, ccypair, tnr, strikeExpr, uuid_final_part);

                                    var sglOptQuoteReqLeg = new SingleVanillaOptionContract(tnr, null, strike, optStrikeType.Value, optCallPut.Value,
                                                                     BuySell.Buy, premccy, leftCcy);

                                    var sglOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair, premccy, leftCcy, sglOptQuoteReqLeg);

                                    vanOptRfqs.Add(sglOptQuoteReq);
                                }

                            }
                        }
                    }
                }
            }

            return vanOptRfqs;
        }

        private static IEnumerable<SingleVanillaOptionQuoteRequest> MonthEndOptionsQuoteRequests_Dec()
        {
            var monthEndOptionsRfqs = new List<SingleVanillaOptionQuoteRequest>();

            var optionsTuples = new List<Tuple<string, string, double>>();

            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20190801", 0.81));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190716", 106));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20191105", 100));
            optionsTuples.Add(new Tuple<string, string, double>("NZDUSD", "20191210", 0.62));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190813", 105));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190411", 1.37));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20191008", 103.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190813", 105.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190430", 106.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190521", 107));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190624", 105));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190917", 0.8575));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190617", 111));
            optionsTuples.Add(new Tuple<string, string, double>("NZDUSD", "20190917", 0.6275));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190409", 1.32));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190205", 1.14));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8269));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190107", 111));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8265));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190521", 104));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8261));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190917", 106));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190319", 107));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190221", 1.155));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190802", 1.33));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190618", 104.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190219", 107));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190611", 1.37));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191112", 0.8825));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190809", 1.43));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190515", 112));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191014", 1.13));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190219", 1.17));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191217", 0.88));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190218", 111.85));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20191210", 100));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190515", 125));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190211", 1.18));
            optionsTuples.Add(new Tuple<string, string, double>("NZDUSD", "20190611", 0.64));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190917", 103));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190618", 104.25));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.1525));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190221", 1.1525));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190118", 1.2915));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191015", 0.9025));
            optionsTuples.Add(new Tuple<string, string, double>("NZDUSD", "20190816", 0.7));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190809", 1.05));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190104", 1.275));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190819", 1.44));
            optionsTuples.Add(new Tuple<string, string, double>("NZDUSD", "20190626", 0.72));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190304", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190212", 1.18));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190115", 109));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190307", 1.33));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190709", 100));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190122", 107.15));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190716", 0.905));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190218", 109));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190130", 1.23));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20191105", 1.32));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190130", 1.3));
            optionsTuples.Add(new Tuple<string, string, double>("NZDUSD", "20190618", 0.6575));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20190801", 0.92));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190410", 73));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191217", 0.96));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20190118", 0.875));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190121", 1.25));
            optionsTuples.Add(new Tuple<string, string, double>("GBPJPY", "20191115", 116.7));
            optionsTuples.Add(new Tuple<string, string, double>("USDTRY", "20190521", 4.6));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190214", 74));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191014", 1.075));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191112", 0.965));
            optionsTuples.Add(new Tuple<string, string, double>("USDTRY", "20190220", 4.7));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190319", 1.2675));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191112", 0.9625));
            optionsTuples.Add(new Tuple<string, string, double>("EURAUD", "20190606", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("USDCNH", "20190618", 7.15));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190226", 116.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190627", 1.392));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190226", 1.23));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190221", 1.2875));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190110", 1.16));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190206", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190711", 1.09));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190118", 112.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191210", 0.97));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190220", 1.35));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190521", 0.7275));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190305", 1.35));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190322", 1.41));
            optionsTuples.Add(new Tuple<string, string, double>("USDMXN", "20190926", 17.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190313", 63));
            optionsTuples.Add(new Tuple<string, string, double>("USDMXN", "20190314", 17.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190917", 0.95));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190910", 0.9725));
            optionsTuples.Add(new Tuple<string, string, double>("USDCNH", "20191210", 7.2));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190212", 1.317));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190305", 1.33));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190516", 105));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191015", 0.955));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20191105", 110));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20191105", 1));
            optionsTuples.Add(new Tuple<string, string, double>("USDCNH", "20190917", 7.2));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20191008", 110.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDMXN", "20190319", 18.1));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190417", 72));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190627", 60.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190411", 67));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190716", 116));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190206", 135.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190521", 1.175));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20191105", 113));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190315", 76));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20191009", 1.27));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20191210", 109));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20191213", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191105", 1.21));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191001", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190805", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190820", 0.695));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20191211", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191219", 1.18));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20191025", 0.6675));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190801", 1.2895));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20191213", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190604", 107));

            var cutoff = "NY";
            var batchCode = "MonthEnd_20181231_2000";

            foreach (var optTuple in optionsTuples)
            {
                var ccypair = optTuple.Item1;
                var leftCcy = ccypair.Substring(0, 3);
                var rightCcy = ccypair.Substring(3, 3);
                var expiryDateExpr = optTuple.Item2;
                var strike = optTuple.Item3;

                if (Dbabfx.DbabfxFIXConstants.NonNYCcyExpiryCuts.ContainsKey(rightCcy))
                    cutoff = Dbabfx.DbabfxFIXConstants.NonNYCcyExpiryCuts[rightCcy];

                var expiryDate = DateTime.ParseExact(expiryDateExpr, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sglOptQuoteReqLeg = new SingleVanillaOptionContract(string.Empty, expiryDate, strike, OptionStrikeType.Outright, OptionCallPut.Call, BuySell.Buy, leftCcy, leftCcy);

                var uuid = Guid.NewGuid().ToString().Substring(0, 12);
                var refId = string.Format("{0}-{1}-{2}-{3}-{4}", batchCode, ccypair, expiryDateExpr, strike, uuid);
                var sglOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair, leftCcy, leftCcy, sglOptQuoteReqLeg);

                monthEndOptionsRfqs.Add(sglOptQuoteReq);
            }

            return monthEndOptionsRfqs;
        }

        private static IEnumerable<SingleVanillaOptionQuoteRequest> MonthEndOptionsQuoteRequests_Nov()
        {
            var monthEndOptionsRfqs = new List<SingleVanillaOptionQuoteRequest>();

            var optionsTuples = new List<Tuple<string, string, double>>();

            optionsTuples.Add(new Tuple<string, string, double>("AUDJPY", "20181130", 83.1));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8269));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8265));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8261));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20181203", 0.74));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20181220", 0.72));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190129", 0.7375));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20181204", 0.724));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.6356));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.635));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.6345));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20191105", 0.7575));
            optionsTuples.Add(new Tuple<string, string, double>("EURAUD", "20190606", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20190215", 0.8475));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20190308", 0.8855));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20190118", 0.875));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20181203", 0.8875));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20181221", 0.875));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20181210", 0.865));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20181210", 0.8875));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20190801", 0.92));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190205", 136.3));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190206", 135.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190109", 125));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190612", 107));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181211", 1.12));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191014", 1.075));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181217", 1.1565));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181203", 1.1375));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191014", 1.13));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181211", 1.1475));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190118", 1.18));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190711", 1.09));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.2655));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.23));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181212", 1.15));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181217", 1.125));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190226", 1.232));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191105", 1.075));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.208));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191001", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190221", 1.155));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190805", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191105", 1.21));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.1525));
            optionsTuples.Add(new Tuple<string, string, double>("GBPJPY", "20191115", 116.7));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190411", 1.37));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190723", 1.45));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181221", 1.325));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181221", 1.27));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181221", 1.28));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190409", 1.32));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181220", 1.3));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181217", 1.275));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181221", 1.25));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190809", 1.05));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20191009", 1.27));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181205", 1.28));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190819", 1.44));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190402", 1.22));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190611", 1.35));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190809", 1.43));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190109", 1.32));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190307", 1.33));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190110", 1.32));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181203", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181218", 1.315));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181218", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190709", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190627", 1.392));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190802", 1.33));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190801", 1.2895));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190917", 0.8575));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190917", 0.95));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190403", 0.91));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190617", 111));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190516", 105));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190131", 105));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190604", 107));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20181219", 112.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20181217", 114.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190521", 114));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20191105", 113));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190716", 113.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDMXN", "20190314", 17.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDMXN", "20190319", 18.1));
            optionsTuples.Add(new Tuple<string, string, double>("USDMXN", "20190926", 17.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDMXN", "20191104", 18));
            optionsTuples.Add(new Tuple<string, string, double>("USDMXN", "20190410", 21));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190410", 73));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190417", 72));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190214", 74));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190627", 60.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190614", 56));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190708", 56.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20191114", 61.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDTRY", "20190521", 4.6));
            optionsTuples.Add(new Tuple<string, string, double>("USDTRY", "20190522", 4.4));
            optionsTuples.Add(new Tuple<string, string, double>("USDTRY", "20190705", 4.55));
            optionsTuples.Add(new Tuple<string, string, double>("USDTRY", "20190220", 4.7));
            optionsTuples.Add(new Tuple<string, string, double>("USDTRY", "20190724", 5.7));
            optionsTuples.Add(new Tuple<string, string, double>("USDZAR", "20190705", 12.1));
            optionsTuples.Add(new Tuple<string, string, double>("USDZAR", "20190927", 12.5));

            var cutoff = "NY";
            var batchCode = "MonthEnd_20181130_2000";

            foreach (var optTuple in optionsTuples)
            {
                var ccypair = optTuple.Item1;
                var leftCcy = ccypair.Substring(0, 3);
                var rightCcy = ccypair.Substring(3, 3);
                var expiryDateExpr = optTuple.Item2;
                var strike = optTuple.Item3;

                if (Dbabfx.DbabfxFIXConstants.NonNYCcyExpiryCuts.ContainsKey(rightCcy))
                    cutoff = Dbabfx.DbabfxFIXConstants.NonNYCcyExpiryCuts[rightCcy];

                var expiryDate = DateTime.ParseExact(expiryDateExpr, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sglOptQuoteReqLeg = new SingleVanillaOptionContract(string.Empty, expiryDate, strike, OptionStrikeType.Outright, OptionCallPut.Call, BuySell.Buy, leftCcy, leftCcy);

                var uuid = Guid.NewGuid().ToString().Substring(0, 12);
                var refId = string.Format("{0}-{1}-{2}-{3}-{4}", batchCode, ccypair, expiryDateExpr, strike, uuid);
                var sglOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair, leftCcy, leftCcy, sglOptQuoteReqLeg);

                monthEndOptionsRfqs.Add(sglOptQuoteReq);
            }

            return monthEndOptionsRfqs;
        }

        private static IEnumerable<SingleVanillaOptionQuoteRequest> MonthEndOptionsQuoteRequests_Oct()
        {
            var monthEndOptionsRfqs = new List<SingleVanillaOptionQuoteRequest>();

            var optionsTuples = new List<Tuple<string, string, double>>();

            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181109", 1.3225));
            optionsTuples.Add(new Tuple<string, string, double>("EURAUD", "20190606", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191014", 1.075));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190917", 0.8575));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8269));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8265));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8261));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190411", 1.37));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181221", 1.16));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181221", 1.325));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20181211", 0.935));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190515", 112));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181101", 1.275));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181212", 1.41));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20181109", 112.57));
            optionsTuples.Add(new Tuple<string, string, double>("AUDJPY", "20181218", 71));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181211", 1.1475));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20181218", 0.7225));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181203", 1.09));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181109", 1.2985));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190618", 110));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190723", 1.45));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20190403", 0.91));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190709", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190604", 107));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181207", 1.295));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20181203", 0.8875));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20181221", 0.875));
            optionsTuples.Add(new Tuple<string, string, double>("USDZAR", "20190927", 12.5));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191014", 1.075));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190411", 1.37));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190627", 1.392));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.208));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190215", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190809", 1.43));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190307", 1.33));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191001", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190221", 1.155));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190205", 1.14));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.1525));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190515", 1.07));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20191025", 0.6675));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20191014", 1.13));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190118", 1.16));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190118", 1.18));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190805", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20191009", 1.27));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.15));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190801", 1.2895));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190604", 107));

            var cutoff = "NY";
            var batchCode = "MonthEnd_20181031_1600";

            foreach (var optTuple in optionsTuples)
            {
                var ccypair = optTuple.Item1;
                var leftCcy = ccypair.Substring(0, 3);
                var rightCcy = ccypair.Substring(3, 3);
                var expiryDateExpr = optTuple.Item2;
                var strike = optTuple.Item3;

                if (Dbabfx.DbabfxFIXConstants.NonNYCcyExpiryCuts.ContainsKey(rightCcy))
                    cutoff = Dbabfx.DbabfxFIXConstants.NonNYCcyExpiryCuts[rightCcy];

                var expiryDate = DateTime.ParseExact(expiryDateExpr, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sglOptQuoteReqLeg = new SingleVanillaOptionContract(string.Empty, expiryDate, strike, OptionStrikeType.Outright, OptionCallPut.Call, BuySell.Buy, leftCcy, leftCcy);

                var uuid = Guid.NewGuid().ToString().Substring(0, 12);
                var refId = string.Format("{0}-{1}-{2}-{3}-{4}", batchCode, ccypair, expiryDateExpr, strike, uuid);
                var sglOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair, leftCcy, leftCcy, sglOptQuoteReqLeg);

                monthEndOptionsRfqs.Add(sglOptQuoteReq);
            }

            return monthEndOptionsRfqs;
        }

        private static IEnumerable<SingleVanillaOptionQuoteRequest> MonthEndOptionsQuoteRequests_Sep()
        {

            var monthEndOptionsRfqs = new List<SingleVanillaOptionQuoteRequest>();

            var optionsTuples = new List<Tuple<string, string, double>>();

            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190411", 1.37));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181221", 1.16));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190215", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190116", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190305", 1.33));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190118", 1.18));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190627", 1.392));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.208));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190801", 1.2895));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190708", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.23));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190628", 0.77));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190530", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190221", 1.155));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.1525));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190611", 1.35));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.15));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190118", 1.16));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190121", 1.21));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190307", 1.33));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190205", 1.14));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190604", 107));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190805", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190809", 1.43));
            optionsTuples.Add(new Tuple<string, string, double>("EURAUD", "20190606", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181212", 1.41));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181105", 1.3125));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181109", 1.3225));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190116", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181220", 1.323));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8269));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8265));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8261));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20181106", 0.735));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190116", 1.028));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181212", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20181213", 0.955));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190215", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("AUDJPY", "20190501", 79.85));
            optionsTuples.Add(new Tuple<string, string, double>("EURAUD", "20190606", 1.5));

            var batchCode = "MonthEnd_20180928_1600";

            foreach (var optTuple in optionsTuples)
            {
                var ccypair = optTuple.Item1;
                var leftCcy = ccypair.Substring(0, 3);
                var rightCcy = ccypair.Substring(3, 3);
                var expiryDateExpr = optTuple.Item2;
                var strike = optTuple.Item3;

                var expiryDate = DateTime.ParseExact(expiryDateExpr, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sglOptQuoteReqLeg = new SingleVanillaOptionContract(string.Empty, expiryDate, strike, OptionStrikeType.Outright, OptionCallPut.Call, BuySell.Buy, leftCcy, leftCcy);

                var uuid = Guid.NewGuid().ToString().Substring(0, 12);
                var refId = string.Format("{0}-{1}-{2}-{3}-{4}", batchCode, ccypair, expiryDateExpr, strike, uuid);
                var sglOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair, leftCcy, leftCcy, sglOptQuoteReqLeg);

                monthEndOptionsRfqs.Add(sglOptQuoteReq);
            }

            return monthEndOptionsRfqs;
        }

        private static IEnumerable<SingleVanillaOptionQuoteRequest> MonthEndOptionsQuoteRequests_Aug()
        {

            var monthEndOptionsRfqs = new List<SingleVanillaOptionQuoteRequest>();

            var optionsTuples = new List<Tuple<string, string, double>>();

            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20181121", 121));
            optionsTuples.Add(new Tuple<string, string, double>("EURAUD", "20190606", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181109", 1.3225));
            optionsTuples.Add(new Tuple<string, string, double>("AUDJPY", "20181218", 71));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190411", 1.37));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190612", 107));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181212", 1.41));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190711", 1.09));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20181221", 1.325));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190116", 1.028));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181203", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190515", 125));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181220", 1.323));
            optionsTuples.Add(new Tuple<string, string, double>("AUDJPY", "20180927", 77));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20180904", 1.1615));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20180905", 0.73));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20181003", 0.91));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8269));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8265));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190812", 0.8261));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190215", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190118", 1.18));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190530", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190221", 1.155));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.1525));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190226", 1.1465));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190708", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190219", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190819", 1.44));
            optionsTuples.Add(new Tuple<string, string, double>("NZDUSD", "20190816", 0.7));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.15));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190226", 1.232));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.23));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.208));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.2655));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20190801", 1.2895));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190205", 1.14));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190121", 1.21));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190628", 0.77));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190116", 1.34));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20190809", 1.43));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190805", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190617", 111));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190118", 1.14));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190604", 107));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20181204", 105));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190515", 112));

            var batchCode = "MonthEnd_20180831_Final";

            foreach (var optTuple in optionsTuples)
            {
                var ccypair = optTuple.Item1;
                var leftCcy = ccypair.Substring(0, 3);
                var rightCcy = ccypair.Substring(3, 3);
                var expiryDateExpr = optTuple.Item2;
                var strike = optTuple.Item3;

                var expiryDate = DateTime.ParseExact(expiryDateExpr, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sglOptQuoteReqLeg = new SingleVanillaOptionContract(string.Empty, expiryDate, strike, OptionStrikeType.Outright, OptionCallPut.Call, BuySell.Buy, leftCcy, leftCcy);

                var uuid = Guid.NewGuid().ToString().Substring(0, 12);
                var refId = string.Format("{0}-{1}-{2}-{3}-{4}", batchCode, ccypair, expiryDateExpr, strike, uuid);
                var sglOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair, leftCcy, leftCcy, sglOptQuoteReqLeg);

                monthEndOptionsRfqs.Add(sglOptQuoteReq);
            }

            return monthEndOptionsRfqs;
        }

        private static IEnumerable<SingleVanillaOptionQuoteRequest> MonthEndOptionsQuoteRequests_Jul()
        {
            var monthEndOptionsRfqs = new List<SingleVanillaOptionQuoteRequest>();

            var optionsTuples = new List<Tuple<string, string, double>>();

            optionsTuples.Add(new Tuple<string, string, double>("EURAUD", "20190606", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181212", 1.41));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180925", 100));
            optionsTuples.Add(new Tuple<string, string, double>("GBPUSD", "20180803", 1.305));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20190515", 112));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190116", 1.028));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20180926", 1.2));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180905", 110.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20181203", 1.42));
            optionsTuples.Add(new Tuple<string, string, double>("USDCHF", "20181003", 0.91));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20180926", 1.17));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20180803", 1.293));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190711", 1.09));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180801", 111));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180919", 100));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20180802", 0.8915));
            optionsTuples.Add(new Tuple<string, string, double>("CADJPY", "20180917", 82));
            optionsTuples.Add(new Tuple<string, string, double>("AUDJPY", "20180927", 77));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190305", 1.19));
            optionsTuples.Add(new Tuple<string, string, double>("EURGBP", "20180802", 0.883));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20181211", 0.795));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181211", 1.1475));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180926", 114));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180926", 108));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190118", 1.18));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20190118", 0.8));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180829", 110.7));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.2655));
            optionsTuples.Add(new Tuple<string, string, double>("USDZAR", "20190522", 11.4));
            //optionsTuples.Add(new Tuple<string, string, double>("USDRUB", "20190614", 56));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20181127", 107.7));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190121", 1.21));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20180926", 1.185));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180831", 109.5));
            optionsTuples.Add(new Tuple<string, string, double>("AUDJPY", "20190329", 61.5));
            optionsTuples.Add(new Tuple<string, string, double>("USDCAD", "20180803", 1.294));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20181203", 1.09));
            //optionsTuples.Add(new Tuple<string, string, double>("USDTRY", "20190312", 4.3));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180801", 110.5));
            optionsTuples.Add(new Tuple<string, string, double>("EURUSD", "20190312", 1.208));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20190604", 107));
            optionsTuples.Add(new Tuple<string, string, double>("AUDUSD", "20180925", 0.755));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180802", 111.15));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180925", 112));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20181026", 143));
            optionsTuples.Add(new Tuple<string, string, double>("EURJPY", "20180801", 130.25));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180927", 100));
            optionsTuples.Add(new Tuple<string, string, double>("USDJPY", "20180802", 111));

            var batchCode = "MonthEnd_20180731_2000";

            foreach (var optTuple in optionsTuples)
            {
                var ccypair = optTuple.Item1;
                var leftCcy = ccypair.Substring(0, 3);
                var rightCcy = ccypair.Substring(3, 3);
                var expiryDateExpr = optTuple.Item2;
                var strike = optTuple.Item3;

                var expiryDate = DateTime.ParseExact(expiryDateExpr, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sglOptQuoteReqLeg = new SingleVanillaOptionContract(string.Empty, expiryDate, strike, OptionStrikeType.Outright, OptionCallPut.Call, BuySell.Buy, leftCcy, leftCcy);

                var uuid = Guid.NewGuid().ToString().Substring(0, 12);
                var refId = string.Format("{0}-{1}-{2}-{3}-{4}", batchCode, ccypair, expiryDateExpr, strike, uuid);
                var sglOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair, leftCcy, leftCcy, sglOptQuoteReqLeg);

                monthEndOptionsRfqs.Add(sglOptQuoteReq);
            }

            return monthEndOptionsRfqs;
        }
    }
}
