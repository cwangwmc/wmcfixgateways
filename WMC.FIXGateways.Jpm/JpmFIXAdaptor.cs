﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

using WMC.FIXStateMachines;

using log4net;

using QuickFix;
using QuickFix.FIX42;
using QuickFix.Fields;

namespace WMC.FIXGateways.Jpm
{
    public class JpmFIXAdaptor : MessageCracker, IApplication
    {
        protected static readonly log4net.ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected string _password;

        protected SessionID _sessionID;
        protected bool _isLoggedOn;
        protected DateTime _lastLogOnUtc;
        protected DateTime _lastLogOffUtc;

        public JpmFIXAdaptor(string password)
        {
            _password = password;
        }

        public virtual void FromApp(QuickFix.Message msg, SessionID sessionID)
        {
            _log.DebugFormat("FromApp: {0}, {1}", sessionID.ToString(), msg.ToString());
            Crack(msg, sessionID);
        }

        public virtual void OnCreate(SessionID sessionID)
        {
            _log.DebugFormat("OnCreate: {0}", sessionID.ToString());
        }

        public virtual void OnLogout(SessionID sessionID)
        {
            _log.DebugFormat("OnLogout: {0}", sessionID.ToString());
            _isLoggedOn = false;
            _lastLogOffUtc = DateTime.UtcNow;
        }

        public virtual void OnLogon(SessionID sessionID)
        {
            _log.DebugFormat("OnLogon: {0}", sessionID.ToString());
            _sessionID = sessionID;
            _isLoggedOn = true;
            _lastLogOnUtc = DateTime.UtcNow;
        }

        public virtual void FromAdmin(QuickFix.Message msg, SessionID sessionID)
        {
            _log.DebugFormat("FromAdmin: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        public virtual void ToAdmin(QuickFix.Message msg, SessionID sessionID)
        {
            if (msg is Logon)
            {
                var logonMsg = (Logon)msg;
                logonMsg.RawData = new RawData(_password);
                logonMsg.RawDataLength = new RawDataLength(_password.Length);
                logonMsg.ResetSeqNumFlag = new ResetSeqNumFlag(true);
            }

            _log.DebugFormat("ToAdmin: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        public virtual void ToApp(QuickFix.Message msg, SessionID sessionID)
        {
            _log.DebugFormat("ToApp: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        protected virtual void OnStateMachineOutboundFIXMessage(object sender, FIXMessageDetailedEventArgs msgDetailsEventArgs)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnStateMachineActivate(object sender, StateMachineActivatedEventArgs args)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnStateMachineDeactivate(object sender, StateMachineDeactivatedEventArgs args)
        {
            throw new NotImplementedException();
        }

    }
}
