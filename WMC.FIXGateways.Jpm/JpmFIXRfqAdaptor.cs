﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

using WMC.FIXDataObjects;
using WMC.FIXKafkaClients;

using QuickFix;
using QuickFix.Fields;
using FIX42 = QuickFix.FIX42;

using log4net;

using Newtonsoft.Json;
using WMC.FIXStateMachines;

namespace WMC.FIXGateways.Jpm
{
    public class JpmFIXRfqAdaptor : JpmFIXAdaptor
    {
        protected string _accountId;

        protected static readonly int _defaultActiveQuoteRequestsLimit = 2; // The default control limit on how many active Rfqs can co-exist, i.e. how demanding we are against dbafbx pricing interface.

        protected int _activeQuoteRequestLimit; // The control limit on how many active Rfqs can co-exist, i.e. how demanding we are against dbafbx pricing interface.

        protected ConcurrentQueue<QuoteRequest> _quoteRequestQueue;
        protected ConcurrentQueue<JpmFxOptionsRFQStateMachine> _pendingRFQStateMachineQueue;

        protected ConcurrentDictionary<string, JpmFxOptionsRFQStateMachine> _activeRFQStateMachines;

        private JpmFxVolExpiryCutCodeHelper _volExpCutCodeHelper = new JpmFxVolExpiryCutCodeHelper();

        protected Timer _actionAfterLogonTimer;
        protected Timer _rfqStateMachineActivationTimer;

        public JpmFIXRfqAdaptor(string accountId, string password)
            : base(password)
        {
            _accountId = accountId;
            _activeQuoteRequestLimit = _defaultActiveQuoteRequestsLimit;
            _pendingRFQStateMachineQueue = new ConcurrentQueue<JpmFxOptionsRFQStateMachine>();
            _activeRFQStateMachines = new ConcurrentDictionary<string, JpmFxOptionsRFQStateMachine>();
            SetupTimers();
        }

        public JpmFIXRfqAdaptor(string accountId, string password, int activeQuoteRequestLimit)
            : this(accountId, password)
        {
            _activeQuoteRequestLimit = activeQuoteRequestLimit;
        }

        public JpmFIXRfqAdaptor(string accountId, string password, int activeQuoteRequestLimit, IEnumerable<VanillaOptionsQuoteRequest> vanillaOptionsQuoteRequests)
            : this(accountId, password, activeQuoteRequestLimit)
        {
            InitialiseAndLoadRfqStateMachineQueue(vanillaOptionsQuoteRequests);
        }

        protected virtual void InitialiseAndLoadRfqStateMachineQueue(IEnumerable<VanillaOptionsQuoteRequest> optionsQuoteRequests)
        {
            var optionsQuoteRequestsStateMachines = MakeRfqStateMachines(optionsQuoteRequests);

            foreach (var rfqStateMachine in optionsQuoteRequestsStateMachines)
            {
                rfqStateMachine.Activated += OnStateMachineActivate;
                rfqStateMachine.Deactivated += OnStateMachineDeactivate;
                rfqStateMachine.StateMachineFIXMessageOut += OnStateMachineOutboundFIXMessage;

                _pendingRFQStateMachineQueue.Enqueue(rfqStateMachine);
            }
        }

        protected virtual void SetupTimers()
        {
            _actionAfterLogonTimer = new Timer();
            _actionAfterLogonTimer.Interval = 30_000; // Time in milliseconds to wait before acting on the app's main objectives 
            _actionAfterLogonTimer.AutoReset = false;
            _actionAfterLogonTimer.Elapsed += OnActionAfterLogonTimerElapsed;

            _rfqStateMachineActivationTimer = new Timer();
            _rfqStateMachineActivationTimer.Interval = 2_000; // Time in milliseconds to wait between rounds to activate pending Rfq state machines
            _rfqStateMachineActivationTimer.AutoReset = true;
            _rfqStateMachineActivationTimer.Elapsed += OnRfqStateMachineActivationTimerElapsed;
        }

        public IEnumerable<JpmFxOptionsRFQStateMachine> MakeRfqStateMachines(IEnumerable<VanillaOptionsQuoteRequest> optionsQuoteRequests)
        {
            var rfqStateMachines = new List<JpmFxOptionsRFQStateMachine>();

            foreach (var optionsQuoteRequest in optionsQuoteRequests)
            {
                if (optionsQuoteRequest is SingleVanillaOptionQuoteRequest)
                {
                    var sglVanOptQuoteReq = (SingleVanillaOptionQuoteRequest)optionsQuoteRequest;

                    var singleVanillaOptionRFQStateMachine = MakeSingleVanillaOptionRfqStateMachine(sglVanOptQuoteReq,
                        countdownInterval: 2_000, waitInterval: 5 * 60_000);

                    rfqStateMachines.Add(singleVanillaOptionRFQStateMachine);
                }
            }

            return rfqStateMachines;
        }

        public JpmFxOptionsRFQStateMachine MakeSingleVanillaOptionRfqStateMachine(SingleVanillaOptionQuoteRequest singleVanillaOptionQuoteRequest,
            long countdownInterval, long waitInterval)
        {

            var refId = singleVanillaOptionQuoteRequest.QuoteRequestRefId;
            var ccypair = singleVanillaOptionQuoteRequest.SecuritySymbol;
            var premCcy = singleVanillaOptionQuoteRequest.OptionPremiumCurrency;
            var notionalCcy = singleVanillaOptionQuoteRequest.OptionNotionalCurrency;
            var sglVanOptLeg = (SingleVanillaOptionContract)singleVanillaOptionQuoteRequest.VanillaOptionContract;

            var optLegSide = sglVanOptLeg.Side == BuySell.Buy ? JpmFxOptionSide.Buy : JpmFxOptionSide.Sell;
            var optLegPutOrCall = sglVanOptLeg.CallPut == OptionCallPut.Call ? JpmFxOptionLegPutOrCall.Call : JpmFxOptionLegPutOrCall.Put;
            var optLegExpDateExpr = sglVanOptLeg.MaturityDate.HasValue ? sglVanOptLeg.MaturityDate.Value.ToString("yyyyMMdd") : string.Empty;

            var optLegStrikeValueExpr = sglVanOptLeg.Strike.HasValue ? sglVanOptLeg.Strike.Value.ToString() : string.Empty;

            var optLegStrikeExpr = string.Empty;

            switch (sglVanOptLeg.StrikeType)
            {
                case OptionStrikeType.DeltaSpot:
                    optLegStrikeExpr = string.Concat(optLegStrikeValueExpr, "D");
                    break;
                case OptionStrikeType.DeltaForward:
                    optLegStrikeExpr = string.Concat(optLegStrikeValueExpr, "D");
                    break;
                case OptionStrikeType.DeltaNeutral:
                    optLegStrikeExpr = JpmFxOptionLegStrike.N.ToString();
                    break;
                case OptionStrikeType.AtmForward:
                    optLegStrikeExpr = JpmFxOptionLegStrike.ATMF.ToString();
                    break;
                case OptionStrikeType.Outright:
                    optLegStrikeExpr = optLegStrikeValueExpr;
                    break;
            }
            
            var expCutCode = _volExpCutCodeHelper.VolExpiryCutCode(ccypair);

            var jpmFxOptLeg = new JpmFxOptionLeg(1, optLegStrikeExpr, sglVanOptLeg.Tenor, desc: JpmFxOptionLegSecurityDesc.VANILLA,
                side: optLegSide, putOrCall: optLegPutOrCall, expCut: expCutCode);

            var jpmFxOptLegs = new JpmFxOptionLeg[] { jpmFxOptLeg };
            var jpmFxHdgLegs = new JpmFxOptionHedgeLeg[] { };

            var jpmFxOpt = new JpmFxOption(notionalCcy, jpmFxOptLegs, jpmFxHdgLegs, optDesc: JpmFxOptionSecurityDesc.VANILLA, optSide: optLegSide, premCcy: premCcy);

            var rfqStateMachine = new JpmFxOptionsRFQStateMachine(refId, jpmFxOpt, ccypair, _isLoggedOn, countdownInterval, waitInterval);

            return rfqStateMachine;

        }


        #region Timers Eventhandlers
        public virtual void OnActionAfterLogonTimerElapsed(object sender, EventArgs args)
        {

            _log.DebugFormat("OnWaitAfterLogonTimerElapsed ...");

            _log.InfoFormat("Action After Logon Timer has elapsed after waiting for {0} secs ...", _actionAfterLogonTimer.Interval / 1_000);

            _log.InfoFormat("Enabling Rfq State Machine Activation Timer ...");

            _rfqStateMachineActivationTimer.Enabled = true;

            _log.InfoFormat("Rfq State Machine Activation Timer has been enabled.");

        }

        public virtual void OnRfqStateMachineActivationTimerElapsed(object sender, EventArgs args)
        {

            _log.DebugFormat("OnRfqStateMachineActivationTimerElapsed ...");

            _log.DebugFormat("Rfq state machine activation timer elapsed after {0} secs ...", _rfqStateMachineActivationTimer.Interval / 1_000);

            _log.DebugFormat("ActiveRFQStateMachines count: {0}; PendingRFQStateMachineQueue count: {1}", _activeRFQStateMachines.Count,
                            _pendingRFQStateMachineQueue.Count);

            while (_activeRFQStateMachines.Count < _activeQuoteRequestLimit
                && _pendingRFQStateMachineQueue.Count > 0)
            {

                JpmFxOptionsRFQStateMachine stMchn = null;

                if (_pendingRFQStateMachineQueue.TryDequeue(out stMchn))
                {
                    _log.InfoFormat("Adding state machine {0} to Active Rfq State Machines Pool ...", stMchn.StateMachineRefId);

                    _activeRFQStateMachines.TryAdd(stMchn.StateMachineRefId, stMchn);

                    stMchn.Activate();
                }

            }

            _log.DebugFormat("PendingRFQStateMachineQueue Count: {0}", _pendingRFQStateMachineQueue.Count);

            if (_pendingRFQStateMachineQueue.Count == 0)
            {
                _log.DebugFormat("Pending Rfq state machine queue is empty. Nothing to be done.");
            }

            _log.DebugFormat("Rfq state machine activation timer will set off again in {0} secs.", _rfqStateMachineActivationTimer.Interval / 1_000);

        }
        #endregion Timers Eventhandlers


        protected FIX42.QuoteRequest MakeOptionQuoteRequest(string qrRefId, JpmFxOptionsQuoteRequestFIXMessageArgs qrArgs)
        {

            var ccypair = string.Format("{0}{1}", qrArgs.Ccy1, qrArgs.Ccy2);

            var qr = new FIX42.QuoteRequest();

            qr.SetField(new CharField(JpmFIXConstants.Tag_9005_QuoteRequestSubscriptionType, qrArgs.QuoteRequestSubscriptionType));

            qr.QuoteReqID = new QuoteReqID(qrRefId);

            qr.SetField(new StringField(JpmFIXConstants.Tag_9004_AccountID, _accountId));

            qr.SetField(new CharField(JpmFIXConstants.Tag_9009_IsRFQ, qrArgs.IsRFQ));

            qr.SetField(new CharField(JpmFIXConstants.Tag_9220_IsSEF, qrArgs.IsSEF));

            var noRelatedSymGrp = new FIX42.QuoteRequest.NoRelatedSymGroup();

            noRelatedSymGrp.Symbol = new Symbol(ccypair);
            
            noRelatedSymGrp.SecurityDesc = new SecurityDesc(qrArgs.SecurityDesc);

            noRelatedSymGrp.SetField(new StringField(JpmFIXConstants.Tag_9124_SolvingFor, qrArgs.SolvingFor));

            noRelatedSymGrp.SecurityType = new SecurityType(qrArgs.SecurityType);

            noRelatedSymGrp.QuoteRequestType = new QuoteRequestType(qrArgs.QuoteRquestType);

            noRelatedSymGrp.Side = new Side(qrArgs.Side);

            noRelatedSymGrp.Currency = new Currency(qrArgs.Currency);

            noRelatedSymGrp.SetField(new IntField(JpmFIXConstants.Tag_9113_HedgeType, qrArgs.HedgeType));

            if (qrArgs.OptionHedgeLegs != null && qrArgs.OptionHedgeLegs.Count() > 0)
            {
                var noHedgeLegsGrp = new Group(JpmFIXConstants.Tag_9165_NoHedgeLegs, JpmFIXConstants.Tag_9107_DeltaHedgeCurrency);

                //...
            }

            if (qrArgs.PremCcy != string.Empty)
                noRelatedSymGrp.SetField(new StringField(JpmFIXConstants.Tag_9118_PremiumCurrency, qrArgs.PremCcy));

            if (qrArgs.PremAmt.HasValue)
                noRelatedSymGrp.SetField(new DecimalField(JpmFIXConstants.Tag_9117_PremiumAmount, (decimal)qrArgs.PremAmt.Value));

            if (qrArgs.PremPayOn.HasValue)
                noRelatedSymGrp.SetField(new IntField(JpmFIXConstants.Tag_9180_PremiumPayOn, qrArgs.PremPayOn.Value));

            if (qrArgs.PxRefMetric != string.Empty)
                noRelatedSymGrp.SetField(new StringField(JpmFIXConstants.Tag_9119_PriceReferenceMetric, qrArgs.PxRefMetric));

            if (qrArgs.OptionLegs != null && qrArgs.OptionLegs.Count() > 0)
            {

                foreach (var optLeg in qrArgs.OptionLegs)
                {
                    var noOptionLegsGrp = new Group(JpmFIXConstants.Tag_555_NoLegs, JpmFIXConstants.Tag_654_LegRefID);

                    noOptionLegsGrp.SetField(new IntField(JpmFIXConstants.Tag_654_LegRefID, optLeg.RefID));

                    if (optLeg.Side.HasValue)
                        noOptionLegsGrp.SetField(new CharField(JpmFIXConstants.Tag_624_LegSide, optLeg.Side.Value));

                    if (optLeg.PutOrCall.HasValue)
                        noOptionLegsGrp.SetField(new IntField(Tags.PutOrCall, optLeg.PutOrCall.Value));

                    noOptionLegsGrp.SetField(new DecimalField(Tags.LegQty, (decimal)optLeg.Qty.Value));

                    noOptionLegsGrp.SetField(new StringField(JpmFIXConstants.Tag_9181_LegStrike, optLeg.Strike));

                    if (optLeg.Tenor != string.Empty)
                        noOptionLegsGrp.SetField(new StringField(JpmFIXConstants.Tag_9182_LegTenor, optLeg.Tenor));

                    if (optLeg.SecurityDesc == JpmFxOptionLegSecurityDesc.SINGLE_BARRIER.ToString())
                    {
                        if (optLeg.Barrier != string.Empty)
                            noOptionLegsGrp.SetField(new StringField(JpmFIXConstants.Tag_9183_LegBarrier, optLeg.Barrier));

                        if (optLeg.BarrierType.HasValue)
                            noOptionLegsGrp.SetField(new IntField(JpmFIXConstants.Tag_9187_BarrierType, optLeg.BarrierType.Value));

                        if (optLeg.BarrierObservation.HasValue)
                            noOptionLegsGrp.SetField(new IntField(JpmFIXConstants.Tag_9188_BarrierObservation, optLeg.BarrierObservation.Value));
                    }

                    if (optLeg.Tenor == string.Empty && optLeg.ExpirationDate.HasValue)
                        noOptionLegsGrp.SetField(new DateOnlyField(JpmFIXConstants.Tag_9112_ExpirationDate, optLeg.ExpirationDate.Value));

                    if (optLeg.ExpirationCut != string.Empty)
                        noOptionLegsGrp.SetField(new StringField(JpmFIXConstants.Tag_9111_ExpirationCut, optLeg.ExpirationCut));

                    if (optLeg.DeliveryDate.HasValue)
                        noOptionLegsGrp.SetField(new DateOnlyField(JpmFIXConstants.Tag_743_DeliveryDate, optLeg.DeliveryDate.Value));

                    if (optLeg.SettlementType.HasValue)
                    {
                        noOptionLegsGrp.SetField(new IntField(Tags.SettlmntTyp, optLeg.SettlementType.Value));
                        if (optLeg.SettlementType.Value != (int)JpmFxOptionLegSettlmntType.RegularPhysical)
                            noOptionLegsGrp.SetField(new StringField(Tags.SettlCurrency, optLeg.SettlementCcy));
                    }

                    noRelatedSymGrp.AddGroup(noOptionLegsGrp);
                }

            }

            qr.AddGroup(noRelatedSymGrp);

            return qr;

        }

        protected override void OnStateMachineActivate(object sender, StateMachineActivatedEventArgs args)
        {
            var refId = args.StateMachineRefId;

            _log.InfoFormat("State Machine {0} has been activated.", refId);

            JpmFxOptionsRFQStateMachine stMchn = null;

            if (_activeRFQStateMachines.TryGetValue(refId, out stMchn))
            {
                if (_isLoggedOn && stMchn.CurrentState is WaitingForLogonS)
                    stMchn.OnInboundFIXMessage(this, new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow));
            }
        }

        protected override void OnStateMachineDeactivate(object sender, StateMachineDeactivatedEventArgs args)
        {
            var refId = args.StateMachineRefId;

            _log.InfoFormat("State Machine {0} has been deactivated. Removing it from ActiveRFQStateMachines ...", refId);

            JpmFxOptionsRFQStateMachine deactivatingRfqStateMachine = null;

            if (_activeRFQStateMachines.ContainsKey(refId))
                if (_activeRFQStateMachines.TryRemove(refId, out deactivatingRfqStateMachine))
                    _log.InfoFormat("State Machine {0} removal has succeeded.", refId);
                else
                    _log.InfoFormat("State Machine {0} removal has failed.", refId);
            else
                _log.WarnFormat("State Machine {0} was not found in the Active RFQ State Machine Pool. Nothing to be done.", refId);

            _log.InfoFormat("ActiveRFQStateMachines count: {0}", _activeRFQStateMachines.Count);
        }


        #region QuickFix iApplication Methods Overrides
        public override void OnLogon(SessionID sessionID)
        {
            base.OnLogon(sessionID);
            _actionAfterLogonTimer.Enabled = true;
        }

        public override void OnLogout(SessionID sessionID)
        {
            base.OnLogout(sessionID);
            _actionAfterLogonTimer.Enabled = false;
            _rfqStateMachineActivationTimer.Enabled = false;
        }
        #endregion QuickFix iApplication Methods Overrides


        protected override void OnStateMachineOutboundFIXMessage(object sender, FIXMessageDetailedEventArgs msgDetailsEventArgs)
        {
            _log.DebugFormat("OnStateMachineOutboundFIXMessage ...");

            if (msgDetailsEventArgs.MessageType == FIXMessageType.QuoteRequest_R)
            {
                var refId = msgDetailsEventArgs.RefId;
                var dbabfxOptionQRArgs = (JpmFxOptionsQuoteRequestFIXMessageArgs)msgDetailsEventArgs.MessageArgs;

                var qrFIXMsg = MakeOptionQuoteRequest(refId, dbabfxOptionQRArgs);

                Session.SendToTarget(qrFIXMsg, _sessionID);
            }
            else
                _log.WarnFormat("Message Type {0} is not supported. The FIX message will not be composed or sent.", msgDetailsEventArgs.MessageType);
        }

        public void OnMessage(FIX42.QuoteAcknowledgement quoteAck, SessionID sessionID)
        {
            var quoteReqRefId = quoteAck.QuoteReqID.getValue();
            var quoteReqAckStatus = quoteAck.QuoteAckStatus.getValue();

            _log.InfoFormat("OnMessage Quote Ack - QuoteReqId: {0} - Ack Status: {1}", quoteReqRefId, quoteReqAckStatus);

            FIXMessageEventArgs fixMsgEvtArgs = null;

            // 0 - Accepted
            if (quoteReqAckStatus == 0)
            {
                fixMsgEvtArgs = new FIXMessageEventArgs(quoteReqRefId, FIXMessageType.QuoteAck_b_Accepted, DateTime.UtcNow);
            }
            // 5 - Rejected
            else if (quoteReqAckStatus == 5)
            {
                fixMsgEvtArgs = new FIXMessageEventArgs(quoteReqRefId, FIXMessageType.QuoteAck_b_Rejected, DateTime.UtcNow);
            }
            // 7 - Expired
            else if (quoteReqAckStatus == 7)
            {
                fixMsgEvtArgs = new FIXMessageEventArgs(quoteReqRefId, FIXMessageType.QuoteAck_b_Expired, DateTime.UtcNow);
            }
            
            if (fixMsgEvtArgs != null)
            {
                JpmFxOptionsRFQStateMachine stMchn = null;

                if (_activeRFQStateMachines.TryGetValue(quoteReqRefId, out stMchn))
                    stMchn.OnInboundFIXMessage(this, fixMsgEvtArgs);
            }
        }

        public void OnMessage(FIX42.Quote quote, SessionID sessionID)
        {
            var quoteReqRefId = quote.QuoteReqID.getValue();

            _log.InfoFormat("OnMessage Quote - QuoteReqId: {0}", quoteReqRefId);

            var fixMsgEvtArgs = new FIXMessageEventArgs(quoteReqRefId, FIXMessageType.Quote_S, DateTime.UtcNow);

            JpmFxOptionsRFQStateMachine stMchn = null;

            if (_activeRFQStateMachines.TryGetValue(quoteReqRefId, out stMchn))
                stMchn.OnInboundFIXMessage(this, fixMsgEvtArgs);

            var quoteId = quote.QuoteID.getValue();
            var symbol = quote.Symbol.getValue();
            symbol = symbol.Replace("/", string.Empty);
            var ccy = quote.Currency.getValue();
            var premCcy = quote.GetString(JpmFIXConstants.Tag_9118_PremiumCurrency);

            double? bidPx, offerPx;

            if (quote.IsSetBidPx())
                bidPx = (double)quote.BidPx.getValue();
        
            if (quote.IsSetOfferPx())
                offerPx = (double)quote.OfferPx.getValue();

            double? bidSpotRate, offerSpotRate;

            if (quote.IsSetBidSpotRate())
                bidSpotRate = (double)quote.BidSpotRate.getValue();

            if (quote.IsSetOfferSpotRate())
                offerSpotRate = (double)quote.OfferSpotRate.getValue();

            #region NoHedgeLegs
            var noHedgeLegsGrp = new Group(JpmFIXConstants.Tag_9165_NoHedgeLegs, JpmFIXConstants.Tag_9106_DeltaHedgeAmount);
            var noHedgeLegsGrpCount = quote.GetInt(JpmFIXConstants.Tag_9165_NoHedgeLegs);

            for (int noHedgeLegsGrpIdx = 1; noHedgeLegsGrpIdx <= noHedgeLegsGrpCount; noHedgeLegsGrpIdx++)
            {
                quote.GetGroup(noHedgeLegsGrpIdx, noHedgeLegsGrp);

                #region HedgeLeg

                var deltaHedgeAmt = noHedgeLegsGrp.GetDecimal(JpmFIXConstants.Tag_9106_DeltaHedgeAmount);
                var deltaHedgeCcy = noHedgeLegsGrp.GetString(JpmFIXConstants.Tag_9107_DeltaHedgeCurrency);
                var deltaHedgeRate = noHedgeLegsGrp.GetDecimal(JpmFIXConstants.Tag_9108_DeltaHedgeRate);

                #endregion HedgeLeg
            }
            #endregion NoHedgeLegs

            #region NoLegs

            var vanOptsPxDtls = new List<SingleVanilaOptionContractPriceDetails>();

            var noLegs = new Group(Tags.NoLegs, Tags.LegRefID);
            var noLegsGrpCount = quote.GetInt(JpmFIXConstants.Tag_555_NoLegs);

            for (int noLegsGrpIdx = 1; noLegsGrpIdx <= noLegsGrpCount; noLegsGrpIdx++)
            {
                quote.GetGroup(noLegsGrpIdx, noLegs);

                #region Leg

                var legSideValue = noLegs.GetInt(JpmFIXConstants.Tag_624_LegSide);
                var legQty = (double)noLegs.GetDecimal(JpmFIXConstants.Tag_687_LegQty);
                var settlmntType = noLegs.GetInt(Tags.SettlmntTyp);
                var settlCcy = noLegs.GetString(Tags.SettlCurrency);

                var isBidFwdPtsSet = noLegs.IsSetField(Tags.BidForwardPoints);
                var isOfferFwdPtsSet = noLegs.IsSetField(Tags.OfferForwardPoints);

                double? bidFwdPts = null;
                double? offerFwdPts = null;

                if (isBidFwdPtsSet)
                    bidFwdPts = (double)noLegs.GetDecimal(Tags.BidForwardPoints);

                if (isOfferFwdPtsSet)
                    offerFwdPts = (double)noLegs.GetDecimal(Tags.OfferForwardPoints);

                var isBidPremSet = noLegs.IsSetField(JpmFIXConstants.Tag_9161_LegBidPremium);
                var isOfferPremSet = noLegs.IsSetField(JpmFIXConstants.Tag_9162_LegOfferPremium);

                double? legBidPrem = null;
                double? legOfferPrem = null;

                if (isBidPremSet)
                    legBidPrem = (double)noLegs.GetDecimal(JpmFIXConstants.Tag_9161_LegBidPremium);

                if (isOfferPremSet)
                    legOfferPrem = (double)noLegs.GetDecimal(JpmFIXConstants.Tag_9162_LegOfferPremium);

                var putCallValue = noLegs.GetInt(Tags.PutOrCall);

                var strikePrice = (double)noLegs.GetDecimal(JpmFIXConstants.Tag_612_LegStrikePrice);

                var expDate = noLegs.GetDateOnly(JpmFIXConstants.Tag_9112_ExpirationDate);
                var delDate = noLegs.GetDateOnly(JpmFIXConstants.Tag_743_DeliveryDate);

                var depoRate = noLegs.GetDecimal(JpmFIXConstants.Tag_9110_DepositRate);
                var counterDepoRate = noLegs.GetDecimal(JpmFIXConstants.Tag_9120_CounterDepositRate);

                var expCut = noLegs.GetString(JpmFIXConstants.Tag_9111_ExpirationCut);

                var bidVol = (double)noLegs.GetDecimal(JpmFIXConstants.Tag_9167_LegBidVolatility);
                var offerVol = (double)noLegs.GetDecimal(JpmFIXConstants.Tag_9168_LegAskVolatility);

                var legCallPut = OptionCallPut.Call;

                switch(putCallValue)
                {
                    case 0:
                        legCallPut = OptionCallPut.Put;
                        break;
                    case 1:
                        legCallPut = OptionCallPut.Call;
                        break;
                }

                var legSide = BuySell.Buy;

                switch(legSideValue)
                {
                    case 1:
                        legSide = BuySell.Buy;
                        break;
                    case 2:
                        legSide = BuySell.Sell;
                        break;
                }

                #endregion Leg

                var sglVanOptPxDtls = new SingleVanilaOptionContractPriceDetails();

                sglVanOptPxDtls.SecuritySymbol = symbol;
                sglVanOptPxDtls.OptionNotionalCurrency = ccy;
                sglVanOptPxDtls.OptionPremiumCurrency = premCcy;

                if (legBidPrem.HasValue)
                    sglVanOptPxDtls.BidPx = legBidPrem.Value;

                if (legOfferPrem.HasValue)
                    sglVanOptPxDtls.OfferPx = legOfferPrem.Value;

                sglVanOptPxDtls.BidVolatility = bidVol;
                sglVanOptPxDtls.OfferVolatility = offerVol;

                sglVanOptPxDtls.Notional = legQty;

                sglVanOptPxDtls.CallPut = legCallPut;
                sglVanOptPxDtls.Side = legSide;
                sglVanOptPxDtls.StrikePrice = strikePrice;

                sglVanOptPxDtls.MaturityDate = expDate;
                sglVanOptPxDtls.SettlementDate = delDate;

                vanOptsPxDtls.Add(sglVanOptPxDtls);

            }
            #endregion NoLegs

            if (vanOptsPxDtls.Count == 1)
            {
                var sglVanOptPxDtls = vanOptsPxDtls[0];
                var sglVanOptQuote = new SingleVanillaOptionQuote(symbol, quoteReqRefId, quoteId, sglVanOptPxDtls);

                ProcessVanillaOptionQuote(sglVanOptQuote);
            }

        }

        public void OnMessage(FIX42.QuoteCancel quoteCancel, SessionID sessionID)
        {
            var quoteReqRefId = quoteCancel.QuoteReqID.getValue();

            _log.InfoFormat("OnMessage QuoteCancel - QuoteReqId: {0}", quoteCancel.QuoteReqID);

            var fixMsgEvtArgs = new FIXMessageEventArgs(quoteReqRefId, FIXMessageType.QuoteCancel_Z, DateTime.UtcNow);

            JpmFxOptionsRFQStateMachine stMchn = null;

            if (_activeRFQStateMachines.TryGetValue(quoteReqRefId, out stMchn))
                stMchn.OnInboundFIXMessage(this, fixMsgEvtArgs);
        }

        protected virtual void ProcessVanillaOptionQuote(SingleVanillaOptionQuote sglLegVanOptQuote)
        {

            var quoteRequestRefId = sglLegVanOptQuote.QuoteRequestRefId;
            var quoteId = sglLegVanOptQuote.QuoteId;
            var currencyPair = sglLegVanOptQuote.SecuritySymbol;
            var maturityDate_yyyyMMdd = sglLegVanOptQuote.OptionPriceDetails.MaturityDate.ToString("yyyyMMdd");
            var bidVol = sglLegVanOptQuote.OptionPriceDetails.BidVolatility;
            var offerVol = sglLegVanOptQuote.OptionPriceDetails.OfferVolatility;

            _log.InfoFormat("QuoteRequestRefId: {0} - QuoteId: {1} - Single Vanilla Option - MaturityDate: {2}, BidVol: {3}, OfferVol: {4}",
                            quoteRequestRefId, quoteId, maturityDate_yyyyMMdd, bidVol, offerVol);

            var sglLegVanOptQtJson = JsonConvert.SerializeObject(sglLegVanOptQuote);

            FIXKafkaProducer.SendRfqAsync(KafkaRFQSource.jpm, currencyPair, sglLegVanOptQtJson);

        }

        protected virtual void ProcessVanillaOptionQuote(MultiLegVanillaOptionsQuote multiLegVanillaOptionsQuotes)
        {
            // To be implemented
            return;
        }

        protected SingleVanillaOptionQuote MakeSingleVanillaOptionQuote()
        {
            // To be implemented
            return null;
        }
    }
}
