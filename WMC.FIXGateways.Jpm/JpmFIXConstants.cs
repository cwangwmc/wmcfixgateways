﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXGateways.Jpm
{
    class JpmFIXConstants
    {

        public static readonly int Tag_9002_Tenor = 9002;
        public static readonly int Tag_9105_Delta = 9105;
        public static readonly int Tag_9110_DepositRate = 9110;
        public static readonly int Tag_9111_ExpirationCut = 9111;
        public static readonly int Tag_9120_CounterDepositRate = 9120;
        public static readonly int Tag_9133_Volatility = 9133;

        public static readonly int Tag_9004_AccountID = 9004;
        public static readonly int Tag_9005_QuoteRequestSubscriptionType = 9005;
        public static readonly int Tag_9008_TraderIdentifer = 9008;
        public static readonly int Tag_9009_IsRFQ = 9009;
        public static readonly int Tag_9220_IsSEF = 9220;
        public static readonly int Tag_9124_SolvingFor = 9124;
        public static readonly int Tag_9170_CustomHedgeSpot = 9170;
        public static readonly int Tag_9113_HedgeType = 9113;
        public static readonly int Tag_9165_NoHedgeLegs = 9165;
        public static readonly int Tag_9107_DeltaHedgeCurrency = 9107;
        public static readonly int Tag_9108_DeltaHedgeRate = 9108;
        public static readonly int Tag_9106_DeltaHedgeAmount = 9106;
        public static readonly int Tag_9182_LegTenor = 9182;
        public static readonly int Tag_9112_ExpirationDate = 9112;
        public static readonly int Tag_9118_PremiumCurrency = 9118;
        public static readonly int Tag_9117_PremiumAmount = 9117;
        public static readonly int Tag_9180_PremiumPayOn = 9180;
        public static readonly int Tag_9119_PriceReferenceMetric = 9119;
        public static readonly int Tag_9221_USIValue = 9221;
        public static readonly int Tag_624_LegSide = 624;
        public static readonly int Tag_687_LegQty = 687;
        public static readonly int Tag_9181_LegStrike = 9181;
        public static readonly int Tag_9183_LegBarrier = 9183;
        public static readonly int Tag_9187_BarrierType = 9187;
        public static readonly int Tag_9188_BarrierObservation = 9188;
        public static readonly int Tag_743_DeliveryDate = 743;
        public static readonly int Tag_9132_EffectiveDate = 9132;
        public static readonly int Tag_9126_DepositInputType = 9126;
        public static readonly int Tag_9122_Yield = 9122;
        public static readonly int Tag_9130_MarginInputType = 9130;
        public static readonly int Tag_9115_Margin = 9115;
        public static readonly int Tag_9185_MarginInCcy = 9185;
        public static readonly int Tag_9186_PayTime = 9186;

        public static readonly int Tag_9134_BidPremiumAmount = 9134;
        public static readonly int Tag_9135_OfferPremiumAmount = 9135;
        public static readonly int Tag_9050_MarketMidRate = 9050;
        public static readonly int Tag_9103_BidVolatility = 9103;
        public static readonly int Tag_9102_AskVolatility = 9102;
        public static readonly int Tag_9166_DeltaHedgeSide = 9166;
        public static readonly int Tag_9109_DeltaHedgeValueDate = 9109;
        public static readonly int Tag_555_NoLegs = 555;
        public static readonly int Tag_654_LegRefID = 654;
        public static readonly int Tag_612_LegStrikePrice = 612;
        public static readonly int Tag_9139_PremiumPaymentDate = 9139;
        public static readonly int Tag_9161_LegBidPremium = 9161;
        public static readonly int Tag_9162_LegOfferPremium = 9162;
        public static readonly int Tag_9053_LegMarketMidRate = 9053;
        public static readonly int Tag_9167_LegBidVolatility = 9167;
        public static readonly int Tag_9168_LegAskVolatility = 9168;
        public static readonly int Tag_9104_DayCount = 9104;
        public static readonly int Tag_9116_OptionNotional = 9116;
        public static readonly int Tag_9101_AltCcyOptionNotional = 9101;
        public static readonly int Tag_9171_SpotDelta = 9171;
        public static readonly int Tag_9172_SpotDeltaPct = 9172;
        public static readonly int Tag_9173_FwdDelta = 9173;
        public static readonly int Tag_9174_FwdDeltaPct = 9174;
        public static readonly int Tag_9175_GammaDetla = 9175;
        public static readonly int Tag_9176_GammaDetlaPct = 9176;
        public static readonly int Tag_9177_VegaDelta = 9177;
        public static readonly int Tag_9178_VegaDeltaPct = 9178;
        public static readonly int Tag_9179_BSTVPct = 9179;

        public static readonly char SubReqType_Sub = '1'; // Snapshot + Updates
        public static readonly char SubReqType_UnSub = '2'; // Disable previous Snapshot + Update Request (Unsubscribe)

        public static readonly char QuoteReqSubType_Sub = '1';
        public static readonly char QuoteReqSubType_UnSub = '2';

        public static readonly int MarketDepth_FullBook = 0; // 0 - Full Book

        public static readonly int MDUpdateType_IncrRefresh = 1; // 1 - Incremental Refresh

        public static readonly char MDEntryType_Bid = '0';
        public static readonly char MDEntryType_Offer = '1';

        public static readonly string SecurityDesc_ATM = "ATM";
        public static readonly string SecurityDesc_RR = "RR";
        public static readonly string SecurityDesc_BF = "BF";

        public static readonly string SecurityType_Option = "OPT";

        public static readonly char MDUpdateAction_New = '0';
        public static readonly char MDUpdateAction_Change = '1';
        public static readonly char MDUpdateAction_Delete = '2';

        public static readonly string ExpiryCut_NewYork10AM = "NYC";

        public static readonly Dictionary<string, string> NonNYCcyExpiryCuts = new Dictionary<string, string>()
        {
            { "SGD", "TOK" },
            { "THB", "TOK" },
            { "MYR", "MYRFIX" },
            { "INR", "INR" },
            { "HKD", "TOK" },
            { "TWD", "TWD" },
            { "KRW", "KRW1530" },
            { "CNY", "CNY" },
            { "CNH", "TOK" },
            { "ILS", "ILS" },
            { "RUB", "RRF" },
            { "TRY", "TRY" },
            { "PLN", "PLN" },
            { "HUF", "HUF" },
            { "RON", "RON1300" },
            { "MXN", "MXN" },
            { "BRL", "BRL" },
        };

    }
}
