﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXStateMachines;

namespace WMC.FIXGateways.Jpm
{

    public class JpmFxOption
    {
        public JpmFxOptionSecurityDesc OptionDesc { get; private set; }
        public JpmFxOptionSolvingFor OptionSolvingFor { get; private set; }
        public JpmFxOptionSide Side { get; private set; }
        public string Currency { get; private set; }

        public string PremCurrency { get; private set; }
        public double? PremAmount { get; private set; }
        public JpmFxOptionPremiumPayOn PremPayOn { get; private set; }
        public JpmFxOptionPriceRefMetric PxRefMetric { get; private set; }

        public double? CustomHedgeSpot { get; private set; }
        public JpmFxOptionHedgeType HedgeType { get; private set; }

        public IEnumerable<JpmFxOptionHedgeLeg> OptionHedgeLegs { get; private set; }
        public IEnumerable<JpmFxOptionLeg> OptionLegs { get; private set; }

        public JpmFxOption(string notionalCcy,
            IEnumerable<JpmFxOptionLeg> optLegs, IEnumerable<JpmFxOptionHedgeLeg> optHedgeLegs,
            JpmFxOptionSecurityDesc optDesc = JpmFxOptionSecurityDesc.VANILLA,
            JpmFxOptionSolvingFor optSolvingFor = JpmFxOptionSolvingFor.PREMIUM,
            JpmFxOptionSide optSide = JpmFxOptionSide.Buy,
            JpmFxOptionPremiumPayOn premPayOn = JpmFxOptionPremiumPayOn.SpotDate,
            JpmFxOptionPriceRefMetric pxRefMetric = JpmFxOptionPriceRefMetric.PctCcy1,
            double? customHedgeSpot = null,
            JpmFxOptionHedgeType hedgeType = JpmFxOptionHedgeType.SpotHedge,
            string premCcy = "", double? premAmt = null)
        {
            OptionDesc = optDesc;
            OptionSolvingFor = optSolvingFor;
            Side = optSide;
            Currency = notionalCcy;

            OptionLegs = optLegs;

            if (optHedgeLegs == null)
                OptionHedgeLegs = new List<JpmFxOptionHedgeLeg>();
            else
                OptionHedgeLegs = optHedgeLegs;

            PremCurrency = premCcy == string.Empty ? notionalCcy : premCcy;
            PremAmount = premAmt;
            PremPayOn = premPayOn;
            PxRefMetric = pxRefMetric;

            CustomHedgeSpot = customHedgeSpot;
            HedgeType = hedgeType;
        }
    }


    public class JpmFxOptionLeg
    {
        public int RefID { get; private set; }
        public string USIValue { get; private set; }
        public JpmFxOptionLegSecurityDesc OptionLegSecDesc { get; private set; }
        public JpmFxOptionSide OptionLegSide { get; private set; }
        public JpmFxOptionLegPutOrCall OptionLegPutOrCall { get; private set; }
        public double OptionLegQty { get; private set; }
        public string OptionLegStrike { get; private set; }
        public string OptionLegTenor { get; private set; }
        public DateTime? ExpirationDate { get; private set; }
        public string ExpirationCut { get; private set; }

        public JpmFxOptionLeg(int refId, string strike, string tenor,
            JpmFxOptionLegSecurityDesc desc = JpmFxOptionLegSecurityDesc.VANILLA,
            JpmFxOptionSide side = JpmFxOptionSide.Buy,
            JpmFxOptionLegPutOrCall putOrCall = JpmFxOptionLegPutOrCall.Call,
            double qty = 1_000_000.0, string expCut = "NYC")
        {
            RefID = refId;
            OptionLegStrike = strike;
            OptionLegTenor = tenor;

            OptionLegSecDesc = desc;
            OptionLegSide = side;
            OptionLegPutOrCall = putOrCall;

            OptionLegQty = qty;
            ExpirationCut = expCut;
        }

        public JpmFxOptionLeg(int refId, string strike, DateTime expDate,
            JpmFxOptionLegSecurityDesc optLegDesc = JpmFxOptionLegSecurityDesc.VANILLA,
            JpmFxOptionSide optLegSide = JpmFxOptionSide.Buy,
            JpmFxOptionLegPutOrCall optLegPutOrCall = JpmFxOptionLegPutOrCall.Call,
            double qty = 1_000_000.0, string expCut = "NYC")
        {
            RefID = refId;
            OptionLegStrike = strike;
            ExpirationDate = expDate;

            OptionLegSecDesc = optLegDesc;
            OptionLegSide = optLegSide;
            OptionLegPutOrCall = optLegPutOrCall;

            OptionLegQty = qty;
            ExpirationCut = expCut;
        }
    }


    public class JpmFxOptionHedgeLeg
    {
        public string DeltaHedgeCurrency { get; private set; }
        public double? DeltaHedgeRate { get; private set; }
        public double? DeltaHedgeAmount { get; private set; }
        public string Tenor { get; private set; }
        public DateTime? ExpirationDate { get; private set; }

        public JpmFxOptionHedgeLeg(string hedgeCcy, double? hedgeRate = null, double? hedgeAmt = null, string tenor = "")
        {

        }

        public JpmFxOptionHedgeLeg(string hedgeCcy, double? hedgeRate = null, double? hedgeAmt = null, DateTime? expDate = null)
        {

        }
    }


    public class JpmFxOptionsRFQStateMachine : RFQStateMachine
    {

        private JpmFxOption _jpmFxOption { get; set; }

        public JpmFxOptionsRFQStateMachine(string stateMachineRefId, JpmFxOption jpmFxOption, string securitySymbol, bool loggedOn,
            long? timeToSendQuoteRequest, long? timeToWaitForQuote)
            :base(stateMachineRefId, securitySymbol, loggedOn, timeToSendQuoteRequest, timeToWaitForQuote)
        {
            _jpmFxOption = jpmFxOption;

            // States
            var waitingLogonState = new WaitingForLogonS(stateMachineRefId);
            var sendingQuoteRequestState = new SendingQuoteRequestS(stateMachineRefId, TimeToSendQuoteRequest);
            var waitingForQuoteRespState = new WaitingForResponseS(stateMachineRefId, TimeToWaitForQuote);
            var doneState = new DoneS(stateMachineRefId);

            // Transitions linking the States
            var logonT = new LogonT(waitingLogonState, sendingQuoteRequestState);

            var logoffT_A = new LogoffT(sendingQuoteRequestState, doneState);
            var logoffT_B = new LogoffT(waitingForQuoteRespState, doneState);

            var quoteRequestT = new QuoteRequestT(sendingQuoteRequestState, waitingForQuoteRespState);

            var quoteAckAcceptT = new QuoteRequestAcceptT(waitingForQuoteRespState, waitingForQuoteRespState);

            var quoteAckRejectT = new QuoteRequestRejectT(waitingForQuoteRespState, doneState);

            var quoteT = new QuoteT(waitingForQuoteRespState, doneState);

            var quoteCancelT = new QuoteCancelT(waitingForQuoteRespState, doneState);

            var timeOutT = new TimedOutT(waitingForQuoteRespState, doneState);

            // Decide the starting state based on whether the FIX engine has already logged on
            if (loggedOn)
                OriginState = sendingQuoteRequestState;
            else
                OriginState = waitingLogonState;

            TerminalState = doneState;

            CurrentState = OriginState;

            CurrentState.StateFIXMessageOut += OnOutboundFIXMessage;
            CurrentState.StateEntered += OnStateEntered;
            CurrentState.StateExited += OnStateExited;

            foreach (var t in CurrentState.Transitions)
                t.Triggered += OnStateTransition;
        }

        protected override FIXMessageDetailedEventArgs MakeFIXMessageDetailedEventArgs(FIXMessageEventArgs args)
        {
            FIXMessageDetailedEventArgs dtlEvtArg = null;

            if (args.MessageType == FIXMessageType.QuoteRequest_R
                || args.MessageType == FIXMessageType.QuoteRequest_R_UnSub)
            {
                var ccy1 = SecuritySymbol.Substring(0, 3);
                var ccy2 = SecuritySymbol.Substring(3, 3);

                var optSolvingFor = _jpmFxOption.OptionSolvingFor.ToString();

                var premCcy = _jpmFxOption.PremCurrency;
                var notionalCcy = _jpmFxOption.Currency;

                var secDesc = _jpmFxOption.OptionDesc.ToString();
                var sideChar = ((int)_jpmFxOption.Side).ToString().ToCharArray()[0];
                var hedgeType = (int)_jpmFxOption.HedgeType;
                var premPayOn = (int)_jpmFxOption.PremPayOn;
                
                var optLegs = new List<JpmFxOptionsLegFIXMessageArgs>();

                foreach (var sglVanOptLeg in _jpmFxOption.OptionLegs)
                {
                    var optLegRefId = sglVanOptLeg.RefID;
                    var optLegSecDesc = sglVanOptLeg.OptionLegSecDesc.ToString();
                    var optLegSide = ((int)sglVanOptLeg.OptionLegSide).ToString().ToCharArray()[0];
                    var optLegPutOrCall = (int)sglVanOptLeg.OptionLegPutOrCall;
                    var optLegQty = sglVanOptLeg.OptionLegQty;
                    var optLegStrike = sglVanOptLeg.OptionLegStrike;
                    var optLegTenor = sglVanOptLeg.OptionLegTenor;
                    var optLegExpCut = sglVanOptLeg.ExpirationCut;
                    
                    var singleVanillaOptLegArgs = new JpmFxOptionsLegFIXMessageArgs(optLegRefId,
                        optLegSecDesc, optLegPutOrCall, optLegStrike, optLegTenor, optLegSide, optLegQty, optLegExpCut);

                    optLegs.Add(singleVanillaOptLegArgs);
                }

                var hedgeLegs = new List<JpmFxOptionsHedgeLegFIXMessageArgs>();

                var quoteRequestSubType = JpmFIXConstants.QuoteReqSubType_Sub;

                if (args.MessageType == FIXMessageType.QuoteRequest_R_UnSub)
                    quoteRequestSubType = JpmFIXConstants.QuoteReqSubType_UnSub;

                var qrArgs = new JpmFxOptionsQuoteRequestFIXMessageArgs(ccy1, ccy2, optLegs, hedgeLegs,
                    optSolvingFor, quoteRequestSubType, secDesc, sideChar, hedgeType, premCcy, premPayOn: premPayOn);

                dtlEvtArg = new FIXMessageDetailedEventArgs(StateMachineRefId, FIXMessageType.QuoteRequest_R, qrArgs);
            }
            
            return dtlEvtArg;
        }
    }

}
