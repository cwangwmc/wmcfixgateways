﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXDataObjects;
using WMC.FIXKafkaClients;
using WMC.FXConventionHelper;

using QuickFix;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WMC.FIXGateways.Jpm
{
    public class JpmFIXRfqVolsAdaptor : JpmFIXRfqAdaptor
    {

        public IEnumerable<string> CurrencyPairs { get; private set; }
        public IEnumerable<Tenor> VolSurfaceTenors { get; private set; }

        private string _quoteRequestRefIdFormat = "Vols_{0}_[{1}]_({2})_{3}";
        private Dictionary<string, VolatilitySurface> _ccypairs_volSurfs = new Dictionary<string, VolatilitySurface>();

        public JpmFIXRfqVolsAdaptor(string accountId, string password,
                                    IEnumerable<string> ccypairs, IEnumerable<string> volSurfTenors)
            :this(accountId, password,
                 _defaultActiveQuoteRequestsLimit, ccypairs, volSurfTenors)
        {

        }

        public JpmFIXRfqVolsAdaptor(string accountId, string password,
            int activeQuoteRequestLimit, IEnumerable<string> ccypairs, IEnumerable<string> volSurfTenors)
            :base(accountId, password, activeQuoteRequestLimit)
        {
            CurrencyPairs = ccypairs;
            VolSurfaceTenors = volSurfTenors.Select(vstnr => Tenor.Parse(vstnr));

            var volsVanOptQRs = MakeVolsVanillaOptionQuoteRequest(ccypairs, volSurfTenors);

            InitialiseAndLoadRfqStateMachineQueue(volsVanOptQRs);
        }

        protected override void InitialiseAndLoadRfqStateMachineQueue(IEnumerable<VanillaOptionsQuoteRequest> optionsQuoteRequests)
        {
            base.InitialiseAndLoadRfqStateMachineQueue(optionsQuoteRequests);

            var utcnow = DateTime.UtcNow;

            foreach (var ccypair in CurrencyPairs)
                _ccypairs_volSurfs.Add(ccypair, new VolatilitySurface(ccypair, utcnow, KafkaRFQSource.jpm.ToString(), refDate: utcnow.Date));
        }

        private IEnumerable<SingleVanillaOptionQuoteRequest> MakeVolsVanillaOptionQuoteRequest(IEnumerable<string> ccypairs, IEnumerable<string> volSurfTenors)
        {
            var volsVanOptQRs = new List<SingleVanillaOptionQuoteRequest>();

            foreach (var ccypair in ccypairs)
            {

                var leftCcy = ccypair.Substring(0, 3);
                var rightCcy = ccypair.Substring(3, 3);

                var premCcy = leftCcy;
                var notionalCcy = leftCcy;

                var pcbp = "pc";

                if (rightCcy.ToUpper() == "USD")
                {
                    premCcy = "USD";
                    pcbp = "bp";
                }

                var atmStrikeType = OptionStrikeType.DeltaNeutral;
                var wingStrikeType = OptionStrikeType.DeltaSpot;

                var fwdspt = "ds";

                if (FXConvHelper.IsEM(ccypair))
                {
                    atmStrikeType = OptionStrikeType.AtmForward;
                    wingStrikeType = OptionStrikeType.DeltaForward;
                    fwdspt = "df";
                }

                foreach (var tnr in VolSurfaceTenors)
                {

                    var guid_str = Guid.NewGuid().ToString();
                    var guid_str_parts = guid_str.Split('-');
                    var guid_str_final_part = guid_str_parts[guid_str_parts.Length - 1];

                    var sglVanOptQuoteReqLeg_10c = new SingleVanillaOptionContract(tnr.TenorCode, null, 10.0, wingStrikeType, OptionCallPut.Call,
                        BuySell.Buy, premCcy, notionalCcy);
                    var refId_10c = string.Format(_quoteRequestRefIdFormat, ccypair, tnr.TenorCode, "10" + pcbp + fwdspt + "c", guid_str_final_part);
                    var sglVanOptQuoteReq_10c = new SingleVanillaOptionQuoteRequest(refId_10c, ccypair, premCcy, notionalCcy, sglVanOptQuoteReqLeg_10c);

                    var sglVanOptQuoteReqLeg_25c = new SingleVanillaOptionContract(tnr.TenorCode, null, 25.0, wingStrikeType, OptionCallPut.Call,
                        BuySell.Buy, premCcy, notionalCcy);
                    var refId_25c = string.Format(_quoteRequestRefIdFormat, ccypair, tnr.TenorCode, "25" + pcbp + fwdspt + "c", guid_str_final_part);
                    var sglVanOptQuoteReq_25c = new SingleVanillaOptionQuoteRequest(refId_25c, ccypair, premCcy, notionalCcy, sglVanOptQuoteReqLeg_25c);

                    var sglVanOptQuoteReqLeg_atm = new SingleVanillaOptionContract(tnr.TenorCode, null, null, atmStrikeType, OptionCallPut.Call,
                        BuySell.Buy, premCcy, notionalCcy);
                    var refId_atm = string.Format(_quoteRequestRefIdFormat, ccypair, tnr.TenorCode, atmStrikeType, guid_str_final_part);
                    var sglVanOptQuoteReq_atm = new SingleVanillaOptionQuoteRequest(refId_atm, ccypair, premCcy, notionalCcy, sglVanOptQuoteReqLeg_atm);

                    var sglVanOptQuoteReqLeg_25p = new SingleVanillaOptionContract(tnr.TenorCode, null, 25.0, wingStrikeType, OptionCallPut.Put,
                        BuySell.Buy, premCcy, notionalCcy);
                    var refId_25p = string.Format(_quoteRequestRefIdFormat, ccypair, tnr.TenorCode, "25" + pcbp + fwdspt + "p", guid_str_final_part);
                    var sglVanOptQuoteReq_25p = new SingleVanillaOptionQuoteRequest(refId_25p, ccypair, premCcy, notionalCcy, sglVanOptQuoteReqLeg_25p);

                    var sglVanOptQuoteReqLeg_10p = new SingleVanillaOptionContract(tnr.TenorCode, null, 10.0, wingStrikeType, OptionCallPut.Put,
                        BuySell.Buy, premCcy, notionalCcy);
                    var refId_10p = string.Format(_quoteRequestRefIdFormat, ccypair, tnr.TenorCode, "10" + pcbp + fwdspt + "p", guid_str_final_part);
                    var sglVanOptQuoteReq_10p = new SingleVanillaOptionQuoteRequest(refId_10p, ccypair, premCcy, notionalCcy, sglVanOptQuoteReqLeg_10p);

                    volsVanOptQRs.Add(sglVanOptQuoteReq_10c);
                    volsVanOptQRs.Add(sglVanOptQuoteReq_25c);
                    volsVanOptQRs.Add(sglVanOptQuoteReq_atm);
                    volsVanOptQRs.Add(sglVanOptQuoteReq_25p);
                    volsVanOptQRs.Add(sglVanOptQuoteReq_10p);

                }
            }

            return volsVanOptQRs;
        }

        private void GetTenorStrikeFromQuoteRequestRefId(string quoteRequestRefId, out string tenor, out string strikeExpr)
        {
            var leftSquareBracketIdx = quoteRequestRefId.IndexOf('[');
            var rightSquareBracketIdx = quoteRequestRefId.IndexOf(']');

            var leftBracketIdx = quoteRequestRefId.IndexOf('(');
            var rightBracketIdx = quoteRequestRefId.IndexOf(')');

            tenor = quoteRequestRefId.Substring(leftSquareBracketIdx + 1, rightSquareBracketIdx - leftSquareBracketIdx - 1);
            strikeExpr = quoteRequestRefId.Substring(leftBracketIdx + 1, rightBracketIdx - leftBracketIdx - 1);
        }

        protected override void ProcessVanillaOptionQuote(SingleVanillaOptionQuote sglLegVanillaOptionQuote)
        {
            base.ProcessVanillaOptionQuote(sglLegVanillaOptionQuote);

            var ccypair = sglLegVanillaOptionQuote.SecuritySymbol;

            var quoteRequestRefId = sglLegVanillaOptionQuote.QuoteRequestRefId;

            var tenorExpr = string.Empty;
            var strikeExpr = string.Empty;

            GetTenorStrikeFromQuoteRequestRefId(quoteRequestRefId, out tenorExpr, out strikeExpr);

            var midVolInPercentage = 0.5 * (sglLegVanillaOptionQuote.BidVolatility + sglLegVanillaOptionQuote.OfferVolatility);

            midVolInPercentage = Math.Round(midVolInPercentage, 2);

            var isAtm = strikeExpr.ToLower() == OptionStrikeType.AtmForward.ToString().ToLower()
                || strikeExpr.ToLower() == OptionStrikeType.DeltaNeutral.ToString().ToLower();

            VolatilitySmilePoint vspt = null;

            if (isAtm)
            {
                var atmType = strikeExpr.ToLower() == OptionStrikeType.AtmForward.ToString().ToLower() ? Atm.Atmf : Atm.Dn;

                vspt = new VolatilitySmilePoint(midVolInPercentage, atmType);
            }
            else
            {
                var delta = int.Parse(strikeExpr.Substring(0, 2));
                var callPut = strikeExpr.Last() == 'c' ? DeltaOrientation.Call : DeltaOrientation.Put;
                var deltaTypeExpr = strikeExpr.Substring(2, 4);
                var deltaType = DeltaType.SpotBp;

                switch (deltaTypeExpr)
                {
                    case "bpds":
                        deltaType = DeltaType.SpotBp;
                        break;
                    case "pcds":
                        deltaType = DeltaType.SpotPc;
                        break;
                    case "bpdf":
                        deltaType = DeltaType.ForwardBp;
                        break;
                    case "pcdf":
                        deltaType = DeltaType.ForwardPc;
                        break;
                }

                vspt = new VolatilitySmilePoint(midVolInPercentage, delta, deltaType, callPut);
            }

            VolatilitySurface volSurf = null;
            if (_ccypairs_volSurfs.TryGetValue(ccypair, out volSurf))
            {
                volSurf.AddVolSmilePoint(Tenor.Parse(tenorExpr), vspt);
                _ccypairs_volSurfs[ccypair] = volSurf;
            }
            
            if (volSurf.HasAtmVolPoints(VolSurfaceTenors) && volSurf.HasWingDeltas(VolSurfaceTenors, new int[] { 25, 10 }))
            {

                _log.InfoFormat("{0} has collected all vol surface points.", ccypair);

                _log.InfoFormat("Encoding Json for all tenor dates of {0} ...", ccypair);

                var volSurfForCcypairJson = JsonConvert.SerializeObject(volSurf, new StringEnumConverter());

                _log.DebugFormat("{0} Rfq Vols Json:\n{1}", ccypair, volSurfForCcypairJson);

                _log.InfoFormat("Sending Json for Vols of {0} via Kafka ...", ccypair);

                FIXKafkaProducer.SendRfqVolsAsync(KafkaRFQSource.jpm, volSurfForCcypairJson);
                FIXKafkaProducer.SendRfqVolsAsync(KafkaRFQSource.jpm, ccypair, volSurfForCcypairJson);

            }
        }

        public override void OnLogout(SessionID sessionID)
        {
            base.OnLogout(sessionID);

            _log.InfoFormat("Shutting down after having received Logout...");
            Environment.Exit(0);
        }

        public override void OnRfqStateMachineActivationTimerElapsed(object sender, EventArgs args)
        {
            base.OnRfqStateMachineActivationTimerElapsed(sender, args);

            if (_pendingRFQStateMachineQueue.Count == 0 && _activeRFQStateMachines.Count == 0)
            {
                _log.InfoFormat("All Vols Rfq requests have been processed.");

                _log.InfoFormat("Sending Logoff request ...");

                var logout = new QuickFix.FIX44.Logout();
                Session.SendToTarget(logout, _sessionID);
            }
        }
    }
}
