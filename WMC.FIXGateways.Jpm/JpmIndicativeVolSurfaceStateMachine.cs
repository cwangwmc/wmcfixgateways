﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXStateMachines;

namespace WMC.FIXGateways.Jpm
{
    public class JpmIndicativeVolSurfaceStateMachine : IndicativeVolSurfaceStateMachine
    {

        private string _securityDesc;

        public JpmIndicativeVolSurfaceStateMachine(string stateMachineRefId, string securitySymbol, string securityDesc, bool loggedOn)
            :this(stateMachineRefId, securitySymbol, securityDesc, loggedOn,
                 _defaultTimeToSendVolSurfaceRequest, _defaultTimeToWaitForVolSurfaceRefresh, _defaultTimeInBetweenVolSurfaceRefreshes)
        {
        }

        public JpmIndicativeVolSurfaceStateMachine(string stateMachineRefId, string securitySymbol, string securityDesc, bool loggedOn,
            long? timeToSendVolSurfaceRequestArg, long? timeToWaitForVolSurfaceRefreshArg, long? timeInBetweenVolSurfaceRefreshesArg)
            : base(stateMachineRefId, securitySymbol, loggedOn, 
                  timeToSendVolSurfaceRequestArg, timeToWaitForVolSurfaceRefreshArg, timeInBetweenVolSurfaceRefreshesArg)
        {

            _securityDesc = securityDesc;

            // States
            var waitingLogonState = new WaitingForLogonS(stateMachineRefId);
            var sendingMDRequestState = new SendingMarketDataRequestS(stateMachineRefId, TimeToSendVolSurfaceRequest);
            var waitingResponseState = new WaitingForResponseS(stateMachineRefId, TimeToWaitForSurfaceRefresh);
            var streamingMDState = new StreamingMarketDataS(stateMachineRefId, TimeInBetweenVolSurfaceRefreshes);
            var doneState = new DoneS(stateMachineRefId);

            // Transitions linking the States
            var LogonT = new LogonT(waitingLogonState, sendingMDRequestState);

            var LogoffT_A = new LogoffT(sendingMDRequestState, waitingLogonState);
            var LogoffT_B = new LogoffT(waitingResponseState, waitingLogonState);
            var LogoffT_C = new LogoffT(streamingMDState, waitingLogonState);

            var MDRequestT = new MDRequestT(sendingMDRequestState, waitingResponseState);

            var MDRequestRejectT_A = new MDRequestRejectT(waitingResponseState, doneState);
            var MDRequestRejectT_B = new MDRequestRejectT(streamingMDState, doneState);

            var TimeOutT = new TimedOutT(waitingResponseState, doneState);

            var MDIncrRefreshT_A = new MDRefreshT(waitingResponseState, streamingMDState);
            var MDIncrRefreshT_B = new MDRefreshT(streamingMDState, streamingMDState);

            // Decide the starting state based on whether the FIX engine has already logged on
            if (loggedOn)
                OriginState = sendingMDRequestState;
            else
                OriginState = waitingLogonState;

            TerminalState = doneState;

            CurrentState = OriginState;
            CurrentState.StateFIXMessageOut += OnOutboundFIXMessage;
            CurrentState.StateEntered += OnStateEntered;
            CurrentState.StateExited += OnStateExited;

            foreach (var t in CurrentState.Transitions)
                t.Triggered += OnStateTransition;
        }

        protected override FIXMessageDetailedEventArgs MakeFIXMessageDetailedEventArgs(FIXMessageEventArgs args)
        {
            FIXMessageDetailedEventArgs fixMsgDtlEvtArgs = null;

            if (args.MessageType == FIXMessageType.MarketDataRequest_V)
            {
                var ccy1 = SecuritySymbol.Substring(0, 3);
                var ccy2 = SecuritySymbol.Substring(3, 3);

                var fixMsgArgs = new JpmVolSecurityMarketDataRequestFIXMessageArgs(ccy1, ccy2, _securityDesc,
                    JpmFIXConstants.SecurityType_Option, JpmFIXConstants.SubReqType_Sub);

                fixMsgDtlEvtArgs = new FIXMessageDetailedEventArgs(StateMachineRefId, FIXMessageType.MarketDataRequest_V, fixMsgArgs);
            }
            else if (args.MessageType == FIXMessageType.MarketDataRequest_V_UnSub)
            {
                var ccy1 = SecuritySymbol.Substring(0, 3);
                var ccy2 = SecuritySymbol.Substring(3, 3);

                var fixMsgArgs = new JpmVolSecurityMarketDataRequestFIXMessageArgs(ccy1, ccy2, _securityDesc,
                    JpmFIXConstants.SecurityType_Option, JpmFIXConstants.SubReqType_UnSub);

                fixMsgDtlEvtArgs = new FIXMessageDetailedEventArgs(StateMachineRefId, FIXMessageType.MarketDataRequest_V_UnSub, fixMsgArgs);
            }

            return fixMsgDtlEvtArgs;
        }
    }
}
