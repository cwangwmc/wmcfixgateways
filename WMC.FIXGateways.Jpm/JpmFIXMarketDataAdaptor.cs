﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;

using WMC.FIXKafkaClients;

using WMC.FIXStateMachines;
using WMC.FIXDataObjects;

using QuickFix;
using QuickFix.FIX42;
using QuickFix.Fields;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WMC.FIXGateways.Jpm
{
    public class JpmFIXMarketDataAdaptor : JpmFIXAdaptor
    {

        IEnumerable<string> CurrencyPairs { get; set; }
        private Dictionary<string, JpmIndicativeVolSurfaceStateMachine> _indicativeVolSurfStateMachines { get; set; }

        private FIXKafkaSpotsConsumer _ksc;
        private SpotQuotes _refSpots;
        private ReaderWriterLockSlim _refSpotRWLock;
        private DateTime? _lastRefSpotsUpdateTs;
        private static readonly int _refSpotRWLockTimeout = 12_000;
        private static readonly int _refSpotTsToleranceInMinutes = 60;

        private JpmFxVolSurfaceConvHelper _volSurfaceConvHelper;

        private Dictionary<string, Dictionary<Tenor, HashSet<string>>> _ccypair_tenor_atm_mdEntryId;
        private Dictionary<string, Dictionary<Tenor, HashSet<string>>> _ccypair_tenor_rr_mdEntryId;
        private Dictionary<string, Dictionary<Tenor, HashSet<string>>> _ccypair_tenor_bf_mdEntryId;

        private Dictionary<string, MDEntryVolSmilePoint> _atm_mdEntryId_volPt;
        private Dictionary<string, MDEntryVolSpread> _rr_mdEntryId_volSprd;
        private Dictionary<string, MDEntryVolSpread> _bf_mdEntryId_volSprd;

        private Dictionary<string, ReaderWriterLockSlim> _ccypair_volRWLock;
        private static readonly int _volRWLockTimeout = 12_000;

        private Thread _volSurfaceMakerThread;
        private bool _volSurfaceMakerAlive;
        private int _volSurfaceMakerSleepInterval = 60_000;
        private int _volSurfaceMakerWorkInterval = 5 * 60_000;
        private DateTime? _lastVolSurfaceMakerRun;

        event EventHandler<MDIncrementalRefreshEventArgs> MarketDataIncrementalRefreshInbound;
        event EventHandler<FIXMessageEventArgs> FIXMessageInbound;

        public JpmFIXMarketDataAdaptor(string password, IEnumerable<string> ccypairs)
            : base(password)
        {
            _refSpotRWLock = new ReaderWriterLockSlim();
            _ksc = new FIXKafkaSpotsConsumer();
            _ksc.SpotQuotesConsumed += OnSpotQuotesConsumed;
            _ksc.StartConsumingSpots(KafkaSpotFeedSource.fenics);

            CurrencyPairs = ccypairs.Select(ccypair => ccypair.ToUpper());

            CreateAndInitialiseMDEntriesCache(CurrencyPairs);

            _volSurfaceConvHelper = new JpmFxVolSurfaceConvHelper();

            CreateAndActivateIndicativeVolSurfaceStateMachines(CurrencyPairs);

            MarketDataIncrementalRefreshInbound += OnMarketDataIncrementalRefreshInbound;

            CreateAndStartVolSurfaceMakerThread(_volSurfaceMakerSleepInterval, _volSurfaceMakerWorkInterval);
        }

        private void CreateAndInitialiseMDEntriesCache(IEnumerable<string> currencypairs)
        {
            _ccypair_tenor_atm_mdEntryId = new Dictionary<string, Dictionary<Tenor, HashSet<string>>>();
            _ccypair_tenor_rr_mdEntryId = new Dictionary<string, Dictionary<Tenor, HashSet<string>>>();
            _ccypair_tenor_bf_mdEntryId = new Dictionary<string, Dictionary<Tenor, HashSet<string>>>();

            _ccypair_volRWLock = new Dictionary<string, ReaderWriterLockSlim>();

            foreach (var ccypair in currencypairs)
            {
                _ccypair_tenor_atm_mdEntryId[ccypair] = new Dictionary<Tenor, HashSet<string>>();
                _ccypair_tenor_rr_mdEntryId[ccypair] = new Dictionary<Tenor, HashSet<string>>();
                _ccypair_tenor_bf_mdEntryId[ccypair] = new Dictionary<Tenor, HashSet<string>>();

                _ccypair_volRWLock[ccypair] = new ReaderWriterLockSlim();
            }

            _atm_mdEntryId_volPt = new Dictionary<string, MDEntryVolSmilePoint>();
            _rr_mdEntryId_volSprd = new Dictionary<string, MDEntryVolSpread>();
            _bf_mdEntryId_volSprd = new Dictionary<string, MDEntryVolSpread>();
        }

        private void ClearMDEntriesCache()
        {
            foreach (var ccypair in CurrencyPairs)
            {
                var volRWLock = _ccypair_volRWLock[ccypair];

                #region volRWLockWrite
                if (volRWLock.TryEnterWriteLock(_volRWLockTimeout))
                {
                    _log.InfoFormat("Clearing Tenor - MDEntryId cache for Currency Pair {0} ...", ccypair);
                    _ccypair_tenor_atm_mdEntryId[ccypair] = new Dictionary<Tenor, HashSet<string>>();
                    _ccypair_tenor_rr_mdEntryId[ccypair] = new Dictionary<Tenor, HashSet<string>>();
                    _ccypair_tenor_bf_mdEntryId[ccypair] = new Dictionary<Tenor, HashSet<string>>();
                    volRWLock.ExitWriteLock();
                }
                #endregion volRWLockWrite
            }

            _log.InfoFormat("Clearing MDEntryId - Vol cache ...");
            _atm_mdEntryId_volPt.Clear();
            _rr_mdEntryId_volSprd.Clear();
            _bf_mdEntryId_volSprd.Clear();
        }

        private void CreateAndStartVolSurfaceMakerThread(int sleepInterval, int workInterval)
        {
            _volSurfaceMakerAlive = true;
            _lastVolSurfaceMakerRun = null;

            _volSurfaceMakerThread = new Thread(() =>
            {
                _log.Info("Starting Vol Surface Maker Thread ...");
                while (_volSurfaceMakerAlive)
                {
                    var utcNow = DateTime.UtcNow;
                    if (!_lastVolSurfaceMakerRun.HasValue
                        || (utcNow.Subtract(_lastVolSurfaceMakerRun.Value).TotalMilliseconds > workInterval))
                    {
                        foreach (var ccypair in CurrencyPairs)
                        {
                            VolatilitySurface volSurfaceForCcypair = null;
                            var failureReason = string.Empty;
                            _log.InfoFormat("Start making Vol Surface for Currency Pair {0} ...", ccypair);
                            if (TryMakeMarketDataVolatilitySurface(ccypair, out volSurfaceForCcypair, out failureReason))
                            {
                                var volSurfJson = JsonConvert.SerializeObject(volSurfaceForCcypair, new StringEnumConverter());
                                _log.DebugFormat("Vol Surface Json: {0}", volSurfJson);

                                FIXKafkaProducer.SendVolsAsync(KafkaVolatilityFeedSource.jpm, volSurfJson);
                                FIXKafkaProducer.SendVolsAsync(KafkaVolatilityFeedSource.jpm, ccypair, volSurfJson);
                            }
                            else
                                _log.WarnFormat("Making Vol Surface for Currency Pair {0} failed - {1}", ccypair, failureReason);
                        }
                        _lastVolSurfaceMakerRun = utcNow;
                    }
                    Thread.Sleep(sleepInterval);
                }
                _log.Info("Ending Vol Surface Maker Thread ...");
            });

            _volSurfaceMakerThread.Start();
        }

        private void CreateAndActivateIndicativeVolSurfaceStateMachines(IEnumerable<string> currencypairs)
        {
            _indicativeVolSurfStateMachines = new Dictionary<string, JpmIndicativeVolSurfaceStateMachine>();
            var securityDescs = new string[] { JpmFIXConstants.SecurityDesc_ATM,
                                               JpmFIXConstants.SecurityDesc_RR,
                                               JpmFIXConstants.SecurityDesc_BF };

            foreach (var pair in currencypairs)
            {
                var uuid = Guid.NewGuid();

                foreach (var secDesc in securityDescs)
                {
                    var stateMachineRefId = string.Format("{0}_{1}_{2}", pair, secDesc, uuid);
                    var indicativeVolSurfStateMachineForPair = new JpmIndicativeVolSurfaceStateMachine(stateMachineRefId, pair, secDesc,
                        loggedOn: _isLoggedOn,
                        timeToSendVolSurfaceRequestArg: 30_000,
                        timeToWaitForVolSurfaceRefreshArg: 30 * 60_000,
                        timeInBetweenVolSurfaceRefreshesArg: 60 * 60_000);

                    FIXMessageInbound += indicativeVolSurfStateMachineForPair.OnInboundFIXMessage;
                    indicativeVolSurfStateMachineForPair.StateMachineFIXMessageOut += OnStateMachineOutboundFIXMessage;

                    indicativeVolSurfStateMachineForPair.Activate();
                    _indicativeVolSurfStateMachines.Add(stateMachineRefId, indicativeVolSurfStateMachineForPair);
                }
            }
        }

        private void RecycleIndicativeVolSurfaceStateMachines()
        {
            foreach (var volSurfStateMachine in _indicativeVolSurfStateMachines.Values)
            {
                FIXMessageInbound -= volSurfStateMachine.OnInboundFIXMessage;
                volSurfStateMachine.StateMachineFIXMessageOut -= OnStateMachineOutboundFIXMessage;
            }

            _indicativeVolSurfStateMachines.Clear();
            CreateAndActivateIndicativeVolSurfaceStateMachines(CurrencyPairs);
        }

        private void DeactiveIndicativeVolSurfaceStateMachines()
        {
            foreach (var refId in _indicativeVolSurfStateMachines.Keys)
            {
                var indicativeVolSurfStateMachineForRefId = _indicativeVolSurfStateMachines[refId];

                indicativeVolSurfStateMachineForRefId.Deactivate();                
            }
        }

        private void OnSpotQuotesConsumed(object sender, SpotQuotesEventArgs args)
        {
            if (_refSpotRWLock.TryEnterWriteLock(_refSpotRWLockTimeout))
            {
                _log.InfoFormat("Updating RefSpotQuotes, Received at UTC Timestamp: {0}, As Of UTC Timestamp: {1}",
                    args.UTCTimestamp.ToString("yyyy-MM-ddTHH:mm:ss"),
                    args.SpotQuotes.time.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss"));
                _refSpots = args.SpotQuotes;
                _lastRefSpotsUpdateTs = args.UTCTimestamp;
                _log.Info("Updated RefSpotQuotes.");
                _refSpotRWLock.ExitWriteLock();
            }
        }

        public override void OnLogon(SessionID sessionID)
        {
            base.OnLogon(sessionID);

            foreach (var stateMachineRefId in _indicativeVolSurfStateMachines.Keys)
            {
                FIXMessageInbound?.Invoke(this, new FIXMessageEventArgs(stateMachineRefId, FIXMessageType.Logon_A, _lastLogOnUtc));
            }
        }

        public override void OnLogout(SessionID sessionID)
        {
            base.OnLogout(sessionID);

            foreach (var stateMachineRefId in _indicativeVolSurfStateMachines.Keys)
            {
                FIXMessageInbound?.Invoke(this, new FIXMessageEventArgs(stateMachineRefId, FIXMessageType.Logoff_5, _lastLogOffUtc));
            }

            ClearMDEntriesCache();
            RecycleIndicativeVolSurfaceStateMachines();
        }

        public void OnMessage(MarketDataRequestReject mdReqReject, SessionID sessionID)
        {
            _log.DebugFormat("Entering OnMessage: MarketDataRequestReject sessionId - {0} ...", sessionID);

            var mdReqId = mdReqReject.MDReqID.getValue();
            _log.WarnFormat("MarketDataRequestReject received with MDReqId: {0}.", mdReqId);

            FIXMessageInbound?.Invoke(this, new FIXMessageEventArgs(mdReqId, FIXMessageType.MarketDataRequestReject_Y, DateTime.UtcNow));

            _log.DebugFormat("Exiting OnMessage: MarketDataRequestReject sessionId - {0} ...", sessionID);
        }

        public void OnMessage(MarketDataIncrementalRefresh mdIncrRefresh, SessionID sessionID)
        {
            _log.DebugFormat("OnMessage: MarketDataIncrementalRefresh sessionId - {0} ...", sessionID);

            var utcNow = DateTime.UtcNow;

            var mdReqId = mdIncrRefresh.MDReqID.getValue();
            _log.InfoFormat("MarketDataIncrementalRefresh received with MDReqId: {0}.", mdReqId);

            _log.DebugFormat("Start preparing FIXMessageEventArgs ...");

            var fixMsgEvtArgs = new FIXMessageEventArgs(mdReqId, FIXMessageType.MarketDataInrementalRefresh_X, utcNow);

            _log.DebugFormat("End preparing FIXMessageEventArgs.");

            _log.DebugFormat("Calling EventHandler OnInboundFIXMessage ...");

            FIXMessageInbound?.Invoke(this, fixMsgEvtArgs);

            #region MarketDataIncrementalRefresh

            var mdEntryGrp = new Group(Tags.NoMDEntries, Tags.MDUpdateAction);

            var mdEntriesList = new List<MDEntry>();

            var mdEntriesCount = mdIncrRefresh.GetInt(Tags.NoMDEntries);

            for (int grpIdx = 1; grpIdx <= mdEntriesCount; grpIdx++)
            {
                mdIncrRefresh.GetGroup(grpIdx, mdEntryGrp);

                var mdEntryItem = new MDEntry();

                var mdUpdateActionChr = mdEntryGrp.GetChar(Tags.MDUpdateAction);

                if (mdUpdateActionChr == JpmFIXConstants.MDUpdateAction_New)
                {
                    mdEntryItem.MDUpdateAction = MDUpdateActionEnum.New;
                    switch (mdEntryGrp.GetString(Tags.SecurityDesc))
                    {
                        case ("ATM"):
                            mdEntryItem.SecurityDesc = SecurityDescEnum.ATM;
                            break;
                        case ("RR"):
                            mdEntryItem.SecurityDesc = SecurityDescEnum.RR;
                            break;
                        case ("BF"):
                            mdEntryItem.SecurityDesc = SecurityDescEnum.BF;
                            break;
                    }
                    switch(mdEntryGrp.GetChar(Tags.MDEntryType))
                    {
                        case ('0'):
                            mdEntryItem.MDEntryType = MDEntryTypeEnum.Bid;
                            break;
                        case ('1'):
                            mdEntryItem.MDEntryType = MDEntryTypeEnum.Offer;
                            break;
                    }
                    switch(mdEntryGrp.GetInt(Tags.PutOrCall))
                    {
                        case (0):
                            mdEntryItem.PutOrCall = PutOrCallEnum.Put;
                            break;
                        case (1):
                            mdEntryItem.PutOrCall = PutOrCallEnum.Call;
                            break;
                    }
                    mdEntryItem.SecuritySymbol = mdEntryGrp.GetString(Tags.Symbol);
                    mdEntryItem.MDEntryID = mdEntryGrp.GetString(Tags.MDEntryID);
                    mdEntryItem.Tenor = mdEntryGrp.GetString(JpmFIXConstants.Tag_9002_Tenor);
                    mdEntryItem.ExpirationCut = mdEntryGrp.GetString(JpmFIXConstants.Tag_9111_ExpirationCut);
                    mdEntryItem.Delta = mdEntryGrp.GetString(JpmFIXConstants.Tag_9105_Delta);
                    mdEntryItem.Volatility = (double)mdEntryGrp.GetDecimal(JpmFIXConstants.Tag_9133_Volatility);
                }
                else if (mdUpdateActionChr == JpmFIXConstants.MDUpdateAction_Change)
                {
                    mdEntryItem.MDUpdateAction = MDUpdateActionEnum.Change;
                    mdEntryItem.MDEntryID = mdEntryGrp.GetString(Tags.MDEntryID);
                    if (mdEntryGrp.IsSetField(JpmFIXConstants.Tag_9133_Volatility))
                        mdEntryItem.Volatility = (double)mdEntryGrp.GetDecimal(JpmFIXConstants.Tag_9133_Volatility);
                }

                if (mdEntryGrp.IsSetField(Tags.MDEntryTime))
                {
                    var mdEntryTimeStr = mdEntryGrp.GetString(Tags.MDEntryTime);
                    mdEntryItem.MDEntryTime = TimeSpan.Parse(mdEntryTimeStr);
                }

                if (mdEntryGrp.IsSetField(JpmFIXConstants.Tag_9110_DepositRate))
                    mdEntryItem.DepositRate_Act365_Continuous = (double)mdEntryGrp.GetDecimal(JpmFIXConstants.Tag_9110_DepositRate);

                if (mdEntryGrp.IsSetField(JpmFIXConstants.Tag_9120_CounterDepositRate))
                    mdEntryItem.CounterDepositRate_Act365_Continuous = (double)mdEntryGrp.GetDecimal(JpmFIXConstants.Tag_9120_CounterDepositRate);

                mdEntriesList.Add(mdEntryItem);
            }

            var mdIncr = new MDIncrementalRefresh();

            mdIncr.MDReqID = mdReqId;
            mdIncr.MDEntries = mdEntriesList;

            #endregion MarketDataIncrementalRefresh

            _log.DebugFormat("End processing MarketDataIncrementalRefresh MDReqID: {0}.", mdReqId);

            _log.DebugFormat("Start preparing MDIncrementalRefreshEventArgs ...");

            var mdIncrEvtArgs = new MDIncrementalRefreshEventArgs();

            mdIncrEvtArgs.MDReqID = mdReqId;
            mdIncrEvtArgs.MDIncrementalRefresh = mdIncr;
            mdIncrEvtArgs.UTCAsOfTs = utcNow;

            _log.DebugFormat("End preparing MDIncrementalRefreshEventArgs.");

            _log.DebugFormat("Calling EventHandler MarketDataIncrementalRefreshInbound ...");

            MarketDataIncrementalRefreshInbound?.Invoke(this, mdIncrEvtArgs);

            _log.DebugFormat("Exiting OnMessage: MarketDataIncrementalRefresh SessionId - {0} ...", sessionID);
        }

        void OnMarketDataIncrementalRefreshInbound(object sender, MDIncrementalRefreshEventArgs mdIncrRefreshEvtArgs)
        {

            _log.DebugFormat("Entering OnMarketDataIncrementalRefreshInbound event handler ...");

            ProcessMDIncrementalRefresh(mdIncrRefreshEvtArgs.MDIncrementalRefresh, mdIncrRefreshEvtArgs.UTCAsOfTs);

            _log.DebugFormat("Exiting OnMarketDataIncrementalRefreshInbound event handler ...");

        }

        protected override void OnStateMachineOutboundFIXMessage(object sender, FIXMessageDetailedEventArgs msgDetailsEventArgs)
        {
            _log.DebugFormat("Entering OnStateMachineOutboundFIXMessage ...");

            QuickFix.Message message = null;

            if (msgDetailsEventArgs.MessageType == FIXMessageType.MarketDataRequest_V
                || msgDetailsEventArgs.MessageType == FIXMessageType.MarketDataRequest_V_UnSub)
            {
                var volSecMDReqMsgArgs = (JpmVolSecurityMarketDataRequestFIXMessageArgs)msgDetailsEventArgs.MessageArgs;

                message = MakeMarketDataRequest(msgDetailsEventArgs.RefId, volSecMDReqMsgArgs);
            }
            else
                _log.WarnFormat("Outbound FIX Message type {0} Not Supported. Skipping processing ...", msgDetailsEventArgs.MessageType);
            
            if (message != null)
            {
                _log.InfoFormat("Sending outbound FIX message {0} over Session Id: {1}", msgDetailsEventArgs.MessageType, _sessionID);
                Session.SendToTarget(message, _sessionID);
            }
        }

        private void ProcessMDIncrementalRefresh(MDIncrementalRefresh mdIncrementalRefresh, DateTime utcAsOfTs)
        {
            var mdEntries = mdIncrementalRefresh.MDEntries;

            foreach (var mdEntry in mdEntries)
            {
                if (mdEntry.MDUpdateAction == MDUpdateActionEnum.New)
                {
                    var secDesc = mdEntry.SecurityDesc.ToString();

                    switch (secDesc)
                    {
                        case ("ATM"):
                            ProcessATMMDEntry(mdEntry, utcAsOfTs);
                            break;
                        case ("RR"):
                            ProcessRRMDEntry(mdEntry, utcAsOfTs);
                            break;
                        case ("BF"):
                            ProcessBFMDEntry(mdEntry, utcAsOfTs);
                            break;
                    }
                }
                else if (mdEntry.MDUpdateAction == MDUpdateActionEnum.Change)
                {
                    var mdEntryId = mdEntry.MDEntryID;

                    if (_atm_mdEntryId_volPt.ContainsKey(mdEntryId))
                        ProcessATMMDEntry(mdEntry, utcAsOfTs);
                    else if (_rr_mdEntryId_volSprd.ContainsKey(mdEntryId))
                        ProcessRRMDEntry(mdEntry, utcAsOfTs);
                    else if (_bf_mdEntryId_volSprd.ContainsKey(mdEntryId))
                        ProcessBFMDEntry(mdEntry, utcAsOfTs);
                }
            }
        }

        private void ProcessATMMDEntry(MDEntry mdEntry, DateTime utcAsOfTs)
        {

            var mdEntryId = mdEntry.MDEntryID;

            if (mdEntry.MDUpdateAction == MDUpdateActionEnum.New)
            {
                var ccypair = mdEntry.SecuritySymbol.Replace("/", string.Empty).ToUpper();

                var secDesc = mdEntry.SecurityDesc.Value;
                var tenor = mdEntry.Tenor;
                var mdEntryType = mdEntry.MDEntryType.Value;
                var mdEntryTime = mdEntry.MDEntryTime;
                var delta = mdEntry.Delta;
                var putOrCall = mdEntry.PutOrCall.Value;
                var vol = mdEntry.Volatility.Value;
                var expCut = mdEntry.ExpirationCut;

                _log.DebugFormat("New ATM MDEntry - MDEntryId: {0}, CurrencyPair: {1}, Tenor: {2}, SecurityDesc: {3}, " +
                    "MDEntryType: {4}, Delta: {5}, PutOrCall: {6}, Vol: {7}, ExpirationCut: {8}, MDEntryTime: {9}",
                    mdEntryId, ccypair, tenor, secDesc, mdEntryType, delta, putOrCall, vol, expCut, mdEntryTime);

                Dictionary<Tenor, HashSet<string>> tenor_mdEntryId = null;

                if (_ccypair_tenor_atm_mdEntryId.TryGetValue(ccypair, out tenor_mdEntryId))
                {
                    var mdEntryVolPt = new MDEntryVolSmilePoint();
                    mdEntryVolPt.MDEntryID = mdEntryId;
                    mdEntryVolPt.SecuritySymbol = ccypair;
                    mdEntryVolPt.MDEntryType = mdEntryType;
                    mdEntryVolPt.Tenor = new Tenor(tenor);
                    mdEntryVolPt.UTCAsOfTs = utcAsOfTs;

                    VolatilitySmilePoint volPt = null;

                    if (delta == "ATM")
                    {
                        var atmType = _volSurfaceConvHelper.VolSurfaceAtmType(ccypair, tenor);
                        volPt = new VolatilitySmilePoint(vol, atmType);
                    }
                    else if (delta.EndsWith("D"))
                    {
                        var deltaInt = int.Parse(delta.Substring(0, delta.Length - 1));
                        var deltaType = _volSurfaceConvHelper.VolSurfaceWingDeltaType(ccypair, tenor);
                        var deltaForPutOrCall = putOrCall == PutOrCallEnum.Call ? DeltaOrientation.Call : DeltaOrientation.Put;
                        volPt = new VolatilitySmilePoint(vol, deltaInt, deltaType, deltaForPutOrCall);
                    }

                    mdEntryVolPt.VolSmilePoint = volPt;

                    #region volRWLockWrite

                    var volRWLock = _ccypair_volRWLock[ccypair];

                    if (volRWLock.TryEnterWriteLock(_volRWLockTimeout))
                    {
                        _log.DebugFormat("Creating ATM MDEntryVolSmilePoint - Currency Pair: {0}, Tenor {1}, MDEntryId: {2}, Vol: {3}, MDEntryTime: {4}",
                                        ccypair, tenor, mdEntryId, vol, mdEntry.MDEntryTime);

                        if (!tenor_mdEntryId.ContainsKey(mdEntryVolPt.Tenor))
                            tenor_mdEntryId[mdEntryVolPt.Tenor] = new HashSet<string>();

                        tenor_mdEntryId[mdEntryVolPt.Tenor].Add(mdEntryId);

                        _atm_mdEntryId_volPt[mdEntryId] = mdEntryVolPt;

                        _log.DebugFormat("Created ATM MDEntryVolSmilePoint - Currency Pair: {0}, MDEntryId: {1}", ccypair, mdEntryId);

                        volRWLock.ExitWriteLock();
                    }

                    #endregion volRWLockWrite

                }
                else
                    _log.WarnFormat("Currency pair {0} is not found. Skipping ...", ccypair);
            }
            else if (mdEntry.MDUpdateAction == MDUpdateActionEnum.Change)
            {
                if (mdEntry.Volatility.HasValue)
                {
                    if (_atm_mdEntryId_volPt.ContainsKey(mdEntryId))
                    {
                        var mdEntryVolPt = _atm_mdEntryId_volPt[mdEntryId];
                        var vol = mdEntry.Volatility.Value;
                        var ccypair = mdEntryVolPt.SecuritySymbol;

                        #region volRWLockWrite

                        var volRWLock = _ccypair_volRWLock[ccypair];

                        if (volRWLock.TryEnterWriteLock(_volRWLockTimeout))
                        {
                            _log.DebugFormat("Updating ATM MDEntryVolSmilePoint - Currency Pair: {0}, MDEntryId: {1}, Vol: {2}, MDEntryTime: {3}",
                                            ccypair, mdEntryId, vol, mdEntry.MDEntryTime);

                            var volPt = mdEntryVolPt.VolSmilePoint;

                            if (volPt.IsAtm)
                                mdEntryVolPt.VolSmilePoint = new VolatilitySmilePoint(vol, volPt.AtmType.Value);
                            else
                                mdEntryVolPt.VolSmilePoint = new VolatilitySmilePoint(vol, volPt.DeltaDetails);

                            mdEntryVolPt.UTCAsOfTs = utcAsOfTs;

                            _atm_mdEntryId_volPt[mdEntryId] = mdEntryVolPt;

                            _log.DebugFormat("Updated ATM MDEntryVolSmilePoint - Currency Pair: {0}, MDEntryId: {1}", ccypair, mdEntryId);

                            volRWLock.ExitWriteLock();
                        }

                        #endregion volRWLockWrite

                    }
                    else
                        _log.WarnFormat("MDEntryId {0} is not found. Skipping ...", mdEntryId);
                }
            }
        }

        private void ProcessRRMDEntry(MDEntry mdEntry, DateTime utcAsOfTs)
        {
            var mdEntryId = mdEntry.MDEntryID;

            if (mdEntry.MDUpdateAction == MDUpdateActionEnum.New)
            {
                var ccypair = mdEntry.SecuritySymbol.Replace("/", string.Empty).ToUpper();
                var secDesc = mdEntry.SecurityDesc.Value;
                var tenor = mdEntry.Tenor;
                var mdEntryType = mdEntry.MDEntryType.Value;
                var mdEntryTime = mdEntry.MDEntryTime;
                var delta = mdEntry.Delta;
                var volSprd = mdEntry.Volatility.Value;
                var expCut = mdEntry.ExpirationCut;

                _log.DebugFormat("New RR MDEntry - MDEntryId: {0}, CurrencyPair: {1}, Tenor: {2}, SecurityDesc: {3}, " +
                    "MDEntryType: {4}, Delta: {5}, Vol Spread: {6}, ExpirationCut: {7}, MDEntryTime: {8}",
                    mdEntryId, ccypair, tenor, secDesc, mdEntryType, delta, volSprd, expCut, mdEntryTime);

                Dictionary<Tenor, HashSet<string>> tenor_mdEntryId = null;

                if (_ccypair_tenor_rr_mdEntryId.TryGetValue(ccypair, out tenor_mdEntryId))
                {
                    var mdEntryVolSprd = new MDEntryVolSpread();
                    mdEntryVolSprd.MDEntryID = mdEntryId;
                    mdEntryVolSprd.SecuritySymbol = ccypair;
                    mdEntryVolSprd.MDEntryType = mdEntryType;
                    mdEntryVolSprd.Tenor = new Tenor(tenor);
                    mdEntryVolSprd.VolSpreadSecurityDesc = secDesc.ToString();
                    mdEntryVolSprd.VolSpread = volSprd;
                    mdEntryVolSprd.UTCAsOfTs = utcAsOfTs;

                    var deltaInt = int.Parse(delta.Substring(0, delta.Length - 1));
                    var deltaType = _volSurfaceConvHelper.VolSurfaceWingDeltaType(ccypair, tenor);

                    mdEntryVolSprd.Delta = deltaInt;
                    mdEntryVolSprd.DeltaType = deltaType;

                    #region volRWLockWrite

                    var volRWLock = _ccypair_volRWLock[ccypair];

                    if (volRWLock.TryEnterWriteLock(_volRWLockTimeout))
                    {
                        _log.DebugFormat("Creating RR MDEntryVolSpread - Currency Pair: {0}, Tenor: {1}, MDEntryId: {2}, Vol Spread: {3}, MDEntryTime: {4}",
                                        ccypair, tenor, mdEntryId, volSprd, mdEntry.MDEntryTime);

                        if (!tenor_mdEntryId.ContainsKey(mdEntryVolSprd.Tenor))
                            tenor_mdEntryId[mdEntryVolSprd.Tenor] = new HashSet<string>();

                        tenor_mdEntryId[mdEntryVolSprd.Tenor].Add(mdEntryId);

                        _rr_mdEntryId_volSprd[mdEntryId] = mdEntryVolSprd;

                        _log.DebugFormat("Created RR MDEntryVolSpread - Currency Pair: {0}, MDEntryId: {1}", ccypair, mdEntryId);

                        volRWLock.ExitWriteLock();
                    }

                    #endregion volRWLockWrite

                }
                else
                    _log.WarnFormat("Currency pair {0} is not found. Skipping ...", ccypair);
            }
            else if (mdEntry.MDUpdateAction == MDUpdateActionEnum.Change)
            {
                if (mdEntry.Volatility.HasValue)
                {
                    if (_rr_mdEntryId_volSprd.ContainsKey(mdEntryId))
                    {
                        var mdEntryVolSprd = _rr_mdEntryId_volSprd[mdEntryId];

                        var volSprd = mdEntry.Volatility.Value;
                        var ccypair = mdEntryVolSprd.SecuritySymbol;

                        #region volRWLockWrite

                        var volRWLock = _ccypair_volRWLock[ccypair];

                        if (volRWLock.TryEnterWriteLock(_volRWLockTimeout))
                        {
                            _log.DebugFormat("Updating RR MDEntryVolSpread - Currency Pair: {0}, MDEntryId: {1}, Vol Spread: {2}, MDEntryTime: {3}",
                                             ccypair, mdEntryId, volSprd, mdEntry.MDEntryTime);

                            mdEntryVolSprd.VolSpread = volSprd;
                            mdEntryVolSprd.UTCAsOfTs = utcAsOfTs;

                            _rr_mdEntryId_volSprd[mdEntryId] = mdEntryVolSprd;

                            _log.DebugFormat("Updated RR MDEntryVolSpread - Currency Pair: {0}, MDEntryId: {1}", ccypair, mdEntryId);

                            volRWLock.ExitWriteLock();
                        }

                        #endregion volRWLockWrite
                    }
                    else
                        _log.WarnFormat("MDEntryId {0} is not found. Skipping ...", mdEntryId);
                }
            }
        }

        private void ProcessBFMDEntry(MDEntry mdEntry, DateTime utcAsOfTs)
        {
            var mdEntryId = mdEntry.MDEntryID;
            
            if (mdEntry.MDUpdateAction == MDUpdateActionEnum.New)
            {
                var ccypair = mdEntry.SecuritySymbol.Replace("/", string.Empty).ToUpper();
                var secDesc = mdEntry.SecurityDesc.Value;
                var tenor = mdEntry.Tenor;
                var mdEntryType = mdEntry.MDEntryType.Value;
                var mdEntryTime = mdEntry.MDEntryTime;
                var delta = mdEntry.Delta;
                var volSprd = mdEntry.Volatility.Value;
                var expCut = mdEntry.ExpirationCut;

                _log.DebugFormat("New BF MDEntry - MDEntryId: {0}, CurrencyPair: {1}, Tenor: {2}, SecurityDesc: {3}, " +
                    "MDEntryType: {4}, Delta: {5}, Vol Spread: {6}, ExpirationCut: {7}, MDEntryTime: {8}",
                    mdEntryId, ccypair, tenor, secDesc, mdEntryType, delta, volSprd, expCut, mdEntryTime);

                Dictionary<Tenor, HashSet<string>> tenor_mdEntryId = null;

                if (_ccypair_tenor_bf_mdEntryId.TryGetValue(ccypair, out tenor_mdEntryId))
                {
                    var mdEntryVolSprd = new MDEntryVolSpread();
                    mdEntryVolSprd.MDEntryID = mdEntryId;
                    mdEntryVolSprd.SecuritySymbol = ccypair;
                    mdEntryVolSprd.MDEntryType = mdEntryType;
                    mdEntryVolSprd.Tenor = new Tenor(tenor);
                    mdEntryVolSprd.VolSpreadSecurityDesc = secDesc.ToString();
                    mdEntryVolSprd.VolSpread = volSprd;
                    mdEntryVolSprd.UTCAsOfTs = utcAsOfTs;

                    var deltaInt = int.Parse(delta.Substring(0, delta.Length - 1));
                    var deltaType = _volSurfaceConvHelper.VolSurfaceWingDeltaType(ccypair, tenor);

                    mdEntryVolSprd.Delta = deltaInt;
                    mdEntryVolSprd.DeltaType = deltaType;

                    #region volRWLockWrite

                    var volRWLock = _ccypair_volRWLock[ccypair];

                    if (volRWLock.TryEnterWriteLock(_volRWLockTimeout))
                    {
                        _log.DebugFormat("Creating BF MDEntryVolSpread - Currency Pair: {0}, Tenor: {1}, MDEntryId: {2}, Vol Spread: {3}, MDEntryTime: {4}",
                                        ccypair, tenor, mdEntryId, volSprd, mdEntry.MDEntryTime);

                        if (!tenor_mdEntryId.ContainsKey(mdEntryVolSprd.Tenor))
                            tenor_mdEntryId[mdEntryVolSprd.Tenor] = new HashSet<string>();

                        tenor_mdEntryId[mdEntryVolSprd.Tenor].Add(mdEntryId);

                        _bf_mdEntryId_volSprd[mdEntryId] = mdEntryVolSprd;

                        _log.DebugFormat("Created BF MDEntryVolSpread - Currency Pair: {0}, MDEntryId: {1}", ccypair, mdEntryId);

                        volRWLock.ExitWriteLock();
                    }

                    #endregion volRWLockWrite
                }
                else
                    _log.WarnFormat("Currency pair {0} is not found. Skipping ...", ccypair);
            }
            else if (mdEntry.MDUpdateAction == MDUpdateActionEnum.Change)
            {
                if (mdEntry.Volatility.HasValue)
                {
                    if (_bf_mdEntryId_volSprd.ContainsKey(mdEntryId))
                    {
                        var mdEntryVolSprd = _bf_mdEntryId_volSprd[mdEntryId];

                        var volSprd = mdEntry.Volatility.Value;
                        var ccypair = mdEntryVolSprd.SecuritySymbol;

                        #region volRWLockWrite

                        var volRWLock = _ccypair_volRWLock[ccypair];

                        if (volRWLock.TryEnterWriteLock(_volRWLockTimeout))
                        {
                            _log.DebugFormat("Updating BF MDEntryVolSpread - Currency Pair: {0}, MDEntryId: {1}, Vol Spread: {2}, MDEntryTime: {3}",
                                             ccypair, mdEntryId, volSprd, mdEntry.MDEntryTime);

                            mdEntryVolSprd.VolSpread = volSprd;
                            mdEntryVolSprd.UTCAsOfTs = utcAsOfTs;

                            _bf_mdEntryId_volSprd[mdEntryId] = mdEntryVolSprd;

                            _log.DebugFormat("Updated BF MDEntryVolSpread - Currency Pair: {0}, MDEntryId: {1}", ccypair, mdEntryId);

                            volRWLock.ExitWriteLock();
                        }

                        #endregion volRWLockWrite
                    }
                    else
                        _log.WarnFormat("MDEntryId {0} is not found. Skipping ...", mdEntryId);
                }
            }
        }

        private MarketDataRequest MakeMarketDataRequest(string refId, JpmVolSecurityMarketDataRequestFIXMessageArgs args)
        {

            MarketDataRequest mdr = new MarketDataRequest();
            mdr.MDReqID = new MDReqID(refId);
            mdr.SubscriptionRequestType = new SubscriptionRequestType(args.SubscriptionRequestType);
            mdr.MarketDepth = new MarketDepth(JpmFIXConstants.MarketDepth_FullBook);

            if (args.SubscriptionRequestType == JpmFIXConstants.SubReqType_Sub)
                mdr.MDUpdateType = new MDUpdateType(JpmFIXConstants.MDUpdateType_IncrRefresh);

            var noMDEntryTypeGrp_Bid = new MarketDataRequest.NoMDEntryTypesGroup();
            var noMDEntryTypeGrp_Offer = new MarketDataRequest.NoMDEntryTypesGroup();
            noMDEntryTypeGrp_Bid.MDEntryType = new MDEntryType(JpmFIXConstants.MDEntryType_Bid);
            noMDEntryTypeGrp_Offer.MDEntryType = new MDEntryType(JpmFIXConstants.MDEntryType_Offer);

            mdr.AddGroup(noMDEntryTypeGrp_Bid);
            mdr.AddGroup(noMDEntryTypeGrp_Offer);

            var expiryCut = JpmFIXConstants.ExpiryCut_NewYork10AM;
            if (JpmFIXConstants.NonNYCcyExpiryCuts.ContainsKey(args.Ccy2))
                expiryCut = JpmFIXConstants.NonNYCcyExpiryCuts[args.Ccy2];
            
            var noRelatedSymGrp = new MarketDataRequest.NoRelatedSymGroup();
            noRelatedSymGrp.Symbol = new Symbol(string.Format("{0}/{1}", args.Ccy1, args.Ccy2));
            noRelatedSymGrp.SecurityDesc = new SecurityDesc(args.SecurityDesc);
            noRelatedSymGrp.SecurityType = new SecurityType(args.SecurityType);
            noRelatedSymGrp.SetField(new StringField(JpmFIXConstants.Tag_9111_ExpirationCut, expiryCut));

            mdr.AddGroup(noRelatedSymGrp);

            return mdr;

        }

        private bool TryMakeMarketDataVolatilitySurface(string ccypair, out VolatilitySurface volSurface, out string failureReason)
        {
            var success = false;
            volSurface = null;
            failureReason = string.Empty;

            var volRWLock = _ccypair_volRWLock[ccypair];

            #region volRWLockRead
            
            if (volRWLock.TryEnterUpgradeableReadLock(_refSpotRWLockTimeout))
            {
                DateTime volSurfUtcAsOfTs = DateTime.MinValue;

                var tenor_atm_mdEntryId = _ccypair_tenor_atm_mdEntryId[ccypair];
                var tenor_rr_mdEntryId = _ccypair_tenor_rr_mdEntryId[ccypair];
                var tenor_bf_mdEntryId = _ccypair_tenor_bf_mdEntryId[ccypair];

                IEnumerable<Tenor> tenors = new List<Tenor>();
                var volSmiles = new List<VolatilitySmile>();

                if (tenor_atm_mdEntryId.Count() == 0
                    || tenor_rr_mdEntryId.Count() == 0
                    || tenor_bf_mdEntryId.Count() == 0)
                {
                    success = false;
                    failureReason = "Incomplete vol surface from ATM, RR and BF subscriptions.";
                }
                else
                {
                    var tenors_atm = tenor_atm_mdEntryId.Keys.ToList();
                    var tenors_rr = tenor_rr_mdEntryId.Keys.ToList();
                    var tenors_bf = tenor_bf_mdEntryId.Keys.ToList();

                    tenors = tenors_atm.Intersect(tenors_rr);
                    tenors = tenors.Intersect(tenors_bf);

                    if (tenors.Count() == 0)
                    {
                        success = false;
                        failureReason = "No common tenor amongst ATM, RR and BF subscriptions.";
                    }
                }

                var atm_mdEntryId_volPt_cache = _atm_mdEntryId_volPt.ToList();
                var rr_mdEntryId_volSprd_cache = _rr_mdEntryId_volSprd.ToList();
                var bf_mdEntryId_volSprd_cache = _bf_mdEntryId_volSprd.ToList();

                foreach (var tnr in tenors.Where(t => t <= Tenor.Parse("2Y")))
                {
                    var atm_mdEntryIds = tenor_atm_mdEntryId[tnr];
                    var atm_mdVolPts = atm_mdEntryId_volPt_cache.Where(kvp => atm_mdEntryIds.Contains(kvp.Key)).Select(kvp => kvp.Value).ToList();

                    var atm_volPt_bid = atm_mdVolPts.Where(mdVolPt => mdVolPt.MDEntryType == MDEntryTypeEnum.Bid && mdVolPt.VolSmilePoint.IsAtm).Select(mdVolPt => mdVolPt).FirstOrDefault();
                    var atm_volPt_offer = atm_mdVolPts.Where(mdVolPt => mdVolPt.MDEntryType == MDEntryTypeEnum.Offer && mdVolPt.VolSmilePoint.IsAtm).Select(mdVolPt => mdVolPt).FirstOrDefault();

                    var rr_mdEntryIds = tenor_rr_mdEntryId[tnr];
                    var rr_mdVolSprds = rr_mdEntryId_volSprd_cache.Where(kvp => rr_mdEntryIds.Contains(kvp.Key)).Select(kvp => kvp.Value).ToList();

                    var d25_rr_volSprd_bid = rr_mdVolSprds.Where(mdVolSprd => mdVolSprd.MDEntryType == MDEntryTypeEnum.Bid && mdVolSprd.Delta == 25).Select(mdVolSprd => mdVolSprd).FirstOrDefault();
                    var d25_rr_volSprd_offer = rr_mdVolSprds.Where(mdVolSprd => mdVolSprd.MDEntryType == MDEntryTypeEnum.Offer && mdVolSprd.Delta == 25).Select(mdVolSprd => mdVolSprd).FirstOrDefault();

                    var d10_rr_volSprd_bid = rr_mdVolSprds.Where(mdVolSprd => mdVolSprd.MDEntryType == MDEntryTypeEnum.Bid && mdVolSprd.Delta == 10).Select(mdVolSprd => mdVolSprd).FirstOrDefault();
                    var d10_rr_volSprd_offer = rr_mdVolSprds.Where(mdVolSprd => mdVolSprd.MDEntryType == MDEntryTypeEnum.Offer && mdVolSprd.Delta == 10).Select(mdVolSprd => mdVolSprd).FirstOrDefault();

                    var bf_mdEntryIds = tenor_bf_mdEntryId[tnr];
                    var bf_mdVolSprds = bf_mdEntryId_volSprd_cache.Where(kvp => bf_mdEntryIds.Contains(kvp.Key)).Select(kvp => kvp.Value).ToList();

                    var d25_bf_volSprd_bid = bf_mdVolSprds.Where(mdVolSprd => mdVolSprd.MDEntryType == MDEntryTypeEnum.Bid && mdVolSprd.Delta == 25).Select(mdVolSprd => mdVolSprd).FirstOrDefault();
                    var d25_bf_volSprd_offer = bf_mdVolSprds.Where(mdVolSprd => mdVolSprd.MDEntryType == MDEntryTypeEnum.Offer && mdVolSprd.Delta == 25).Select(mdVolSprd => mdVolSprd).FirstOrDefault();

                    var d10_bf_volSprd_bid = bf_mdVolSprds.Where(mdVolSprd => mdVolSprd.MDEntryType == MDEntryTypeEnum.Bid && mdVolSprd.Delta == 10).Select(mdVolSprd => mdVolSprd).FirstOrDefault();
                    var d10_bf_volSprd_offer = bf_mdVolSprds.Where(mdVolSprd => mdVolSprd.MDEntryType == MDEntryTypeEnum.Offer && mdVolSprd.Delta == 10).Select(mdVolSprd => mdVolSprd).FirstOrDefault();

                    var atm_bid_offers = new MDEntryVolSmilePoint[] { atm_volPt_bid, atm_volPt_offer };
                    var rr_bf_bid_offers = new MDEntryVolSpread[] { d25_rr_volSprd_bid, d25_rr_volSprd_offer, d10_rr_volSprd_bid, d10_rr_volSprd_offer,
                                                                    d25_bf_volSprd_bid, d25_bf_volSprd_offer, d10_bf_volSprd_bid, d10_bf_volSprd_offer};

                    if (atm_bid_offers.Any(x => x == null) || rr_bf_bid_offers.Any(x => x == null))
                    {
                        _log.WarnFormat("Missing Bids or Offers detected for Currency Pair: {0}, Tenor: {1} ...", ccypair, tnr);
                        _log.WarnFormat("Skipping Currency Pair: {0}, Tenor: {1} ...", ccypair, tnr);
                    }
                    else
                    {
                        var atm_mid_vol = Math.Round(0.5 * (atm_volPt_bid.VolSmilePoint.Vol + atm_volPt_offer.VolSmilePoint.Vol), 4);
                        var d25_rr_mid_vol = Math.Round(0.5 * (d25_rr_volSprd_bid.VolSpread + d25_rr_volSprd_offer.VolSpread), 4);
                        var d25_bf_mid_vol = Math.Round(0.5 * (d25_bf_volSprd_bid.VolSpread + d25_bf_volSprd_offer.VolSpread), 4);
                        var d10_rr_mid_vol = Math.Round(0.5 * (d10_rr_volSprd_bid.VolSpread + d10_rr_volSprd_offer.VolSpread), 4);
                        var d10_bf_mid_vol = Math.Round(0.5 * (d10_bf_volSprd_bid.VolSpread + d10_bf_volSprd_offer.VolSpread), 4);

                        var d25_c_mid_vol = atm_mid_vol + d25_bf_mid_vol + 0.5 * d25_rr_mid_vol;
                        var d25_p_mid_vol = atm_mid_vol + d25_bf_mid_vol - 0.5 * d25_rr_mid_vol;
                        var d10_c_mid_vol = atm_mid_vol + d10_bf_mid_vol + 0.5 * d10_rr_mid_vol;
                        var d10_p_mid_vol = atm_mid_vol + d10_bf_mid_vol - 0.5 * d10_rr_mid_vol;

                        var atmType = _volSurfaceConvHelper.VolSurfaceAtmType(ccypair, tnr.TenorCode);
                        var deltaType = _volSurfaceConvHelper.VolSurfaceWingDeltaType(ccypair, tnr.TenorCode);

                        var atm_volPt = new VolatilitySmilePoint(atm_mid_vol, atmType);
                        var d25_c_volPt = new VolatilitySmilePoint(d25_c_mid_vol, 25, deltaType, DeltaOrientation.Call);
                        var d25_p_volPt = new VolatilitySmilePoint(d25_p_mid_vol, 25, deltaType, DeltaOrientation.Put);
                        var d10_c_volPt = new VolatilitySmilePoint(d10_c_mid_vol, 10, deltaType, DeltaOrientation.Call);
                        var d10_p_volPt = new VolatilitySmilePoint(d10_p_mid_vol, 10, deltaType, DeltaOrientation.Put);

                        var tnrVolSmile = new VolatilitySmile(tnr, new VolatilitySmilePoint[] { d25_c_volPt, d25_p_volPt, d10_c_volPt, d10_p_volPt, atm_volPt });

                        var atm_utcAsOfTs = atm_bid_offers.Select(x => x.UTCAsOfTs);
                        var wing_utcAsOfTs = rr_bf_bid_offers.Select(x => x.UTCAsOfTs);

                        var tnrUtcAsOfTs = wing_utcAsOfTs.Concat(atm_utcAsOfTs).Max();

                        volSurfUtcAsOfTs = new DateTime[] { volSurfUtcAsOfTs, tnrUtcAsOfTs }.Max();

                        volSmiles.Add(tnrVolSmile);
                    }
                }

                if (volSmiles.Count() > 0)
                {
                    double? ccypairRefSpot = null;

                    #region refSpotRWLock

                    if (_refSpotRWLock.TryEnterReadLock(_refSpotRWLockTimeout))
                    {
                        if (_refSpots != null)
                        {
                            var refSpotEntry = _refSpots.quotes.Where(q => q.ccy_pair.ToUpper() == ccypair.ToUpper()).Select(q => q).FirstOrDefault();
                            var refSpotUtcAsTs = refSpotEntry.ts.ToUniversalTime();

                            _log.InfoFormat("Vol Surface UTC Timestamp: {0}, Ref Spot UTC Timestamp: {1}", volSurfUtcAsOfTs, refSpotUtcAsTs);

                            var refSpotVolSurfTsDiffInMinutes = (int)Math.Abs(refSpotUtcAsTs.Subtract(volSurfUtcAsOfTs).TotalMinutes);

                            if (refSpotVolSurfTsDiffInMinutes < _refSpotTsToleranceInMinutes)
                            {
                                ccypairRefSpot = refSpotEntry.mid;
                                volSurface = new VolatilitySurface(ccypair, volSurfUtcAsOfTs, volSmiles,
                                    source: KafkaVolatilityFeedSource.jpm.ToString(), refDate: volSurfUtcAsOfTs.Date, refSpot: ccypairRefSpot);
                                success = true;
                            }
                            else
                            {
                                _log.WarnFormat("Ref Spot and Vol Surface differ by {0} mins, exceeding tolerance {1} mins.",
                                    refSpotVolSurfTsDiffInMinutes, _refSpotTsToleranceInMinutes);
                                success = false;
                                failureReason = "Timestamp gap between ref spot and vol surface is larger than tolerance.";
                            }
                        }
                        else
                        {
                            success = false;
                            failureReason = "No ref spot quote available.";
                        }
                        _refSpotRWLock.ExitReadLock();
                    }

                    #endregion refSpotRWLock
                }

                volRWLock.ExitUpgradeableReadLock();
            }
            
            #endregion volRWLockRead

            return success;
        }

    }

    class MDEntryVolSmilePoint
    {
        public string MDEntryID { get; set; }
        public string SecuritySymbol { get; set; }
        public MDEntryTypeEnum MDEntryType { get; set; }
        public Tenor Tenor { get; set; }
        public VolatilitySmilePoint VolSmilePoint { get; set; }
        public DateTime UTCAsOfTs { get; set; }
    }

    class MDEntryVolSpread
    {
        public string MDEntryID { get; set; }
        public string SecuritySymbol { get; set; }
        public MDEntryTypeEnum MDEntryType { get; set; }
        public Tenor Tenor { get; set; }
        public string VolSpreadSecurityDesc { get; set; }
        public int Delta { get; set; }
        public DeltaType DeltaType { get; set; }
        public double VolSpread { get; set; }
        public DateTime UTCAsOfTs { get; set; }
    }

    class MDIncrementalRefresh
    {
        public string MDReqID { get; set; }
        public IEnumerable<MDEntry> MDEntries { get; set; }
    }

    class MDEntry
    {
        public string SecuritySymbol { get; set; }
        public TimeSpan MDEntryTime { get; set; }
        public MDUpdateActionEnum MDUpdateAction { get; set; }
        public SecurityDescEnum? SecurityDesc { get; set; }
        public MDEntryTypeEnum? MDEntryType { get; set; }
        public string MDEntryID { get; set; }
        public PutOrCallEnum? PutOrCall { get; set; }
        public string Tenor { get; set; }
        public string ExpirationCut { get; set; }
        public string Delta { get; set; }
        public double? Volatility { get; set; }
        public double? DepositRate_Act365_Continuous { get; set; }
        public double? CounterDepositRate_Act365_Continuous { get; set; }
    }

    class MDIncrementalRefreshEventArgs : EventArgs
    {
        public string MDReqID { get; set; }
        public MDIncrementalRefresh MDIncrementalRefresh { get; set; }
        public DateTime UTCAsOfTs { get; set; }
    }

    enum MDUpdateActionEnum
    {
        New,
        Change,
        Delete
    }

    enum SecurityDescEnum
    {
        ATM,
        BF,
        RR
    }

    enum MDEntryTypeEnum
    {
        Bid,
        Offer
    }

    enum PutOrCallEnum
    {
        Put,
        Call
    }
}
