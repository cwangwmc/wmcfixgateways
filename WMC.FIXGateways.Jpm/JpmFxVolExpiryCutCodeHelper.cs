﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FXConventionHelper;

namespace WMC.FIXGateways.Jpm
{
    public class JpmFxVolExpiryCutCodeHelper : iFxVolExpiryCutCodeHelper
    {

        public string VolExpiryCutCode(string ccypair)
        {
            var ccy1 = ccypair.Substring(0, 3);
            var ccy2 = ccypair.Substring(3, 3);

            if (JpmFIXConstants.NonNYCcyExpiryCuts.ContainsKey(ccy1))
                return JpmFIXConstants.NonNYCcyExpiryCuts[ccy1];
            else if (JpmFIXConstants.NonNYCcyExpiryCuts.ContainsKey(ccy2))
                return JpmFIXConstants.NonNYCcyExpiryCuts[ccy2];
            else
                return JpmFIXConstants.ExpiryCut_NewYork10AM;
        }

    }
}
