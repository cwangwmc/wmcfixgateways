﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXDataObjects;
using WMC.FXConventionHelper;

namespace WMC.FIXGateways.Jpm
{
    public class JpmFxVolSurfaceConvHelper : iFxVolSurfaceConvHelper
    {
        public JpmFxVolSurfaceConvHelper()
        {
        }

        public DeltaType VolSurfaceWingDeltaType(string ccypair, string tenor)
        {
            var ccy1 = ccypair.Substring(0, 3);
            var ccy2 = ccypair.Substring(3, 3);

            if (ccy2.ToUpper() == "USD")
            {
                if (Tenor.Parse(tenor) <= Tenor.Parse("1Y"))
                    return DeltaType.SpotBp;
                else
                    return DeltaType.ForwardBp;
            }
            else if (FXConvHelper.IsG10(ccypair))
            {
                if (Tenor.Parse(tenor) <= Tenor.Parse("1Y"))
                    return DeltaType.SpotPc;
                else
                    return DeltaType.ForwardPc;
            }
            
            return DeltaType.ForwardPc;
        }
        
        public Atm VolSurfaceAtmType(string ccypair, string tenor)
        {
            if (FXConvHelper.IsEMLatam(ccypair))
                return Atm.Atmf;

            return Atm.Dn;
        }
    }
}
