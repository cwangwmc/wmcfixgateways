﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXStateMachines;

namespace WMC.FIXGateways.Jpm
{

    public class JpmVolSecurityMarketDataRequestFIXMessageArgs : MarketDataRequestFIXMessageArgs
    {

        public string SecurityDesc { get; private set; }
        public string SecurityType { get; private set; }
        public char SubscriptionRequestType { get; private set; }

        public JpmVolSecurityMarketDataRequestFIXMessageArgs(string ccy1, string ccy2,
                                                             string secDesc, string secType,
                                                             char subReqType)
            : base(ccy1, ccy2)
        {
            SecurityDesc = secDesc;
            SecurityType = secType;
            SubscriptionRequestType = subReqType;
        }

    }
    
    public enum JpmMDReqSubType
    {
        Subscribe = 1,
        Unsubscribe = 2
    }

    public enum JpmMDEntryType
    {
        Bid = 0,
        Offer = 1
    }

    public class JpmFxOptionsQuoteRequestFIXMessageArgs : QuoteRequestFIXMessageArgs
    {

        public char QuoteRequestSubscriptionType { get; private set; }

        public char IsRFQ { get { return 'Y'; } }
        public char IsSEF { get { return 'N'; } }

        public string SecurityDesc { get; private set; }
        public string SolvingFor { get; private set; }
        public string SecurityType { get { return "OPT"; } }
        public int QuoteRquestType { get { return 2; } } // FIX 4.2 - Tag 303 QuoteRequestType: 1-Manual; 2-Automatic
        public char Side { get; private set; }
        public string Currency { get; private set; }

        public double? CustomHedgeSpot { get; private set; }
        public int HedgeType { get; private set; }

        public string PremCcy { get; private set; }
        public double? PremAmt { get; private set; }
        public int? PremPayOn { get; private set; }
        public string PxRefMetric { get; private set; }

        public IEnumerable<JpmFxOptionsLegFIXMessageArgs> OptionLegs { get; private set; }
        public IEnumerable<JpmFxOptionsHedgeLegFIXMessageArgs> OptionHedgeLegs { get; private set; }

        public JpmFxOptionsQuoteRequestFIXMessageArgs(string ccy1, string ccy2,
            IEnumerable<JpmFxOptionsLegFIXMessageArgs> optLegArgs, IEnumerable<JpmFxOptionsHedgeLegFIXMessageArgs> optHedgeLegArgs,
            string solvingFor = "PREMIUM", char quoteRequestSubType = '1',
            string secDesc = "VANILLA", char side = '1', int hedgeType = 2, string premCcy = "", string pxRefMetric = "", int premPayOn = 0)
            :base(ccy1, ccy2)
        {

            QuoteRequestSubscriptionType = quoteRequestSubType;
            SolvingFor = solvingFor;
            SecurityDesc = secDesc;
            Side = side;
            Currency = ccy1;

            HedgeType = hedgeType;

            PremCcy = premCcy == string.Empty ? ccy1 : premCcy;
            PxRefMetric = pxRefMetric == string.Empty ? string.Format("%{0}", premCcy) : pxRefMetric;
            PremPayOn = premPayOn;

            OptionLegs = optLegArgs;
            OptionHedgeLegs = optHedgeLegArgs;

        }

    }

    public class JpmFxOptionsLegFIXMessageArgs
    {

        public int RefID { get; private set; }
        public string USIValue { get; private set; }

        public string SecurityDesc { get; private set; }
        public char? Side { get; private set; }
        public int? PutOrCall { get; private set; }
        public double? Qty { get; private set; }
        public string Strike { get; private set; }
        public string Tenor { get; private set; }

        public string Barrier { get; private set; }
        public int? BarrierType { get; private set; }
        public int? BarrierObservation { get; private set; }

        public DateTime? ExpirationDate { get; private set; }
        public string ExpirationCut { get; private set; }
        public DateTime? DeliveryDate { get; private set; }

        public DateTime? EffectiveDate { get; private set; }

        public int? SettlementType { get; private set; }
        public string SettlementCcy { get; private set; }

        public double? DepoRate { get; private set; }
        public char? DepositInputType { get; private set; }
        public double? Yield { get; private set; }
        public char? MarginInputType { get; private set; }
        public double? Margin { get; private set; }
        public string MarginInCcy { get; private set; }

        public int? PayTime { get; private set; }

        public JpmFxOptionsLegFIXMessageArgs(int refId, string secDesc, int putOrCall, string strike, string tenor,
            char side = '1', double qty = 1_000_000.0, string expCut = "NYC", int settlType = 0, string settlCcy = "")
        {
            RefID = refId;
            SecurityDesc = secDesc;

            PutOrCall = putOrCall;
            Strike = strike;
            Tenor = tenor;

            Side = side;
            Qty = qty;
            ExpirationCut = expCut;

            SettlementType = settlType;
            SettlementCcy = settlCcy;
        }

        public JpmFxOptionsLegFIXMessageArgs(int refId, string secDesc, int putOrCall, string strike, DateTime expDate,
            char side = '1', double qty = 1_000_000.0, string expCut = "NYC", int settlType = 0, string settlCcy = "")
        {
            RefID = refId;
            SecurityDesc = secDesc;

            PutOrCall = putOrCall;
            Strike = strike;
            ExpirationDate = expDate;

            Side = side;
            Qty = qty;
            ExpirationCut = expCut;

            SettlementType = settlType;
            SettlementCcy = settlCcy;
        }

    }

    public class JpmFxOptionsHedgeLegFIXMessageArgs
    {

        public string DeltaHedgeCurrency { get; private set; }
        public double? DeltaHedgeRate { get; private set; }
        public double? DeltaHedgeAmount { get; private set; }
        public string DeltaHedgeLegTenor { get; private set; }
        public DateTime? DeltaHedgeExpDate { get; private set; }

        public JpmFxOptionsHedgeLegFIXMessageArgs(string ccy, string tenor,
            double? rate = null, double? amount = null)
        {
            DeltaHedgeCurrency = ccy;
            DeltaHedgeLegTenor = tenor;

            DeltaHedgeRate = rate;
            DeltaHedgeAmount = amount;
        }

        public JpmFxOptionsHedgeLegFIXMessageArgs(string ccy, DateTime expDate,
            double? rate = null, double? amount = null)
        {
            DeltaHedgeCurrency = ccy;
            DeltaHedgeExpDate = expDate;

            DeltaHedgeRate = rate;
            DeltaHedgeAmount = amount;
        }

    }

    public enum JpmFxOptionSecurityDesc
    {
        VANILLA,
        SINGLE_BARRIER,
        AT_EXP_DIGITAL,
        ONE_TOUCH,
        NO_TOUCH,
        RISKREVERSAL,
        STRADDLE,
        STRANGLE,
        SPREAD,
        SEAGULL,
        BUTTERFLY,
        MULTILEG
    }

    public enum JpmFxOptionSolvingFor
    {
        PREMIUM,
        STRIKE,
        STRIKE2,
        STRIKE3,
        QUANTITY,
        QUANTITY2,
        QUANTITY3,
        YIELD,
        MARGIN,
        BARRIER
    }

    public enum JpmFxOptionSide
    {
        Buy = 1,
        Sell = 2
    }

    public enum JpmFxOptionHedgeType
    {
        NoHedge = 1,
        SpotHedge = 2,
        ForwardHedge = 3
    }

    public enum JpmFxOptionPremiumPayOn
    {
        SpotDate = 0,
        ForwardDate = 1
    }

    public enum JpmFxOptionPriceRefMetric
    {
        PctCcy1,
        PctCcy2,
        Ccy1PerCcy2,
        Ccy2PerCcy1
    }

    public enum JpmFxOptionLegSecurityDesc
    {
        VANILLA,
        SINGLE_BARRIER
    }

    public enum JpmFxOptionLegPutOrCall
    {
        Put = 0,
        Call = 1
    }

    public enum JpmFxOptionLegStrike
    {
        Delta,
        Pips,
        ATM,
        ATMF,
        F,
        S,
        N,
        StrikeValue,
    }

    public enum JpmFxOptionLegBarrier
    {
        Delta,
        Pips,
        ATM,
        ATMF,
        F,
        S,
        N,
        BarrierValue,
    }

    public enum JpmFxOptionLegBarrierType
    {
        UpOut = 0,
        DownOut = 1,
        UpIn = 2,
        DownIn = 3
    }

    public enum JpmFxOptionLegBarrierObservation
    {
        Continuous = 0,
        AtExpiry = 1
    }

    public enum JpmFxOptionLegSettlmntType
    {
        RegularPhysical = 0,
        Cash = 1
    }

    public enum JpmFxOptionOneTouchPayTime
    {
        Expiry = 0,
        Hit = 1
    }
}
