﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXDataObjects;
using WMC.FIXKafkaClients;
using WMC.FXConventionHelper;

using QuickFix;
using FIX44=QuickFix.FIX44;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WMC.FIXGateways.Dbabfx
{
    public class DbabfxFIXOptsTenorDatesAdaptor : DbabfxFIXOptionsAdaptor
    {

        public IEnumerable<string> CurrencyPairs { get; private set; }
        public IEnumerable<string> VolSurfaceTenors { get; private set; }

        private Dictionary<string, TenorDates> _ccypairTenorDates = new Dictionary<string, TenorDates>();

        private string _quoteRequestRefIdFormat = "TenorDates_{0}_[{1}]_{2}";

        public DbabfxFIXOptsTenorDatesAdaptor(IEnumerable<string> ccypairs, IEnumerable<string> volSurfTenors)
            :base()
        {
            CurrencyPairs = ccypairs;
            VolSurfaceTenors = volSurfTenors;

            var tnrDtsVanOptQRs = MakeTenorDatesVanillaOptionQuoteRequests(ccypairs, volSurfTenors);

            InitialiseAndLoadRfqStateMachineQueue(tnrDtsVanOptQRs);
        }

        public DbabfxFIXOptsTenorDatesAdaptor(int activeQuoteRequestLimit, IEnumerable<string> ccypairs, IEnumerable<string> volSurfTenors)
            :base(activeQuoteRequestLimit)
        {
            CurrencyPairs = ccypairs;
            VolSurfaceTenors = volSurfTenors;

            var tnrDtsVanOptQRs = MakeTenorDatesVanillaOptionQuoteRequests(ccypairs, volSurfTenors);

            InitialiseAndLoadRfqStateMachineQueue(tnrDtsVanOptQRs);
        }

        protected override void InitialiseAndLoadRfqStateMachineQueue(IEnumerable<VanillaOptionsQuoteRequest> optionsQuoteRequets)
        {
            base.InitialiseAndLoadRfqStateMachineQueue(optionsQuoteRequets);

            foreach (var ccypair in CurrencyPairs)
                _ccypairTenorDates.Add(ccypair, new TenorDates(DateTime.UtcNow.Date, KafkaRFQSource.dbabfx.ToString(), ccypair));
        }

        private IEnumerable<SingleVanillaOptionQuoteRequest> MakeTenorDatesVanillaOptionQuoteRequests(IEnumerable<string> ccypairs, IEnumerable<string> volSurfTenors)
        {
            var tnrDtsVanOptQRs = new List<SingleVanillaOptionQuoteRequest>();
            var nonNdfCcypairs = ccypairs.Where(ccypair => !FXConvHelper.IsNDF(ccypair));

            foreach (var ccypair in nonNdfCcypairs)
            {

                var leftCcy = ccypair.Substring(0, 3);
                var rightCcy = ccypair.Substring(3, 3);

                foreach (var tenor in volSurfTenors)
                {
                    var sglVanOptQouteReqLeg = new SingleVanillaOptionContract(tenor, null, null, OptionStrikeType.DeltaNeutral,
                        OptionCallPut.Call, BuySell.Buy, leftCcy, leftCcy);

                    var uuid = Guid.NewGuid().ToString();

                    var refId = string.Format(_quoteRequestRefIdFormat, ccypair, tenor, uuid);
                    var sglVanOptQuoteReq = new SingleVanillaOptionQuoteRequest(refId, ccypair, leftCcy, leftCcy, sglVanOptQouteReqLeg);

                    tnrDtsVanOptQRs.Add(sglVanOptQuoteReq);
                }
                
            }

            return tnrDtsVanOptQRs;
        }

        private string GetTenorFromQuoteRequestRefId(string quoteRequestRefId)
        {
            var tenor = string.Empty;

            var leftSquareBracketIdx = quoteRequestRefId.IndexOf('[');
            var rightSquareBracketIdx = quoteRequestRefId.IndexOf(']');

            tenor = quoteRequestRefId.Substring(leftSquareBracketIdx + 1, rightSquareBracketIdx - leftSquareBracketIdx - 1);

            return tenor;
        }

        #region Override Methods

        protected override void ProcessVanillaOptionQuote(SingleVanillaOptionQuote sglLegVanillaOptionQuote)
        {

            base.ProcessVanillaOptionQuote(sglLegVanillaOptionQuote);

            var ccypair = sglLegVanillaOptionQuote.SecuritySymbol;

            TenorDates tenorDatesForCcypair = null;

            if (_ccypairTenorDates.TryGetValue(ccypair, out tenorDatesForCcypair))
            {
                
                var tenor = GetTenorFromQuoteRequestRefId(sglLegVanillaOptionQuote.QuoteRequestRefId);
                var maturityDate = sglLegVanillaOptionQuote.OptionPriceDetails.MaturityDate;

                _log.DebugFormat("Adding Tenor: {0}, Maturity Date: {1} to currency pair: {2}", tenor, maturityDate.ToString("yyyy-MM-dd"), ccypair);

                tenorDatesForCcypair.AddTenorDate(tenor, maturityDate);

                _ccypairTenorDates[ccypair] = tenorDatesForCcypair;

                if (tenorDatesForCcypair.HasAllTenorDates(VolSurfaceTenors))
                {

                    _log.InfoFormat("{0} has collected all tenor dates.", ccypair);
                    
                    foreach (var volSurfTnr in VolSurfaceTenors)
                    {
                        DateTime? tenorMaturityDate = null;

                        tenorDatesForCcypair.TryGetTenorDate(volSurfTnr, out tenorMaturityDate);

                        _log.DebugFormat("{0}: {1} - {2}", ccypair, volSurfTnr, ((DateTime)tenorMaturityDate).ToString("yyyy-MM-dd"));
                    }

                    _log.InfoFormat("Encoding Json for all tenor dates of {0} ...", ccypair);

                    var tenorDatesForCcypairJson = JsonConvert.SerializeObject(tenorDatesForCcypair, new StringEnumConverter());

                    _log.DebugFormat("{0} Rfq Tenor Dates Json:\n{1}", ccypair, tenorDatesForCcypairJson);

                    _log.InfoFormat("Sending Json for Tenor Dates of {0} via Kafka ...", ccypair);

                    FIXKafkaProducer.SendRfqTenorDatesAsync(tenorDatesForCcypairJson);
                    FIXKafkaProducer.SendRfqTenorDatesAsync(ccypair, tenorDatesForCcypairJson);

                }
            }

        }

        public override void OnLogout(SessionID sessionID)
        {
            base.OnLogout(sessionID);

            Environment.Exit(0);
        }

        public override void OnRfqStateMachineActivationTimerElapsed(object sender, EventArgs args)
        {
            base.OnRfqStateMachineActivationTimerElapsed(sender, args);

            if (_pendingRFQStateMachineQueue.Count == 0 && _activeRFQStateMachines.Count == 0)
            {
                _log.InfoFormat("All Tenor Dates Rfq requests have been processed.");

                _log.InfoFormat("Sending Logoff request ...");

                var logout = new FIX44.Logout();
                Session.SendToTarget(logout, _sessionID);
            }
        }

        #endregion Override Methods
    }
}
