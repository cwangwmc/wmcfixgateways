﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

using WMC.FIXDataObjects;
using WMC.FIXStateMachines;
using WMC.FIXKafkaClients;

using QuickFix;
using FIX44=QuickFix.FIX44;
using QuickFix.Fields;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using log4net;

namespace WMC.FIXGateways.Dbabfx
{
    public class DbabfxFIXOptionsAdaptor : MessageCracker, IApplication
    {

        protected static readonly log4net.ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        protected SessionID _sessionID;
        protected bool _loggedOn;

        protected static readonly int _defaultActiveQuoteRequestsLimit = 2; // The default control limit on how many active Rfqs can co-exist, i.e. how demanding we are against dbafbx pricing interface.

        protected int _activeQuoteRequestLimit; // The control limit on how many active Rfqs can co-exist, i.e. how demanding we are against dbafbx pricing interface.

        protected ConcurrentQueue<QuoteRequest> _quoteRequestQueue;
        protected ConcurrentQueue<DbabfxOptionsRFQStateMachine> _pendingRFQStateMachineQueue;

        protected ConcurrentDictionary<string, DbabfxOptionsRFQStateMachine> _activeRFQStateMachines;

        protected DbabfxVolExpiryCutCodeHelper _volExpCutCodeHelper = new DbabfxVolExpiryCutCodeHelper();

        protected Timer _actionAfterLogonTimer;
        protected Timer _rfqStateMachineActivationTimer;

        public DbabfxFIXOptionsAdaptor()
        {
            _activeQuoteRequestLimit = _defaultActiveQuoteRequestsLimit;
            _pendingRFQStateMachineQueue = new ConcurrentQueue<DbabfxOptionsRFQStateMachine>();
            _activeRFQStateMachines = new ConcurrentDictionary<string, DbabfxOptionsRFQStateMachine>();
            SetupTimers();
        }

        public DbabfxFIXOptionsAdaptor(int activeQuoteRequestLimit)
            :this()
        {
            _activeQuoteRequestLimit = activeQuoteRequestLimit;
        }

        public DbabfxFIXOptionsAdaptor(IEnumerable<VanillaOptionsQuoteRequest> vanillaOptionsQuoteRequests)
            :this()
        {
            InitialiseAndLoadRfqStateMachineQueue(vanillaOptionsQuoteRequests);
        }

        public DbabfxFIXOptionsAdaptor(int activeQuoteRequestLimit, IEnumerable<VanillaOptionsQuoteRequest> vanillaOptionsQuoteRequests)
            :this(activeQuoteRequestLimit)
        {
            InitialiseAndLoadRfqStateMachineQueue(vanillaOptionsQuoteRequests);
        }

        protected virtual void InitialiseAndLoadRfqStateMachineQueue(IEnumerable<VanillaOptionsQuoteRequest> optionsQuoteRequests)
        {
            var optionsQuoteRequestsStateMachines = MakeRfqStateMachines(optionsQuoteRequests);

            foreach (var rfqStateMachine in optionsQuoteRequestsStateMachines)
            {
                rfqStateMachine.Activated += OnStateMachineActivate;
                rfqStateMachine.Deactivated += OnStateMachineDeactivate;
                rfqStateMachine.StateMachineFIXMessageOut += OnStateMachineOutboundFIXMessage;

                _pendingRFQStateMachineQueue.Enqueue(rfqStateMachine);
            }
        }

        protected virtual void SetupTimers()
        {
            _actionAfterLogonTimer = new Timer();
            _actionAfterLogonTimer.Interval = 30_000; // Time in milliseconds to wait before acting on the app's main objectives 
            _actionAfterLogonTimer.AutoReset = false;
            _actionAfterLogonTimer.Elapsed += OnActionAfterLogonTimerElapsed;

            _rfqStateMachineActivationTimer = new Timer();
            _rfqStateMachineActivationTimer.Interval = 2_000; // Time in milliseconds to wait between rounds to activate pending Rfq state machines
            _rfqStateMachineActivationTimer.AutoReset = true;
            _rfqStateMachineActivationTimer.Elapsed += OnRfqStateMachineActivationTimerElapsed;
        }

        public IEnumerable<DbabfxOptionsRFQStateMachine> MakeRfqStateMachines(IEnumerable<VanillaOptionsQuoteRequest> optionsQuoteRequests)
        {
            var rfqStateMachines = new List<DbabfxOptionsRFQStateMachine>();

            foreach (var optionsQuoteRequest in optionsQuoteRequests)
            {
                if (optionsQuoteRequest is SingleVanillaOptionQuoteRequest)
                {
                    var sglVanOptQuoteReq = (SingleVanillaOptionQuoteRequest)optionsQuoteRequest;

                    var singleVanillaOptionRFQStateMachine = MakeSingleVanillaOptionRfqStateMachine(sglVanOptQuoteReq,
                        countdownInterval: 2_000, waitInterval: 5 * 60_000);

                    rfqStateMachines.Add(singleVanillaOptionRFQStateMachine);
                }
            }

            return rfqStateMachines;
        }

        public DbabfxOptionsRFQStateMachine MakeSingleVanillaOptionRfqStateMachine(SingleVanillaOptionQuoteRequest singleVanillaOptionQuoteRequest,
            long countdownInterval, long waitInterval)
        {

            // countdownInterval - Time in milliseconds for countdown from state machine activation to firing off Quote Request.
            // waitInterval - Time in milliseconds to wait for Quote reply before aborting, moving to Done state followed by deactivation.

            var refId = singleVanillaOptionQuoteRequest.QuoteRequestRefId;
            var ccypair = singleVanillaOptionQuoteRequest.SecuritySymbol;
            var premCcy = singleVanillaOptionQuoteRequest.OptionPremiumCurrency;
            var notionalCcy = singleVanillaOptionQuoteRequest.OptionNotionalCurrency;
            var singleVanillaOptionLeg = (SingleVanillaOptionContract)singleVanillaOptionQuoteRequest.VanillaOptionContract;

            var optLegBuySell = singleVanillaOptionLeg.Side == BuySell.Buy ? DbabfxOptionLegSide.Buy : DbabfxOptionLegSide.Sell;
            var optLegCallPut = singleVanillaOptionLeg.CallPut == OptionCallPut.Call ? DbabfxOptionLegCFICode.OCECPN : DbabfxOptionLegCFICode.OPECPN;
            var optLegExpDateExpr = singleVanillaOptionLeg.MaturityDate.HasValue ? singleVanillaOptionLeg.MaturityDate.Value.ToString("yyyyMMdd") : string.Empty;
            var optLegPremDelType = singleVanillaOptionLeg.OptionPremiumDeliveryType == OptionPremiumDeliveryType.Spot ? DbabfxOptionPremDeliveryType.S : DbabfxOptionPremDeliveryType.F;

            var optLegHedgeType = DbabfxOptionHedgeTradeType.Spot;

            switch (singleVanillaOptionLeg.HedgeTradeType)
            {
                case HedgeTradeType.Spot:
                    optLegHedgeType = DbabfxOptionHedgeTradeType.Spot;
                    break;
                case HedgeTradeType.Forward:
                    optLegHedgeType = DbabfxOptionHedgeTradeType.Forward;
                    break;
                case HedgeTradeType.LIVE:
                    optLegHedgeType = DbabfxOptionHedgeTradeType.Live;
                    break;
            }

            var optLegStrikeType = DbabfxOptionLegStrikeType.DN;

            switch (singleVanillaOptionLeg.StrikeType)
            {
                case OptionStrikeType.AtmSpot:
                    optLegStrikeType = DbabfxOptionLegStrikeType.ATMS;
                    break;
                case OptionStrikeType.AtmForward:
                    optLegStrikeType = DbabfxOptionLegStrikeType.ATMF;
                    break;
                case OptionStrikeType.DeltaSpot:
                    optLegStrikeType = DbabfxOptionLegStrikeType.DS;
                    break;
                case OptionStrikeType.DeltaForward:
                    optLegStrikeType = DbabfxOptionLegStrikeType.DF;
                    break;
                case OptionStrikeType.DeltaNeutral:
                    optLegStrikeType = DbabfxOptionLegStrikeType.DN;
                    break;
                case OptionStrikeType.Outright:
                    optLegStrikeType = DbabfxOptionLegStrikeType.OutRight;
                    break;
            }

            var expiryCutoff = _volExpCutCodeHelper.VolExpiryCutCode(ccypair);

            var dbabfxOptLeg = new DbabfxOptionLeg(optLegCallPut, singleVanillaOptionLeg.Tenor, optLegExpDateExpr, singleVanillaOptionLeg.Strike, optLegStrikeType, optLegBuySell, singleVanillaOptionLeg.Notional);

            var dbabfxOpt = new DbabfxOption(DbabfxOptionSecurityType.OPT, DbabfxOptionSecuritySubType.VAN, ccypair, premCcy, notionalCcy, optLegHedgeType, optLegPremDelType,
                new DbabfxOptionLeg[] { dbabfxOptLeg }, expiryCutoff);

            var rfqStateMachine = new DbabfxOptionsRFQStateMachine(refId, dbabfxOpt, ccypair, _loggedOn, countdownInterval, waitInterval);

            return rfqStateMachine;

        }

        protected FIX44.QuoteRequest MakeOptionQuoteRequest(string refId, DbabfxOptionsQuoteRequestFIXMessageArgs qrArgs)
        {
            var ccypair = string.Format("{0}/{1}", qrArgs.Ccy1, qrArgs.Ccy2);

            var quoteRequest = new FIX44.QuoteRequest();

            quoteRequest.QuoteReqID = new QuoteReqID(refId);

            var noRelatedSymGrp = new FIX44.QuoteRequest.NoRelatedSymGroup();

            // Tag 55 Symbol
            // Should be set to "[N/A]"?
            noRelatedSymGrp.Symbol = new Symbol("[N/A]");

            // Tag 9016 HedgeTradeType
            // 0 - LIVE
            // 1 - Spot
            // 2 - Forward
            noRelatedSymGrp.SetField(new CharField(DbabfxFIXConstants.Tag_9016_HedgeTradeType, qrArgs.HedgeTradeType));

            #region NoUnderlyings

            var noUnderlyingsGrp = new FIX44.QuoteRequest.NoRelatedSymGroup.NoUnderlyingsGroup();
            noUnderlyingsGrp.UnderlyingSymbol = new UnderlyingSymbol(ccypair);

            // Tag 310 UnderlyingSecurityType
            // OPT - Single Option
            // MLEG - Multi-leg strategy
            noUnderlyingsGrp.UnderlyingSecurityType = new UnderlyingSecurityType(qrArgs.SecurityType);

            // Tag 763 UnderlyingSecuritySubType
            // Case 310 = OPT => 763 = VAN - Single Vanilla; 
            // Case 310 = MLEG => 763 = STD - Staddle; STG - Strangle; RR - Risk Reversal; GS - Generic spread
            noUnderlyingsGrp.UnderlyingSecuritySubType = new UnderlyingSecuritySubType(qrArgs.SecuritySubType);

            noUnderlyingsGrp.UnderlyingCurrency = new UnderlyingCurrency(qrArgs.NotionalCcy);

            // Tag 5336 Unique number of the underlying in quote, defaulting to 1
            noUnderlyingsGrp.SetField(new IntField(DbabfxFIXConstants.Tag_5336_UnderlyingID, 1));

            // Tag 20007 Expiration cutoff, NY, TK
            noUnderlyingsGrp.SetField(new StringField(DbabfxFIXConstants.Tag_20007_Cutoff, qrArgs.ExpiryCutoff));

            // Tag 20010 UnderlyingPremiumCurrency
            noUnderlyingsGrp.SetField(new StringField(DbabfxFIXConstants.Tag_20010_UnderlyingPremiumCurrency, qrArgs.PremCcy));

            // Tag 5475 PremDeliveryType
            // S - Spot
            // F - Forward
            noUnderlyingsGrp.SetField(new CharField(DbabfxFIXConstants.Tag_5475_PremDeliveryType, qrArgs.PremDeliveryType));

            noRelatedSymGrp.AddGroup(noUnderlyingsGrp);

            #endregion

            #region NoLegs

            foreach (var optLeg in qrArgs.OptionLegs)
            {
                var noLegsGrp = new FIX44.QuoteRequest.NoRelatedSymGroup.NoLegsGroup();
                noLegsGrp.LegSymbol = new LegSymbol(ccypair);

                // Tag 600 LegCFICode
                // OCECPN - Call
                // OPECPN - Put
                noLegsGrp.LegCFICode = new LegCFICode(optLeg.CFICode);

                if (optLeg.MaturityDate != string.Empty)
                    // Tag 611 LegMaturityDate
                    // YYYYMMDD
                    noLegsGrp.LegMaturityDate = new LegMaturityDate(optLeg.MaturityDate);
                else
                    // Tag 7911 LegTenorValue
                    // ON, TN, nD, nW, nM, nY
                    noLegsGrp.SetField(new StringField(DbabfxFIXConstants.Tag_7911_LegTenorValue, optLeg.TenorValue));

                if (!DbabfxFIXConstants.AtmStrikeTypes.Contains(optLeg.StrikeType.ToUpper()) && optLeg.StrikePrice.HasValue)
                    noLegsGrp.LegStrikePrice = new LegStrikePrice(new decimal(optLeg.StrikePrice.Value));

                // Tag 624 LegSide
                // '1' - Instrument is traded in the same direction as whole structure
                // '2' - Instrument is traded in the opposition direction as whole structure
                noLegsGrp.LegSide = new LegSide(optLeg.Side);

                noLegsGrp.SetField(new LegOrderQty(new decimal(optLeg.Qty)));

                // Tag 20012 LegStrikeType
                // '1' - Strike Value
                // 'DS' - Spot Delta
                // 'DF' - Forward Delta
                // 'ATMS' - ATM Spot
                // 'ATMF' - ATM Forward
                // 'DN' - Delta Neutral
                noLegsGrp.SetField(new StringField(DbabfxFIXConstants.Tag_20012_LegStrikeType, optLeg.StrikeType));

                noLegsGrp.SetField(new IntField(DbabfxFIXConstants.Tag_20036_LegRefUnderlyingId, 1));

                noRelatedSymGrp.AddGroup(noLegsGrp);
            }

            #endregion

            quoteRequest.AddGroup(noRelatedSymGrp);

            return quoteRequest;
        }

        #region QuickFix iApplication Methods
        public virtual void FromApp(Message msg, SessionID sessionID)
        {
            _log.DebugFormat("FromApp: {0}, {1}", sessionID.ToString(), msg.ToString());
            Crack(msg, sessionID);
        }

        public virtual void OnCreate(SessionID sessionID)
        {
            _log.DebugFormat("OnCreate: {0}", sessionID.ToString());
        }

        public virtual void OnLogon(SessionID sessionID)
        {
            _log.DebugFormat("OnLogon: {0}", sessionID.ToString());
            _sessionID = sessionID;
            _loggedOn = true;
            _actionAfterLogonTimer.Enabled = true;
        }

        public virtual void OnLogout(SessionID sessionID)
        {
            _log.DebugFormat("OnLogout: {0}", sessionID.ToString());
            _sessionID = null;
            _loggedOn = false;
            _actionAfterLogonTimer.Enabled = false;
            _rfqStateMachineActivationTimer.Enabled = false;
        }

        public virtual void FromAdmin(Message msg, SessionID sessionID)
        {
            _log.DebugFormat("FromAdmin: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        public virtual void ToAdmin(Message msg, SessionID sessionID)
        {
            msg.Header.SetField(DbabfxFIXConstants.Tag_6958_ProductType_field);

            if (msg is FIX44.Logon)
            {
                var logonMsg = (FIX44.Logon)msg;
                logonMsg.ResetSeqNumFlag = new ResetSeqNumFlag(true);
            }

            _log.DebugFormat("ToAdmin: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        public virtual void ToApp(Message msg, SessionID sessionID)
        {
            msg.Header.SetField(DbabfxFIXConstants.Tag_6958_ProductType_field);

            _log.DebugFormat("ToApp: {0}, {1}", sessionID.ToString(), msg.ToString());
        }
        #endregion QuickFix iApplication Methods

        public virtual void OnStateMachineActivate(object sender, StateMachineActivatedEventArgs args)
        {

            var refId = args.StateMachineRefId;

            _log.InfoFormat("State Machine {0} has been activated.", refId);

            DbabfxOptionsRFQStateMachine stMchn = null;

            if (_activeRFQStateMachines.TryGetValue(refId, out stMchn))
            {
                if (_loggedOn && stMchn.CurrentState is WaitingForLogonS)
                    stMchn.OnInboundFIXMessage(this, new FIXMessageEventArgs(refId, FIXMessageType.Logon_A, DateTime.UtcNow));
            }

        }

        public virtual void OnStateMachineDeactivate(object sender, StateMachineDeactivatedEventArgs args)
        {

            var refId = args.StateMachineRefId;

            _log.InfoFormat("State Machine {0} has been deactivated. Removing it from ActiveRFQStateMachines ...", refId);

            DbabfxOptionsRFQStateMachine deactivatingRfqStateMachine = null;

            if (_activeRFQStateMachines.ContainsKey(refId))
                if (_activeRFQStateMachines.TryRemove(refId, out deactivatingRfqStateMachine))
                    _log.InfoFormat("State Machine {0} removal has succeeded.", refId);
                else
                    _log.InfoFormat("State Machine {0} removal has failed.", refId);
            else
                _log.WarnFormat("State Machine {0} was not found in the Active RFQ State Machine Pool. Nothing to be done.", refId);

            _log.InfoFormat("ActiveRFQStateMachines count: {0}", _activeRFQStateMachines.Count);

        }


        #region Timers Eventhandlers
        public virtual void OnActionAfterLogonTimerElapsed(object sender, EventArgs args)
        {

            _log.DebugFormat("OnWaitAfterLogonTimerElapsed ...");

            _log.InfoFormat("Action After Logon Timer has elapsed after waiting for {0} secs ...", _actionAfterLogonTimer.Interval / 1_000);

            _log.InfoFormat("Enabling Rfq State Machine Activation Timer ...");

            _rfqStateMachineActivationTimer.Enabled = true;

            _log.InfoFormat("Rfq State Machine Activation Timer has been enabled.");

        }

        public virtual void OnRfqStateMachineActivationTimerElapsed(object sender, EventArgs args)
        {

            _log.DebugFormat("OnRfqStateMachineActivationTimerElapsed ...");

            _log.DebugFormat("Rfq state machine activation timer elapsed after {0} secs ...", _rfqStateMachineActivationTimer.Interval / 1_000);

            _log.DebugFormat("ActiveRFQStateMachines count: {0}; PendingRFQStateMachineQueue count: {1}", _activeRFQStateMachines.Count,
                            _pendingRFQStateMachineQueue.Count);

            while (_activeRFQStateMachines.Count < _activeQuoteRequestLimit
                && _pendingRFQStateMachineQueue.Count > 0)
            {

                DbabfxOptionsRFQStateMachine stMchn = null;

                if (_pendingRFQStateMachineQueue.TryDequeue(out stMchn))
                {
                    _log.DebugFormat("Adding state machine {0} to Active Rfq State Machines Pool ...", stMchn.StateMachineRefId);

                    _activeRFQStateMachines.TryAdd(stMchn.StateMachineRefId, stMchn);

                    stMchn.Activate();
                }

            }

            _log.DebugFormat("PendingRFQStateMachineQueue Count: {0}", _pendingRFQStateMachineQueue.Count);

            if (_pendingRFQStateMachineQueue.Count == 0)
            {
                _log.DebugFormat("Pending Rfq state machine queue is empty. Nothing to be done.");
            }

            _log.DebugFormat("Rfq state machine activation timer will set off again in {0} secs.", _rfqStateMachineActivationTimer.Interval / 1_000);

        }
        #endregion Timers Eventhandlers


        public virtual void OnStateMachineOutboundFIXMessage(object sender, FIXMessageDetailedEventArgs msgDetailsEventArgs)
        {
            _log.DebugFormat("OnStateMachineOutboundFIXMessage ...");

            if (msgDetailsEventArgs.MessageType == FIXMessageType.QuoteRequest_R)
            {
                var refId = msgDetailsEventArgs.RefId;
                var dbabfxOptionQRArgs = (DbabfxOptionsQuoteRequestFIXMessageArgs)msgDetailsEventArgs.MessageArgs;

                var qrFIXMsg = MakeOptionQuoteRequest(refId, dbabfxOptionQRArgs);

                Session.SendToTarget(qrFIXMsg, _sessionID);
            }
            else
                _log.WarnFormat("Message Type {0} is not supported. The FIX message will not be composed or sent.", msgDetailsEventArgs.MessageType);
        }


        #region MessageCracker OnMessage Overrides
        public void OnMessage(FIX44.Quote quote, SessionID sessionID)
        {
            var quoteReqRefId = quote.QuoteReqID.getValue();

            _log.InfoFormat("OnMessage Quote - QuoteReqId: {0}", quoteReqRefId);

            var fixMsgEvtArgs = new FIXMessageEventArgs(quoteReqRefId, FIXMessageType.Quote_S, DateTime.UtcNow);

            DbabfxOptionsRFQStateMachine stMchn = null;

            if (_activeRFQStateMachines.TryGetValue(quoteReqRefId, out stMchn))
                stMchn.OnInboundFIXMessage(this, fixMsgEvtArgs);

            var quoteId = quote.QuoteID.getValue();
            var symbol = quote.Symbol.getValue();
            var validUntilTime = quote.ValidUntilTime.getValue();
            var totalBidPrem = quote.BidPx.getValue();
            var totalOfferPrem = quote.OfferPx.getValue();
            var quoteType = quote.QuoteType.getValue();
            var hedgeTradeType = quote.GetInt(DbabfxFIXConstants.Tag_9016_HedgeTradeType);

            #region NoUnderlyings

            var noUnderlyingsGrp = new Group(Tags.NoUnderlyings, Tags.UnderlyingSymbol);

            var noUnderlyingGrpCount = quote.GetInt(Tags.NoUnderlyings);

            quote.GetGroup(1, noUnderlyingsGrp); // Number of instruments in the quote. Should always be 1.

            var underlyingSymbol = noUnderlyingsGrp.GetString(Tags.UnderlyingSymbol);
            var underlyingSecurityType = noUnderlyingsGrp.GetString(Tags.UnderlyingSecurityType);
            var underlyingSecuritySubType = noUnderlyingsGrp.GetString(Tags.UnderlyingSecuritySubType);
            var underlyingCurrency = noUnderlyingsGrp.GetString(Tags.UnderlyingCurrency);
            var underlyingID = noUnderlyingsGrp.GetInt(DbabfxFIXConstants.Tag_5336_UnderlyingID);
            
            var cutoff = noUnderlyingsGrp.GetString(DbabfxFIXConstants.Tag_20007_Cutoff);
            var underlyingPremCcy = noUnderlyingsGrp.GetString(DbabfxFIXConstants.Tag_20010_UnderlyingPremiumCurrency);
            var underlyingPremSettlementDate = noUnderlyingsGrp.GetString(DbabfxFIXConstants.Tag_20011_UnderlyingPremiumSettlementDate);

            var isHedgeSideSet = noUnderlyingsGrp.IsSetField(DbabfxFIXConstants.Tag_9679_HedgeSide);
            var isHedgeCurrencySet = noUnderlyingsGrp.IsSetField(DbabfxFIXConstants.Tag_20020_HedgeCurrency);
            var isHedgeAmountSet = noUnderlyingsGrp.IsSetField(DbabfxFIXConstants.Tag_20022_HedgeAmount);
            var isHedgeFXRateSet = noUnderlyingsGrp.IsSetField(DbabfxFIXConstants.Tag_20023_HedgeFXRate);
            var isHedgeSettlementDateSet = noUnderlyingsGrp.IsSetField(DbabfxFIXConstants.Tag_20025_HedgeSettlementRate);

            char? hedgeSide = null;
            var hedgeCurrency = string.Empty;
            double? hedgeAmount = null;
            double? hedgeFXRate = null;
            var hedgeSettlementDate = string.Empty;

            if (isHedgeSideSet)
                hedgeSide = noUnderlyingsGrp.GetChar(DbabfxFIXConstants.Tag_9679_HedgeSide);

            if (isHedgeCurrencySet)
                hedgeCurrency = noUnderlyingsGrp.GetString(DbabfxFIXConstants.Tag_20020_HedgeCurrency);

            if (isHedgeAmountSet)
                hedgeAmount = (double)noUnderlyingsGrp.GetDecimal(DbabfxFIXConstants.Tag_20022_HedgeAmount);

            if (isHedgeFXRateSet)
                hedgeFXRate = (double)noUnderlyingsGrp.GetDecimal(DbabfxFIXConstants.Tag_20023_HedgeFXRate);

            if (isHedgeSettlementDateSet)
                hedgeSettlementDate = noUnderlyingsGrp.GetString(DbabfxFIXConstants.Tag_20025_HedgeSettlementRate);

            var underlyingBidPx = (double)noUnderlyingsGrp.GetDecimal(DbabfxFIXConstants.Tag_20032_UnderlyingBidPx);
            var underlyingOfferPx = (double)noUnderlyingsGrp.GetDecimal(DbabfxFIXConstants.Tag_20034_UnderlyingOfferPx);

            #endregion NoUnderlyings

            #region NoLegs

            var vanOptsPxDtls = new List<SingleVanilaOptionContractPriceDetails>();

            var noLegsGrp = new Group(Tags.NoLegs, Tags.LegSymbol);
            var noLegsGrpCount = quote.GetInt(Tags.NoLegs);

            for (int noLegsGrpIdx = 1; noLegsGrpIdx <= noLegsGrpCount; noLegsGrpIdx++)
            {
                quote.GetGroup(noLegsGrpIdx, noLegsGrp);

                #region Leg

                var legSymbol = noLegsGrp.GetString(Tags.LegSymbol).Replace("/", string.Empty);
                var legCFICode = noLegsGrp.GetString(Tags.LegCFICode);
                var legMaturityDate = DateTime.ParseExact(noLegsGrp.GetString(Tags.LegMaturityDate), "yyyyMMdd", null);
                var legStrikePrice = (double)noLegsGrp.GetDecimal(Tags.LegStrikePrice);
                var legSide = noLegsGrp.GetInt(Tags.LegSide);

                var legBidPx = (double)noLegsGrp.GetDecimal(Tags.LegBidPx);
                var legOfferPx = (double)noLegsGrp.GetDecimal(Tags.LegOfferPx);

                var legOrderQty = (double)noLegsGrp.GetDecimal(Tags.LegOrderQty);

                var legLastSpotRate = (double)noLegsGrp.GetDecimal(DbabfxFIXConstants.Tag_5190_LegLastSpotRate);
                var forwardRate = (double)noLegsGrp.GetDecimal(DbabfxFIXConstants.Tag_9099_ForwardRate);

                var legStrikeType = noLegsGrp.GetString(DbabfxFIXConstants.Tag_20012_LegStrikeType);

                var legBidVol = (double)noLegsGrp.GetDecimal(DbabfxFIXConstants.Tag_20029_LegBidVolatility);
                var legOfferVol = (double)noLegsGrp.GetDecimal(DbabfxFIXConstants.Tag_20030_LegOfferVolatility);

                var legRefUnderlyingId = noLegsGrp.GetInt(DbabfxFIXConstants.Tag_20036_LegRefUnderlyingId);

                var legSettlementDate = DateTime.ParseExact(noLegsGrp.GetString(DbabfxFIXConstants.Tag_20051_LegSettlementDate), "yyyyMMdd", null);

                var isLegHedgeSideSet = noLegsGrp.IsSetField(DbabfxFIXConstants.Tag_20040_LegHedgeSide);
                var isLegHedgeCurrencySet = noLegsGrp.IsSetField(DbabfxFIXConstants.Tag_20041_LegHedgeCurrency);
                var isLegHedgeAmountSet = noLegsGrp.IsSetField(DbabfxFIXConstants.Tag_20042_LegHedgeAmount);
                var isLegHedgeFXRateSet = noLegsGrp.IsSetField(DbabfxFIXConstants.Tag_20043_LegHedgeFXRate);
                var isLegHedgeSettlementDateSet = noLegsGrp.IsSetField(DbabfxFIXConstants.Tag_20044_LegHedgeSettlementDate);

                char? legHedgeSide = null;
                var legHedgeCurrency = string.Empty;
                double? legHedgeAmount = null;
                double? legHedgeFXRate = null;
                var legHedgeSettlementDate = string.Empty;

                if (isLegHedgeSideSet)
                    legHedgeSide = noLegsGrp.GetChar(DbabfxFIXConstants.Tag_20040_LegHedgeSide);

                if (isLegHedgeCurrencySet)
                    legHedgeCurrency = noLegsGrp.GetString(DbabfxFIXConstants.Tag_20041_LegHedgeCurrency);

                if (isLegHedgeAmountSet)
                    legHedgeAmount = (double)noLegsGrp.GetDecimal(DbabfxFIXConstants.Tag_20042_LegHedgeAmount);

                if (isLegHedgeFXRateSet)
                    legHedgeFXRate = (double)noLegsGrp.GetDecimal(DbabfxFIXConstants.Tag_20043_LegHedgeFXRate);

                if (isLegHedgeSettlementDateSet)
                    legHedgeSettlementDate = noLegsGrp.GetString(DbabfxFIXConstants.Tag_20044_LegHedgeSettlementDate);

                var legCallPut = OptionCallPut.Call;

                switch(legCFICode)
                {
                    case "OCECPN":
                        legCallPut = OptionCallPut.Call;
                        break;
                    case "OPECPN":
                        legCallPut = OptionCallPut.Put;
                        break;
                }

                var legBuySell = BuySell.Buy;

                switch(legSide)
                {
                    case 1:
                        legBuySell = BuySell.Buy;
                        break;
                    case 2:
                        legBuySell = BuySell.Sell;
                        break;
                }

                #endregion Leg

                var sglVanOptPxDtls = new SingleVanilaOptionContractPriceDetails(legBidPx, legOfferPx,
                    cutoff, legSymbol, underlyingPremCcy, underlyingCurrency,
                    legCallPut, legMaturityDate, legSettlementDate, legStrikePrice, legBuySell, legOrderQty, legBidVol, legOfferVol);

                vanOptsPxDtls.Add(sglVanOptPxDtls);
            }

            #endregion NoLegs

            if (vanOptsPxDtls.Count == 1)
            {
                var sglVanOptPxDtls = vanOptsPxDtls[0];
                var sglVanOptQuote = new SingleVanillaOptionQuote(sglVanOptPxDtls.SecuritySymbol, quoteReqRefId, quoteId, sglVanOptPxDtls);

                ProcessVanillaOptionQuote(sglVanOptQuote);
            }
        }

        public void OnMessage(FIX44.QuoteCancel quoteCancel, SessionID sessionID)
        {
            var quoteReqRefId = quoteCancel.QuoteReqID.getValue();

            _log.InfoFormat("OnMessage QuoteCancel - QuoteReqId: {0} - ", quoteReqRefId);

            var fixMsgEvtArgs = new FIXMessageEventArgs(quoteReqRefId, FIXMessageType.QuoteCancel_Z, DateTime.UtcNow);

            DbabfxOptionsRFQStateMachine stMchn = null;

            if (_activeRFQStateMachines.TryGetValue(quoteReqRefId, out stMchn))
                stMchn.OnInboundFIXMessage(this, fixMsgEvtArgs);
        }
        #endregion MessageCracker OnMessage Overrides


        protected virtual void ProcessVanillaOptionQuote(SingleVanillaOptionQuote sglLegVanillaOptionQuote)
        {

            var quoteRequestRefId = sglLegVanillaOptionQuote.QuoteRequestRefId;
            var quoteId = sglLegVanillaOptionQuote.QuoteId;
            var currencyPair = sglLegVanillaOptionQuote.SecuritySymbol;
            var maturityDate_yyyyMMdd = sglLegVanillaOptionQuote.OptionPriceDetails.MaturityDate.ToString("yyyyMMdd");
            var bidVol = sglLegVanillaOptionQuote.OptionPriceDetails.BidVolatility;
            var offerVol = sglLegVanillaOptionQuote.OptionPriceDetails.OfferVolatility;

            _log.InfoFormat("QuoteRequestRefId: {0} - QuoteId: {1} - Single Vanilla Option - MaturityDate: {2}, BidVol: {3}, OfferVol: {4}",
                            quoteRequestRefId, quoteId, maturityDate_yyyyMMdd, bidVol, offerVol);

            var sglLegVanOptQtJson = JsonConvert.SerializeObject(sglLegVanillaOptionQuote);

            FIXKafkaProducer.SendRfqAsync(KafkaRFQSource.dbabfx, currencyPair, sglLegVanOptQtJson);

        }

        protected virtual void ProcessVanillaOptionQuote(MultiLegVanillaOptionsQuote multiLegVanillaOptionsQuotes)
        {
            // To be implemented
            return;
        }

        protected SingleVanillaOptionQuote MakeSingleVanillaOptionQuote()
        {
            // To be implemented
            return null;
        }
    }
}
