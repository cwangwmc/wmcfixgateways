﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXStateMachines;

namespace WMC.FIXGateways.Dbabfx
{
    public class DbabfxOptionsQuoteRequestFIXMessageArgs : FIXMessageArgs
    {

        public string Ccy1 { get; private set; }
        public string Ccy2 { get; private set; }
        public string PremCcy { get; private set; }
        public string NotionalCcy { get; private set; }
        public string SecurityType { get; private set; }
        public string SecuritySubType { get; private set; }
        public char HedgeTradeType { get; private set; }
        public string ExpiryCutoff { get; private set; }
        public char PremDeliveryType { get; private set; }

        public IEnumerable<DbabfxOptionsLegFIXMessageArgs> OptionLegs { get; private set; }

        public DbabfxOptionsQuoteRequestFIXMessageArgs(string ccy1, string ccy2, string securityType, string securitySubType,
            char hedgeTradeType, string expiryCutoff, char premDeliveryType, IEnumerable<DbabfxOptionsLegFIXMessageArgs> optLegs, 
            string premCcy = "", string notionalCcy = "")
        {
            Ccy1 = ccy1;
            Ccy2 = ccy2;
            PremCcy = premCcy;
            NotionalCcy = notionalCcy;
            SecurityType = securityType;
            SecuritySubType = securitySubType;
            HedgeTradeType = hedgeTradeType;
            ExpiryCutoff = expiryCutoff;
            PremDeliveryType = premDeliveryType;

            OptionLegs = optLegs;
        }

    }

    public class DbabfxOptionsLegFIXMessageArgs
    {

        public string CFICode { get; private set; }
        public string TenorValue { get; private set; }
        public string MaturityDate { get; private set; }
        public double? StrikePrice { get; private set; }
        public string StrikeType { get; private set; }
        public char Side { get; private set; }
        public double Qty { get; private set; }

        public DbabfxOptionsLegFIXMessageArgs(string cfiCode, string tenorValue, string maturityDate_YYYYMMDD,
            double? strikePrice, string strikeType,
            char side, double qty)
        {
            CFICode = cfiCode;
            TenorValue = tenorValue;
            MaturityDate = maturityDate_YYYYMMDD;
            StrikePrice = strikePrice;
            StrikeType = strikeType;
            Side = side;
            Qty = qty;
        }

    }

    public enum DbabfxOptionPremDeliveryType
    {
        S,
        F
    }

    public enum DbabfxOptionHedgeTradeType
    {
        Live = 0,
        Spot = 1,
        Forward = 2
    }

    public enum DbabfxOptionSecurityType
    {
        OPT,
        MLEG
    }

    public enum DbabfxOptionSecuritySubType
    {
        VAN,
        STD,
        STG,
        RR,
        GS
    }

    public enum DbabfxOptionLegCFICode
    {
        OCECPN,
        OPECPN
    }

    public enum DbabfxOptionLegStrikeType
    {
        OutRight = 1,
        DS,
        DF,
        ATMS,
        ATMF,
        DN
    }

    public enum DbabfxOptionLegSide
    {
        Buy = 1,
        Sell = 2
    }
}
