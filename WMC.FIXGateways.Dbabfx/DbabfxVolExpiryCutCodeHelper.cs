﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FXConventionHelper;

namespace WMC.FIXGateways.Dbabfx
{
    public class DbabfxVolExpiryCutCodeHelper : iFxVolExpiryCutCodeHelper
    {

        public string VolExpiryCutCode(string ccypair)
        {
            var ccy1 = ccypair.Substring(0, 3);
            var ccy2 = ccypair.Substring(3, 3);

            if (DbabfxFIXConstants.NonNYCcyExpiryCuts.ContainsKey(ccy1))
                return DbabfxFIXConstants.NonNYCcyExpiryCuts[ccy1];
            else if (DbabfxFIXConstants.NonNYCcyExpiryCuts.ContainsKey(ccy2))
                return DbabfxFIXConstants.NonNYCcyExpiryCuts[ccy2];
            else
                return DbabfxFIXConstants.ExpiryCut_NewYork10AM;
        }

    }
}
