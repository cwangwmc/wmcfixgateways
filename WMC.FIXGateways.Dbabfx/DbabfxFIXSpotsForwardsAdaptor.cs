﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using log4net;

using QuickFix;
using QuickFix.Fields;

namespace WMC.FIXGateways.Dbabfx
{
    public class DbabfxFIXSpotsForwardsAdaptor: MessageCracker, IApplication
    {
        private static readonly log4net.ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void FromApp(Message msg, SessionID sessionID)
        {
            _log.DebugFormat("FromApp: {0}, {1}", sessionID.ToString(), msg.ToString());
            Crack(msg, sessionID);
        }

        public void OnCreate(SessionID sessionID)
        {
            _log.DebugFormat("OnCreate: {0}", sessionID.ToString());
        }

        public void OnLogout(SessionID sessionID)
        {
            _log.DebugFormat("OnLogout: {0}", sessionID.ToString());
        }

        public void OnLogon(SessionID sessionID)
        {
            _log.DebugFormat("OnLogon: {0}", sessionID.ToString());
        }

        public void FromAdmin(Message msg, SessionID sessionID)
        {
            _log.DebugFormat("FromAdmin: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        public void ToAdmin(Message msg, SessionID sessionID)
        {
            _log.DebugFormat("ToAdmin: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

        public void ToApp(Message msg, SessionID sessionID)
        {
            _log.DebugFormat("ToApp: {0}, {1}", sessionID.ToString(), msg.ToString());
        }

    }
}
