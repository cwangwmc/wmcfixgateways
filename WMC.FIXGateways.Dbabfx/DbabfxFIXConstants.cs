﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using QuickFix.Fields;


namespace WMC.FIXGateways.Dbabfx
{
    public class DbabfxFIXConstants
    {
        public static readonly StringField Tag_6958_ProductType_field = new StringField(6958, "DB ABFX OPTION");
        public static readonly string[] AtmStrikeTypes = new string[] { "ATMS", "ATMF", "DN" };

        public static readonly int Tag_5190_LegLastSpotRate = 5190;
        public static readonly int Tag_5336_UnderlyingID = 5336;
        public static readonly int Tag_5475_PremDeliveryType = 5475;
        public static readonly int Tag_6958_ProductType = 6958;
        public static readonly int Tag_7706_OriginatorAcc = 7706;
        public static readonly int Tag_7911_LegTenorValue = 7911;
        public static readonly int Tag_9016_HedgeTradeType = 9016;
        public static readonly int Tag_9099_ForwardRate = 9099;
        public static readonly int Tag_9679_HedgeSide = 9679;
        public static readonly int Tag_20007_Cutoff = 20007;
        public static readonly int Tag_20010_UnderlyingPremiumCurrency = 20010;
        public static readonly int Tag_20011_UnderlyingPremiumSettlementDate = 20011;
        public static readonly int Tag_20012_LegStrikeType = 20012;
        public static readonly int Tag_20020_HedgeCurrency = 20020;
        public static readonly int Tag_20022_HedgeAmount = 20022;
        public static readonly int Tag_20023_HedgeFXRate = 20023;
        public static readonly int Tag_20025_HedgeSettlementRate = 20025;
        public static readonly int Tag_20029_LegBidVolatility = 20029;
        public static readonly int Tag_20030_LegOfferVolatility = 20030;
        public static readonly int Tag_20032_UnderlyingBidPx = 20032;
        public static readonly int Tag_20034_UnderlyingOfferPx = 20034;
        public static readonly int Tag_20036_LegRefUnderlyingId = 20036;
        public static readonly int Tag_20040_LegHedgeSide = 20040;
        public static readonly int Tag_20041_LegHedgeCurrency = 20041;
        public static readonly int Tag_20042_LegHedgeAmount = 20042;
        public static readonly int Tag_20043_LegHedgeFXRate = 20043;
        public static readonly int Tag_20044_LegHedgeSettlementDate = 20044;
        public static readonly int Tag_20051_LegSettlementDate = 20051;

        public static readonly string ExpiryCut_NewYork10AM = "NY";

        public static readonly Dictionary<string, string> NonNYCcyExpiryCuts = new Dictionary<string, string>()
        {
            { "SGD", "TK" },
            { "KRW", "KR" },
            { "CNY", "BE" },
            { "CNH", "TK" },
            { "RUB", "MO" },
            { "TRY", "LO" },
            { "PLN", "WA" },
            { "HUF", "BP" },
            { "MXN", "BA" },
            { "BRL", "BR" },
        };

    }
}
