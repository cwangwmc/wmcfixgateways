﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WMC.FIXStateMachines;

namespace WMC.FIXGateways.Dbabfx
{
    public class DbabfxOption
    {
        public DbabfxOptionSecurityType OptionType { get; private set; }
        public DbabfxOptionSecuritySubType OptionSubType { get; private set; }
        public string CurrencyPair { get; private set; }
        public string PremCurrency { get; private set; }
        public string NotionalCurrency { get; private set; }
        public DbabfxOptionHedgeTradeType HedgeTradeType { get; private set; }
        public string ExpiryCutoff { get; private set; }
        public DbabfxOptionPremDeliveryType PremDeliveryType { get; private set; }

        public IEnumerable<DbabfxOptionLeg> OptionLegs { get; private set; }

        public DbabfxOption(DbabfxOptionSecurityType optionType, DbabfxOptionSecuritySubType optionSubType,
            string ccypair, string premCcy, string notionalCcy,
            DbabfxOptionHedgeTradeType hedgeTradeType, DbabfxOptionPremDeliveryType premDeliveryType,
            IEnumerable<DbabfxOptionLeg> optionLegs, string expiryCutoff = "NY")
        {
            OptionType = optionType;
            OptionSubType = optionSubType;
            CurrencyPair = ccypair;
            PremCurrency = premCcy;
            NotionalCurrency = notionalCcy;
            HedgeTradeType = hedgeTradeType;
            ExpiryCutoff = expiryCutoff;
            PremDeliveryType = premDeliveryType;

            OptionLegs = optionLegs;
        }
    }

    public class DbabfxOptionLeg
    {
        public DbabfxOptionLegCFICode CFICode { get; private set; }
        public string TenorValue { get; private set; }
        public string MaturityDate { get; private set; }
        public double? StrikePrice { get; private set; }
        public DbabfxOptionLegStrikeType StrikeType { get; private set; }
        public DbabfxOptionLegSide Side { get; private set; }
        public double Qty { get; private set; }

        public DbabfxOptionLeg(DbabfxOptionLegCFICode cfiCode, string tenorValue, string maturityDate_YYYYMMDD,
            double? strikePrice, DbabfxOptionLegStrikeType strikeType, DbabfxOptionLegSide side, double qty)
        {
            CFICode = cfiCode;
            TenorValue = tenorValue;
            MaturityDate = maturityDate_YYYYMMDD;
            StrikePrice = strikePrice;
            StrikeType = strikeType;
            Side = side;
            Qty = qty;
        }
    }

    public class DbabfxOptionsRFQStateMachine : RFQStateMachine
    {

        private DbabfxOption _dbabfxOption { get; set; }

        public DbabfxOptionsRFQStateMachine(string stateMachineRefId, DbabfxOption dbabfxOption, string securitySymbol, bool loggedOn,
            long? timeToSendQuoteRequest, long? timeToWaitForQuote)
            : base(stateMachineRefId, securitySymbol, loggedOn, timeToSendQuoteRequest, timeToWaitForQuote)
        {
            _dbabfxOption = dbabfxOption;

            // States
            var waitingLogonState = new WaitingForLogonS(stateMachineRefId);
            var sendingQuoteRequestState = new SendingQuoteRequestS(stateMachineRefId, TimeToSendQuoteRequest);
            var waitingForQuoteRespState = new WaitingForResponseS(stateMachineRefId, TimeToWaitForQuote);
            var doneState = new DoneS(stateMachineRefId);

            // Transitions linking the States
            var logonT = new LogonT(waitingLogonState, sendingQuoteRequestState);

            var logoffT_A = new LogoffT(sendingQuoteRequestState, doneState);
            var logoffT_B = new LogoffT(waitingForQuoteRespState, doneState);

            var quoteRequestT = new QuoteRequestT(sendingQuoteRequestState, waitingForQuoteRespState);

            var quoteT = new QuoteT(waitingForQuoteRespState, doneState);

            var quoteCancelT = new QuoteCancelT(waitingForQuoteRespState, doneState);

            var timeOutT = new TimedOutT(waitingForQuoteRespState, doneState);

            // Decide the starting state based on whether the FIX engine has already logged on
            if (loggedOn)
                OriginState = sendingQuoteRequestState;
            else
                OriginState = waitingLogonState;

            TerminalState = doneState;

            CurrentState = OriginState;

            CurrentState.StateFIXMessageOut += OnOutboundFIXMessage;
            CurrentState.StateEntered += OnStateEntered;
            CurrentState.StateExited += OnStateExited;

            foreach (var t in CurrentState.Transitions)
                t.Triggered += OnStateTransition;
        }

        protected override FIXMessageDetailedEventArgs MakeFIXMessageDetailedEventArgs(FIXMessageEventArgs args)
        {
            FIXMessageDetailedEventArgs dtlEvtArgs = null;

            if (args.MessageType == FIXMessageType.QuoteRequest_R)
            {

                var ccy1 = SecuritySymbol.Substring(0, 3);
                var ccy2 = SecuritySymbol.Substring(3, 3);

                var premCcy = _dbabfxOption.PremCurrency;
                var notionalCcy = _dbabfxOption.NotionalCurrency;

                var optType = _dbabfxOption.OptionType.ToString();
                var optSubType = _dbabfxOption.OptionSubType.ToString();
                var hedgeTrdType = ((int)_dbabfxOption.HedgeTradeType).ToString().ToCharArray()[0];
                var expCutoff = _dbabfxOption.ExpiryCutoff;
                var premDelType = _dbabfxOption.PremDeliveryType.ToString().ToCharArray()[0];

                var singleVanillaOptLeg = _dbabfxOption.OptionLegs.First();

                var cfiCode = singleVanillaOptLeg.CFICode.ToString();
                var tenorVal = singleVanillaOptLeg.TenorValue;
                var maturityDt = singleVanillaOptLeg.MaturityDate;
                var strikePx = singleVanillaOptLeg.StrikePrice;
                var strikeType = singleVanillaOptLeg.StrikeType == DbabfxOptionLegStrikeType.OutRight ?
                    ((int)singleVanillaOptLeg.StrikeType).ToString() : singleVanillaOptLeg.StrikeType.ToString();

                var sideChar = ((int)singleVanillaOptLeg.Side).ToString().ToCharArray()[0];
                var qty = singleVanillaOptLeg.Qty;

                var singleVanillaOptLegArgs = new DbabfxOptionsLegFIXMessageArgs(cfiCode, tenorVal, maturityDt, strikePx, strikeType, sideChar, qty);

                var qrArgs = new DbabfxOptionsQuoteRequestFIXMessageArgs(ccy1, ccy2, optType, optSubType, hedgeTrdType, expCutoff, premDelType,
                    new List<DbabfxOptionsLegFIXMessageArgs> { singleVanillaOptLegArgs }, premCcy, notionalCcy);

                dtlEvtArgs = new FIXMessageDetailedEventArgs(StateMachineRefId, FIXMessageType.QuoteRequest_R, qrArgs);

            }

            return dtlEvtArgs;
        }

    }
}