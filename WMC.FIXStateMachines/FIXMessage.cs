﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXStateMachines
{
    public enum FIXMessageType
    {
        Logon_A,
        Logoff_5,

        MarketDataRequest_V,
        MarketDataRequest_V_UnSub,
        MarketDataSnapshotFullRefresh_W,
        MarketDataInrementalRefresh_X,
        MarketDataRequestReject_Y,

        QuoteRequest_R,
        QuoteRequest_R_UnSub,
        QuoteAck_b_Accepted,
        QuoteAck_b_Rejected,
        QuoteAck_b_Expired,
        Quote_S,
        QuoteCancel_Z,
        QuoteRequestReject_AG,
        QuoteResponse_AJ
    }

    public class FIXMessageArgs
    {
    }

    public class MarketDataRequestFIXMessageArgs : FIXMessageArgs
    {

        public string Ccy1 { get; private set; }
        public string Ccy2 { get; private set; }

        public MarketDataRequestFIXMessageArgs(string ccy1, string ccy2)
        {
            Ccy1 = ccy1;
            Ccy2 = ccy2;
        }

    }

    public class QuoteRequestFIXMessageArgs : FIXMessageArgs
    {
        public string Ccy1 { get; private set; }
        public string Ccy2 { get; private set; }

        public QuoteRequestFIXMessageArgs(string ccy1, string ccy2)
        {
            Ccy1 = ccy1;
            Ccy2 = ccy2;
        }
    }
}
