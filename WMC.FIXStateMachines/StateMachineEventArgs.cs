﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXStateMachines
{

    public class StateMachineActivatedEventArgs : EventArgs
    {
        public StateMachineActivatedEventArgs(string stateMachineRefId, StateMachine stateMachine, State startingState, DateTime utcTimestamp)
        {
            StateMachineRefId = stateMachineRefId;
            StateMachineActivated = stateMachine;
            StartingState = startingState;
            UTCTimestamp = utcTimestamp;
        }

        public string StateMachineRefId { get; private set; }
        public StateMachine StateMachineActivated { get; private set; }
        public State StartingState { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
    }

    public class StateMachineDeactivatedEventArgs : EventArgs
    {
        public StateMachineDeactivatedEventArgs(string stateMachineRefId, StateMachine stateMachine, State endingState, DateTime utcTimestamp)
        {
            StateMachineRefId = stateMachineRefId;
            StateMachineDeactivated = stateMachine;
            EndingState = endingState;
            UTCTimestamp = utcTimestamp;
        }

        public string StateMachineRefId { get; private set; }
        public StateMachine StateMachineDeactivated { get; private set; }
        public State EndingState { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
    }

    public class StateEnteredEventArgs : EventArgs
    {
        public StateEnteredEventArgs(State enteredState, DateTime utcTimestamp)
        {
            EnteredState = enteredState;
            UTCTimestamp = utcTimestamp;
        }

        public State EnteredState { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
    }

    public class StateExitedEventArgs : EventArgs
    {
        public StateExitedEventArgs(State exitedState, DateTime utcTimestamp)
        {
            ExitedState = exitedState;
            UTCTimestamp = utcTimestamp;
        }

        public State ExitedState { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
    }

    public class StateTransitionEventArgs : EventArgs
    {
        public StateTransitionEventArgs(string transitionName, State fromState, State toState, DateTime utcTimestamp)
        {
            TransitionName = transitionName;
            FromState = fromState;
            ToState = toState;
            UTCTimestamp = utcTimestamp;
        }

        public string TransitionName { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
        public State FromState { get; private set; }
        public State ToState { get; private set; }
    }

    public class FIXMessageDetailedEventArgs : EventArgs
    {
        public FIXMessageDetailedEventArgs(string refId, FIXMessageType messageType, FIXMessageArgs messageArgs)
        {
            RefId = refId;
            MessageType = messageType;
            MessageArgs = messageArgs;
            UTCTimestamp = DateTime.UtcNow;
        }

        public string RefId { get; private set; }
        public FIXMessageType MessageType { get; private set; }
        public FIXMessageArgs MessageArgs { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
    }

    public class FIXMessageEventArgs : EventArgs
    {
        public FIXMessageEventArgs(string stateMachineRefId, FIXMessageType messageType, DateTime utcTimestamp)
        {
            StateMachineRefId = stateMachineRefId;
            MessageType = messageType;
            UTCTimestamp = utcTimestamp;
        }

        public string StateMachineRefId { get; private set; }
        public FIXMessageType MessageType { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
    }

    public class MDStreamingIntervalElapsedEventArgs : EventArgs
    {
        public MDStreamingIntervalElapsedEventArgs(string stateMachineRefId, long intervalInMilliSeconds, DateTime utcTimestamp)
        {
            StateMachineRefId = stateMachineRefId;
            IntervalInMilliSeconds = intervalInMilliSeconds;
            UTCTimestamp = utcTimestamp;
        }

        public string StateMachineRefId { get; private set; }
        public long IntervalInMilliSeconds { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
    }

    public class ResponseWaitingIntervalElapsedEventArgs : EventArgs
    {
        public ResponseWaitingIntervalElapsedEventArgs(string stateMachineRefId, long intervalInMilliSeconds, DateTime utcTimestamp)
        {
            StateMachineRefId = stateMachineRefId;
            IntervalInMilliSeconds = intervalInMilliSeconds;
            UTCTimestamp = utcTimestamp;
        }

        public string StateMachineRefId { get; private set; }
        public long IntervalInMilliSeconds { get; private set; }
        public DateTime UTCTimestamp { get; private set; }
    }

}
