﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

using log4net;

namespace WMC.FIXStateMachines
{
    public class State
    {

        protected static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Name { get; private set; }
        public string StateMachineRefId { get; private set; }
        public List<Transition> Transitions { get; private set; }

        public event EventHandler<StateEnteredEventArgs> StateEntered;
        public event EventHandler<StateExitedEventArgs> StateExited;
        public event EventHandler<FIXMessageEventArgs> StateFIXMessageOut;

        public State(string name, string stateMachineRefId)
        {
            Name = name;
            StateMachineRefId = stateMachineRefId;
            Transitions = new List<Transition>();
        }

        public virtual void OnFIXMessageIn(object sender, FIXMessageEventArgs args)
        {
            _log.DebugFormat("State {0} of StateMachine RefId {1} FIX message type {2} IN.", Name, StateMachineRefId, args.MessageType.ToString());

            if (args.MessageType == FIXMessageType.Logoff_5)
                Transitions.Where(t => t is LogoffT).FirstOrDefault()?.OnTrigger();
        }

        public virtual void OnFIXMessageOut(object sender, FIXMessageEventArgs args)
        {
            _log.DebugFormat("State {0} of StateMachine RefId {1} FIX message type {2} OUT.", Name, StateMachineRefId, args.MessageType.ToString());

            StateFIXMessageOut?.Invoke(sender, args);
        }

        public virtual void OnEnter()
        {
            _log.DebugFormat("Entering State {0} of StateMachine {1} ...", Name, StateMachineRefId);

            var args = new StateEnteredEventArgs(this, DateTime.UtcNow);

            _log.DebugFormat("Raising State {0} of StateMachine {1} Entering Event.", Name, StateMachineRefId);

            StateEntered?.Invoke(this, args);
        }

        public virtual void OnExit()
        {
            _log.DebugFormat("Exiting State {0} of StateMachine {1} ...", Name, StateMachineRefId);

            var args = new StateExitedEventArgs(this, DateTime.UtcNow);

            _log.DebugFormat("Raising State {0} of StateMachine {1} Exiting Event.", Name, StateMachineRefId);

            StateExited?.Invoke(this, args);
        }

    }

    public class DoneS : State
    {
        private static readonly string _name = "Done";

        public DoneS(string stateMachineRefId) : base(_name, stateMachineRefId) { }

        public override void OnFIXMessageIn(object sender, FIXMessageEventArgs args)
        {
            base.OnFIXMessageIn(sender, args);
        }
    }
        
    public class WaitingForLogonS : State
    {
        public static readonly string _name = "WaitingForLogon";

        public WaitingForLogonS(string stateMachineRefId) : base(_name, stateMachineRefId)
        {

        }

        public override void OnFIXMessageIn(object sender, FIXMessageEventArgs args)
        {
            base.OnFIXMessageIn(sender, args);

            if (args.MessageType == FIXMessageType.Logon_A)
                Transitions.Where(t => t is LogonT).FirstOrDefault()?.OnTrigger();
        }
    }

    public class WaitingForResponseS : State
    {
        public static readonly string _name = "WaitingForResponse";

        private Timer _responseWaitingTimer;

        public WaitingForResponseS(string stateMachineRefId, long waitTimeoutInterval) : base(_name, stateMachineRefId)
        {
            _responseWaitingTimer = new Timer();
            _responseWaitingTimer.AutoReset = false;
            _responseWaitingTimer.Interval = waitTimeoutInterval;
            _responseWaitingTimer.Elapsed += OnResponseWaitingIntervalElapsed;
            _responseWaitingTimer.Enabled = false;
        }

        public override void OnFIXMessageIn(object sender, FIXMessageEventArgs args)
        {
            base.OnFIXMessageIn(sender, args);

            if (args.MessageType == FIXMessageType.MarketDataSnapshotFullRefresh_W)
                Transitions.Where(t => t is MDRefreshT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.MarketDataInrementalRefresh_X)
                Transitions.Where(t => t is MDRefreshT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.MarketDataRequestReject_Y)
                Transitions.Where(t => t is MDRequestRejectT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.QuoteAck_b_Accepted)
                Transitions.Where(t => t is QuoteRequestAcceptT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.QuoteAck_b_Rejected)
                Transitions.Where(t => t is QuoteRequestRejectT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.Quote_S)
                Transitions.Where(t => t is QuoteT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.QuoteRequestReject_AG)
                Transitions.Where(t => t is QuoteRequestRejectT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.QuoteCancel_Z)
                Transitions.Where(t => t is QuoteCancelT).FirstOrDefault()?.OnTrigger();
        }

        public override void OnEnter()
        {
            _responseWaitingTimer.Enabled = true;

            base.OnEnter();
        }

        public override void OnExit()
        {
            _responseWaitingTimer.Enabled = false;

            base.OnExit();
        }

        private void OnResponseWaitingIntervalElapsed(object sender, EventArgs args)
        {
            Transitions.Where(t => t is TimedOutT).FirstOrDefault()?.OnTrigger();
        }

    }

    public class SendingMarketDataRequestS : State
    {
        public static readonly string _name = "SendingMarketDataRequest";

        private Timer _countdownToFireTimer;

        public SendingMarketDataRequestS(string stateMachineRefId, long countdownToFireInterval) : base(_name, stateMachineRefId)
        {
            _countdownToFireTimer = new Timer();
            _countdownToFireTimer.AutoReset = false;
            _countdownToFireTimer.Interval = countdownToFireInterval;
            _countdownToFireTimer.Elapsed += OnCountdownToFireIntervalElapsed;
            _countdownToFireTimer.Enabled = false;
        }

        public override void OnEnter()
        {
            _log.DebugFormat("Enabling Countdown to Send Market Data Request Timer with Interval {0} secs", _countdownToFireTimer.Interval / 1_000);

            _countdownToFireTimer.Enabled = true;

            base.OnEnter();
        }

        public override void OnExit()
        {
            _log.DebugFormat("Disabling Countdown to Send Market Data Request Timer.");

            _countdownToFireTimer.Enabled = false;

            base.OnExit();
        }

        private void OnCountdownToFireIntervalElapsed(object sender, EventArgs args)
        {
            var FIXMsgEvtArgs = new FIXMessageEventArgs(StateMachineRefId, FIXMessageType.MarketDataRequest_V, DateTime.UtcNow);
            OnFIXMessageOut(this, FIXMsgEvtArgs);

            Transitions.Where(t => t is MDRequestT).FirstOrDefault()?.OnTrigger();
        }
    }

    public class SendingQuoteRequestS : State
    {
        public static readonly string _name = "SendingQuoteRequest";

        private Timer _countdownToFireTimer;

        public SendingQuoteRequestS(string stateMachineRefId, long countdownToFireInterval) : base(_name, stateMachineRefId)
        {
            _countdownToFireTimer = new Timer();
            _countdownToFireTimer.AutoReset = false;
            _countdownToFireTimer.Interval = countdownToFireInterval;
            _countdownToFireTimer.Elapsed += OnCountdownToFireIntervalElapsed;
            _countdownToFireTimer.Enabled = false;
        }

        public override void OnEnter()
        {
            _log.DebugFormat("Enabling Countdown to Fire Quote Request Timer with Interval {0} secs", _countdownToFireTimer.Interval / 1_000);

            _countdownToFireTimer.Enabled = true;

            base.OnEnter();
        }

        public override void OnExit()
        {
            _log.DebugFormat("Disabling Countdown to Fire Quote Request Timer.");

            _countdownToFireTimer.Enabled = false;

            base.OnExit();
        }

        private void OnCountdownToFireIntervalElapsed(object sender, EventArgs args)
        {
            var FIXMsgEvtArgs = new FIXMessageEventArgs(StateMachineRefId, FIXMessageType.QuoteRequest_R, DateTime.UtcNow);
            OnFIXMessageOut(this, FIXMsgEvtArgs);

            Transitions.Where(t => t is QuoteRequestT).FirstOrDefault()?.OnTrigger();
        }
    }

    public class StreamingMarketDataS : State
    {
        public static readonly string _name = "StreamingMarketData";

        private Timer _mdStreamingIntervalTimer;

        public StreamingMarketDataS(string stateMachineRefId, long mdStreamingInterval) : base(_name, stateMachineRefId)
        {
            _mdStreamingIntervalTimer = new Timer();
            _mdStreamingIntervalTimer.AutoReset = false;
            _mdStreamingIntervalTimer.Interval = mdStreamingInterval;
            _mdStreamingIntervalTimer.Elapsed += OnMDStreamingIntervalTimerElapsed;
            _mdStreamingIntervalTimer.Enabled = false;
        }

        public override void OnEnter()
        {
            _log.DebugFormat("Enabling Market Data Streaming Interval Timer with Interval {0} secs", _mdStreamingIntervalTimer.Interval / 1_000);

            _mdStreamingIntervalTimer.Enabled = true;

            base.OnEnter();
        }

        public override void OnExit()
        {
            _log.DebugFormat("Disabling Countdown to Send Quote Request Timer.");

            _mdStreamingIntervalTimer.Enabled = false;

            base.OnExit();
        }

        private void OnMDStreamingIntervalTimerElapsed(object sender, EventArgs args)
        {
            _log.WarnFormat("Market Data Streaming Interval Timer has elapsed.");
        }

        public override void OnFIXMessageIn(object sender, FIXMessageEventArgs args)
        {
            base.OnFIXMessageIn(sender, args);

            if (args.MessageType == FIXMessageType.MarketDataSnapshotFullRefresh_W)
                Transitions.Where(t => t is MDRefreshT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.MarketDataInrementalRefresh_X)
                Transitions.Where(t => t is MDRefreshT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.MarketDataRequestReject_Y)
                Transitions.Where(t => t is MDRequestRejectT).FirstOrDefault()?.OnTrigger();
        }
    }

    public class QuoteReceivedS : State
    {
        private static readonly string _name = "QuoteReceived";

        public QuoteReceivedS(string stateMachineRefId) : base(_name, stateMachineRefId)
        {

        }

        public override void OnFIXMessageIn(object sender, FIXMessageEventArgs args)
        {
            base.OnFIXMessageIn(sender, args);

            if (args.MessageType == FIXMessageType.Quote_S)
                Transitions.Where(t => t is QuoteT).FirstOrDefault()?.OnTrigger();
            else if (args.MessageType == FIXMessageType.QuoteCancel_Z)
                Transitions.Where(t => t is QuoteCancelT).FirstOrDefault()?.OnTrigger();
        }
    }
    
}
