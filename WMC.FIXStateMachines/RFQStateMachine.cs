﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMC.FIXStateMachines
{
    public class RFQStateMachine : StateMachine
    {

        public string SecuritySymbol { get; private set; }

        public long TimeToSendQuoteRequest { get; private set; }
        public long TimeToWaitForQuote { get; private set; }

        protected static readonly long _defaultTimeToSendQuoteRequest = 30_000;
        protected static readonly long _defaultTimeToWaitForQuote = 10 * 60_000;

        protected RFQStateMachine(string stateMachineRefId, string securitySymbol, bool loggedOn)
            : this(stateMachineRefId, securitySymbol, loggedOn,
                  _defaultTimeToSendQuoteRequest, _defaultTimeToWaitForQuote)
        {
        }

        protected RFQStateMachine(string stateMachineRefId, string securitySymbol, bool loggedOn,
            long? timeToSendQuoteRequestArg, long? timeToWaitForQuoteArg)
            : base(stateMachineRefId)
        {

            TimeToSendQuoteRequest = timeToSendQuoteRequestArg.HasValue ? timeToSendQuoteRequestArg.Value : _defaultTimeToSendQuoteRequest;
            TimeToWaitForQuote = timeToWaitForQuoteArg.HasValue ? timeToWaitForQuoteArg.Value : _defaultTimeToWaitForQuote;

            SecuritySymbol = securitySymbol;

        }

    }
}
