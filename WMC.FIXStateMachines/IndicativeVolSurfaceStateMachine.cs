﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace WMC.FIXStateMachines
{

    public class IndicativeVolSurfaceStateMachine : StateMachine
    {

        public string SecuritySymbol { private set; get; }

        public long TimeToSendVolSurfaceRequest { private set; get; }
        public long TimeToWaitForSurfaceRefresh { private set; get; }
        public long TimeInBetweenVolSurfaceRefreshes { private set; get; }

        protected static readonly long _defaultTimeToSendVolSurfaceRequest = 30_000;
        protected static readonly long _defaultTimeToWaitForVolSurfaceRefresh = 15 * 60_000;
        protected static readonly long _defaultTimeInBetweenVolSurfaceRefreshes = 30 * 60_000;

        public IndicativeVolSurfaceStateMachine(string stateMachineRefId, string securitySymbol, bool loggedOn)
            : this(stateMachineRefId, securitySymbol, loggedOn,
                  _defaultTimeToSendVolSurfaceRequest, _defaultTimeToWaitForVolSurfaceRefresh, _defaultTimeInBetweenVolSurfaceRefreshes)
        {
        }

        public IndicativeVolSurfaceStateMachine(string stateMachineRefId, string securitySymbol, bool loggedOn,
            long? timeToSendVolSurfaceRequestArg, long? timeToWaitForVolSurfaceRefreshArg, long? timeInBetweenVolSurfaceRefreshesArg)
            : base(stateMachineRefId)
        {

            SecuritySymbol = securitySymbol;

            TimeToSendVolSurfaceRequest = timeToSendVolSurfaceRequestArg.HasValue ? timeToSendVolSurfaceRequestArg.Value : _defaultTimeToSendVolSurfaceRequest;
            TimeToWaitForSurfaceRefresh = timeToWaitForVolSurfaceRefreshArg.HasValue ? timeToWaitForVolSurfaceRefreshArg.Value : _defaultTimeToWaitForVolSurfaceRefresh;
            TimeInBetweenVolSurfaceRefreshes = timeInBetweenVolSurfaceRefreshesArg.HasValue ? timeInBetweenVolSurfaceRefreshesArg.Value : _defaultTimeInBetweenVolSurfaceRefreshes;

        }

        public override void OnStateTransition(object sender, StateTransitionEventArgs args)
        {

            _log.DebugFormat("IndicativeMktDataStateMachine {0} state transition event handler invoke START.", StateMachineRefId);

            if (CurrentState != args.ToState)
            {
                _log.DebugFormat("State Transition occurred: Current State {0}; To State {1}", CurrentState.Name, args.ToState.Name);

                base.OnStateTransition(sender, args);

                _log.DebugFormat("IndicativeMktDataStateMachine {0} state transition event handler invoke END.", StateMachineRefId);
            }

        }

    }

}
