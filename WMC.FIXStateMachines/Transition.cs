﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using log4net;

namespace WMC.FIXStateMachines
{

    public class Transition
    {
        private static readonly log4net.ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Name { get; protected set; }
        public State FromState { get; private set; }
        public State ToState { get; private set; }

        public event EventHandler<StateTransitionEventArgs> Triggered;

        protected Transition(string name, State fromState, State toState)
        {
            Name = name;
            FromState = fromState;
            ToState = toState;

            FromState.Transitions.Add(this);
        }

        public virtual void OnTrigger()
        {

            _log.DebugFormat("Transition {0} triggered from State {1} to State {2}.", Name, FromState.Name, ToState.Name);

            _log.DebugFormat("Exiting the FromState {0} of Transition {1} ...", FromState.Name, Name);
            FromState.OnExit();

            _log.DebugFormat("Raising transition triggered event {0} from {1} to {2} ...", Name, FromState.Name, ToState.Name);
            var args = new StateTransitionEventArgs(Name, FromState, ToState, DateTime.UtcNow);
            Triggered?.Invoke(this, args);

            _log.DebugFormat("Entering the ToState {0} of Transition {1} ...", ToState.Name, Name);
            ToState.OnEnter();

        }
    }

    public class LogonT : Transition
    {
        private static readonly string _name = "Logon";

        public LogonT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class LogoffT : Transition
    {
        private static readonly string _name = "Logoff";

        public LogoffT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class MDRequestT : Transition
    {
        private static readonly string _name = "MarketDataRequest";

        public MDRequestT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class MDRefreshT : Transition
    {
        private static readonly string _name = "MarketDataRefresh";

        public MDRefreshT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class MDRequestRejectT : Transition
    {
        private static readonly string _name = "MarketDataRequestReject";

        public MDRequestRejectT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class QuoteRequestT :  Transition
    {
        private static readonly string _name = "QuoteRequest";

        public QuoteRequestT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class QuoteT : Transition
    {
        private static readonly string _name = "Quote";

        public QuoteT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class QuoteCancelT : Transition
    {
        private static readonly string _name = "QuoteCancel";

        public QuoteCancelT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class QuoteRequestAcceptT : Transition
    {
        private static readonly string _name = "QuoteRequestAccept";

        public QuoteRequestAcceptT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class QuoteRequestRejectT : Transition
    {
        private static readonly string _name = "QuoteRequestReject";

        public QuoteRequestRejectT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class QuoteRequestExpiredT : Transition
    {
        private static readonly string _name = "QuoteRequestExpired";

        public QuoteRequestExpiredT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }

    public class TimedOutT : Transition
    {
        private static readonly string _name = "TimeOut";

        public TimedOutT(State fromState, State toState) : base(_name, fromState, toState)
        {

        }
    }
}
