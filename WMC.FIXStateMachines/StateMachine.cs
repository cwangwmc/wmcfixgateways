﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using log4net;

namespace WMC.FIXStateMachines
{
    public abstract class StateMachine
    {
        protected static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool IsActive { get; protected set; }

        public string StateMachineRefId { get; protected set; }

        public State OriginState { get; protected set; }
        public State CurrentState { get; protected set; }
        public State TerminalState { get; protected set; }

        public event EventHandler<FIXMessageDetailedEventArgs> StateMachineFIXMessageOut;
        public event EventHandler<StateMachineActivatedEventArgs> Activated;
        public event EventHandler<StateMachineDeactivatedEventArgs> Deactivated;

        protected StateMachine(string stateMachineRefId)
        {
            StateMachineRefId = stateMachineRefId;
            Activated += OnActivated;
            Deactivated += OnDeactivated;
        }

        protected virtual FIXMessageDetailedEventArgs MakeFIXMessageDetailedEventArgs(FIXMessageEventArgs args)
        {
            throw new NotImplementedException();
        }

        public virtual void OnStateTransition(object sender, StateTransitionEventArgs args)
        {
            _log.DebugFormat("Deregistering current state's transitions event handlers ...");
            // De-register current state's transition event handlers
            foreach (var t in CurrentState.Transitions)
                t.Triggered -= OnStateTransition;

            _log.DebugFormat("Deregistering current state's outbound FIX message event handler ...");
            // De-register current state's Outbound FIX event handler
            CurrentState.StateFIXMessageOut -= OnOutboundFIXMessage;

            _log.DebugFormat("Deregistering current state's StateEntered event handler ...");
            CurrentState.StateEntered -= OnStateEntered;

            _log.DebugFormat("Deregistering current state's StateExited event handler ...");
            CurrentState.StateExited -= OnStateExited;

            // Switch the ToState to become the current state
            CurrentState = args.ToState;

            _log.DebugFormat("Registering the new state's transitions event handlers ...");
            // Register the new current state's transition event handlers
            foreach (var t in CurrentState.Transitions)
                t.Triggered += OnStateTransition;

            _log.DebugFormat("Registering the new state's outbound FIX message event handler ...");
            // Regsiter the new current state's Outbound FIX event handler
            CurrentState.StateFIXMessageOut += OnOutboundFIXMessage;

            _log.DebugFormat("Registering the new state's StateEntered event handler ...");
            CurrentState.StateEntered += OnStateEntered;

            _log.DebugFormat("Registering the new state's StateExited event handler ...");
            CurrentState.StateExited += OnStateExited;
        }

        public virtual void OnInboundFIXMessage(object sender, FIXMessageEventArgs args)
        {
            _log.DebugFormat("State Machine {0}: Received Inbound FIX Message type {1} for State Machine {2} ...", StateMachineRefId, args.MessageType, args.StateMachineRefId);

            if (args.StateMachineRefId == StateMachineRefId)
            {
                _log.DebugFormat("StateMachineRefId {0} matched, relaying the FIX Message type to current state {1} ...", StateMachineRefId, CurrentState.Name);
                CurrentState.OnFIXMessageIn(sender, args);
            }
        }

        public virtual void OnOutboundFIXMessage(object sender, FIXMessageEventArgs args)
        {
            _log.DebugFormat("State Machine {0}: Outbound FIX Message {1} needs to be sent ...", StateMachineRefId, args.MessageType);

            try
            {
                var detailedArgs = MakeFIXMessageDetailedEventArgs(args);
                if (detailedArgs != null)
                    StateMachineFIXMessageOut?.Invoke(sender, detailedArgs);
                else
                    _log.ErrorFormat("State Machine {0}: Outbound FIX Message {1} error - FIXMessageArgsDetailedEventArgs is null.", StateMachineRefId, args.MessageType);
            }
            catch (NotImplementedException ex)
            {
                _log.ErrorFormat("State Machine {0}: Outbound FIX Message {1} error - {2}.", StateMachineRefId, args.MessageType, ex.Message);
            }   
        }

        public virtual void OnStateEntered(object sender, StateEnteredEventArgs args)
        {
            _log.DebugFormat("Entered State {0}.", args.EnteredState.Name);

            var enteredState = args.EnteredState;

            if (enteredState == TerminalState)
                Deactivated?.Invoke(this, new StateMachineDeactivatedEventArgs(StateMachineRefId, this, enteredState, DateTime.UtcNow));
        }

        public virtual void OnStateExited(object sender, StateExitedEventArgs args)
        {
            _log.DebugFormat("Exited State {0}.", args.ExitedState.Name);
        }

        public virtual void Activate()
        {

            if (!IsActive)
            {
                _log.InfoFormat("Activating State Machine {0} ...", StateMachineRefId);

                Activated?.Invoke(this, new StateMachineActivatedEventArgs(StateMachineRefId, this, CurrentState, DateTime.UtcNow));

                _log.InfoFormat("State Machine {0} activated.", StateMachineRefId);
            }
            else
                _log.WarnFormat("State Mahcine {0} is already activated", StateMachineRefId);
            
        }

        public virtual void OnActivated(object sender, EventArgs evtArgs)
        {

            _log.DebugFormat("Entering State Machine {0} OnActivated event handler ...", StateMachineRefId);

            _log.DebugFormat("Invoking OnEnter on Origin State {0} in State Machine {1} ...", OriginState.Name, StateMachineRefId);

            OriginState.OnEnter();
            
            _log.DebugFormat("OnEnter on Origin State {0} in State Machine {1} invoked.", CurrentState.Name, StateMachineRefId);

            IsActive = true;

            _log.DebugFormat("Exiting State Machine {0} OnActivated event handler ...", StateMachineRefId);

        }

        public virtual void Deactivate()
        {

            if (IsActive)
            {
                _log.InfoFormat("Deactivating State Machine {0} ...", StateMachineRefId);

                Deactivated?.Invoke(this, new StateMachineDeactivatedEventArgs(StateMachineRefId, this, CurrentState, DateTime.UtcNow));

                _log.InfoFormat("State Machine {0} deactivated.", StateMachineRefId);
            }
            else
                _log.WarnFormat("State Machine {0} is already deactived.", StateMachineRefId);

        }

        public virtual void OnDeactivated(object sender, EventArgs evtArgs)
        {

            _log.DebugFormat("Entering State Machine {0} OnDeactivated event handler ...", StateMachineRefId);

            IsActive = false;

            _log.DebugFormat("Exiting State Machine {0} OnDeactivated event handler ...", StateMachineRefId);

        }
    }
}
